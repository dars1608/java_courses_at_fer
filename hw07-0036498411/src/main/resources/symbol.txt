 * This command changes or obtains following shell symbols:
 * 
 * 	- prompt symbol - printed before user input
 * 	- multiline symbol - printed whenever user wants to write multiple lines
 * 	- more lines symbol - user must enter this input whenever he/she wants to enter the input in the next line
 *
 * Replacement symbol must be one character.