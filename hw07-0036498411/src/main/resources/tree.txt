 * This command writes directory tree recursively on the  shell. Every level
 * of tree is indented by level * 2 spaces. "." symbol represents root
 * directory.