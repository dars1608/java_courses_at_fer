 * This command takes one argument; path of some directory. Command writes file directory
 * listing of provided directory on  the shell.
 * Output format of this command is:
 * 	- First column indicates if current object is directory (d), readable (r),
 * 	writable (w) and executable (x).
 * 	- Second column contains object size in bytes that is right aligned and
 * 	occupies 10 characters.
 * 	- Third column contains file creation date/time
 * 	- Forth column contains file name.