 * This command produces hex-output of some file and prints it on the shell.
 * Output format of this method is:
 * 
 *  - first column contains the index of first byte to be printed
 *  - table of bytes (16 columns, every column has 2 bytes)
 *  - string representation of bytes (unprintable characters are described with ".")
