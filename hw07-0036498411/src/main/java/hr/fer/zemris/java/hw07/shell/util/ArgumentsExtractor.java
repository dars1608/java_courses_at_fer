package hr.fer.zemris.java.hw07.shell.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw07.shell.MyShell;
import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;

/**
 * This is helper class used for extracting arguments for {@link ShellCommand}
 * obtained from {@link MyShell}. It calls {@link ArgumentsParser} to parse
 * argument line into simple string arguments.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class ArgumentsExtractor {

	/**
	 * This method is used for extracting arguments from line of arguments.
	 * 
	 * @param line
	 *            line of arguments
	 * @return string array of arguments
	 * @throws ParsingException
	 *             if parsing of the arguments fails
	 */
	public static List<String> extract(String line) {
		Objects.requireNonNull(line, "Provided line argument can't be null.");
		ArgumentsParser parser = new ArgumentsParser(line.trim());
		List<String> arguments = new ArrayList<>();

		while (true) {
			String nextArgument = parser.nextArgument();
			if (nextArgument == null) {
				break;
			}

			arguments.add(nextArgument);
		}

		return arguments;
	}
}
