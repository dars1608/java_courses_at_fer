/**
 * This package contains classes that will allow the user to
 * encrypt/decrypt given file using the AES crypto-algorithm and the 128-bit
 * encryption key or calculate and check the SHA-256 file digest.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.crypto;