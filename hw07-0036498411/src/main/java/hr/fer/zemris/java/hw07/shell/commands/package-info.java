/**
 * This package contains {@link hr.fer.zemris.java.hw07.shell.ShellCommand}
 * objects which describe commands of
 * {@link hr.fer.zemris.java.hw07.shell.MyShell} program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.shell.commands;