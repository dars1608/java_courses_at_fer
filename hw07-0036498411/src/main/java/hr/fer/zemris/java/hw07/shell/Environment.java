package hr.fer.zemris.java.hw07.shell;

import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;

/**
 * This interface defines an environment of shell object. It predicts that class
 * which implements this interface should have a way of communicating with user
 * through standard input/output. It should also have a map of some sort of
 * command objects mapped by name, which will represent commands defined in the
 * shell. Finally, it should store characters representing prompt, multiline and
 * more lines symbols, which will be written on console when needed.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface Environment {

	/**
	 * This method is used for reading from console (<code>System.in</code>).
	 * 
	 * @return line which is obtained from the console
	 * @throws ShellIOException
	 *             if reading fails
	 */
	String readLine() throws ShellIOException;

	/**
	 * This method is used for writing some text on the console
	 * (<code>System.out</code>)
	 * 
	 * @param text
	 *            text to be written
	 * @throws ShellIOException
	 *             if writing fails
	 */
	void write(String text) throws ShellIOException;

	/**
	 * This method writes one line on the console (<code>System.out</code>)
	 * 
	 * @param text
	 *            line to be written
	 * @throws ShellIOException
	 *             if writing fails
	 */
	void writeln(String text) throws ShellIOException;

	/**
	 * This method is used for obtaining commands defined in the shell environment.
	 * 
	 * @return unmodifiable {@link SortedMap} of pairs: ({@link String} key,
	 *         {@link ShellCommand} value)
	 */
	SortedMap<String, ShellCommand> commands();

	/**
	 * Getter method for the multiline symbol.
	 * 
	 * @return current multiline symbol
	 */
	Character getMultilineSymbol();

	/**
	 * Setter method for the multiline symbol.
	 * 
	 * @param symbol
	 *            the multiline symbol to be set
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	void setMultilineSymbol(Character symbol);

	/**
	 * Getter method for the prompt symbol.
	 * 
	 * @return current prompt symbol
	 */
	Character getPromptSymbol();

	/**
	 * Setter method for the prompt symbol.
	 * 
	 * @param symbol
	 *            the prompt symbol to be set
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	void setPromptSymbol(Character symbol);

	/**
	 * Getter method for the more lines symbol.
	 * 
	 * @return current more lines symbol
	 */
	Character getMorelinesSymbol();

	/**
	 * Setter method for the more lines symbol.
	 * 
	 * @param symbol
	 *            the more lines symbol to be set
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	void setMorelinesSymbol(Character symbol);
}
