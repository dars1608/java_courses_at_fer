package hr.fer.zemris.java.hw07.shell.util;

import java.util.Objects;

import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;

/**
 * This class represents parser for parsing arguments for {@link ShellCommand}
 * objects. It is used in {@link ArgumentsExtractor#extract(String)} method.
 * <p>
 * In command where a file-path is expected, parser supports quotes to allow
 * paths with spaces (such as "Documents and Settings"). In order to do so, if
 * argument starts with quotation, parser supports during parsing \" as escape
 * sequence representing " as regular character (and not string end).
 * Additionally, a sequence \\ is treated as single \. Every other situation in
 * which after \ follows anything but " and \ is literally copied as two
 * characters.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ArgumentsParser {
	
	/** Lexer object*/
	private ArgumentsLexer lexer;

	/**
	 * Constructor method for class {@link ArgumentsParser}.
	 * 
	 * @param line
	 */
	public ArgumentsParser(String line) {
		Objects.requireNonNull(line, "Line argument can't be null.");
		lexer = new ArgumentsLexer(line);

	}

	/**
	 * This method is used for generating next argument from line obtained through
	 * constructor.
	 * 
	 * @return next argument, <code>null</code> if there is no other arguments to
	 *         parse
	 */
	public String nextArgument() {
		return lexer.next();
	}

	/**
	 * This class represents simple lexer for {@link ArgumentsParser}.
	 * <p>
	 * It differs non-string from string arguments. String arguments are noted with
	 * quotation marks and can contain one or many spaces while non-string arguments
	 * can't. Simple escaping is allowed:
	 * <ul>
	 * <li>\\ for \ symbol
	 * <li>\" for " symbol </u> Every other Every other situation in which after \
	 * follows anything but " and \ should be literally copied as two characters.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private static class ArgumentsLexer {
		/** Array of input characters */
		private char[] data;
		/** Index of currently processed character */
		private int currentIndex;
		/** {@link StringBuilder} used for generating arguments */
		private StringBuilder sb;

		/**
		 * Constructor method for class {@link ArgumentsLexer}.
		 * 
		 * @param line
		 *            line to be parsed
		 */
		public ArgumentsLexer(String line) {
			data = line.toCharArray();
			sb = new StringBuilder();
		}

		/**
		 * This method generates next argument from input provided in constructor.
		 * 
		 * @return next argument, <code>null</code> if lexer came to the end of line
		 * @throws ParserException
		 *             if error while parsing occurs
		 */
		public String next() {
			if (currentIndex >= data.length) {
				return null;
			}

			skipBlankSpaces();
			sb.delete(0, sb.length());

			if (currentIndex >= data.length) {
				return null;
			} else if (data[currentIndex] == '"') {
				currentIndex++;
				if (currentIndex >= data.length) {
					return null;
				}
				extractString();
			} else {
				extractNonString();
			}

			return sb.toString();
		}

		/**
		 * This method is used internally for parsing non string arguments (not in
		 * quotes).
		 */
		private void extractNonString() {
			char c = data[currentIndex];

			while (!isBlank(c) && c != '"') {
				sb.append(c);
				currentIndex++;
				if (currentIndex >= data.length) {
					break;
				}
				c = data[currentIndex];
			}
		}

		/**
		 * This method is used internally for parsing string. It also checks escaping
		 * sequences like \\ or \".
		 */
		private void extractString() {
			char c = data[currentIndex];

			while (c != '"') {
				if (c == '\\' && currentIndex + 1 < data.length) {
					if (data[currentIndex + 1] == '"') {
						currentIndex++;
						sb.append('"');
						continue;
					} else if (data[currentIndex + 1] == '\\') {
						currentIndex++;
					}
				}
				sb.append(c);
				currentIndex++;
				if (currentIndex >= data.length) {
					throw new ParserException("Invalid string format.");
				}
				c = data[currentIndex];
			}
			
			currentIndex++;
			if(currentIndex<data.length) {
				c = data[currentIndex];
				if(!isBlank(c)) {
					throw new ParserException("After the ending double-quote, either no "
							+ "more characters must be present or at least one space"
							+ " character must be present");
				}
			}

		}

		/**
		 * This method is used internally for skipping blank spaces stored in
		 * <code>data</code> field.
		 */
		private void skipBlankSpaces() {
			while (currentIndex < data.length) {
				char c = data[currentIndex];
				if (!isBlank(c)) {
					break;
				}
				currentIndex++;
			}
		}

		/**
		 * This method checks if given character is blank character.
		 * 
		 * @param c
		 *            checked character
		 * @return <code>true</code> if character is blank, <code>false</code> otherwise
		 */
		private static boolean isBlank(char c) {
			return "\n\r\t ".contains(Character.toString(c));
		}
	}
}
