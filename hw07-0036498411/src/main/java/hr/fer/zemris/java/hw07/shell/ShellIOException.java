package hr.fer.zemris.java.hw07.shell;

/**
 * This class describes an illegal state of {@link MyShell} program. It is
 * subclass of {@link RuntimeException}. It is thrown when reading or writing
 * from console/file fails.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ShellIOException extends RuntimeException {

	/** Auto-generated serial version unique ID */
	private static final long serialVersionUID = 1765778797326418377L;

	/**
	 * Constructor method for class {@link ShellIOException}.
	 */
	public ShellIOException() {
	}

	/**
	 * Constructor method for class {@link ShellIOException}.
	 * 
	 * @param message
	 *            detail message
	 */
	public ShellIOException(String message) {
		super(message);
	}

	/**
	 * Constructor method for class {@link ShellIOException}.
	 * 
	 * @param cause
	 *            the cause of exception
	 */
	public ShellIOException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor method for class {@link ShellIOException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            the cause of exception
	 */
	public ShellIOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor method for class {@link ShellIOException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            the cause of exception
	 * @param enableSuppression
	 *            whether or not suppression is enabled
	 * @param writableStackTrace
	 *            whether or not the stack trace should be writable
	 */
	public ShellIOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
