package hr.fer.zemris.java.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import hr.fer.zemris.java.hw07.crypto.Util;
import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * This class represents {@link ShellCommand} produces hex-output of some file.
 * Output format of this method is:
 * <ul>
 * <li>first column contains the index of first byte to be printed
 * <li>table of bytes (16 columns, every column has 2 bytes)
 * <li>string representation of bytes (unprintable characters are described with ".")
 * </ul>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class HexDumpShellCommand extends AbstractShellCommand implements ShellCommand {
	
	/** Position of "|" symbol in output*/
	private static final int SEPARATOR_POSITION = 15;
	/** Number of bytes written per row in output*/
	private static final int BYTES_PER_ROW = 32;

	/**
	 * Constructor method for class {@link HexDumpShellCommand}.
	 */
	public HexDumpShellCommand() {
		super("src/main/resources/hexdump.txt", "hexdump");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);

		if (argumentsList != null) {
			writeHexdumpOnShell(argumentsList.get(0), env);
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used for printing hex-dump of given file on the shell.
	 * 
	 * @param fileName file of which hex-dump is printed
	 * @param env {@link Environment} of shell
	 */
	private static void writeHexdumpOnShell(String fileName, Environment env) {
		Path p = Paths.get(fileName);
		File f = p.toFile();

		if (f.isDirectory()) {
			env.writeln("Cannot provide hexdump of a directory.");
			return;
		} else if(!f.exists()) {
			env.writeln("File doesn't exist.");
			return;
		}else if (!f.canRead()) {
			env.writeln("File isn't readable.");
			return;
		}

		try (BufferedInputStream bsIn = new BufferedInputStream(
				Files.newInputStream(p, StandardOpenOption.READ))) {
			
			for(int row = 0; true; row+=10) {
				byte[] buff = new byte[BYTES_PER_ROW];
				int r = bsIn.read(buff);
				if (r < 1) {
					break;
				}

				env.write(String.format("%08d: ", row));

				char[] hexEncoded = Util.bytetohex(buff).toCharArray();
				for (int i = 0; i < BYTES_PER_ROW; i++) {
					if (i < r) {
						env.write(Character.toString(hexEncoded[i]));
					} else {
						env.write(" ");
					}

					if (i == SEPARATOR_POSITION) {
						env.write("|");
					} else if (i % 2 == 1) {
						env.write(" ");
					}
				}

				env.write("| ");

				for (int i = 0; i < r; i++) {
					byte b = buff[i];

					if (isPrintable(b)) {
						env.write(String.format("%c", (char) b));
					} else {
						env.write(".");
					}
				}

				env.writeln("");

			}
		} catch (IOException ex) {
			env.writeln(String.format("File %s couldn't be opened.", p.toAbsolutePath()));
		}

	}

	/**
	 * This method is used internally to check if given byte can represent printable
	 * ASCII char (its value is in range [32,127]).
	 * 
	 * @param b
	 *            byte to be checked
	 * @return <code>true</code> if is printable, <code>false</code> otherwise
	 */
	private static boolean isPrintable(byte b) {
		return b >= 32 && b < 127;
	}

}
