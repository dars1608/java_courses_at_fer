package hr.fer.zemris.java.hw07.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static hr.fer.zemris.java.hw07.crypto.Util.hextobyte;
import static hr.fer.zemris.java.hw07.crypto.Util.bytetohex;

import static java.security.MessageDigest.getInstance;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * This program has 3 features:
 * <li>performing digest calculation (sha-256 algorithm)
 * <li>encrypting file (AES standard)
 * <li>decrypting file (AES standard)
 *
 * <h1>Digest calculation</h1>
 * <p>
 * This program is able to check if given message digest is valid (if it is same
 * as the one digested from given file). Therefore you can compare the
 * calculated digest with the digest which is published on the web site from
 * which you have started the download. If something has changed during the
 * download, there is extremely high probability that the calculated digest will
 * be different from the one published on the web site.
 * 
 * To perform digesting, provided command line arguments must be of format:
 * <ul>
 * <i>checksha fileName </i>
 * <p>
 * </ul>
 * 
 * <h1>Encryption/Decryption</h1>
 * <p>
 * This program can also encrypt/decrypt files using symmetric crypto-algorithm
 * AES which can work with three different key sizes: 128 bit, 192 bit and 256
 * bit. Since AES is block cipher, it always consumes 128 bit of data at a time
 * (or adds padding if no more data is available) and produces 128 bits of
 * encrypted text. Therefore, the length (in bytes) of encrypted file file will
 * always be divisible by 16.
 * 
 * </p>
 * To perform encryption, provided command line arguments must be of format:
 * <ul>
 * <i>encrypt originalFileName encryptedFileName </i>
 * <p>
 * </ul>
 * </p>
 * To perform dercryption, provided command line arguments must be of format:
 * <ul>
 * <i>decrypt encryptedFileName originalFileName</i>
 * <p>
 * </ul>
 * 
 * 
 * @author Darko Britvec
 * @version 1.0        
 */
public class Crypto { // provided test files are in folder src/test/resources

	/**
	 * This method is called when program starts. Method expects 2 or 3 command line
	 * arguments.
	 * 
	 * @see Crypto
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {

			if (args.length == 2) {
				String command = args[0];
				String fileName = args[1];

				if (!command.equals("checksha")) {
					System.err.println("Invalid command: " + command);
					return;
				}

				digestingMode(fileName, sc);

			} else if (args.length == 3) {
				String command = args[0];
				String fileNameIn = args[1];
				String fileNameOut = args[2];

				if (command.equals("encrypt")) {
					cryptoMode(fileNameIn, fileNameOut, sc, true);
				} else if (command.equals("decrypt")) {
					cryptoMode(fileNameIn, fileNameOut, sc, false);
				} else {
					System.err.println("Invalid command: " + command);
					return;
				}

			} else {
				System.err.println("Invalid number of arguments. Must be 3");
			}
		}

	}

	/**
	 * This method is called when user wants to use encryption/decription of some
	 * file. Method communicates with user via standard input/output to obtain
	 * password and initialization vector for initializing {@link Cipher} object.
	 * Method performs encryption/decryption based on AES encryption standard.
	 * 
	 * @param fileNameIn
	 *            name of file to be encrypted/decrypted
	 * @param fileNameOut
	 *            output file name
	 * @param sc
	 *            {@link Scanner} used for communication with user
	 * @param encrypt
	 *            flag defining if encryption mode is active or not
	 */
	private static void cryptoMode(String fileNameIn, String fileNameOut, Scanner sc, boolean encrypt) {
		byte[] password = null;
		byte[] initVector = null;

		while (true) {
			System.out.printf("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):\n> ");
			String passwordString = sc.nextLine();

			password = inputToByteArray(passwordString, "Invalid password. Provided hex-encoded string is not valid.");

			if (password == null)
				continue;

			if (checkByteArrayLength(password, 16, "Provided password has invalid length. Is %d bits long.\n")) {
				break;
			}
		}

		while (true) {
			System.out.printf(
					"Please provide initializatoin vector as hex-encoded text (16 bytes, i.e. 32 hex-digits):\n> ");
			String initVectorString = sc.nextLine();

			initVector = inputToByteArray(initVectorString,
					"Invalid initializatoin vector. Provided hex-encoded string is not valid.");

			if (initVector == null)
				continue;

			if (checkByteArrayLength(initVector, 16,
					"Provided initializatoin vector has invalid length. Is %d bits long.\n")) {
				break;
			}
		}

		if (runCrypto(password, initVector, fileNameIn, fileNameOut, encrypt)) {
			System.out.printf("%s completed. Generated file %s based on file %s.\n",
					encrypt ? "Encryption" : "Decryption", fileNameOut, fileNameIn);
		}

	}

	/**
	 * This method is used internally for initializing {@link Cipher} object and
	 * starting the encryption/decryption process.
	 * 
	 * @param password
	 *            password for encryption/decryption
	 * @param initVector
	 *            initialization vector
	 * @param fileNameIn
	 *            name of file to be encrypted/decrypted
	 * @param fileNameOut
	 *            output file name
	 * @param encrypt
	 *            flag defining if encryption mode is active or not
	 * @return <code>true</code> if encryption/decryption completed successfully,
	 *         <code>false</code> otherwise
	 */
	private static boolean runCrypto(byte[] password, byte[] initVector, String fileNameIn, String fileNameOut,
			boolean encrypt) {
		SecretKeySpec keySpec = new SecretKeySpec(password, "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(initVector);

		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);
		} catch (Exception ignorable) {
		}

		Path pathIn = Paths.get(fileNameIn);
		Path pathOut = Paths.get(fileNameOut);

		return runCipher(cipher, pathIn, pathOut);
	}

	/**
	 * This method is used internally for reading file, encrypting/decrypting data
	 * and creating output file.
	 * 
	 * @param aesCipher
	 *            {@link Cipher} object used for encryption/decryption
	 * @param pathIn
	 *            {@link Path} for input file
	 * @param pathOut
	 *            {@link Path} for output file
	 */
	private static boolean runCipher(Cipher aesCipher, Path pathIn, Path pathOut) {
		try (BufferedInputStream bsIn = new BufferedInputStream(Files.newInputStream(pathIn, StandardOpenOption.READ));
				BufferedOutputStream bsOut = new BufferedOutputStream(
						Files.newOutputStream(pathOut, StandardOpenOption.CREATE))) {

			while (true) {
				byte[] buff = new byte[1024];
				int r = bsIn.read(buff);
				if (r < 1) {
					break;
				}
				byte[] ciphertext = aesCipher.update(buff, 0, r);
				bsOut.write(ciphertext);
			}
			aesCipher.doFinal();
			bsOut.flush();
		} catch (IOException ex) {
			System.err.println("Can't open file: " + pathIn.getFileName());
			return false;
		} catch (IllegalBlockSizeException | BadPaddingException ignorable) {}

		return true;
	}

	/**
	 * This method is called when user wants to check sha-256 digest of some file.
	 * 
	 * @param fileName
	 *            file to be checked
	 * @param sc
	 *            {@link Scanner} for providing input
	 */
	private static void digestingMode(String fileName, Scanner sc) {
		byte[] digest = null;

		while (true) {
			System.out.printf("Please provide expected sha-256 digest for %s:\n> ", fileName);
			String digestString = sc.nextLine();

			digest = inputToByteArray(digestString, "Invalid digest. Provided hex-encoded string is not valid.");

			if (digest == null)
				continue;

			if (checkByteArrayLength(digest, 32, "Invalid digest. Must be 256 bits long. Is: %d bits long.\n")) {
				break;
			}
		}

		checkSHA256(digest, fileName);
	}

	/**
	 * This method is used internally for checking if given input is sha-256 digest
	 * of given file.
	 * 
	 * @param digest
	 *            checked digest
	 * @param fileName
	 */
	private static void checkSHA256(byte[] digest, String fileName) {
		MessageDigest sha = null;
		Path p = Paths.get(fileName);

		try {
			sha = getInstance("SHA-256");
		} catch (NoSuchAlgorithmException ignorable) {
		}

		try (BufferedInputStream bs = new BufferedInputStream(Files.newInputStream(p, StandardOpenOption.READ))) {
			while (true) {
				byte[] buff = new byte[4096];
				int r = bs.read(buff);
				if (r < 1) {
					break;
				}

				sha.update(buff, 0, r);
			}
		} catch (IOException e) {
			System.err.println("Can't open file: " + fileName);
			return;
		}

		byte[] realDigest = sha.digest();

		if (Arrays.equals(digest, realDigest)) {
			System.out.printf("Digesting completed. Digest of %s matches expected digest.", fileName);
		} else {
			System.out.printf("Digesting completed. Digest of %s does not match the expected digest.\nDigest was: %s",
					fileName, bytetohex(realDigest));
		}
	}

	/**
	 * Helper method for converting hex-encoded string to byte array.
	 * 
	 * @param input
	 *            string to be converted
	 * @param message
	 *            message to be printed on <code>System.out</code> if converting
	 *            fails
	 * @return converted byte array
	 */
	private static byte[] inputToByteArray(String input, String message) {
		byte[] ret = null;
		try {
			ret = hextobyte(input);
		} catch (IllegalArgumentException ex) {
			System.out.println(message);
		}

		return ret;
	}

	/**
	 * Helper method used for checking if the array length is as big as expected to
	 * be.
	 * 
	 * @param array
	 *            array which length is checked
	 * @param length
	 *            expected length
	 * @param message
	 *            message which is printed on <code>System.out</code> if array
	 *            length isn't valid
	 * @return <code>true</code> if array length is equal to expected length,
	 *         <code>false</code> otherwise
	 */
	private static boolean checkByteArrayLength(byte[] array, int length, String message) {
		if (array.length != length) {
			System.out.printf(message, array.length * 8);
			return false;
		}
		return true;
	}
}
