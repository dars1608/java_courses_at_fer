package hr.fer.zemris.java.hw07.shell.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.util.Collections.unmodifiableList;

import java.io.IOException;

import hr.fer.zemris.java.hw07.shell.commands.ShellCommand;

/**
 * This class is helper class used for loading description of
 * {@link ShellCommand} objects from text file.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class DescriptionLoader {
	
	/**
	 * This method is used for loading description of
	 * {@link ShellCommand} objects from text file.
	 * 
	 * @param pathName string representing path of text file
	 * @return unmodifiable list of strings; every string contains single line of file
	 */
	public static List<String> load(String pathName) {
		Path p = Paths.get(pathName);
		try {
			return unmodifiableList(Files.readAllLines(p));
		} catch (IOException e) {
			return unmodifiableList(List.of("No description found"));
		}
	}
}
