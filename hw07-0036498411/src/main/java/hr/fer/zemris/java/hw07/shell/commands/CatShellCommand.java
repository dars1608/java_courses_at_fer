package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * This class represents {@link ShellCommand} which takes one or two arguments.
 * The first argument is path to some file and is mandatory. The second argument
 * is charset name that should be used to interpret chars from bytes. If not
 * provided, a default platform charset should be used. This command opens given
 * file and writes its content to console.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CatShellCommand extends AbstractShellCommand implements ShellCommand{

	/**
	 * Constructor method for class {@link CatShellCommand}.
	 */
	public CatShellCommand() {
		super("src/main/resources/cat.txt", "cat");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(
				arguments, env, i -> i.size() == 1 || i.size() == 2);
		
		if(argumentsList == null) {
			return ShellStatus.CONTINUE;
		}else if (argumentsList.size() == 1) {
			writeOnShell(argumentsList.get(0), null, env);
		} else  {
			try {
				Charset cs = Charset.forName(argumentsList.get(1));
				writeOnShell(argumentsList.get(0), cs, env);
			} catch (IllegalCharsetNameException e) {
				env.writeln("Illegal charset name");
			}
		} 
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used internally for writing file on console. Parameter charset
	 * is not required. If it is passed, it is used to interpret chars from bytes.
	 * 
	 * @param pathName
	 *            name of file to be written on console
	 * @param charset
	 *            used to interpret chars from bytes
	 * @param env
	 *            {@link Environment} of shell
	 */
	private void writeOnShell(String pathName, Charset charset, Environment env) {
		Path p = Paths.get(pathName);
		File f = p.toFile();
		
		List<String> lines = null;
		
		if (f.isDirectory()) {
			env.writeln("Given argument is path of directory. File path expected.");
			return;
		} else if(!f.exists()) {
			env.writeln("File doesn't exist.");
			return;
		} else if (!f.canRead()) {
			env.writeln("File isn't readable.");
			return;
		}

		try {
			if (charset == null) {
				lines = Files.readAllLines(p);
			} else {
				lines = Files.readAllLines(p, charset);
			}
		} catch (IOException e) {
			env.writeln("File can't be opened");
			return;
		}
		
		lines.stream().forEach(i ->{ 
			env.writeln(i);
		});
	}
}
