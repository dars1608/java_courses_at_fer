package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * This class represents {@link ShellCommand} which writes directory tree on
 * shell. Every level of tree is indented by level*2 spaces. "." represents root
 * directory.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TreeShellCommand extends AbstractShellCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link TreeShellCommand}.
	 */
	public TreeShellCommand() {
		super("src/main/resources/tree.txt", "tree");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);
		if (argumentsList != null) {
			File dir = Paths.get(argumentsList.get(0)).toFile();
			if (!dir.isDirectory()) {
				env.writeln("Given argument isn't path of a directory.");
			} else {
				directoryTree(dir, env, 0);
			}
		}
		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used internally for writing directory tree on shell recursively.
	 * 
	 * @param root
	 *            root directory
	 * @param env
	 *            {@link Environment} of shell
	 * @param offset
	 *            offset of every tree level in output
	 */
	private void directoryTree(File root, Environment env, int offset) {
		File[] files = root.listFiles();
		if (offset == 0) {
			env.writeln(".");
			offset++;
		}
		String toWrite = "%" + String.format("%d", offset * 2) + "s%s";

		for (File file : files) {
			env.writeln(String.format(toWrite, " ", file.getName()));
			if (file.isDirectory()) {
				directoryTree(file, env, offset + 1);
			}
		}

	}
}
