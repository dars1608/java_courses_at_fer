package hr.fer.zemris.java.hw07.shell.commands;

import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.MyShell;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * This class represents {@link ShellCommand} used for obtaining info about commands
 * defined in {@link MyShell} program. If started with no arguments, it lists names 
 * of all supported commands. If started with single argument, it prints name and the
 * description of selected command (or prints appropriate error message if no such
 * command exists).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class HelpShellCommand extends AbstractShellCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link HelpShellCommand}.
	 */
	public HelpShellCommand() {
		super("src/main/resources/help.txt", "help");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 0 || i.size() == 1);
		
		if(argumentsList!=null) {
			if(argumentsList.size() == 0) {
				env.commands().forEach((k,v) -> {
					env.writeln(String.format("---  Description of %s command:\n", k));
					v.getCommandDescription()
						.stream()
						.forEach(d -> env.writeln(d));
					env.writeln("");
				});
				
			} else {
				String commandName = argumentsList.get(0);
				
				if (!env.commands().containsKey(commandName)) {
					env.writeln(String.format("Command %s is undefined.", commandName));
				} else {
					env.writeln(String.format("--- Description of %s command:\n", commandName));
					env.commands()
						.get(commandName)
						.getCommandDescription()
						.stream()
						.forEach(i->env.writeln(i));
					env.writeln("");
				}
			}
		}
		
		return ShellStatus.CONTINUE;
		
	}

}
