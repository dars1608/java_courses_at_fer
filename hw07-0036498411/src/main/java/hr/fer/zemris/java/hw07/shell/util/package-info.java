/**
 * This package contains helper classes for
 * {@link hr.fer.zemris.java.hw07.shell.MyShell} program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.shell.util;