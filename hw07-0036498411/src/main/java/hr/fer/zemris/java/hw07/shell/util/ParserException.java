package hr.fer.zemris.java.hw07.shell.util;

/**
 * This class defines an exception generated while parsing arguments.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ParserException extends RuntimeException {

	/**
	 * Generated serial version.
	 */
	private static final long serialVersionUID = -1173741942168406063L;

	/**
	 * Default constructor for {@link ParserException}.
	 */
	public ParserException() {

	}

	/**
	 * Constructor method for {@link ParserException}.
	 * 
	 * @param message
	 *            Detail message.
	 */
	public ParserException(String message) {
		super(message);
	}

	/**
	 * Constructor method for {@link ParserException}.
	 * 
	 * @param t
	 *            Cause.
	 */
	public ParserException(Throwable t) {
		super(t);
	}

	/**
	 * Constructor method for {@link ParserException}.
	 * 
	 * @param message
	 *            Detail message.
	 * @param t
	 *            Cause.
	 */
	public ParserException(String message, Throwable t) {
		super(message, t);
	}
}
