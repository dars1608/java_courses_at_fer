package hr.fer.zemris.java.hw07.crypto;

import java.util.Objects;

/**
 * This class contains some of the helping methods such as converting
 * hex-encoded String to byte array and vice versa.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Util {

	/**
	 * This method is used for converting hex-encoded String to byte array.
	 * 
	 * @param keyText
	 *            hex-encoded String to be converted
	 * @return converted byte array
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 * @throws IllegalArgumentException
	 *             if argument is in bad format
	 */
	public static byte[] hextobyte(String keyText) {
		Objects.requireNonNull(keyText, "Argument must be non null.");

		int len = keyText.length();
		if (len % 2 != 0) {
			throw new IllegalArgumentException("String argument can't be odd-sized.");
		}

		byte[] retArray = new byte[len / 2];
		char[] charArray = keyText.toCharArray();

		for (int i = 0; i < len; i += 2) {
			byte big = getNibble(charArray[i]);
			byte little = getNibble(charArray[i + 1]);
			retArray[i / 2] = (byte) ((big << 4) | little);
		}

		return retArray;
	}

	/**
	 * This method is used for converting byte array to hex-encoded String.
	 * 
	 * @param byteArray
	 *            byte array to be converted
	 * @return converted hex-encoded String
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public static String bytetohex(byte[] byteArray) {
		Objects.requireNonNull(byteArray, "Argument must be non null.");

		char[] retArr = new char[byteArray.length * 2];
		for (int i = 0; i < byteArray.length; i++) {
			byte big = (byte) ((byteArray[i] >> 4) & 0x0f);
			byte small = (byte) (byteArray[i] & 0x0f);

			retArr[2 * i] = getHex(big);
			retArr[2 * i + 1] = getHex(small);
		}

		return new String(retArr);
	}

	/**
	 * This method is used internally to convert nibble (half of byte) argument to
	 * hexadecimal number.
	 * 
	 * @param b
	 *            nibble to convert
	 * @return converted hexadecimal number
	 */
	private static char getHex(byte b) {
		return b < 10 ? (char) ('0' + b) : (char) ('a' + b - 10);
	}

	/**
	 * This method is used internally to convert hexadecimal number to nibble (half
	 * of byte).
	 * 
	 * @param c
	 *            hexadecimal number to convert
	 * @return converted nibble
	 */
	private static byte getNibble(char c) {
		if (c >= 'a' && c <= 'f') {
			return (byte) (c - 'a' + 10);
		} else if (c >= 'A' && c <= 'F') {
			return (byte) (c - 'A' + 10);
		} else if (c >= '0' && c <= '9') {
			return (byte) (c - '0');
		}

		throw new IllegalArgumentException("String argument contains invalid characters.");
	}
}
