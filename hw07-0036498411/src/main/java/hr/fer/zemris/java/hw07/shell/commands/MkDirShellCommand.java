package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * This class represents {@link ShellCommand}. The mkdir command takes a single
 * argument: directory name, and creates the appropriate directory structure.
 * If creation fails, writes appropriate message on shell.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class MkDirShellCommand extends AbstractShellCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link MkDirShellCommand}.
	 */
	public MkDirShellCommand() {
		super("src/main/resources/mkdir.txt", "mkdir");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);

		if (argumentsList != null) {
			File f = new File(argumentsList.get(0));

			try {
				if (f.mkdir()) {
					env.writeln(String.format("Directory %s created successfuly.", f.getAbsolutePath()));
				} else {
					env.writeln(String.format("Directory %s failed to create.", f.getAbsolutePath()));
				}
			} catch (SecurityException ex) {
				env.writeln(String.format("Security manager permits %s to be created.", f.getAbsolutePath()));
			}
		}

		return ShellStatus.CONTINUE;
	}
}
