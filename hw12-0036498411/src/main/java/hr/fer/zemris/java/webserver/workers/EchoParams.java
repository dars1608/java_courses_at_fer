package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.ServerLogger;

/**
 * This class represents simple {@link IWebWorker} used for showing
 * http request parameters in the html table.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class EchoParams implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) throws Exception {
		context.setMimeType("text/html");
		
		try {
			context.write("<html><body><table border = 1px solid black>");
			
			context.getParameterNames().forEach(name -> {
				try {
					context.write(String.format(
							"<tr><th>%s</th><th>%s</th></tr>",
							name, 
							context.getParameter(name)));
				} catch (IOException e) {
					ServerLogger.getInstance().write(e);
				}
			});
			
			context.write("</table></body></html>");
		} catch (IOException ex) {
			ServerLogger.getInstance().write(ex);
		}

	}

}