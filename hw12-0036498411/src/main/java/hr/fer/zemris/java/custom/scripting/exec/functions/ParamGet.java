package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which obtains
 * from requestContext parameters map a value mapped for name and pushes it onto
 * stack. If there is no such mapping, it pushes instead defValue onto stack.
 * Conceptually, equals to: dv = pop(), name = pop(),
 * value=reqCtx.getParam(name), push(value==null ? defValue : value).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ParamGet implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		Object dv = temp.pop();
		String name = temp.pop().toString();
		String value = requestContext.getParameter(name);
		
		temp.push(value == null ? dv : value);
	}

}
