package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This interface describes a function supported by {@link SmartScriptEngine}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface SSFunction {

	/**
	 * This method initiates the function described by this object.
	 * 
	 * @param temp temporary stack
	 * @param requestContext request context
	 */
	void accept(Stack<Object> temp, RequestContext requestContext);
}
