package hr.fer.zemris.java.custom.scripting.lexer;

import hr.fer.zemris.java.custom.scripting.lexer.Token.TokenType;

/**
 * This class represents simple lexer used for characters grouping and
 * extracting next token from given string.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Lexer {
	
	/*--------------------------defined constants----------------------------*/
	
	private static final char CR = '\r';
	private static final char NEXT_LINE = '\n';
	private static final char TAB = '\t';
	private static final char SPACE = ' ';
	private static final char HASH_SIGN = '#';
	private static final char ESCAPE_SIGN = '\\';
	
	/*-------------------------------------------------------------------------*/
	
	/** Field used for storing input characters. */
	private char[] data;
	/** Current {@link Token}. */
	private Token token;
	/** Index of first unprocessed character in field <code>data</code>. */
	private int currentIndex;
	/** Current lexer state. */
	private LexerState state = LexerState.BASIC;

	/**
	 * Constructor method for class {@link Lexer}. Given input text will be
	 * tokenized.
	 * 
	 * @param text
	 *            Text input.
	 * @throws NullPointerException
	 * 			  if given string reference is <code>null</code>.
	 */
	public Lexer(String text) {
		if (text == null) {
			throw new NullPointerException("Text parameter cannot be null");
		}
		data = text.toCharArray();
	}

	/**
	 * Method used for generating next token from input data. Returns generated
	 * token and sets current token.
	 * 
	 * @return Generated token.
	 * @throws LexerException
	 *             if error while generating token occurs.
	 */
	public Token nextToken() {
		generateToken();
		return token;
	}

	/**
	 * Getter method for current token.
	 * 
	 * @return Last generated token.
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * Setter method for current lexer state.
	 * 
	 * @param state
	 *            {@link LexerState}
	 * @throws NullPointerException
	 * 		      if given state is <code>null</code>.
	 */
	public void setState(LexerState state) {
		if (state == null) {
			throw new NullPointerException("State cannot be null");
		}
		this.state = state;
	}

	/**
	 * Method used internally for generating token and setting <code>token</code> to
	 * generated value.
	 * 
	 * @throws LexerException
	 *             if error while generating token occurs.
	 */
	private void generateToken() {
		if (token != null && token.getType() == TokenType.EOF) {
			throw new LexerException("No tokens available.");
		}
		skipBlankSpaces();

		if (currentIndex >= data.length) {
			token = new Token(TokenType.EOF, null);
			return;
		}

		char c = data[currentIndex];
		if (state == LexerState.BASIC) {
			if (Character.isLetter(c) || c == ESCAPE_SIGN) {
				extractWord();
			} else if (Character.isDigit(c)) {
				extractNumber();
			} else {
				extractSymbol();
			}
		} else {
			extractWordExtended();
		}

	}

	/**
	 * This method is used internally for generating token type
	 * <code>TokenType.WORD</code> and sets current token value while this
	 * {@link Lexer} is in extended state.
	 */
	private void extractWordExtended() {
		char c = data[currentIndex];
		if (c == '#') {
			token = new Token(TokenType.SYMBOL, c);
			setState(LexerState.BASIC);
			currentIndex++;
			return;
		}

		StringBuilder sb = new StringBuilder();
		while (c != HASH_SIGN && c != SPACE) {
			sb.append(c);
			currentIndex++;
			if (currentIndex >= data.length)
				break;
			c = data[currentIndex];
		}
		token = new Token(TokenType.WORD, sb.toString());
	}

	/**
	 * This method is used internally for generating token type
	 * <code>TokenType.NUMBER</code> and sets current token value.
	 * 
	 * @throws LexerException
	 *             if number is too long.
	 */
	private void extractNumber() {
		StringBuilder sb = new StringBuilder();
		char c = data[currentIndex];

		while (Character.isDigit(c)) {
			sb.append(c);
			currentIndex++;
			if (currentIndex >= data.length)
				break;
			c = data[currentIndex];
		}

		try {
			token = new Token(TokenType.NUMBER, Long.parseLong(sb.toString()));
		} catch (NumberFormatException ex) {
			throw new LexerException("Invalid input. Text contains number which cannot be stored in long type");
		}
	}

	/**
	 * This method is used internally for generating token type
	 * <code>TokenType.WORD</code> and sets current token value.
	 * 
	 * @throws LexerException
	 *             if escaping is invalid.
	 */
	private void extractWord() {
		StringBuilder sb = new StringBuilder();
		char c = data[currentIndex];

		while (Character.isLetter(c) || c == ESCAPE_SIGN) {
			if (c == ESCAPE_SIGN) {
				currentIndex++;
				if (currentIndex >= data.length || Character.isLetter(data[currentIndex]))
					throw new LexerException("Invalid input. Text cannot be tokenized.");
				c = data[currentIndex];
			}
			sb.append(c);
			currentIndex++;
			if (currentIndex >= data.length)
				break;
			c = data[currentIndex];
		}

		token = new Token(TokenType.WORD, sb.toString());
	}

	/**
	 * This method is used internally for generating token type
	 * <code>TokenType.NUMBER</code> and sets current token value.
	 */
	private void extractSymbol() {
		char c = data[currentIndex];
		if (c == HASH_SIGN) {
			setState(LexerState.EXTENDED);
		}
		token = new Token(TokenType.SYMBOL, Character.valueOf(c));
		currentIndex++;
	}

	/**
	 * This method is used internally for skipping blank spaces stored in
	 * <code>data</code> field.
	 */
	private void skipBlankSpaces() {
		while (currentIndex < data.length) {
			char currentChar = data[currentIndex];
			if (!isBlank(currentChar)) {
				break;
			}
			currentIndex++;
		}

	}

	/**
	 * This method is used internally for checking if given character represents
	 * blank space.
	 * 
	 * @param c
	 *            Checked character
	 * @return <code>true</code> if it is blank, <code>false</code> otherwise.
	 */
	private static boolean isBlank(char c) {
		if (c == SPACE)
			return true;
		if (c == TAB)
			return true;
		if (c == CR)
			return true;
		if (c == NEXT_LINE)
			return true;
		
		return false;

	}

	/**
	 * Enumeration representing in which state is lexer working
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public enum LexerState {
		/** Lexer generates all tokens normally */
		BASIC,
		/**
		 * Lexer generates one word until next # character or next call of
		 * <code>setState(TokenType.BASIC)</code>
		 */
		EXTENDED;
	}
}
