/**
 * This package contains demonstration programs.
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.demo;