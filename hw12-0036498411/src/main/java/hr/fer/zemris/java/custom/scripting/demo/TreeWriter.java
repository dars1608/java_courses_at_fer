package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * This program demonstrates the functionality of {@link INodeVisitor}. It
 * accepts one command line argument: path of a smart script document.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TreeWriter { //test file: src/test/resources/example.txt

	/**
	 * This method is called when program starts.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.err.println("Expected one argument (path of smart script document).");
			return;
		}

		Path path = Paths.get(args[0]);

		StringBuilder sb = new StringBuilder();
		Files.readAllLines(path, StandardCharsets.UTF_8).forEach(l -> sb.append(l).append("\r\n"));
		String docBody = sb.toString();

		SmartScriptParser p = new SmartScriptParser(docBody);
		WriterVisitor visitor = new WriterVisitor();
		p.getDocumentNode().accept(visitor);
	}

	/**
	 * This class represents an implementation of the {@link INodeVisitor} which
	 * reads the smart script and writes it on standard output.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private static class WriterVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			System.out.print(node.getText());

		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.print(String.format("{$ FOR %s %s %s %s $}", 
					node.getVariable().asText(),
					node.getStartExpression().asText(),
					node.getEndExpression().asText(),
					node.getStepExpression() != null ? node.getStepExpression().asText() : ""));

			int size = node.numberOfChildren();
			for (int i = 0; i < size; i++) {
				node.getChild(i).accept(this);
			}

			System.out.print("{$END$}");

		}

		@Override
		public void visitEchoNode(EchoNode node) {
			StringBuilder sb = new StringBuilder();

			sb.append("{$= ");

			Element[] array = ((EchoNode) node).getElements();
			for (Element e : array) {
				sb.append(e.asText());
				sb.append(" ");
			}

			sb.append("$}");

			System.out.print(sb.toString());
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			int size = node.numberOfChildren();
			for (int i = 0; i < size; i++) {
				node.getChild(i).accept(this);
			}
		}

	}

}
