/** 
 * This package contains classes used to model a simple web server.
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.webserver;