package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents {@link IWebWorker} which initializes the home page
 * which contains all of the functionalities of the server.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Home implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) throws Exception {
		String background = context.getPersistentParameter("bgcolor");

		if (background != null) {
			context.setTemporaryParameter("background", background);
		} else {
			context.setTemporaryParameter("background", "7F7F7F");
		}

		context.getDispatcher().dispatchRequest("/private/home.smscr");
	}

}
