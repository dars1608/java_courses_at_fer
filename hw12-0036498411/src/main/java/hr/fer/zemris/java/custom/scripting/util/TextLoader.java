package hr.fer.zemris.java.custom.scripting.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * This is utility class used for independent loading from text file.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TextLoader {

	/**
	 * This method is used for loading the text from file with given filename.
	 * 
	 * @param filename file name
	 * @param o object in relation to which the file is loaded
	 * @return read text
	 */
	public static String loader(String filename, Object o) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try (InputStream is = o.getClass().getResourceAsStream(filename)) {
			if (is == null) {
				throw new IllegalArgumentException("File " + filename + " doesn't exist");
			}
			
			byte[] buffer = new byte[1024];
			while (true) {
				int read = is.read(buffer);
				if (read < 1)
					break;
				bos.write(buffer, 0, read);
			}
			return new String(bos.toByteArray(), StandardCharsets.UTF_8);
		} catch (IOException ex) {
			return null;
		}
	}
}
