package hr.fer.zemris.java.custom.scripting.parser;

/**
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartScriptParserException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7146405271722626823L;

	/**
	 * Default constructor method for {@link SmartScriptParserException}.
	 */
	public SmartScriptParserException() {

	}

	/**
	 * 
	 * Constructor method for {@link SmartScriptParserException}.
	 * 
	 * @param message
	 *            Detail message.
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}

	/**
	 * Constructor method for {@link SmartScriptParserException}.
	 * 
	 * @param message
	 *            Detail message.
	 * @param t
	 *            Cause.
	 */
	public SmartScriptParserException(String message, Throwable t) {
		super(message, t);
	}

	/**
	 * Construcor method for {@link SmartScriptParserException}.
	 * 
	 * @param t Cause.
	 */
	public SmartScriptParserException(Throwable t) {
		super(t);
	}

}
