package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which
 * calculates sinus from given argument and stores the result back to stack.
 * Conceptually, equals to: x = pop(), r = sin(x), push(r).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Sinus implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		Object o = temp.pop();
		
		double ret;
		if(o instanceof Integer) {
			ret = sin(toRadians((Integer) o));
		} else if(o instanceof Double) {
			ret = sin(toRadians((Double) o));
		} else {
			throw new IllegalArgumentException("Sinus doesn't support argumet " + o.toString());
		}

		temp.push(ret);
	}

}
