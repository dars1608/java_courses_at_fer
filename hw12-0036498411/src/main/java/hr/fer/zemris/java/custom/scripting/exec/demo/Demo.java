package hr.fer.zemris.java.custom.scripting.exec.demo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.util.TextLoader;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * This program demonstrates functionalities of {@link SmartScriptEngine}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Demo {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 * @throws IOException
	 *             if reading fails
	 */
	public static void main(String[] args) throws IOException {
		Demo d = new Demo();

		System.out.println("Example 1:");
		example1(d);
		System.out.println();

		System.out.println("Example 2:");
		example2(d);
		System.out.println();

		System.out.println("Example 3:");
		example3(d);
		System.out.println();

		System.out.println("Example 4:");
		example4(d);
		System.out.println();

		System.out.println("Example 5:");
		example5(d);

	}

	/**
	 * First example from homework pdf.
	 * 
	 * @param d
	 *            for reference
	 * @throws IOException
	 *             if reading fails
	 */
	private static void example1(Demo d) throws IOException {
		String documentBody = TextLoader.loader("./examples/osnovni.smscr", d);

		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		// create engine and execute it
		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)).execute();

	}

	/**
	 * Second example from homework pdf.
	 * 
	 * @param d
	 *            for reference
	 * @throws IOException
	 *             if reading fails
	 */
	private static void example2(Demo d) throws IOException {
		String documentBody = TextLoader.loader("./examples/zbrajanje.smscr", d);
		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		parameters.put("a", "4");
		parameters.put("b", "2");
		// create engine and execute it
		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)).execute();

	}

	/**
	 * Third example from homework pdf.
	 * 
	 * @param d
	 *            for reference
	 * @throws IOException
	 *             if reading fails
	 */
	private static void example3(Demo d) throws IOException {
		String documentBody = TextLoader.loader("./examples/brojPoziva.smscr", d);

		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		persistentParameters.put("brojPoziva", "3");
		RequestContext rc = new RequestContext(System.out, parameters, persistentParameters, cookies);

		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(), rc).execute();
		System.out.println("Vrijednost u mapi: " + rc.getPersistentParameter("brojPoziva"));

	}

	/**
	 * Forth example from homework pdf.
	 * 
	 * @param d
	 *            for reference
	 * @throws IOException
	 *             if reading fails
	 */
	private static void example4(Demo d) throws IOException {
		String documentBody = TextLoader.loader("./examples/fibonacci.smscr", d);

		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		// create engine and execute it
		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)).execute();

	}

	/**
	 * Fifth example from homework pdf.
	 * 
	 * @param d
	 *            for reference
	 * @throws IOException
	 *             if reading fails
	 */
	private static void example5(Demo d) throws IOException {
		String documentBody = TextLoader.loader("./examples/fibonaccih.smscr", d);

		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		// create engine and execute it
		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(),
				new RequestContext(System.out, parameters, persistentParameters, cookies)).execute();

	}

}
