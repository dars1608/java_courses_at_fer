package hr.fer.zemris.java.webserver;

/**
 * This interface defines the behavior of http request dispatcher used in
 * {@link SmartHttpServer}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IDispatcher {

	/**
	 * This method is used for dispatching http request.
	 * 
	 * @param urlPath
	 *            url path
	 * @throws Exception
	 *             if unexpected behavior occurs
	 */
	void dispatchRequest(String urlPath) throws Exception;
}
