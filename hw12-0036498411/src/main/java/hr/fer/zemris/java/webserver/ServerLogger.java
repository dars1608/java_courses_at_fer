package hr.fer.zemris.java.webserver;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents simple server logger used for logging events and errors of 
 * {@link SmartHttpServer} into the text file. It represents singleton object (so there's
 * only one logger active at the time).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ServerLogger implements Closeable{
	
	/** Date-time formatter */
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd_HH-mm-ss");
	/** Default number of logs*/
	private static final int DEFAULT_NUMBER_OF_LOGS = 10;
	
	/** Logger instance*/
	private static ServerLogger instance;
	/** Print stream */
	private PrintStream ps;
	/** Log directory path */
	private static String logDirPath;
	/** Number of logs in log directory*/
	private static int numberOfLogs = DEFAULT_NUMBER_OF_LOGS;
	
	/**
	 * Constructor method for class {@link ServerLogger}.
	 * 
	 * @param logPath path of log directory
	 * @throws IOException if log directory doesn't exist
	 */
	private ServerLogger(String logPath) throws IOException {
		String logFilePath = 
				logPath +
				"server_log"+
				FORMATTER.format(LocalDateTime.now()) + 
				".txt";
		
		this.ps = new PrintStream(Files.newOutputStream(Paths.get(logFilePath)));
		deleteOldLogs(logPath);
	}

	/**
	 * This method returns an instance of the {@link ServerLogger} object.
	 * 
	 * @return instance of {@link ServerLogger}
	 * @throws IllegalStateException if logPath isn't set
	 */
	public static ServerLogger getInstance() {
		if(instance == null) {
			throw new IllegalStateException(
					"Log path isn't set. Call setLogPath(String) to set the logger path.");
		}
		
		return instance;
	}
	
	/**
	 * This method is used for setting the log directory path. It must be called
	 * before method getInstance() - otherwise, it will throw exception.
	 * 
	 * @param logPath path of log directory
	 * @throws IOException if log directory doesn't exist
	 */
	synchronized public static void setLogPath(String logPath) throws IOException {
		if(instance != null) {
			try {
				instance.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			instance = null;
		}
		
		logDirPath = logPath;
		instance = new ServerLogger(logPath);
	}
	
	/**
	 * This method is used to log a throwable object which was thrown while the
	 * log session was active.
	 * 
	 * @param t throwable object
	 */
	synchronized public void write(Throwable t){
		ps.println(FORMATTER.format(LocalDateTime.now()));
		t.printStackTrace(ps);
		ps.println();
	}
	
	/**
	 * This method is used to log some message.
	 * 
	 * @param message message
	 */
	synchronized public void write(String message) {
		ps.println(FORMATTER.format(LocalDateTime.now()));
		ps.println(message);
		ps.println();
	}

	@Override
	synchronized public void close() throws IOException {
		ps.flush();
		ps.close();
	}
	
	/**
	 * This method is used for setting the number of logs in the 
	 * log directory.
	 * 
	 * @param numberOfLogs number of logs in log directory
	 */
	public static void setNumberOfLogs(int numberOfLogs) {
		if(numberOfLogs <= 0) {
			throw new IllegalArgumentException("Argument must be greater than 0.");
		}
		
		ServerLogger.numberOfLogs = numberOfLogs;
		if(logDirPath != null) {
			deleteOldLogs(logDirPath);
		}
	}

	/**
	 * This method is used internally for cleaning old logs from log directory.
	 * 
	 * @param logPath log directory path
	 */
	private static void deleteOldLogs(String logPath) {
		File logDir = new File(logPath);
		
		File[] allFiles = logDir.listFiles();
		List<File> logs = new ArrayList<>();
		for(int i = 0; i<allFiles.length; i++) {
			if(allFiles[i].getName().startsWith("server_log")) {
				logs.add(allFiles[i]);
			}
		}
		
		if(logs.size() <= numberOfLogs) {
			return;
		}
		
		logs.sort((l1, l2) -> l2.getName().compareTo(l1.getName()));
		for(int i = logs.size() - 1; i >= numberOfLogs; i--) {		
			logs.get(i).delete();
		}
		
	}
}
