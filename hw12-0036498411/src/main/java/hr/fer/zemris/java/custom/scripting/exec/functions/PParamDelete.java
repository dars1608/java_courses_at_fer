package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which removes
 * association for name from requestContext persistentParameters map.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PParamDelete implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		String name = temp.pop().toString();
		
		requestContext.removePersistentParameter(name);
	}

}
