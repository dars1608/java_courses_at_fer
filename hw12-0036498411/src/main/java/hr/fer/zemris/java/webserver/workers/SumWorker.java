package hr.fer.zemris.java.webserver.workers;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents {@link IWebWorker} used for calculating sum based on
 * http request parameters.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SumWorker implements IWebWorker {

	/** Default value for a */
	private static final int DEF_A = 1;
	/** Default value for b */
	private static final int DEF_B = 2;

	@Override
	public void processRequest(RequestContext context) throws Exception {
		int a = DEF_A;
		Integer tempA = parseInt(context.getParameter("a"));
		if (tempA != null) {
			a = tempA;
		}

		int b = DEF_B;
		Integer tempB = parseInt(context.getParameter("b"));
		if (tempB != null) {
			b = tempB;
		}

		context.setTemporaryParameter("a", Integer.toString(a));
		context.setTemporaryParameter("b", Integer.toString(b));
		context.setTemporaryParameter("zbroj", Integer.toString(a + b));

		context.getDispatcher().dispatchRequest("/private/calc.smscr");
	}

	/**
	 * This method is used internally for parsing {@code Integer} from
	 * {@code String}.
	 * 
	 * @param s
	 *            string to be parsed
	 * @return parsed integer, {@code null} if parsing fails
	 */
	private Integer parseInt(String s) {
		Integer a = null;

		if (s != null) {
			try {
				a = Integer.parseInt(s);
			} catch (NumberFormatException ignore) {
			}
		}

		return a;
	}
}
