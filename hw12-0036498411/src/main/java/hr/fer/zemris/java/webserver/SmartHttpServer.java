package hr.fer.zemris.java.webserver;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;

/**
 * This class represents simple http web server. User must provide the location
 * of configuration file as the command line argument to start the server. Sever
 * is able to provide files from the root folder. It also can run SmartScripts on
 * {@link SmartScriptEngine} dynamically. It uses multithreading for efficiency.
 * Server uses {@link ServerLogger} for logging errors in log files.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartHttpServer {

	/** Pattern used for parsing host port */
	private static final Pattern HOST_PATTERN = Pattern.compile("Host: (.*)");
	/** Pattern used for extracting cookies from request header line*/
	private static final Pattern COOKIE_PATTERN = Pattern.compile("Cookie: (.*)");

	/** Server address */
	private String address;
	/** Server domain name */
	private String domainName;
	/** Server port */
	private int port;
	/** Number of active worker threads */
	private int workerThreads;
	/** Session timeout (in miliseconds)*/
	private int sessionTimeout;
	/** Map of mime types */
	private Map<String, String> mimeTypes = new HashMap<String, String>();
	/** Main server {@link ServerThread thread} */
	private ServerThread serverThread;
	/** Session cleaner thread */
	private SessionCleaner sessionCleaner;
	/** Thread pool used for multithreading */
	private ExecutorService threadPool;
	/** Root directory of web server */
	private Path documentRoot;
	/** Flag used for stopping the server thread from the outside */
	private volatile boolean stop = false;
	/** {@link IWebWorker Workers} map */
	private Map<String, IWebWorker> workersMap;
	/** Session map */
	private Map<String, SessionMapEntry> sessions = new HashMap<String, SessionMapEntry>();
	/** {@link Random} object */
	private Random sessionRandom = new Random(System.currentTimeMillis());

	
	/**
	 * Constructor method for class {@link SmartHttpServer}. It initializes the
	 * properties of the server from the configuration file.
	 * 
	 * @param configFileName
	 *            configuration file location
	 */
	public SmartHttpServer(String configFileName) {
		Properties p = new Properties();
		InputStream is = null;

		try {
			is = Files.newInputStream(Paths.get(configFileName));
			p.load(is);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		this.address = Objects.requireNonNull(p.getProperty("server.address"));
		this.domainName = p.getProperty("server.domainName") == null ? 
				address : p.getProperty("server.domainName");
		this.port = Integer.parseInt(p.getProperty("server.port"));
		this.workerThreads = Integer.parseInt(p.getProperty("server.workerThreads"));
		this.documentRoot = Paths.get(p.getProperty("server.documentRoot"));
		this.sessionTimeout = Integer.parseInt(p.getProperty("session.timeout")) * 1000;

		try {
			ServerLogger.setLogPath(p.getProperty("server.log"));
		} catch (IOException e) {
			System.err.println("Set log path in " + configFileName);
		}

		Path mimePath = Paths.get(p.getProperty("server.mimeConfig"));
		Path workersPath = Paths.get(p.getProperty("server.workers"));

		try {
			initWorkers(workersPath);
			initMimes(mimePath);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * This method is used for initializing mime types from mime configuration file.
	 * 
	 * @param mimePath
	 *            path of configuration file
	 * @throws IOException
	 *             if error while reading from file occurs
	 */
	private void initMimes(Path mimePath) throws IOException {
		Properties p = new Properties();
		InputStream is = Files.newInputStream(mimePath);
		p.load(is);

		p.entrySet().forEach((e -> {
			mimeTypes.put((String) e.getKey(), (String) e.getValue());
		}));

		is.close();
	}

	/**
	 * This method is used for initializing informations about web workers.
	 * 
	 * @param workersPath
	 *            path of configuration file
	 * @throws IOException
	 *             if error while reading from file occurs
	 */
	private void initWorkers(Path workersPath) throws IOException {
		Properties p = new Properties();
		InputStream is = Files.newInputStream(workersPath);
		p.load(is);
		
		workersMap = new HashMap<>();
		for (Entry<Object, Object> e : p.entrySet()) {
			String path = (String) e.getKey();
			String fqcn = (String) e.getValue();

			workersMap.put(path, instantiateWorker(fqcn));
		}
	}

	/**
	 * This method is used for instantiating an {@link IWebWorker} using class loader
	 * based on fully qualified class name.
	 * 
	 * @param fqcn
	 *            fully qualified class name
	 * @return instantiated {@link IWebWorker} object
	 */
	private IWebWorker instantiateWorker(String fqcn) {
		Class<?> referenceToClass;
		Object newObject = null;
		try {
			referenceToClass = this.getClass().getClassLoader().loadClass(fqcn);
			newObject = referenceToClass.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException 
				| ClassNotFoundException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException ex) {

			ex.printStackTrace();
			System.exit(1);
		}

		return (IWebWorker) newObject;
	}

	/**
	 * This method initiates the thread pool and starts the main server thread.
	 */
	protected synchronized void start() {
		if (serverThread != null && serverThread.isAlive()) {
			return;
		}

		threadPool = Executors.newFixedThreadPool(workerThreads, new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread worker = new Thread(r);
				worker.setDaemon(true);
				return worker;
			}
		});
		
		sessionCleaner = new SessionCleaner();
		sessionCleaner.setDaemon(true);
		sessionCleaner.start();

		serverThread = new ServerThread();
		serverThread.start();
	}

	/**
	 * This method stops the main server thread (shuts down the server)
	 */
	protected synchronized void stop() {
		stop = true;
		threadPool.shutdown();

		try {
			ServerLogger.getInstance().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This class represents the server thread. The main task of this thread is to
	 * listen the server socket on given port and wait for the request. When request
	 * arrives, it simply asks the thread pool for the worker thread and passes the
	 * request to the worker, which does requested task.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	protected class ServerThread extends Thread {

		@SuppressWarnings("resource")
		@Override
		public void run() {
			ServerSocket serverSocket = null;
			try {
				serverSocket = new ServerSocket(port);
				serverSocket.setSoTimeout(sessionTimeout);
			} catch (IOException e) {
				ServerLogger.getInstance().write(e);
				return;
			}

			while (true) {
				if (stop) {
					break;
				}

				Socket client = null;
				try {
					client = serverSocket.accept();
				} catch (IOException e) {
					continue;
				}

				ClientWorker cw = new ClientWorker(client);
				threadPool.submit(cw);
			}

			try {
				serverSocket.close();
			} catch (IOException e) {
				ServerLogger.getInstance().write(e);
			}
		}
	}

	/**
	 * This class defines a thread used for cleaning old sessions (ones which validity
	 * timestamp reached the value of current time.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	protected class SessionCleaner extends Thread {
		
		/** Timeout between two calls of the cleaner */
		private static final long TIMEOUT = 300000;

		@Override
		public void run() {
			while(true) {
				long currMilis = System.currentTimeMillis();
				Iterator<Map.Entry<String, SmartHttpServer.SessionMapEntry>> it =
						sessions.entrySet().iterator();
				
				while(it.hasNext()) {
					Map.Entry<String, SmartHttpServer.SessionMapEntry> entry = it.next();
					if (entry.getValue().validUntil < currMilis) {
						it.remove();
					}
				}
				
				try {
					sleep(TIMEOUT);
				} catch (InterruptedException e) {
					ServerLogger.getInstance().write(e);
				}
			}
		}
	}

	/**
	 * This class represents a server worker in charge of processing the http
	 * request.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class ClientWorker implements Runnable, IDispatcher {

		/** Session id length */
		private static final int SID_LENGTH = 20;

		/** Clients socket */
		private Socket csocket;
		/** Sockets input stream */
		private PushbackInputStream istream;
		/** Sockets output stream */
		private OutputStream ostream;
		/** Http version */
		private String version;
		/** Http method */
		private String method;
		/** Host name */
		private String host;
		/** Requests parameters */
		private Map<String, String> params = new HashMap<String, String>();
		/** Requests temporary parameters */
		private Map<String, String> tempParams = new HashMap<String, String>();
		/** Requests permanent parameters */
		private Map<String, String> permPrams = new HashMap<String, String>();
		/** Requests output cookies */
		private List<RCCookie> outputCookies = new ArrayList<RequestContext.RCCookie>();
		/** Session ID */
		private String SID;
		/** Current request context */
		private RequestContext context;

		/**
		 * Constructor method for class {@link ClientWorker}.
		 * 
		 * @param csocket
		 *            clients socket
		 */
		public ClientWorker(Socket csocket) {
			this.csocket = csocket;
		}

		@Override
		public void run() {
			
			try {
				istream = new PushbackInputStream(csocket.getInputStream());
				ostream = csocket.getOutputStream();

				byte[] request = readRequest(istream);

				if (request == null) {
					sendError(ostream, 400, "Bad request");
					return;
				}

				List<String> headers = extractHeaders(new String(request, StandardCharsets.US_ASCII));
				String firstLine = headers.get(0);
				String[] parts = firstLine.split("\\s+");
				method = parts[0];
				version = parts[2];

				if (parts.length != 3) {
					sendError(ostream, 400, "BAD REQUEST");
					return;
				}

				if (!method.startsWith("GET")) {
					sendError(ostream, 405, "METHOD NOT ALLOWED");
					return;
				}

				if (!(version.startsWith("HTTP/1.0") || version.startsWith("HTTP/1.1"))) {
					sendError(ostream, 505, "VERSION NOT SUPPORTED");
					return;
				}

				String requestedPath = parts[1];
				String sidCandidate = null;

				for (String l : headers) {
					Matcher m = HOST_PATTERN.matcher(l);
					if (m.matches()) {
						host = m.group(1).split(":")[0];
						continue;
					}

					m = COOKIE_PATTERN.matcher(l);
					if (m.matches()) {
						sidCandidate = findSID(m.group(1));
					}
				}

				if (host == null) {
					host = domainName;
				}

				if (checkSession(sidCandidate)) {
					outputCookies.add(new RCCookie("sid", SID, null, host, "/"));
				}
				permPrams = sessions.get(SID).map;

				int index = requestedPath.indexOf('?');
				String path = requestedPath.substring(0, index == -1 ? requestedPath.length() : index).trim();
				String paramString = index == -1 ? null : requestedPath.substring(index);

				if (paramString != null) {
					if (!parseParameters(paramString)) {
						return;
					}
				}

				internalDispatchRequest(path, true);
			} catch (Exception e) {
				ServerLogger.getInstance().write(e);
				sendError(ostream, 500, "INTERNAL SERVER ERROR");
			}
			
			try {
				csocket.close();
			} catch (IOException e) {
				ServerLogger.getInstance().write(e);
			}
		}

		/**
		 * This method is delegated method of {@link #dispatchRequest(String)} method.
		 * 
		 * @param urlPath
		 *            url path
		 * @param directCall
		 *            flag determining if the method is delegated or not
		 * @throws Exception
		 *             if exception occurs
		 */
		public void internalDispatchRequest(String urlPath, boolean directCall) throws Exception {

			if (directCall) {
				context = new RequestContext(ostream, params, permPrams, outputCookies, tempParams, this);
				context.setStatusCode(200);
				context.setStatusText("OK");
			}

			if (workersMap != null && workersMap.containsKey(urlPath)) {
				workersMap.get(urlPath).processRequest(context);
				return;
			}

			if (urlPath.startsWith("/ext/")) {
				String fqcn = "hr.fer.zemris.java.webserver.workers." + urlPath.split("/ext/")[1];
				IWebWorker worker = instantiateWorker(fqcn);
				worker.processRequest(context);
				return;
			}

			if (directCall && urlPath.startsWith("/private/")) {
				sendError(ostream, 404, "NOT FOUND");
				return;
			}

			Path reqPath = documentRoot.resolve(urlPath.substring(1));
			if (!Files.exists(reqPath) || Files.isDirectory(reqPath)
					|| !reqPath.toString().startsWith(documentRoot.toString())) {
				sendError(ostream, 403, "FORBIDDEN");
				return;
			}

			if (!Files.isReadable(reqPath)) {
				sendError(ostream, 404, "NOT FOUND");
				return;
			}

			String extension = "";
			int i = reqPath.toString().lastIndexOf('.');
			if (i > 0) {
				extension = reqPath.toString().substring(i + 1);
			}

			String mime = mimeTypes.get(extension);
			if (mime == null) {
				mime = "application/octet-stream";
			}

			if (extension.equals("smscr")) {
				runScript(reqPath, context);
			} else {
				sendFile(reqPath, mime, context);
			}
		}

		@Override
		public void dispatchRequest(String urlPath) throws Exception {
			internalDispatchRequest(urlPath, false);
		}

		/**
		 * This method is used internally to parse parameters string.
		 * 
		 * @param paramString
		 *            parameters string
		 * @return {@code true} if the parsing completed successfully, {@code false}
		 *         otherwise
		 */
		private boolean parseParameters(String paramString) {
			String[] parts = paramString.substring(1).split("&");

			for (String s : parts) {
				if(!s.contains("=")) {
					params.put(s, null);
					continue;
				}
				
				if(s.startsWith("=")) {
					sendError(ostream, 400, "BAD REQUEST");
					return false;
				}
				
				String[] entry = s.split("=");
				if(entry.length == 1) {
					params.put(entry[0], "");
					continue;
				}
				
				if (entry.length > 2) {
					sendError(ostream, 400, "BAD REQUEST");
					return false;
				}

				params.put(entry[0], entry[1]);
			}

			return true;
		}

		/**
		 * This method is used internally for extracting the header of http request. It
		 * is basically a simple deterministic automaton which
		 * 
		 * @param is
		 *            input stream
		 * @return read bytes
		 * @throws IOException if reading fails
		 * @author marcupic
		 */
		private byte[] readRequest(InputStream is) throws IOException {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int state = 0;

			/*@formatter:off*/
	l:		while(true) {
				int b = is.read();
				if(b==-1) return null;
				if(b!=13) {
					bos.write(b);
				}
				switch(state) {
				case 0: 
					if(b==13) { state=1; } else if(b==10) state=4;
					break;
				case 1: 
					if(b==10) { state=2; } else state=0;
					break;
				case 2: 
					if(b==13) { state=3; } else state=0;
					break;
				case 3: 
					if(b==10) { break l; } else state=0;
					break;
				case 4: 
					if(b==10) { break l; } else state=0;
					break;
				}
				/*@formatter:on*/
			}

			return bos.toByteArray();
		}

		/**
		 * This method is used internally for extracting separate headers from lines of
		 * headers
		 * 
		 * @param requestHeader
		 *            lines of headers
		 * @return list of headers
		 * @author marcupic
		 */
		private List<String> extractHeaders(String requestHeader) {
			List<String> headers = new ArrayList<String>();
			String currentLine = null;

			for (String s : requestHeader.split("\n")) {
				if (s.isEmpty())
					break;
				char c = s.charAt(0);
				if (c == 9 || c == 32) {
					currentLine += s;
				} else {
					if (currentLine != null) {
						headers.add(currentLine);
					}
					currentLine = s;
				}
			}
			if (!currentLine.isEmpty()) {
				headers.add(currentLine);
			}

			return headers;
		}

		/**
		 * This method is used internally for sending the error message to client.
		 * 
		 * @param cos
		 *            clients output stream
		 * @param statusCode
		 *            http status code
		 * @param statusText
		 *            http status message
		 * @author marcupic
		 */
		private void sendError(OutputStream cos, int statusCode, String statusText) {
			try {
				/*@formatter:off*/
				cos.write((
						"HTTP/1.1 " + statusCode + " " + statusText + "\r\n" 
						+ "Server: " + domainName + " \r\n"
						+ "Content-Type: text/plain;charset=UTF-8\r\n" 
						+ "Content-Length: 0\r\n"
						+ "Connection: close\r\n"
						+ "\r\n").getBytes(StandardCharsets.US_ASCII));
				cos.flush();
				/*@formatter:on*/
			} catch (IOException ex) {
				ServerLogger.getInstance().write(ex);
			}
		}

		/**
		 * This method is used for sending file from servers root folder to client.
		 * 
		 * @param reqPath
		 *            requested path
		 * @param mime
		 *            mime type
		 * @param rc
		 *            request context
		 * @throws IOException
		 *             if error while sending occurs
		 */
		private void sendFile(Path reqPath, String mime, RequestContext rc) throws IOException {
			rc.setMimeType(mime);
			File file = reqPath.toFile();
			rc.setContentLength(file.length());

			try (InputStream is = new BufferedInputStream(Files.newInputStream(reqPath))) {
				while (true) {
					byte[] buf = new byte[1024];
					int r = is.read(buf);

					if (r < 0)
						break;

					rc.write(buf);
				}
			}

		}

		/**
		 * This method is used internally when the client requested the SmartScript to
		 * be loaded from the server.
		 * 
		 * @param urlPath
		 *            url path
		 * @param rc
		 *            {@link RequestContext} object
		 * @throws IOException
		 *             if any error occurs
		 */
		private void runScript(Path urlPath, RequestContext rc) throws IOException {
			ByteArrayOutputStream bs = new ByteArrayOutputStream();
			try (InputStream is = new BufferedInputStream(Files.newInputStream(urlPath))) {
				while (true) {
					byte[] buf = new byte[1024];
					int r = is.read(buf);

					if (r < 0)
						break;

					bs.write(buf, 0, r);
				}
			}

			String document = new String(bs.toByteArray());
			DocumentNode documentNode = new SmartScriptParser(document).getDocumentNode();
			new SmartScriptEngine(documentNode, context).execute();
		}

		/**
		 * This method is used internally for generating pseudo-unique session id (20
		 * pseudo-random uppercase letters)
		 * 
		 * @return generated SID
		 */
		private String generateSID() {
			char[] sid = new char[SID_LENGTH];

			for (int i = 0; i < SID_LENGTH; i++) {
				sid[i] = (char) (sessionRandom.nextInt(26) + 'A');
			}

			return new String(sid);
		}

		/**
		 * This method is used for finding sid cookie in the line of cookies obtained
		 * from http header.
		 *
		 * @param cookies
		 *            line representing cookies
		 * @return obtained sid, {@code null} if there's no sid cookie
		 */
		private String findSID(String cookies) {
			String[] parts = cookies.split(";");
			for (String cookie : parts) {
				if (cookie.startsWith("sid=")) {
					return cookie.split("=")[1].replaceAll("\"", "");
				}
			}

			return null;
		}

		/**
		 * This method is used for checking if the client already has been connected to
		 * server. If it has, its validity period is increased by session timeout.
		 * Otherwise, method creates a new session entry with new SID and other
		 * informations.
		 * 
		 * @param sidCandidate
		 *            candidate sid
		 * @return {@code true} if the new session entry created, {@code false}
		 *         otherwise
		 */
		synchronized private boolean checkSession(String sidCandidate) {
			SessionMapEntry session = null;

			if (sidCandidate != null) {
				session = sessions.get(sidCandidate);
			} else {
				sidCandidate = generateSID();
			}

			if (session != null) {
				if (session.validUntil > System.currentTimeMillis() && session.host.equals(host)) {
					session.validUntil = System.currentTimeMillis() + sessionTimeout;
					SID = session.sid;
					return false;
				} else {
					sessions.remove(session.sid);
				}
			}

			session = new SessionMapEntry();
			session.map = new ConcurrentHashMap<String, String>();
			session.sid = sidCandidate;
			session.host = host;
			session.validUntil = System.currentTimeMillis() + sessionTimeout;

			sessions.put(session.sid, session);
			SID = session.sid;

			return true;
		}
	}

	/**
	 * This class represents an entry of session map.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private static class SessionMapEntry {
		/** Session id */
		String sid;
		/** Session host */
		String host;
		/** Time stamp */
		long validUntil;
		/** Map of persistent parameters*/
		Map<String, String> map;
	}

	/**
	 * This method is called when program starts.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String args[]) {
		if (args.length != 1) {
			System.err.println("Expected one argument: main configuration path");
			return;
		}

		SmartHttpServer server = new SmartHttpServer(args[0]);
		server.start();
	}
}
