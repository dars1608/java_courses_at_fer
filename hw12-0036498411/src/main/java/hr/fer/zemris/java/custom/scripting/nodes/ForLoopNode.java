package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * A node representing a single for-loop construct. It inherits from
 * {@link Node} class.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ForLoopNode extends Node {
	/** counter variable in for loop */
	private final ElementVariable variable;
	/** from */
	private final Element startExpression;
	/** to */
	private final Element endExpression;
	/** step */
	private final Element stepExpression;

	/**
	 * Constructor method for class {@link ForLoopNode}.
	 * 
	 * @param variable
	 *            Counter variable
	 * @param startExpression
	 *            Expression which describes the beginning of counting.
	 * @param endExpression
	 *            Expression which describes the end of counting.
	 * @param stepExpression
	 *            Expression which describes step of counting.
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {
		super();
		if (variable == null || startExpression == null || endExpression == null) {
			throw new IllegalArgumentException("Only stepExpression argument can be null");
		}
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}

	/**
	 * Alternative constructor for class {@link ForLoopNode}.
	 * 
	 * 
	 * @see hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode#ForLoopNode(ElementVariable,
	 *      Element, Element, Element)
	 */
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression) {
		this(variable, startExpression, endExpression, null);
	}

	/**
	 * Getter function for variable.
	 *
	 * @return the variable
	 */
	public ElementVariable getVariable() {
		return variable;
	}

	/**
	 * Getter function for startExpression.
	 *
	 * @return the startExpression
	 */
	public Element getStartExpression() {
		return startExpression;
	}

	/**
	 * Getter function for endExpression.
	 *
	 * @return the endExpression
	 */
	public Element getEndExpression() {
		return endExpression;
	}

	/**
	 * Getter function for stepExpression.
	 *
	 * @return the stepExpression
	 */
	public Element getStepExpression() {
		return stepExpression;
	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitForLoopNode(this);
	}

}