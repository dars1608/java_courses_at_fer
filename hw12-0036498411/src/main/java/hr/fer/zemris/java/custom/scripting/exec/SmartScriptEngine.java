package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;
import java.util.function.IntBinaryOperator;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.exec.functions.Decfmt;
import hr.fer.zemris.java.custom.scripting.exec.functions.Duplicate;
import hr.fer.zemris.java.custom.scripting.exec.functions.PParamDelete;
import hr.fer.zemris.java.custom.scripting.exec.functions.PParamGet;
import hr.fer.zemris.java.custom.scripting.exec.functions.PParamSet;
import hr.fer.zemris.java.custom.scripting.exec.functions.ParamGet;
import hr.fer.zemris.java.custom.scripting.exec.functions.SSFunction;
import hr.fer.zemris.java.custom.scripting.exec.functions.SetMimeType;
import hr.fer.zemris.java.custom.scripting.exec.functions.Sinus;
import hr.fer.zemris.java.custom.scripting.exec.functions.Swap;
import hr.fer.zemris.java.custom.scripting.exec.functions.TParamDelete;
import hr.fer.zemris.java.custom.scripting.exec.functions.TParamGet;
import hr.fer.zemris.java.custom.scripting.exec.functions.TParamSet;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.ServerLogger;

/**
 * This class represents an engine which can execute any SmartStript file.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartScriptEngine {

	/** Document root node */
	private DocumentNode documentNode;
	/** Request context */
	private RequestContext requestContext;
	/** Multistack collection */
	private ObjectMultistack multistack = new ObjectMultistack();
	/** Implemetation of a visitor */
	private INodeVisitor visitor = new NodeVisitorImpl();

	/**
	 * Constructor method for class {@link SmartScriptEngine}.
	 * 
	 * @param documentNode
	 *            root document node
	 * @param requestContext
	 *            request context
	 */
	public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
		this.documentNode = Objects.requireNonNull(documentNode);
		this.requestContext = Objects.requireNonNull(requestContext);
	}

	/**
	 * This method is used to execute the script passed through the constructor.
	 */
	public void execute() {
		documentNode.accept(visitor);
	}

	/**
	 * This class is an implementation of {@link INodeVisitor} used as the core of
	 * {@link SmartScriptEngine}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class NodeVisitorImpl implements INodeVisitor {

		/** Map of double binary operators */
		private Map<String, DoubleBinaryOperator> doubleBinOp = new HashMap<>();
		/** Map of integer binary operators */
		private Map<String, IntBinaryOperator> intBinOp = new HashMap<>();
		/** Map of {@link SSFunction} objects */
		private Map<String, SSFunction> functions = new HashMap<>();

		/**
		 * Constructor method for class {@link NodeVisitorImpl}.
		 */
		public NodeVisitorImpl() {
			initOperators();
			initFunctions();
		}

		@Override
		public void visitTextNode(TextNode node) {
			try {
				requestContext.write(node.getText());
			} catch (IOException ex) {
				ServerLogger.getInstance().write(ex);
			}

		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			String varName = node.getVariable().asText();
			ValueWrapper startValue = new ValueWrapper(node.getStartExpression().asText());
			ValueWrapper endValue = new ValueWrapper(node.getEndExpression().asText());
			Element step = node.getStepExpression();
			ValueWrapper incValue = new ValueWrapper(step == null ? "0" : step.asText());

			multistack.push(varName, startValue);

			while (true) {
				visitDirectChildren(node);

				ValueWrapper var = multistack.peek(varName);
				var.add(incValue.getValue());

				if (var.numCompare(endValue.getValue()) > 0) {
					break;
				}
			}

			multistack.pop(varName);
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			Stack<Object> temp = new Stack<>();

			for (Element e : node.getElements()) {

				if (e instanceof ElementConstantInteger) {
					temp.push(Integer.parseInt(e.asText()));
				} else if (e instanceof ElementConstantDouble) {
					temp.push(Double.parseDouble(e.asText()));
				} else if (e instanceof ElementString) {
					temp.push(((ElementString) e).getValue());
				} else if (e instanceof ElementVariable) {
					ValueWrapper var = multistack.peek(e.asText());
					temp.push(var.getValue());
				} else if (e instanceof ElementOperator) {
					operatorProcess((ElementOperator) e, temp);
				} else if (e instanceof ElementFunction) {
					functionProcess((ElementFunction) e, temp);
				}
			}

			temp.forEach(o -> {
				try {
					requestContext.write(o.toString());
				} catch (IOException ex) {
					ServerLogger.getInstance().write(ex);
				}
			});

		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			visitDirectChildren(node);
		}

		/**
		 * This method initiates {@link Node#accept(INodeVisitor)} method with every
		 * direct child of the provided {@link Node}
		 * 
		 * @param node
		 *            parent node
		 */
		private void visitDirectChildren(Node node) {
			int len = node.numberOfChildren();
			for (int i = 0; i < len; i++) {
				node.getChild(i).accept(visitor);
			}
		}

		/**
		 * This method is used internally to instantiate all {@link SSFunction} objects
		 * and store them into the map.
		 */
		private void initFunctions() {
			functions.put("@sin", new Sinus());
			functions.put("@decfmt", new Decfmt());
			functions.put("@dup", new Duplicate());
			functions.put("@swap", new Swap());
			functions.put("@setMimeType", new SetMimeType());
			functions.put("@paramGet", new ParamGet());
			functions.put("@pparamGet", new PParamGet());
			functions.put("@pparamSet", new PParamSet());
			functions.put("@pparamDel", new PParamDelete());
			functions.put("@tparamGet", new TParamGet());
			functions.put("@tparamSet", new TParamSet());
			functions.put("@tparamDel", new TParamDelete());

		}

		/**
		 * This method is used internally to instantiate all {@link DoubleBinaryOperator} and
		 * {@link IntBinaryOperator} objects and store them into the map.
		 */
		private void initOperators() {
			doubleBinOp.put("+", (a, b) -> a + b);
			doubleBinOp.put("-", (a, b) -> a - b);
			doubleBinOp.put("*", (a, b) -> a * b);
			doubleBinOp.put("/", (a, b) -> a / b);

			intBinOp.put("+", (a, b) -> a + b);
			intBinOp.put("-", (a, b) -> a - b);
			intBinOp.put("*", (a, b) -> a * b);
			intBinOp.put("/", (a, b) -> a / b);
		}

		/**
		 * This method is used internally to initiate the process of an operator.
		 * 
		 * @param e {@link ElementOperator} object
		 * @param temp temporary stack
		 */
		private void operatorProcess(ElementOperator e, Stack<Object> temp) {
			String operator = e.asText();

			Object o1 = temp.pop();
			Object o2 = temp.pop();

			Number num1 = parseNum(o1);
			Number num2 = parseNum(o2);

			if (num1 instanceof Integer && num2 instanceof Integer) {
				int res = intBinOp.get(operator).applyAsInt((Integer) num1, (Integer) num2);
				temp.push(res);
			} else {
				double res = doubleBinOp.get(operator).applyAsDouble((Double) num1, (Double) num2);
				temp.push(res);
			}
		}

		/**
		 * This method is used internally to parse an object into the number.
		 * 
		 * @param o object to be parsed
		 * @return the number
		 */
		private Number parseNum(Object o) {
			Number num = null;

			if (!(o instanceof Number)) {
				try {
					num = Integer.parseInt(o.toString());
				} catch (NumberFormatException ex) {
					num = Double.parseDouble(o.toString());
				}
			} else {
				num = (Number) o;
			}

			return num;
		}

		/**
		 * This method is used internally to initiate the process of a function.
		 * 
		 * @param e {@link ElementFunction} object
		 * @param temp temporary stack
		 */
		private void functionProcess(ElementFunction e, Stack<Object> temp) {
			functions.get(e.asText()).accept(temp, requestContext);
		}
	}
}
