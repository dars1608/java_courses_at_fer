package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Base class representing basic element of an expression.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Element {
	
	/**
	 * 
	 * Constructor method for {@link Element}.
	 */
	public Element() {
		
	}
	/**
	 * Method which converts this element to its string representation.
	 * 
	 * @return String representation of an element.
	 */
	public String asText() {
		return "";
	}
}
