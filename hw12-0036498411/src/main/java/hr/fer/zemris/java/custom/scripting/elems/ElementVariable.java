package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class extends {@link Element} class. It defines a variable element.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ElementVariable extends Element {
	/** Name of this variable element */
	private final String name;

	/**
	 * Constructor method for class {@link ElementVariable}.
	 * 
	 * @param name
	 *            Name of variable.
	 */
	public ElementVariable(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Name parameter cannot be null");
		}
		this.name = name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.custom.scripting.elems.Element#asText()
	 */
	@Override
	public String asText() {
		return name;
	}
}
