package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.text.DecimalFormat;
import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which formats
 * decimal number using given format f which is compatible with DecimalFormat;
 * produces a string. X can be integer, double or string representation of a
 * number. Conceptually, equals to: f = pop(), x = pop(), r = decfmt(x,f),
 * push(r).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Decfmt implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		String pattern = (String) temp.pop();
		Number num = (Number) temp.pop();
		DecimalFormat format = new DecimalFormat(pattern);
		
		temp.push(format.format(num));

	}

}
