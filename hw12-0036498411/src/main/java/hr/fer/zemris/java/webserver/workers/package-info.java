/**
 * This package contains {@link hr.fer.zemris.java.webserver.IWebWorker workers}
 * of {@link hr.fer.zemris.java.webserver.SmartHttpServer server}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.webserver.workers;