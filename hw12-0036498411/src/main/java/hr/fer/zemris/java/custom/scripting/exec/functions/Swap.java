package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which replaces
 * the order of two topmost items on stack. Conceptually, equals to: a = pop(),
 * b = pop(), push(a), push(b).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Swap implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		Object o1 = temp.pop();
		Object o2 = temp.pop();
		
		temp.push(o1);
		temp.push(o2);
	}

}
