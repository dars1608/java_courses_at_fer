/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class extends {@link Element} class. It defines a function element.
 * @author Darko Britvec
 * @version 1.0
 */
public class ElementFunction extends Element {
	/** Name of this function element */
	private final String name;
	
	/**
	 * Constructor method for class {@link ElementFunction}.
	 @param name
	 *            Name of function.
	 */
	public ElementFunction(String name) {
		if(name == null) {
			throw new IllegalArgumentException("Name parameter cannot be null");
		}
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.elems.Element#asText()
	 */
	@Override
	public String asText() {
		return name;
	}	
}
