/**
 * This subpackage contains class {@link SmartScriptLexer} used for solution of
 * second assignment.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.lexer;