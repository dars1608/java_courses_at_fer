package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which takes
 * string x and calls requestContext.setMimeType(x). Does not produce any
 * result.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SetMimeType implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		requestContext.setMimeType((String) temp.pop());
	}

}
