package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * This class describes a data structure called multistack. Multistack is an
 * abstraction which can be described as a map of stack like structures. Under
 * some key, there must be a stack in which values with same key are stored.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ObjectMultistack {

	/** Map used for storing stacks */
	private Map<String, MultistackEntry> map = new HashMap<>();

	/**
	 * This method is used for pushing the value on stack at given key. Key musn't
	 * be null, but value may be.
	 * 
	 * @param name
	 *            Key of an entry
	 * @param valueWrapper
	 *            {@link ValueWrapper} object containing details about value to be
	 *            pushed on multistack
	 * @throws NullPointerException
	 *             if given name or valueWrapper references are <code>null</code>
	 */
	public void push(String name, ValueWrapper valueWrapper) {
		Objects.requireNonNull(name);

		MultistackEntry oldEntry = map.get(name);
		MultistackEntry newEntry = new MultistackEntry(oldEntry, valueWrapper);

		map.put(name, newEntry);
	}

	/**
	 * This method is used for acquiring the {@link ValueWrapper} from top of the
	 * stack at given key. After acquiring the wrapper, entry is removed from stack.
	 * If there's no value stored at given key, returns <code>null</code>.
	 * 
	 * @param name
	 *            Key of a stack entry
	 * @return wrapper of stored value
	 * @throws NullPointerException
	 *             if given name reference is <code>null</code>
	 * @throws EmptyStackException
	 *             if stack at given key is empty
	 */
	public ValueWrapper pop(String name) {
		Objects.requireNonNull(name);

		if (isEmpty(name)) {
			throw new EmptyStackException();
		}

		ValueWrapper value = map.get(name).getValue();
		map.put(name, map.get(name).getPrevious());
		return value;

	}

	/**
	 * This method is used for getting the {@link ValueWrapper} from the top of the
	 * stack at given key.
	 * 
	 * @param name
	 *            Key of a stack entry
	 * @return wrapper of stored value
	 * @throws NullPointerException
	 *             if given name reference is <code>null</code>
	 * @throws EmptyStackException
	 *             if stack at given key is empty
	 */
	public ValueWrapper peek(String name) {
		Objects.requireNonNull(name);

		if (isEmpty(name)) {
			throw new EmptyStackException();
		}

		return map.get(name).getValue();
	}

	/**
	 * This method checks if the stack at given key is empty.
	 * 
	 * @param name
	 *            Key of a stack entry
	 * @return <code>true</code> if it is empty, <code>false</code> otherwise
	 */
	public boolean isEmpty(String name) {
		return map.get(name) == null;
	}

	/**
	 * This class describes an entry of {@link ObjectMultistack} structure. It
	 * models a simple stack-like structure as single-linked list.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public static class MultistackEntry {

		/** Reference of an entry stored before this */
		private final MultistackEntry previous;
		/** Value stored on stack */
		private ValueWrapper value;

		/**
		 * Constructor method for class {@link MultistackEntry}.
		 * 
		 * @param previous
		 *            Reference of an entry stored before this
		 * @param value
		 *            Wrapper of stored value
		 * @throws NullPointerException
		 *             if given value reference is <code>null</code>
		 */
		public MultistackEntry(MultistackEntry previous, ValueWrapper value) {
			super();
			this.previous = previous;
			this.value = Objects.requireNonNull(value);
		}

		/**
		 * Getter method for the <code>previous</code>.
		 * 
		 * @return the previous
		 */
		public MultistackEntry getPrevious() {
			return previous;
		}

		/**
		 * Getter method for the <code>value</code>.
		 * 
		 * @return the value
		 */
		public ValueWrapper getValue() {
			return value;
		}

		/**
		 * Setter method for the <code>value</code>.
		 * 
		 * @param value
		 *            the value to set
		 * @throws NullPointerException
		 *             if given value reference is <code>null</code>
		 */
		public void setValue(ValueWrapper value) {
			this.value = Objects.requireNonNull(value);
		}
	}
}
