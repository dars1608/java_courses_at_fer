package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which
 * duplicates current top value from stack. Conceptually, equals to: x = pop(),
 * push(x), push(x).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Duplicate implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		temp.push(temp.peek());
	}

}
