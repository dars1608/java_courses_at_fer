package hr.fer.zemris.java.webserver;

/**
 * This interface describes a web worker, which can process a http request based
 * on provided request context.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IWebWorker {

	/**
	 * This method is used for processing the http request based on provided
	 * {@link RequestContext request context}.
	 * 
	 * @param context
	 *            context
	 * @throws Exception
	 *             if any error while processing occurs
	 */
	public void processRequest(RequestContext context) throws Exception;

}
