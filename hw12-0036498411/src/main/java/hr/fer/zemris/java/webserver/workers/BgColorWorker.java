package hr.fer.zemris.java.webserver.workers;

import java.awt.Color;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents {@link IWebWorker} used for changing the
 * background color.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class BgColorWorker implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) throws Exception {
		String bgcolor = context.getParameter("bgcolor");
		
		if(bgcolor == null) {
			context.setTemporaryParameter("bgcolormessage", "Color can't be set!");
		} else if(bgcolor.length() != 6) {
			context.setTemporaryParameter("bgcolormessage", "Color can't be set!");
		} else {
			try{
				Color.decode("#"+bgcolor);
				context.setPersistentParameter("bgcolor", bgcolor);
				context.setTemporaryParameter("bgcolormessage", "");
			} catch (NumberFormatException ex){
				context.setTemporaryParameter("bgcolormessage", "Color can't be set!");
			}
		}
		
		context.getDispatcher().dispatchRequest("/index2.html");		
	}

}
