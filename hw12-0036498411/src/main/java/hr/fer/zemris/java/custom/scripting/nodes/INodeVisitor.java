package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * This interface describes a
 * <a href="https://en.wikipedia.org/wiki/Visitor_pattern">visitor</a> of a
 * {@link Node}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface INodeVisitor {

	/** This method is called when the visitor visits the {@link TextNode}.
	 * 
	 * @param node node to be visited
	 */
	public void visitTextNode(TextNode node);

	/** This method is called when the visitor visits the {@link ForLoopNode}.
	 * 
	 * @param node node to be visited
	 */
	public void visitForLoopNode(ForLoopNode node);

	/** This method is called when the visitor visits the {@link EchoNode}.
	 * 
	 * @param node node to be visited
	 */
	public void visitEchoNode(EchoNode node);

	/** This method is called when the visitor visits the {@link DocumentNode}.

	 * 
	 * @param node node to be visited
	 */
	public void visitDocumentNode(DocumentNode node);

}
