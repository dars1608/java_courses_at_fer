package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;

/**
 * A node representing a command which generates some textual output
 * dynamically. It inherits from {@link Node} class.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class EchoNode extends Node {
	
	/** Array used for storing elements. */
	private final Element[] elements;

	/**
	 * Construcor method for {@link EchoNode}.
	 * 
	 * @param elements
	 */
	public EchoNode(Element[] elements) {
		super();
		this.elements = elements;
	}

	/**
	 * Getter method for field <code>elements</code>.
	 * 
	 * @return Elements stored in EchoNode.
	 */
	public Element[] getElements() {
		return elements;
	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);
	}
}
