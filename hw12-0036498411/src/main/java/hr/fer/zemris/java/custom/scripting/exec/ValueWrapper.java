package hr.fer.zemris.java.custom.scripting.exec;

/**
 * This class represents a wrapper of types {@link Integer}, {@link Double} and
 * {@link String}. It can also wrap a <code>null</code> value.
 * <p>
 * It allows some simple operations like:
 * <li>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ValueWrapper {

	/** Value which is wrapped by this wrapper */
	private Object value;

	/**
	 * Constructor method for class {@link ValueWrapper}.
	 * 
	 * @param value
	 *            Initial value
	 */
	public ValueWrapper(Object value) {
		this.value = value;
	}

	/**
	 * Getter method for the <code>value</code>.
	 * 
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Setter method for the <code>value</code>.
	 * 
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * This method is used to add the argument value to wrapped value and store the
	 * result in wrapper.
	 * 
	 * @param incValue
	 *            Argument to be added
	 * @throws IllegalArgumentException
	 *             if argument is not acceptable for arithmetic operation
	 */
	public void add(Object incValue) {
		if (!validType(incValue) || !validType(value)) {
			throw new IllegalArgumentException(ERROR_MESSAGE);
		}

		Number arg1 = prepareArgument(value);
		Number arg2 = prepareArgument(incValue);

		if (arg1.getClass() == Integer.class && arg2.getClass() == Integer.class) {
			value = Integer.valueOf(arg1.intValue() + arg2.intValue());
		} else {
			value = Double.valueOf(arg1.doubleValue() + arg2.doubleValue());
		}
	}

	/**
	 * This method is used to subtract the argument value from wrapped value and
	 * store the result in wrapper.
	 * 
	 * @param decValue
	 *            Argument to be added
	 * @throws IllegalArgumentException
	 *             if argument is not acceptable for arithmetic operation
	 */
	public void subtract(Object decValue) {
		if (!validType(decValue) || !validType(value)) {
			throw new IllegalArgumentException(ERROR_MESSAGE);
		}

		Number arg1 = prepareArgument(value);
		Number arg2 = prepareArgument(decValue);

		if (arg1.getClass() == Integer.class && arg2.getClass() == Integer.class) {
			value = Integer.valueOf(arg1.intValue() - arg2.intValue());
		} else {
			value = Double.valueOf(arg1.doubleValue() - arg2.doubleValue());
		}
	}

	/**
	 * This method is used to multiply the argument value with wrapped value and
	 * store the result in wrapper.
	 * 
	 * @param mulValue
	 *            Argument to be added
	 * @throws IllegalArgumentException
	 *             if argument is not acceptable for arithmetic operation
	 */
	public void multiply(Object mulValue) {
		if (!validType(mulValue) || !validType(value)) {
			throw new IllegalArgumentException(ERROR_MESSAGE);
		}

		Number arg1 = prepareArgument(value);
		Number arg2 = prepareArgument(mulValue);

		if (arg1.getClass() == Integer.class && arg2.getClass() == Integer.class) {
			value = Integer.valueOf(arg1.intValue() * arg2.intValue());
		} else {
			value = Double.valueOf(arg1.doubleValue() * arg2.doubleValue());
		}
	}

	/**
	 * This method is used to add the argument value to wrapped value and store the
	 * result in wrapper.
	 * 
	 * @param divValue
	 *            Argument to be added
	 * @throws IllegalArgumentException
	 *             if argument is not acceptable for arithmetic operation
	 * @throws ArithmeticException
	 *             if division by zero occurs
	 */
	public void divide(Object divValue) {
		if (!validType(divValue) || !validType(value)) {
			throw new IllegalArgumentException(ERROR_MESSAGE);
		}

		Number arg1 = prepareArgument(value);
		Number arg2 = prepareArgument(divValue);

		if (arg2.doubleValue() == 0) { // only if second argument is literally zero (to keep the precision)
			throw new ArithmeticException("Division by zero.");
		}

		if (arg1.getClass() == Integer.class && arg2.getClass() == Integer.class) {
			value = Integer.valueOf(arg1.intValue() / arg2.intValue());
		} else {
			value = Double.valueOf(arg1.doubleValue() / arg2.doubleValue());
		}
	}

	/**
	 * This method is used to compare some value which has numerical meaning (
	 * {@link Integer}, {@link Double} or {@link String} literal representing double
	 * or int value). If some of the values are <code>null</code>, they are greater
	 * than the other, except when they are both <code>null</code>.
	 * 
	 * @param withValue
	 *            object to compare with wrapped object
	 * @return
	 *         <li>positive number if wrapped object is greater than the argument
	 *         <li>negative number if wrapped object is smaller than the argument
	 *         <li>zero if the wrapped value and the argument are the same
	 * 
	 * @throws IllegalArgumentException
	 *             if argument is not acceptable for comparing
	 */
	public int numCompare(Object withValue) {
		if (value == null && withValue == null) {
			return 0;
		} else if (value == null) {
			return 1;
		} else if (withValue == null) {
			return -1;
		}

		double arg1 = prepareArgument(value).doubleValue();
		double arg2 = prepareArgument(withValue).doubleValue();

		if(Math.abs(arg1-arg2) < 1E-8) {
			return 0;
		} else if (arg1<arg2) {
			return -1;
		} else {
			return +1;
		}
	}

	/**
	 * This method is used for preparing the argument for arithmetic operations. It
	 * converts argument to some numeric type ( {@link Integer} or {@link Double} ).
	 * 
	 * @param value
	 *            Argument to be prepared
	 * @return {@link Number} object representing a numeric value of argument
	 */
	private Number prepareArgument(Object value) {
		Number num = null;
		if (value == null) {
			num = Integer.valueOf(0);
		} else {
			Class<?> type = value.getClass();
			if (type == String.class) {
				num = convertStringToNumber((String) value);

			} else if (type == Integer.class || type == Double.class) {
				num = (Number) value;
			}
		}

		return num;
	}
	
	@Override
	public String toString() {
		if(value == null) {
			return("null");
		}
		
		return value.toString();
	}

	/**
	 * This method is used internally for checking if the value to be wrapped is
	 * valid (type {@link String}, {@link Double}, {@link Integer} or
	 * <code>null</code> value).
	 * 
	 * @param value
	 *            Value to be checked
	 * @return <code>true</code> if value is of valid type, <code>false</code>
	 *         otherwise
	 */
	private boolean validType(Object value) {
		if (value == null) {
			return true;
		} else if (value instanceof String) {
			return true;
		} else if (value instanceof Double) {
			return true;
		} else if (value instanceof Integer) {
			return true;
		}

		return false;
	}

	/**
	 * This method is used internally for converting String literal to numeric type.
	 * 
	 * @param s
	 *            String literal to be converted
	 * @return Parsed number
	 * @throws IllegalArgumentException
	 *             if literal can't be parsed into numeric type
	 */
	private static Number convertStringToNumber(String s) {
		boolean formated = false;
		Number ret = null;
		
		try {
			ret = Integer.valueOf(Integer.parseInt(s));
			formated = true;
		} catch(NumberFormatException ignrbl) {}
		
		if(formated) {
			return ret;
		}
		
		try {
			ret = Double.valueOf(Double.parseDouble(s));
		} catch(NumberFormatException ex) {
			throw new IllegalArgumentException("String literal can't be parsed into numeric type.");
		}
		
		return ret;
	}

	/** Error message*/
	private static final String ERROR_MESSAGE = "Operation arguments must be of type String, Integer, Double or null value";

}
