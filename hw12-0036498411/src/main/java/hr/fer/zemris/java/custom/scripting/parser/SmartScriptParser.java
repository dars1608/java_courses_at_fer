package hr.fer.zemris.java.custom.scripting.parser;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.LexerException;
import hr.fer.zemris.java.custom.scripting.lexer.SSTokenType;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.zemris.java.custom.scripting.lexer.SmartScriptToken;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

/**
 * This class is used for parsing document type "Smart Script", and creating
 * document tree.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartScriptParser {
	
	/** Stack used for generating document tree*/
	private Stack<Node> stack;
	/** Lexer used for generating tokens*/
	private SmartScriptLexer lexer;
	/** Root {@link Node} of a document*/
	private Node root;
	/** {@link Node} on top of the stack*/
	private Node onStack;

	/**
	 * 
	 * Constructor method for {@link SmartScriptParser}.
	 * 
	 * @param document
	 *            String to be parsed.
	 */
	public SmartScriptParser(String document) {
		if (document == null) {
			throw new NullPointerException("String reference can't be null");
		}

		this.lexer = new SmartScriptLexer(document);
	}

	/**
	 * This method initiates parsing of document if document isn't already passed.
	 * Returns root {@link DocumentNode}.
	 * 
	 * @return Root node.
	 */
	public DocumentNode getDocumentNode() {
		if (root == null) {
			stack = new Stack<Node>();
			root = new DocumentNode();
			stack.push(root);
			parseDocument();
		}
		return (DocumentNode) root;
	}

	/**
	 * This method constructs text form given {@link DocumentNode}.
	 * 
	 * @param node
	 *            Root node.
	 * @return Reconstructed text.
	 */
	public static String createOriginalDocumentBody(Node node) {
		StringBuilder sb = new StringBuilder("");

		if (node instanceof DocumentNode) {
			int size = node.numberOfChildren();
			for (int i = 0; i < size; i++) {
				sb.append(createOriginalDocumentBody(node.getChild(i)));
			}

		} else if (node instanceof TextNode) {
			sb.append(((TextNode) node).getAsText());

		} else if (node instanceof EchoNode) {
			sb.append("{$= ");

			Element[] array = ((EchoNode) node).getElements();
			for (Element e : array) {
				sb.append(e.asText());
				sb.append(" ");
			}

			sb.append("$}");

		} else if (node instanceof ForLoopNode) {
			ForLoopNode temp = (ForLoopNode) node;

			sb.append("{$ FOR ");
			sb.append(temp.getVariable().asText());
			sb.append(" ");
			sb.append(temp.getStartExpression().asText());
			sb.append(" ");
			sb.append(temp.getEndExpression().asText());
			sb.append(" ");
			if (temp.getStepExpression() != null) {
				sb.append(temp.getStepExpression().asText());
			}
			sb.append("$}");

			int size = node.numberOfChildren();
			for (int i = 0; i < size; i++) {
				sb.append(createOriginalDocumentBody(node.getChild(i)));
			}

			sb.append("{$END$}");
		}

		return sb.toString();
	}

	/*----------------------------------Parsing methods--------------------------------------*/

	/**
	 * Base method for parsing document.
	 */
	private void parseDocument() {
		SmartScriptToken currentToken;
		do {
			currentToken = getNextToken();
			onStack = (Node) stack.peek();

			if (currentToken.getType() == SSTokenType.TEXT) {
				onStack.addChildNode(new TextNode((String) currentToken.getValue()));

			} else if (currentToken.getType() == SSTokenType.BEGIN_TAG) {
				currentToken = getNextToken();

				if (currentToken.getType() == SSTokenType.EQUALS) {
					parseEcho();

				} else if (currentToken.getType() == SSTokenType.FOR) {
					parseFor();

				} else if (currentToken.getType() == SSTokenType.END) {

					try {
						stack.pop();
					} catch (EmptyStackException e) {
						throw new SmartScriptParserException("Invalid input. Unexpected END tag.");
					}

					if (stack.isEmpty()) {
						throw new SmartScriptParserException("Invalid input. For loop body isn't closed.");
					}

				} else {
					throw new SmartScriptParserException("Document can't be parsed.");
				}
			}
		} while (currentToken.getType() != SSTokenType.EOF);

		if (stack.size() != 1) {
			throw new SmartScriptParserException("Invalid input. For loop body isn't closed.");
		}
	}

	/**
	 * This method is used internally for parsing echo node.
	 */
	private void parseEcho() {
		List<Element> elements = new ArrayList<>();
		SmartScriptToken temp = null;
		SSTokenType type = null;
		String value;

		while (true) {
			temp = getNextToken();
			type = temp.getType();
			value = (String) temp.getValue();

			if (type == SSTokenType.END_TAG) {
				break;
			}

			switch (type) {
			case INTEGER:
				elements.add(new ElementConstantInteger(Integer.parseInt(value)));
				break;
			case DOUBLE:
				elements.add(new ElementConstantDouble(Double.parseDouble(value)));
				break;
			case VARIABLE:
				elements.add(new ElementVariable(value));
				break;
			case FUNCTION:
				elements.add(new ElementFunction(value));
				break;
			case STRING:
				elements.add(new ElementString(value));
				break;
			case OPERATOR:
				elements.add(new ElementOperator(value));
				break;
			default:
				throw new SmartScriptParserException("Document can't be parsed. Invalid echo tag in document.");
			}
		}

		onStack.addChildNode(new EchoNode(toElementArray(elements.toArray(), elements.size())));
	}

	/**
	 * This method is used for parsing for-loop node.
	 */
	private void parseFor() {
		SmartScriptToken variable = getNextToken();
		SmartScriptToken firstArg = getNextToken();
		SmartScriptToken secondArg = getNextToken();
		SmartScriptToken thirdArg = getNextToken();

		if (!validateFor(variable, firstArg, secondArg)) {
			throw new SmartScriptParserException("Invalid input. Wrong elements in for-loop.");
		}

		if (thirdArg.getType() == SSTokenType.END_TAG) {
			stack.push(new ForLoopNode((ElementVariable) createArgument(variable), createArgument(firstArg),
					createArgument(secondArg)));
			onStack.addChildNode((Node) stack.peek());
			onStack = (Node) stack.peek();
			return;
		} else if (isArgument(thirdArg.getType())) {
			SmartScriptToken temp = getNextToken();
			if (temp.getType() == SSTokenType.END_TAG) {
				stack.push(new ForLoopNode((ElementVariable) createArgument(variable), createArgument(firstArg),
						createArgument(secondArg), createArgument(thirdArg)));
				onStack.addChildNode((Node) stack.peek());
				onStack = (Node) stack.peek();
				return;
			}

		}

		throw new SmartScriptParserException("Invalid input. Wrong number of arguments in for-loop tag");

	}

	/*-----------------------------------Helper methods-------------------------------------------------*/

	/**
	 * This method is used internally. It checks if for-loop tag arguments are
	 * valid.
	 * 
	 * @param variable
	 *            for-loop counter.
	 * @param firstArg
	 *            startExpression.
	 * @param secondArg
	 *            endExpression.
	 * @return <code>true</code> if they are valid, <code>false</code> otherwise.
	 */
	private static boolean validateFor(SmartScriptToken variable, SmartScriptToken firstArg,
			SmartScriptToken secondArg) {
		if (variable.getType() != SSTokenType.VARIABLE) {
			return false;
		}
		if (!isArgument(firstArg.getType())) {
			return false;
		}
		if (!isArgument(secondArg.getType())) {
			return false;
		}
		return true;
	}

	/**
	 * This method is used internally for checking if given {@link SSTokenType} is
	 * valid in for-loop tag.
	 * 
	 * @param arg
	 * @return
	 */
	private static boolean isArgument(SSTokenType arg) {
		switch (arg) {
		case INTEGER:
			return true;
		case DOUBLE:
			return true;
		case STRING:
			return true;
		case VARIABLE:
			return true;
		default:
			return false;
		}
	}

	/**
	 * This method creates an {@link Element} object based on argument properties.
	 * 
	 * @param arg
	 *            {@link SmartScriptToken}.
	 * @return {@link Element}.
	 */
	private static Element createArgument(SmartScriptToken arg) {
		String value = (String) arg.getValue();
		SSTokenType type = arg.getType();

		switch (type) {
		case INTEGER:
			return new ElementConstantInteger(Integer.parseInt(value));
		case DOUBLE:
			return new ElementConstantDouble(Double.parseDouble(value));
		case STRING:
			return new ElementString(value);
		case VARIABLE:
			return new ElementVariable(value);
		default:
			return null;
		}
	}

	/**
	 * This method is used for getting the next token from <code>lexer</code>.
	 * 
	 * @return Next {@link SmartScriptToken}.
	 * @throws SmartScriptParserException
	 *             if {@linkplain LexerException} occurs.
	 */
	private SmartScriptToken getNextToken() {
		try {
			return lexer.nextToken();
		} catch (LexerException ex) {
			throw ex;
		}
	}

	/**
	 * Method used for converting {@link Object} array to {@link Element} array.
	 * 
	 * @param array
	 *            {@link Object} array.
	 * @return{@link Element} array.
	 */
	private static Element[] toElementArray(Object[] array, int size) {
		Element[] retArr = new Element[size];

		for (int i = 0; i < size; i++) {
			if (array[i] instanceof Element) {
				retArr[i] = (Element) array[i];
			}
		}
		return retArr;
	}
}
