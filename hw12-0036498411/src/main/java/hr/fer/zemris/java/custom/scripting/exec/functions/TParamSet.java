package hr.fer.zemris.java.custom.scripting.exec.functions;

import java.util.Stack;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This class represents an implementation of {@link SSFunction} which stores a
 * value into requestContext temporary parameters map. Conceptually, equals to:
 * name = pop(), value = pop(), reqCtx.setPerParam(name, value).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TParamSet implements SSFunction {

	@Override
	public void accept(Stack<Object> temp, RequestContext requestContext) {
		String name = temp.pop().toString();
		String value = temp.pop().toString();
		
		requestContext.setTemporaryParameter(name, value);
	}

}
