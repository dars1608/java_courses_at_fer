package hr.fer.zemris.java.custom.scripting.lexer;

import static java.lang.Character.isDigit;
import static java.lang.Character.isLetter;


/**
 * This class defines lexer for document type "Smart Script".
 * {@link SmartScriptParser} uses this lexer for generating tokens.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartScriptLexer {
	
	/*--------------------------defined constants----------------------------*/

	/** Character defining begin of the tag*/
	private static final char BEGIN_TAG = '{';
	/** Character defining end of the tag*/
	private static final char END_TAG = '}';
	/** Value={@value #DOLLAR}*/
	private static final char DOLLAR = '$';
	/** Value={@value #EQUALS}*/
	private static final char EQUALS = '=';
	/** Value={@value #MINUS}*/
	private static final char MINUS = '-';
	/** Value={@value #PLUS}*/
	private static final char PLUS = '+';
	/** Value={@value #DOT}*/
	private static final char DOT = '.';
	/** Value={@value #UNDERSCORE}*/
	private static final char UNDERSCORE = '_';
	/** Value={@value #ESCAPE_SIGN}*/
	private static final char ESCAPE_SIGN = '\\';
	/** Value={@value #QUOTATION_SIGN}*/
	private static final char QUOTATION_SIGN = '\"';
	/** Value={@value #AT}*/
	private static final char AT = '@';
	/** Value={@value #END}*/
	private static final String END = "end";
	/** Value={@value #FOR}*/
	private static final String FOR = "for";
	/** Value={@value #BLANKS}*/
	private static final String BLANKS = "\\n\\r\\t ";
	/** Value={@value #OPERATORS}*/
	private static final String OPERATORS = "+-*/%";
	/** Value={@value #KEY_SYMBOLS}*/
	private static final String KEY_SYMBOLS = "{&$=";

	/*-------------------------------------------------------------------------*/
	
	/** Array used for storing input characters. */
	private char[] data;
	/** Current {@link Token}. */
	private SmartScriptToken token;
	/** Index of first unprocessed character in array <code>data</code>. */
	private int currentIndex;
	/** Current lexer state. */
	private SSLexerState state = SSLexerState.TEXT;

	/**
	 * Constructor method for class {@link SmartScriptLexer}. Given input text will
	 * be tokenized.
	 * 
	 * @param text
	 *            Text input.
	 * @throws NullPointerException
	 *             if given string reference is <code>null</code>.
	 */
	public SmartScriptLexer(String text) {
		if (text == null) {
			throw new NullPointerException("Text parameter cannot be null");
		}
		data = text.toCharArray();
	}

	/**
	 * Method used for generating next token from input data. Returns generated
	 * token and sets current token.
	 * 
	 * @return Generated token.
	 * @throws LexerException
	 *             if error while generating token occurs.
	 */
	public SmartScriptToken nextToken() {
		generateToken();
		return token;
	}

	/**
	 * Getter method for current token.
	 * 
	 * @return Last generated token.
	 */
	public SmartScriptToken getToken() {
		return token;
	}

	/**
	 * Method used internally for generating token and setting <code>token</code> to
	 * generated value.
	 * 
	 * @throws LexerException
	 *             if error occurs while generating token.
	 */
	private void generateToken() {
		if (token != null && token.getType() == SSTokenType.EOF) {
			throw new LexerException("No tokens available.");
		}
		if (invalidIndex()) {
			token = new SmartScriptToken(SSTokenType.EOF, null);
			return;
		}

		char c = data[currentIndex];
		StringBuilder sb = new StringBuilder("");

		if (state == SSLexerState.TEXT && c != BEGIN_TAG) {
			extractText(c, sb);
			return;
		} else {
			setState(SSLexerState.TAG);
			skipBlankSpaces();

			if (invalidIndex()) {
				token = new SmartScriptToken(SSTokenType.EOF, null);
				return;
			}

			c = data[currentIndex];
			if (isOperator(c)) {
				extractOperator(c, sb);
				return;
			}
			if (isKeySymbol(c)) {
				extractKeySymbol(c, sb);
				currentIndex++;
				return;
			}
			if (c == QUOTATION_SIGN) {
				extractString(sb);
				return;
			}
			if (isLetter(c)) {
				sb.append(c);
				extractVariable(sb);
				return;
			}
			if (isDigit(c)) {
				sb.append(c);
				extractNumber(sb);
				return;
			}
			if (c == AT) {
				sb.append(c);
				extractFunction(sb);
				return;
			}
		}
		throw new LexerException("Error at index: " + currentIndex);
	}

	/*--------------------------extraction methods------------------------------*/

	/**
	 * This method is used for generating token type
	 * <code>SSTokenType.FUNCTION</code>.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 */
	private void extractFunction(StringBuilder sb) {
		extractVariable(sb);
		token = new SmartScriptToken(SSTokenType.FUNCTION, token.getValue());

	}

	/**
	 * This method is used for generating token type
	 * <code>SSTokenType.INTEGER</code> or <code>SSTokenType.DOUBLE</code>.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 */
	private void extractNumber(StringBuilder sb) {
		char c = 0;
		while (true) {
			currentIndex++;
			if (invalidIndex()) {
				break;
			}
			c = data[currentIndex];
			if (!isDigit(c)) {
				break;
			}
			sb.append(c);
		}

		if (c == DOT) {
			sb.append(c);
			extractDouble(sb);
			return;
		}

		token = new SmartScriptToken(SSTokenType.INTEGER, sb.toString());
	}

	/**
	 * This method is used for generating token type
	 * <code>SSTokenType.DOUBLE</code>.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 */
	private void extractDouble(StringBuilder sb) {
		char c = 0;
		while (true) {
			currentIndex++;
			if (invalidIndex()) {
				break;
			}
			c = data[currentIndex];
			if (!isDigit(c)) {
				break;
			}
			sb.append(c);
		}

		token = new SmartScriptToken(SSTokenType.DOUBLE, sb.toString());

	}

	/**
	 * This method is used for generating token type
	 * <code>SSTokenType.VARIABLE</code>.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 */
	private void extractVariable(StringBuilder sb) {
		char c = 0;
		while (true) {
			currentIndex++;
			if (invalidIndex()) {
				break;
			}
			c = data[currentIndex];
			if (!(isLetter(c) || isDigit(c) || c == UNDERSCORE)) {
				break;
			}
			sb.append(c);
		}

		String value = sb.toString();
		if (value.toLowerCase().equals(FOR)) {
			token = new SmartScriptToken(SSTokenType.FOR, null);
			return;
		} else if (value.toLowerCase().equals(END)) {
			token = new SmartScriptToken(SSTokenType.END, null);
			return;
		}

		token = new SmartScriptToken(SSTokenType.VARIABLE, value);
	}

	/**
	 * This method is used internally. It checks if lexer came to the key symbol
	 * ('$', '=', '{' or '}'). It sets current token to one which matches given
	 * symbol.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 * @return <code>true</code> if <code>currentIndex</code> points to key symbol,
	 *         <code>false</code> otherwise.
	 */
	private void extractKeySymbol(char c, StringBuilder sb) {

		switch (c) {
		case BEGIN_TAG:
			currentIndex++;
			if (invalidIndex() || data[currentIndex] != DOLLAR) {
				throw new LexerException("Invalid input. Tag notation error.");
			}
			token = new SmartScriptToken(SSTokenType.BEGIN_TAG, null);
			break;
		case DOLLAR:
			currentIndex++;
			if (invalidIndex() || data[currentIndex] != END_TAG) {
				throw new LexerException("Invalid input. Tag notation error.");
			}
			token = new SmartScriptToken(SSTokenType.END_TAG, null);
			setState(SSLexerState.TEXT);
			break;
		case EQUALS:
			token = new SmartScriptToken(SSTokenType.EQUALS, null);
			break;
		}
	}

	/**
	 * This method is used internally for generating token type
	 * <code>TokenType.TEXT</code> and sets current token value to generated one.
	 * 
	 * @param c
	 *            Initial character
	 * @throws LexerException
	 *             if input is invalid.
	 */
	private void extractText(char c, StringBuilder sb) {

		while (true) {
			c = data[currentIndex];
			
			if(c == BEGIN_TAG) {
				break;
			}
			if (c == ESCAPE_SIGN) {
				escapingText(sb);
				continue;
			}
			
			sb.append(c);
			currentIndex++;
			if (invalidIndex())
				break;
			
		}

		setState(SSLexerState.TAG);

		if (c == 0) {
			// if text starts with tag
			return;
		}

		token = new SmartScriptToken(SSTokenType.TEXT, sb.toString());
	}

	/**
	 * This method is used internally for generating operator type
	 * <code>SSTokenType.OPERATOR</code> and it sets current token value to
	 * generated one.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 */
	private void extractOperator(char c, StringBuilder sb) {
		if(currentIndex+1<=data.length) {
			char temp = data[currentIndex + 1];
			if (c == MINUS && isDigit(temp) || c == PLUS && isDigit(temp)) {
				extractNumber(sb.append(c));
				return;
			}
		}
		token = new SmartScriptToken(SSTokenType.OPERATOR, Character.toString(c));
		currentIndex++;
	}

	/**
	 * This method is used internally for generating token type string and it sets
	 * current token value.
	 * 
	 * @param c
	 *            Initial character
	 * @param sb
	 *            Current {@link StringBuilder}
	 * @throws LexerException
	 *             if string element has no ending quotation sign.
	 */
	private void extractString(StringBuilder sb) {
		char c = 0;
		while (true) {
			currentIndex++;
			if (currentIndex >= data.length) {
				throw new LexerException(
						"Invalid input. String element has no ending quotation sign");
			}
			c = data[currentIndex];
			if (c == ESCAPE_SIGN) {
				escapingString(sb);
				continue;
			}
			if (c == QUOTATION_SIGN) { //we only want to extract string value
				currentIndex++;
				break;
			}
			sb.append(c);
		}

		token = new SmartScriptToken(SSTokenType.STRING, sb.toString());
	}

	/*--------------------------helper methods----------------------------------*/

	/**
	 * Helper method for escaping.
	 * 
	 * @param condition
	 *            {@link CharTester} which defines what characters can be
	 *            alternatively interpreted.
	 * @return Escaped character.
	 * 
	 */
	private void escapingString(StringBuilder sb) {
		currentIndex++;
		if (invalidIndex()) {
			throw new LexerException("Invalid input. Text cannot be tokenized");
		}

		char c = data[currentIndex];

		if (c == ESCAPE_SIGN || c == QUOTATION_SIGN) {
			sb.append(c);
		} else if (c == 'n') {
			sb.append('\n');
		} else if (c == 'r') {
			sb.append('\r');
		} else if (c == 't') {
			sb.append('\t');
		} else {
			throw new LexerException(
					"Invalid input. Wrong escaping. Text cannot be tokenized. Is \\" + c);
		}
	}

	private void escapingText(StringBuilder sb) {
		currentIndex++;
		if (invalidIndex()) {
			throw new LexerException("Invalid input. Text cannot be tokenized");
		}

		char c = data[currentIndex];
		
		if (c == ESCAPE_SIGN) {
			sb.append("\\\\");
		} else if (c == BEGIN_TAG) {
			sb.append("\\{");
		} else {
			throw new LexerException(
					"Invalid input. Wrong escaping. Text cannot be tokenized. Is \\" + c);
		}
		
		currentIndex++;

	}

	/**
	 * This method is used internally for skipping blank spaces stored in
	 * <code>data</code> field.
	 */
	private void skipBlankSpaces() {
		while (currentIndex < data.length) {
			char currentChar = data[currentIndex];
			if (!isBlank(currentChar)) {
				break;
			}
			currentIndex++;
		}
	}

	/**
	 * Checks if current index exceeds the dimension of <code>data</code> field.
	 * 
	 * @return <code>true</code> if exceeds, <code>false</code> otherwise.
	 */
	private boolean invalidIndex() {
		return currentIndex >= data.length;
	}

	/**
	 * Setter method for current lexer state.
	 * 
	 * @param state
	 *            {@link SSLexerState}
	 * @throws NullPointerException
	 *             if given state is <code>null</code>.
	 */
	private void setState(SSLexerState state) {
		if (state == null) {
			throw new NullPointerException("State cannot be null");
		}
		this.state = state;
	}

	/*-------------------method used for checking conditions--------------------*/

	/**
	 * This method is used internally for checking if given character represents
	 * blank space.
	 * 
	 * @param c
	 *            Checked character
	 * @return <code>true</code> if it is blank, <code>false</code> otherwise.
	 */
	private static boolean isBlank(char c) {
		return BLANKS.contains(Character.toString(c)) || Character.isWhitespace(c);
	}

	/**
	 * This method is used internally to determine whether the character is valid
	 * operator.
	 * 
	 * @param c
	 *            Input character.
	 * @return <code>true</code> if character is valid operator, <code>false</code>
	 *         otherwise.
	 */
	private static boolean isOperator(char c) {
		return OPERATORS.contains(Character.toString(c));
	}

	/**
	 * This method is used internally to determine whether the character is valid
	 * key symbol.
	 * 
	 * @param c
	 *            Input character.
	 * @return <code>true</code> if character is valid operator, <code>false</code>
	 *         otherwise.
	 */
	private static boolean isKeySymbol(char c) {
		return KEY_SYMBOLS.contains(Character.toString(c));
	}
	
	/**
	 * Enumeration which defines state in which lexer works.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public enum SSLexerState {
		/** State in which lexer reads text. */
		TEXT,
		/** State in which lexer reads tag. */
		TAG;
	}
}
