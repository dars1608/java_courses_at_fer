package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * This class models a simple
 * <a href="https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol">HTTP</a>
 * request context
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class RequestContext {

	/** Default encoding: value = {@value #DEFAULT_ENCODING} */
	private static final String DEFAULT_ENCODING = "UTF-8";
	/** Default status code: value = {@value #DEFAULT_STATUS_CODE} */
	private static final int DEFAULT_STATUS_CODE = 200;
	/** Default status text: value = {@value #DEFAULT_STATUS_TEXT} */
	private static final String DEFAULT_STATUS_TEXT = "OK";
	/** Default mime type: value = {@value #DEFAULT_MIME_TYPE} */
	private static final String DEFAULT_MIME_TYPE = "text/html";

	/** Used output stream */
	private OutputStream outputStream;
	/** Used charset */
	private Charset charset;
	/** Used encoding */
	private String encoding = DEFAULT_ENCODING;
	/** Current status code */
	private int statusCode = DEFAULT_STATUS_CODE;
	/** Current status text */
	private String statusText = DEFAULT_STATUS_TEXT;
	/** Current mime type */
	private String mimeType = DEFAULT_MIME_TYPE;
	/** Content length */
	private Long contentLength = null;

	/** Map of current parameters (read-only) */
	private Map<String, String> parameters;
	/** Map of temporary parameters */
	private Map<String, String> temporaryParameters;
	/** Map ot persistent parameters */
	private Map<String, String> persistentParameters;
	/** List of output cookies */
	private List<RCCookie> outputCookies;
	/** Flag determining weather the header is generated */
	private boolean headerGenerated = false;
	/** Request dispatcher */
	private final IDispatcher dispatcher;

	/**
	 * Constructor method for class {@link RequestContext}. If any of arguments is
	 * null, it is treated as empty collection.
	 * 
	 * @param outputStream
	 *            output stream to be used
	 * @param parameters
	 *            map of parameters
	 * @param persistentParameters
	 *            map o persistent parameters
	 * @param outputCookies
	 *            list of output cookies
	 * @throws NullPointerException
	 *             if the argument {@code outputStream} is {@code null}
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies) {
		this(outputStream, parameters, persistentParameters, outputCookies, null, null);
	}

	/**
	 * Constructor method for class {@link RequestContext}. If any of arguments is
	 * null, it is treated as empty collection.
	 * 
	 * @param outputStream
	 *            output stream to be used
	 * @param parameters
	 *            map of parameters
	 * @param persistentParameters
	 *            map o persistent parameters
	 * @param outputCookies
	 *            list of output cookies
	 * @param temporaryParameters
	 * 			  list of temporary parameters
	 * @param dispatcher
	 * 			  request dispatcher
	 * @throws NullPointerException
	 *             if the argument {@code outputStream} is {@code null}
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies,
			Map<String, String> temporaryParameters, IDispatcher dispatcher) {

		this.outputStream = Objects.requireNonNull(outputStream);
		this.parameters = parameters == null ? new HashMap<>() : parameters;
		this.persistentParameters = parameters == null ? new HashMap<>() : persistentParameters;
		this.temporaryParameters = temporaryParameters == null ? new HashMap<>() : temporaryParameters;
		this.outputCookies = outputCookies == null ? new ArrayList<>() : outputCookies;
		this.dispatcher = dispatcher;
	}

	/**
	 * Setter method for the {@code encoding}.
	 * 
	 * @param encoding
	 *            the encoding to set
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 * @throws RuntimeException
	 *             if the header is already generated
	 */
	public void setEncoding(String encoding) {
		if (headerGenerated) {
			throw new RuntimeException("Value can't be set because the header is allready generated.");
		}

		this.encoding = Objects.requireNonNull(encoding);
	}

	/**
	 * Setter method for the {@code statusCode}.
	 * 
	 * @param statusCode
	 *            the statusCode to set
	 * @throws RuntimeException
	 *             if the header is already generated
	 */
	public void setStatusCode(int statusCode) {
		if (headerGenerated) {
			throw new RuntimeException("Value can't be set because the header is allready generated.");
		}

		this.statusCode = statusCode;
	}

	/**
	 * Setter method for the {@code statusText}. If argument is {@code null}, the
	 * statusText is set to empty string.
	 * 
	 * @param statusText
	 *            the statusText to set
	 * @throws RuntimeException
	 *             if the header is already generated
	 */
	public void setStatusText(String statusText) {
		if (headerGenerated) {
			throw new RuntimeException("Value can't be set because the header is allready generated.");
		}

		this.statusText = statusText == null ? "" : statusText;
	}

	/**
	 * Setter method for the {@code mimeType}. If argument is {@code null}, the
	 * mimeType is set to empty string.
	 * 
	 * @param mimeType
	 *            the mimeType to set
	 * @throws RuntimeException
	 *             if the header is already generated
	 */
	public void setMimeType(String mimeType) {
		if (headerGenerated) {
			throw new RuntimeException("Value can't be set because the header is allready generated.");
		}

		this.mimeType = mimeType == null ? "" : mimeType;
	}

	/**
	 * Setter method for property {@code contentLength}.
	 * 
	 * @param contentLength
	 *            content length
	 */
	public void setContentLength(long contentLength) {
		if (headerGenerated) {
			throw new RuntimeException("Value can't be set because the header is allready generated.");
		}

		this.contentLength = contentLength;
	}

	/**
	 * This method adds the {@link RCCookie} to internal list of cookies.
	 * 
	 * @param outputCookie
	 *            the outputCookie to be added
	 * @throws RuntimeException
	 *             if the header is already generated
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public void addRCCookie(RCCookie cookie) {
		if (headerGenerated) {
			throw new RuntimeException("Value can't be set because the header is allready generated.");
		}

		outputCookies.add(Objects.requireNonNull(cookie));
	}

	/**
	 * This method retrieves a value from parameters map.
	 * 
	 * @param name
	 *            key under which the desired value is stored
	 * @return value at given key (or {@code null} if no association exists)
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public String getParameter(String name) {
		return parameters.get(Objects.requireNonNull(name));
	}

	/**
	 * This method retrieves names of all parameters in parameters map (<b>note:</b>
	 * this set is be read-only).
	 *
	 * @return set of all parameter names stored in parameters map
	 */
	public Set<String> getParameterNames() {
		return Collections.unmodifiableSet(parameters.keySet());
	}

	/**
	 * This method retrieves a value from persistentParameters map.
	 * 
	 * @param name
	 *            key under which the desired value is stored
	 * @return value at given key (or {@code null} if no association exists)
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters.get(Objects.requireNonNull(name));
	}

	/**
	 * This method retrieves names of all parameters in persistent parameters map
	 * (<b>note:</b> this set must be read-only).
	 * 
	 * @return set of all persistent parameter names
	 */
	public Set<String> getPersistentParameterNames() {
		return Collections.unmodifiableSet(persistentParameters.keySet());
	}
	
	/**
	 * Getter method for the property {@code dispatcher}.
	 * 
	 * @return the dispatcher
	 */
	public IDispatcher getDispatcher() {
		return dispatcher;
	}

	/**
	 * This method stores a value to persistentParameters map.
	 * 
	 * @param name
	 *            key under which the value is stored
	 * @param value
	 *            value to be stored
	 * @throws NullPointerException
	 *             if any of the arguments is {@code null}
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(Objects.requireNonNull(name), Objects.requireNonNull(value));
	}

	/**
	 * This method removes a value from persistentParameters map.
	 * 
	 * @param name
	 *            key under which the value is stored
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public void removePersistentParameter(String name) {
		persistentParameters.remove(Objects.requireNonNull(name));
	}

	/**
	 * This method retrieves a value from temporaryParameters map.
	 * 
	 * @param name
	 *            key under which the value is stored
	 * @return stored value (or {@code null} if no association exists)
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public String getTemporaryParameter(String name) {
		return temporaryParameters.get(Objects.requireNonNull(name));
	}

	/**
	 * This method retrieves names of all parameters in temporary parameters map
	 * (<b>note:</b> this set must be read-only).
	 * 
	 * @return set of all temporary parameter names
	 */
	public Set<String> getTemporaryParameterNames() {
		return Collections.unmodifiableSet(temporaryParameters.keySet());
	}

	/**
	 * This method stores a value to temporaryParameters map
	 * 
	 * @param name
	 *            key under which the value is stored
	 * @return stored value (or {@code null} if no association exists)
	 * @throws NullPointerException
	 *             if any of the argument is {@code null}
	 */
	public void setTemporaryParameter(String name, String value) {
		temporaryParameters.put(Objects.requireNonNull(name), Objects.requireNonNull(value));
	}

	/**
	 * This method removes a value from temporaryParameters map.
	 * 
	 * @param name
	 *            key under which the value is stored
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public void removeTemporaryParameter(String name) {
		temporaryParameters.remove(Objects.requireNonNull(name));
	}

	/**
	 * This method writes the data into output stream that was given to
	 * {@link RequestContext} in constructor.
	 * 
	 * @param data
	 *            bytes to be written
	 * @return {@code RequestContext} described by this object
	 * @throws IOException
	 *             if writing fails
	 */
	public RequestContext write(byte[] data) throws IOException {
		Objects.requireNonNull(data);

		if (!headerGenerated) {
			generateHeader();
		}

		outputStream.write(data);
		return this;
	}

	/**
	 * This method converts {@code String} argument into bytes with charset which is
	 * set and then writes the data into output stream that was given to
	 * {@link RequestContext} in constructor.
	 * 
	 * @param text
	 *            string to be written
	 * @return {@code RequestContext} described by this object
	 * @throws IOException
	 *             if writing fails
	 */
	public RequestContext write(String text) throws IOException {
		Objects.requireNonNull(text);

		if (!headerGenerated) {
			generateHeader();
		}

		byte[] data = text.getBytes(charset);
		outputStream.write(data);

		return this;
	}

	/**
	 * This method writes the data of given offset and length into output stream
	 * that was given to {@link RequestContext} in constructor.
	 * 
	 * @param data
	 *            byte array
	 * @param offset
	 *            offset from which the data is read
	 * @param len
	 *            length of data to be read
	 * @return {@link RequestContext} object
	 * @throws IOException
	 *             if error occurs
	 */
	public RequestContext write(byte[] data, int offset, int len) throws IOException {
		Objects.requireNonNull(data);
		return write(Arrays.copyOfRange(data, offset, len));
	}

	/**
	 * This method is used for generating and writing the HTTP heading based on
	 * predefined informations.
	 * 
	 * @throws IOException
	 *             if writing fails
	 */
	private void generateHeader() throws IOException {
		headerGenerated = true;
		charset = Charset.forName(encoding);

		StringBuilder sb = new StringBuilder();
		sb.append(String.format("HTTP/1.1 %d %s\r\n", statusCode, statusText));
		sb.append(String.format("Content-Type: %s", mimeType));

		if (mimeType.startsWith("text/")) {
			sb.append(String.format("; charset=%s", encoding));
		}
		sb.append("\r\n");
		
		if (contentLength != null) {
			sb.append(String.format("Content-Length: %d", contentLength.longValue()));
			sb.append("\r\n");
		}

		outputCookies.forEach(c -> sb.append(String.format("Set-Cookie: %s\r\n", c.toString())));
		sb.append("\r\n");

		byte[] data = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		outputStream.write(data);

	}

	/**
	 * This class models a
	 * <a href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a> used in
	 * {@link RequestContext}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public static class RCCookie {
		/** Cookie name */
		private final String name;
		/** Cookie value */
		private final String value;
		/** Cookie domain */
		private final String domain;
		/** Cookie path */
		private final String path;
		/** Cookie max age */
		private final Integer maxAge;

		/**
		 * Constructor method for class {@link RCCookie}.
		 * 
		 * @param name
		 *            cookie name
		 * @param value
		 *            cookie value
		 * @param domain
		 *            cookie domain
		 * @param path
		 *            cookie path
		 * @param maxAge
		 *            cookies max age
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}

		/**
		 * Getter method for the {@code name}.
		 * 
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Getter method for the {@code value}.
		 * 
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Getter method for the {@code domain}.
		 * 
		 * @return the domain
		 */
		public String getDomain() {
			return domain;
		}

		/**
		 * Getter method for the {@code path}.
		 * 
		 * @return the path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * Getter method for the {@code maxAge}.
		 * 
		 * @return the maxAge
		 */
		public Integer getMaxAge() {
			return maxAge;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();

			if (name != null && value != null) {
				sb.append(String.format("%s=\"%s\"; ", name, value));
			}

			if (domain != null) {
				sb.append(String.format("Domain=%s; ", domain));
			}

			if (path != null) {
				sb.append(String.format("Path=%s; ", path));
			}

			if (maxAge != null) {
				sb.append(String.format("Max-Age=%s", maxAge.toString()));
			}

			String ret = sb.toString();
			if (ret.endsWith("; ")) {
				ret = ret.substring(0, ret.length() - 2);
			}

			return ret;
		}

	}
}
