/**
 * This package contains demonstration programs for
 * {@link hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.exec.demo;