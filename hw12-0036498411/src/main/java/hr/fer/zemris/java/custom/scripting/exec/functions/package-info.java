/**
 * This package contains
 * {@link hr.fer.zemris.java.custom.scripting.exec.SSFunction functions}
 * supported by
 * {@link hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.exec.functions;