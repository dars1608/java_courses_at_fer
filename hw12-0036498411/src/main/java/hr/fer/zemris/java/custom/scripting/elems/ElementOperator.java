/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class extends {@link Element} class. It defines an operator element.
 * @author Darko Britvec
 * @version 1.0
 */
public class ElementOperator extends Element {
	/** Symbol of this operator */
	private final String symbol;
	
	/**
	 * Constructor method for class {@link ElementOperator}.
	 @param value
	 *            Symbol of operator.
	 */
	public ElementOperator(String symbol) {
		if(symbol == null) {
			throw new IllegalArgumentException("Name parameter cannot be null");
		}
		this.symbol = symbol;
	}

	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.elems.Element#asText()
	 */
	@Override
	public String asText() {
		return symbol;
	}	
}
