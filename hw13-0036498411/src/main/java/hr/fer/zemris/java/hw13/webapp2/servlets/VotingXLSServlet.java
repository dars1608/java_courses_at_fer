package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.hw13.webapp2.servlets.VotingVoteServlet.Result;
import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for generating .xls file based
 * on the provided voting results.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class VotingXLSServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String resultsPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String definitionPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");

		List<Result> results = WebAppUtil.getResults(definitionPath, resultsPath);
		HSSFWorkbook hwb = createXLS(results);

		resp.setHeader("Content-Disposition", "attachment; filename=\"rezultati.xls\"");
		resp.setContentType("application/vnd.ms-excel");
		hwb.write(resp.getOutputStream());
	}

	/**
	 * This method is used internally for generating xls file.
	 * 
	 * @param list
	 *            list of results to be written
	 * @return {@link HSSFWorkbook} object
	 */
	private HSSFWorkbook createXLS(List<Result> list) {
		HSSFWorkbook hwb = new HSSFWorkbook();

		HSSFSheet sheet = hwb.createSheet("Sheet 1");

		HSSFRow rowhead = sheet.createRow((short) 0);
		rowhead.createCell((short) 0).setCellValue("Name");
		rowhead.createCell((short) 1).setCellValue("Result");

		for (int i = 1, len = list.size(); i <= len; i++) {
			Result r = list.get(i - 1);
			HSSFRow row = sheet.createRow((short) i);
			row.createCell((short) 0).setCellValue(r.name);
			row.createCell((short) 1).setCellValue(r.result);
		}

		return hwb;
	}
}
