package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.sun.net.httpserver.HttpServer;

/**
 * This class describes {@link HttpServer} which dynamically creates a Microsoft
 * Excel document with n pages. On page i there must be a table with two
 * columns. The first column should contain integer numbers from a to b. The
 * second column should contain i-th powers of these numbers.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PowersServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer a = null;
		Integer b = null;
		Integer n = null;

		try {
			a = obtainIntParam(req.getParameter("a"));
			b = obtainIntParam(req.getParameter("b"));
			n = obtainIntParam(req.getParameter("n"));
		} catch (NumberFormatException ex) {
			req.setAttribute("errorMessage", "Wrong argument type.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		if (a == null || b == null || n == null) {
			req.setAttribute("errorMessage", "Invalid number of arguments.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		if (!checkParam("a", a, -100, 100, req, resp)) {
			return;
		}

		if (!checkParam("b", b, -100, 100, req, resp)) {
			return;
		}

		if (!checkParam("n", n, 1, 5, req, resp)) {
			return;
		}

		HSSFWorkbook hwb = createXLS(a, b, n);

		resp.setContentType("application/vnd.ms-excel");
		resp.addHeader("Content-Disposition", "inline");
		hwb.write(resp.getOutputStream());
	}

	/**
	 * This method is used internally for generating xls file.
	 * 
	 * @param a
	 *            lower limit
	 * @param b
	 *            upper limit
	 * @param n
	 *            number of sheets
	 * @return {@link HSSFWorkbook} object
	 */
	private HSSFWorkbook createXLS(Integer a, Integer b, Integer n) {
		HSSFWorkbook hwb = new HSSFWorkbook();

		for (int i = 1; i <= n; i++) {
			HSSFSheet sheet = hwb.createSheet("Sheet " + i);

			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell((short) 0).setCellValue("x");
			rowhead.createCell((short) 1).setCellValue("x^2");

			for (int j = 1, x = a; x <= b; j++, x++) {
				HSSFRow row = sheet.createRow((short) j);
				row.createCell((short) 0).setCellValue(Integer.toString(x));
				row.createCell((short) 1).setCellValue(Integer.toString((int) Math.pow(x, i)));
			}
		}

		return hwb;
	}

	/**
	 * This method is used internally for obtaining integer parameter from string.
	 * 
	 * @param parameter
	 *            string representation of parameter
	 * @return integer representation of parameter, {@code null} if argument is null
	 */
	private Integer obtainIntParam(String parameter) {
		if (parameter == null) {
			return null;
		}

		return Integer.valueOf(parameter);
	}

	/**
	 * This method is used internally for checking parameters used for generating
	 * xls document.
	 * 
	 * @param name
	 *            parameter name
	 * @param param
	 *            parameter value
	 * @param min
	 *            lower limit
	 * @param max
	 *            upper limit
	 * @param req
	 *            http request
	 * @param resp
	 *            http response
	 * @return {@code true} in arguments are valid, {@code false} otherwise
	 * @throws ServletException
	 *             ignorable
	 * @throws IOException
	 *             ignorable
	 */
	private boolean checkParam(String name, Integer value, int min, int max, HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {

		if (value < min || value > max) {
			req.setAttribute("errorMessage", String.format("Parameter %s must be in range [-%d,%d].", name, min, max));
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return false;
		}

		return true;
	}
}
