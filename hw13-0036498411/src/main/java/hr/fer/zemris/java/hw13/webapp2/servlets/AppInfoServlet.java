package hr.fer.zemris.java.hw13.webapp2.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * This class describes {@link ServletContextListener} used for tracking how
 * long is this web application running. It stores the starting time of the
 * application (in milliseconds) in servlet context attribute map under the key
 * "startTime".
 *
 */
@WebListener
public class AppInfoServlet implements ServletContextListener {
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		arg0.getServletContext().setAttribute("startTime", System.currentTimeMillis());
	}

}
