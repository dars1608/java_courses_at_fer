package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.webapp2.servlets.VotingVoteServlet.Result;
import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for preparing the voting
 * results which will be shown on screen.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class VotingResultsServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String resultsPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String definitionPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");

		List<Result> results = WebAppUtil.getResults(definitionPath, resultsPath);

		req.setAttribute("results", results);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}
}
