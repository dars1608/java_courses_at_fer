/**
 * This package contains utility classes.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw13.webapp2.util;