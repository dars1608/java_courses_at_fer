package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import hr.fer.zemris.java.hw13.webapp2.servlets.VotingVoteServlet.Result;
import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for preparing the png image
 * which visualizes the voting results (creates simple pie-chart).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class VotingGraphicsServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String resultsPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String definitionPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");

		List<Result> results = WebAppUtil.getResults(definitionPath, resultsPath);
		JFreeChart chart = WebAppUtil.createChart(createDataset(results), "Rezultati glasanja");
		resp.setContentType("image/png");

		int width = WebAppUtil.intFromString(req.getParameter("width"), 500);
		int height = WebAppUtil.intFromString(req.getParameter("height"), 270);

		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, width, height);
	}

	/**
	 * This method is used internally for generating dataset which will be shown on
	 * the pie-chart.
	 * 
	 * @param results
	 *            list of results to be shown
	 * @return {@link DefaultPieDataset} object
	 */
	private PieDataset createDataset(List<Result> results) {
		DefaultPieDataset dataset = new DefaultPieDataset();

		results.forEach(result -> {
			if (result.result != 0) {
				dataset.setValue(result.name, result.result);
			}
		});

		return dataset;
	}
}
