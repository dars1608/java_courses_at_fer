/**
 * This package contains servlets used in webapp2 application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw13.webapp2.servlets;