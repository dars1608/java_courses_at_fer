package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for creating simple pie chart.
 * Information to be shown on chart are predefined (statistics about the usage
 * of certain operation systems).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ReportServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		JFreeChart chart = WebAppUtil.createChart(createDataset(), "OS usage");
		resp.setContentType("image/png");
		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, 500, 270);

	}

	/**
	 * Creates a sample dataset.
	 */
	private PieDataset createDataset() {
		DefaultPieDataset result = new DefaultPieDataset();

		result.setValue("Linux", 29);
		result.setValue("Mac", 20);
		result.setValue("Windows", 51);

		return result;

	}
}
