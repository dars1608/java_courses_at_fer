package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for loading the voting list.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class VotingServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		List<Artist> list = WebAppUtil.readArtists(fileName);

		req.setAttribute("artistsList", list);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}

	/**
	 * JavaBean used for maping artists.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public static class Artist {
		/** Unique id */
		String id;
		/** Name */
		String name;
		/** Link of the song */
		String link;

		/**
		 * Constructor method for class {@link Artist}.
		 * 
		 * @param id
		 *            unique id
		 * @param name
		 *            artists name
		 * @param link
		 *            link of the song
		 */
		public Artist(String id, String name, String link) {
			this.id = id;
			this.name = name;
			this.link = link;
		}

		/**
		 * Getter method for the {@code id}.
		 * 
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Getter method for the {@code name}.
		 * 
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Getter method for the {@code link}.
		 * 
		 * @return the link
		 */
		public String getLink() {
			return link;
		}

	}
}
