package hr.fer.zemris.java.hw13.webapp2.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.PieDataset;

import hr.fer.zemris.java.hw13.webapp2.servlets.VotingServlet.Artist;
import hr.fer.zemris.java.hw13.webapp2.servlets.VotingVoteServlet.Result;

/**
 * This class contains utility methods for webapp2 application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class WebAppUtil {

	/**
	 * This method is used for reading the results from the file and generating a
	 * list of {@link Result} objects.
	 * 
	 * @param filePath
	 *            path of the results file
	 * @param artists
	 *            list of artists
	 * @return list of results
	 * @throws IOException
	 *             if reading fails
	 */
	public static List<Result> readResults(Path filePath, List<Artist> artists) throws IOException {
		List<Result> results = new ArrayList<>();
		BufferedReader reader = Files.newBufferedReader(filePath);

		while (reader.ready()) {
			String parts[] = reader.readLine().split("\t");
			if (parts.length != 2)
				continue;

			String name = null;
			String link = null;
			for (Artist a : artists) {
				if (a.getId().equals(parts[0])) {
					name = a.getName();
					link = a.getLink();
					break;
				}
			}

			if (name == null)
				continue;

			try {
				results.add(new Result(parts[0], name, link, Integer.valueOf(parts[1])));
			} catch (NumberFormatException ignorable) {
			}
		}

		reader.close();
		return results;
	}

	/**
	 * This method is used for creating pie chart.
	 * 
	 * @param dataset
	 *            data to be shown on chart
	 * @param title
	 *            chart title
	 * @return {@link JFreeChart} object
	 */
	public static JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, true, true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);

		return chart;
	}

	/**
	 * This method is used for reading the artists informations from the file and
	 * generating a list of {@link Artist} objects.
	 * 
	 * @param filePath
	 *            path of the results file
	 * @return list of artists
	 * @throws IOException
	 *             if reading fails
	 */
	public static List<Artist> readArtists(String fileName) throws IOException {
		BufferedReader is = Files.newBufferedReader(Paths.get(fileName));
		List<Artist> list = new ArrayList<>();

		while (is.ready()) {
			String[] parts = is.readLine().split("\t");
			list.add(new Artist(parts[0], parts[1], parts[2]));
		}

		return list;

	}

	/**
	 * This method is used obtaining the integer value from string.
	 * 
	 * @param s
	 *            string representation of integer
	 * @param defaultValue
	 *            default value, if parsing fails
	 * @return parsed integer
	 */
	public static Integer intFromString(String s, int defaultValue) {
		if (s != null) {
			try {
				return Integer.valueOf(s);
			} catch (NumberFormatException ignorable) {
				return defaultValue;
			}
		}

		return defaultValue;
	}

	/**
	 * This method is used for getting the voting results. If results aren't
	 * initialized, it will initialize it.
	 * 
	 * @param definitionPath
	 *            string representing path of voting definition file
	 * @param resultsPath
	 *            string representing path of voting results file
	 * @return list of {@link Result} objects
	 * @throws IOException
	 *             if any error while reading occurs
	 */
	public static List<Result> getResults(String definitionPath, String resultsPath) throws IOException {
		Path filePath = Paths.get(resultsPath);

		List<Result> results = null;
		List<Artist> artists = WebAppUtil.readArtists(definitionPath);

		if (!Files.exists(filePath)) {
			results = new ArrayList<>();

			for (Artist a : artists) {
				results.add(new Result(a.getId(), a.getName(), a.getLink(), 0));
			}

			vote(results, null, resultsPath);
		} else {
			results = WebAppUtil.readResults(filePath, artists);
		}

		results.sort((a, b) -> -Integer.compare(a.getResult(), b.getResult()));

		return results;
	}

	/**
	 * This method is used for writing into results file. If vote is null, the new
	 * file is created.
	 * 
	 * @param results
	 *            list of results
	 * @param vote
	 *            id which result should be increased
	 * @param filePath
	 *            path of the results file
	 * @throws IOException
	 *             if writing fails
	 */
	synchronized public static void vote(List<Result> results, String vote, String resultsPath) throws IOException {
		Path filePath = Paths.get(resultsPath);
		Writer writer = Files.newBufferedWriter(filePath,
				vote == null ? StandardOpenOption.CREATE : StandardOpenOption.TRUNCATE_EXISTING);

		for (Result r : results) {
			if (r.getId().equals(vote)) {
				r.setResult(r.getResult() + 1);
			}

			writer.write(String.format("%s\t%d\n", r.getId(), r.getResult()));
		}

		writer.flush();
		writer.close();
	}
}
