package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for saving the vote.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class VotingVoteServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String vote = req.getParameter("id");
		if (vote == null) { // illegal vote, could break the results data
			resp.sendError(400);
			return;
		}
		String resultsPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		String definitionPath = req.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		List<Result> results = WebAppUtil.getResults(definitionPath, resultsPath);

		boolean invalidVote = true;
		for (Result r : results) {
			if (r.id.equals(vote)) {
				invalidVote = false;
				break;
			}
		}

		if (invalidVote) { // if there's no artist with that id
			resp.sendError(400);
			return;
		}

		WebAppUtil.vote(results, vote, resultsPath);
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}

	/**
	 * JavaBean used for mapping results.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public static class Result {

		/** Unique id */
		final String id;
		/** Unique id */
		final String name;
		/** Current result */
		int result;
		/** Song lik */
		final String link;

		/**
		 * Constructor method for class {@link Result}.
		 * 
		 * @param id
		 *            unique id
		 * @param result
		 *            current result
		 */
		public Result(String id, String name, String link, int result) {
			this.id = id;
			this.result = result;
			this.name = name;
			this.link = link;
		}

		/**
		 * Getter method for the {@code id}.
		 * 
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * Getter method for the {@code result}.
		 * 
		 * @return the result
		 */
		public int getResult() {
			return result;
		}

		/**
		 * Setter method for the {@code result}.
		 * 
		 * @param result
		 *            to be set
		 */
		public void setResult(int result) {
			this.result = result;
		}

		/**
		 * Getter method for the {@code name}.
		 * 
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Getter method for the {@code link}.
		 * 
		 * @return the link
		 */
		public String getLink() {
			return link;
		}
	}
}
