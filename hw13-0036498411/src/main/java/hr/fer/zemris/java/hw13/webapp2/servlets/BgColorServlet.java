package hr.fer.zemris.java.hw13.webapp2.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class represents {@link HttpServlet} used for changing the background of
 * the pages which will be applied to each page in the current session. Color is
 * stored in session attribute map under the key "pickedBgCol".
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class BgColorServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("pickedBgCol", req.getParameter("bgcolor"));
		req.getRequestDispatcher("/index.jsp").forward(req, resp);
	}
}
