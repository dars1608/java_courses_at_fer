package hr.fer.zemris.java.hw13.webapp2.servlets;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw13.webapp2.util.WebAppUtil;

/**
 * This class represents {@link HttpServlet} used for calculating sine and
 * cosine value of angle in degrees. Angle arguments are integers in range a to
 * b, where a < b and b < a + 720.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TrigonometricServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Integer a = WebAppUtil.intFromString(req.getParameter("a"), 0);
		Integer b = WebAppUtil.intFromString(req.getParameter("b"), 360);

		if (a > b) {
			int temp = a;
			a = b;
			b = temp;
		}

		if (b > a + 720) {
			b = a + 720;
		}

		List<NumSinCos> numSinCos = new ArrayList<>();
		for (; a <= b; a++) {
			numSinCos.add(new NumSinCos(a.intValue(), sin(toRadians(a)), cos(toRadians(a))));
		}

		req.setAttribute("numSinCos", numSinCos);
		req.getRequestDispatcher("/WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}

	/**
	 * This class represents triplets (number, sine value, cosine value).
	 *
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public class NumSinCos {
		/** Number */
		int num;
		/** Sinus value */
		double sin;
		/** Cosine value */
		double cos;

		/**
		 * Constructor method for class {@link NumSinCos}.
		 * 
		 * @param num
		 *            a number
		 * @param sin
		 *            sine value of a number
		 * @param cos
		 *            cosine value of a number
		 */
		public NumSinCos(int num, double sin, double cos) {
			this.num = num;
			this.sin = sin;
			this.cos = cos;
		}

		/**
		 * Getter method for the {@code num}.
		 * 
		 * @return the num
		 */
		public int getNum() {
			return num;
		}

		/**
		 * Getter method for the {@code sin}.
		 * 
		 * @return the sin
		 */
		public double getSin() {
			return sin;
		}

		/**
		 * Getter method for the {@code cos}.
		 * 
		 * @return the cos
		 */
		public double getCos() {
			return cos;
		}

	}
}
