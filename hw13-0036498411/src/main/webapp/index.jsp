<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=uft-8">
<style>
body {
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
<title>Home page</title>
</head>
<body>
  <h1 class="heading">Home page</h1>
  <p>
    <a href="colors.jsp">Background color chooser</a><br>
    <a href="trigonometric?a=0&b=90">Trigonometric example</a><br>
    <a href="stories/funny.jsp">Funny story</a><br>
    <a href="report.jsp">OS usage report</a><br>
    <a href="powers?a=1&b=100&n=3">Powers.xls</a><br>
    <a href="appinfo.jsp">Application info</a><br>
    <a href="glasanje">Voting section</a><br>
  </p>

  
  <form action="trigonometric" method="GET">
    Start angle:<br>
    <input type="number" name="a" min="0" max="360" step="1" value="0">
    <br> End angle:<br>
    <input type="number" name="b" min="0" max="360" step="1" value="360"><br> 
    <input type="submit" value="Create table"><input type="reset" value="Reset">
  </form>
</body>
</html>
