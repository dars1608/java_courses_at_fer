<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
  background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
<title>Trigonometric</title>
</head>
<body>
  <table border="1" bordercolor="black">
    <thead>
    <tr><th>x</th><th>sin(x)</th><th>cos(x)</th></tr>
    </thead>
    <tbody>
    <c:forEach var="record" items="${numSinCos}">
      <tr>
        <td>${record.num}</td>
        <td>${String.format("%.4f", record.sin)}</td>
        <td>${String.format("%.4f", record.cos)}</td>
      </tr>
    </c:forEach>
    </tbody>
  </table>
</body>
</html>