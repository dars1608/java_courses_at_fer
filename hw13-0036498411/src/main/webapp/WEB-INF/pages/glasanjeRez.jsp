<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"
 import="java.util.List,hr.fer.zemris.java.hw13.webapp2.servlets.VotingVoteServlet.Result"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
body {
	background-color: <%=session.getAttribute("pickedBgCol")%>;
}
table.rez td {
	text-align: center;
}
</style>
<title>Rezultati glasanja</title>
</head>
<body>
		<h1>Rezultati glasanja</h1>
		<p>Ovo su rezultati glasanja. Vrati se na <a href="index.jsp">početnu stranicu</a> ili <a href="glasanje">glasuj ponovo.</a></p>
		<table border="1" cellspacing="0" class="rez">
				<thead><tr><th>Izvođač</th><th>Broj glasova</th></tr></thead>
				<tbody>
					<c:forEach var="result" items="${results}">
						<tr><td>${result.name}</td><td>${result.result }</td></tr>
					</c:forEach>
				</tbody>
		</table>
		<h2>Grafički prikaz rezultata</h2>
		<img alt="Pie-chart" src="glasanje-grafika" width="500" height="270" />
		<h2>Rezultati u XLS formatu</h2>
		<p>
				Rezultati u XLS formatu dostupni su <a href="glasanje-xls">ovdje</a>
		</p>
		<h2>Razno</h2>
		<p>Primjeri pjesama pobjednika:</p>
		
<%
@SuppressWarnings("unchecked")
List<Result> results = (List<Result>) request.getAttribute("results");
int maxResult = -1;
for(int i = 0, len = results.size(); i < len; i++){
	Result result = results.get(i);
	if(result.getResult() >= maxResult){
		maxResult = result.getResult();
		%><iframe width="420" height="315" src="<%= result.getLink() %>"> </iframe><%
	} else {
		break;
	}
}%>

</body>
</html>