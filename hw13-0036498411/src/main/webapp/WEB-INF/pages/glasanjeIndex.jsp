<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
<title>Glasanje</title>
</head>
<body>
		<h1>Glasanje za svog omiljenog West Coast repera:</h1>
		<p>Od sljedećih repera, koji Vam je najdraži? Kliknite na link kako biste glasali! Vrati se na 
		<a href="index.jsp">početnu stranicu</a> ili <a href="glasanje-rezultati">vidi rezultate.</a></p>
		<ol>
				<c:forEach var="artist" items="${artistsList}">
						<li><a href="glasanje-glasaj?id=${artist.id}">${artist.name}</a></li>
						<iframe width="420" height="315" src="${artist.link}"> </iframe>
				</c:forEach>
		</ol>
</body>
</html>