<%@page import="java.io.IOException" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%!
/** 1 day in miliseconds */
private static final int DAYS_TO_MILLIS = 86400000;
/** 1 hour in miliseconds */
private static final int HOURS_TO_MILLIS = 3600000;
/** 1 minute in miliseconds */
private static final int MINUTES_TO_MILLIS = 60000;
/** 1 second in miliseconds */
private static final int SECONDS_TO_MILLIS = 1000;

/**
 * This method is used for writing the elapsed time period.
 * 
 * @param out
 *            writer object
 * @param startTime
 *            reference time
 * @throws IOException
 *             if error while writing occurs
 */
void writeTime(JspWriter out, Long startTime) throws IOException {
	if (startTime == null) {
		out.write("Start time isn't set!");
		return;
	}
	long time = System.currentTimeMillis() - startTime;

	int days = (int) time / DAYS_TO_MILLIS;
	if (days > 0) {
		time -= days * DAYS_TO_MILLIS;
	}

	int hours = (int) time / HOURS_TO_MILLIS;
	if (hours > 0) {
		time -= hours * HOURS_TO_MILLIS;
	}

	int minutes = (int) time / MINUTES_TO_MILLIS;
	if (minutes > 0) {
		time -= minutes * MINUTES_TO_MILLIS;
	}

	int seconds = (int) time / SECONDS_TO_MILLIS;
	if (seconds > 0) {
		time -= seconds * SECONDS_TO_MILLIS;
	}

	int miliseconds = (int) time;

	out.print(String.format("%s%s%s%s%s", 
			days == 0 ? "" : (days + "d "), 
			hours == 0 ? "" : (hours + "h "),
			minutes == 0 ? "" : (minutes + "min "),
			seconds == 0 ? "" : (seconds + "sec "),
			miliseconds == 0 ? "" : (miliseconds + "msec")));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Application info</title>
<style>
body {
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
</head>
<body>
		Your application is running for: 
		<%  writeTime(out,(Long) pageContext.getServletContext().getAttribute("startTime")); %>
</body>
</html>