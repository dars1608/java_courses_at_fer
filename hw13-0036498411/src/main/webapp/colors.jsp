<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
<title>Choose color...</title>
</head>
<body>
		<p>Pick a color:</p>
		<ol>
				<li><a href="setcolor?bgcolor=white">white</a>
				<li><a href="setcolor?bgcolor=red">red</a>
				<li><a href="setcolor?bgcolor=green">green</a>
				<li><a href="setcolor?bgcolor=cyan">cyan</a>
		</ol>
</body>
</html>