<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%!String[] colors = { "blue", "green", "yellow", "red", "brown", "black" };

	String pickColor() {
		return "color:" + colors[(int) (Math.random() * 6)];
	}%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	background-color: <%=session.getAttribute("pickedBgCol")%>
}
</style>
<title>Funny story</title>
</head>
<body>
  <p style="<%= pickColor()%>">How to win at video games: When I was little, I would go on Nickelodeon.com all the time and they had this game
    similar to Club Penguin, except it was called Nicktropolis. And if you forgot your password, a security question you
    could choose was “What is your eye color?” and if you got it right it’d tell you your password. So I would go to
    popular locations in Nicktropolis and write down random usernames who were also in those areas, and then I would log
    out and type in the username as if it were my own and see which of these usernames had a security question set to
    “What is your eye color?” (Which was most of them, since it was easy and we were all kids). I would then try either
    brown, blue, or green, and always get in, then I would go to their house and send all of their furniture and
    decorations to my own accounts. And if I didn’t want it, I could sell it for money.</p>
</body>
</html>