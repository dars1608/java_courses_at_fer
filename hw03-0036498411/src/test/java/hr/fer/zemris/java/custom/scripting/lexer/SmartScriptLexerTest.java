package hr.fer.zemris.java.custom.scripting.lexer;

import static org.junit.Assert.*;

import org.junit.Test;

import hr.fer.zemris.java.hw03.prob1.LexerException;

import static hr.fer.zemris.java.custom.scripting.tests.loader.TestDataLoader.loader;


public class SmartScriptLexerTest {
	
	SmartScriptLexer lexer;

	@Test
	public void testNotNull() {
		lexer = new SmartScriptLexer("");
		
		assertNotNull("Token was expected but null was returned.", lexer.nextToken());
	}

	@Test(expected=NullPointerException.class)
	public void testNullInput() {
		new SmartScriptLexer(null);
	}
	
	@Test(expected=LexerException.class)
	public void testExceptionEscaping() {
		lexer = new SmartScriptLexer("       \\of");
		lexer.nextToken();
	}
	
	@Test(expected=LexerException.class)
	public void testExceptionEscaping2() {
		lexer = new SmartScriptLexer("       {");
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test(expected=LexerException.class)
	public void testInvalidDouble() {
		lexer = new SmartScriptLexer("{$ 22.22.2$}");
		lexer.nextToken();
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test
	public void testNegativeNumber() {
		lexer = new SmartScriptLexer("{$ -22.22 - 22.11$}");
		lexer.nextToken();
		assertEquals("-22.22",(String)lexer.nextToken().getValue());
		assertEquals(SSTokenType.OPERATOR, lexer.nextToken().getType());
		assertEquals("22.11", (String)lexer.nextToken().getValue());
		
	}
	
	@Test(expected=LexerException.class)
	public void testInvalidQuotation() {
		lexer = new SmartScriptLexer("{$ \"safagdss");
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test(expected=LexerException.class)
	public void testInvalidExample1() {
		lexer = new SmartScriptLexer("{$ $ asda");
		lexer.nextToken();
		lexer.nextToken();
		
	}
	
	@Test(expected=LexerException.class)
	public void testInvalidExample2() {
		lexer = new SmartScriptLexer("{$ @f.ja");
		lexer.nextToken();
		lexer.nextToken();
		lexer.nextToken();
	}
	
	@Test
	public void testInvalidExample3() {
		lexer = new SmartScriptLexer("{$ 2a_55");
		lexer.nextToken();
		assertEquals(SSTokenType.INTEGER,lexer.nextToken().getType());
	}
	
	@Test(expected=LexerException.class)
	public void testInvalidExample4() {
		lexer = new SmartScriptLexer("{$ _aa66");
		lexer.nextToken();
		lexer.nextToken();
	}

	@Test
	public void testText() {
		lexer = new SmartScriptLexer("Example \\{$=1$}. Now actually write one {$=1$}");
		assertEquals(lexer.nextToken().getValue(), "Example \\{$=1$}. Now actually write one ");
	}
	
	@Test
	public void testTag() {
		lexer = new SmartScriptLexer("Example \\{$=1$}. Now actually write one {$  for =  1  \"aba\" 22.3 -11.23 A111_2$}");
		lexer.nextToken();
		SmartScriptToken test = lexer.nextToken();
		assertEquals(SSTokenType.BEGIN_TAG,test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.FOR,test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.EQUALS,test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.INTEGER,test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.STRING,test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.DOUBLE, test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.DOUBLE, test.getType());
		assertEquals("-11.23", test.getValue());
		test=lexer.nextToken();
		assertEquals(SSTokenType.VARIABLE,test.getType());
		test=lexer.nextToken();
		assertEquals(SSTokenType.END_TAG,test.getType());
		
	}
	
	@Test
	public void testBigInput() {
		String input = loader("example3.txt", this);
		lexer = new SmartScriptLexer(input);
		SSTokenType[] test = new SSTokenType[45];
		for(int i = 0; i<45; i++) {
			test[i] = lexer.nextToken().getType();
			//System.out.println(test[i] + " " + lexer.getToken().getValue());
		}
		
		assertArrayEquals(EXPECTED_TEST, test);
	}
	
	
	//data used for testing, append before EOF if you are adding some new text
	//in test example.

	private static final SSTokenType[] EXPECTED_TEST = new SSTokenType[] {
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.FOR,
			SSTokenType.VARIABLE,
			SSTokenType.INTEGER,
			SSTokenType.INTEGER,
			SSTokenType.INTEGER,
			SSTokenType.END_TAG,
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.EQUALS,
			SSTokenType.VARIABLE,
			SSTokenType.END_TAG,
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.END,
			SSTokenType.END_TAG,
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.FOR,
			SSTokenType.VARIABLE,
			SSTokenType.INTEGER,
			SSTokenType.INTEGER,
			SSTokenType.INTEGER,
			SSTokenType.END_TAG,
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.EQUALS,
			SSTokenType.VARIABLE,
			SSTokenType.END_TAG,
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.EQUALS,
			SSTokenType.VARIABLE,
			SSTokenType.VARIABLE,
			SSTokenType.OPERATOR,
			SSTokenType.FUNCTION,
			SSTokenType.STRING,
			SSTokenType.FUNCTION,
			SSTokenType.END_TAG,
			SSTokenType.TEXT,
			SSTokenType.BEGIN_TAG,
			SSTokenType.END,
			SSTokenType.END_TAG,
			SSTokenType.EOF
	};
	
}