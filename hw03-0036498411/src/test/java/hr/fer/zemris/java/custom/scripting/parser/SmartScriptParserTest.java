package hr.fer.zemris.java.custom.scripting.parser;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static hr.fer.zemris.java.custom.scripting.tests.loader.TestDataLoader.loader;

import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

public class SmartScriptParserTest {
	private String dat1;
	private String dat2;

	@Before
	public void setUp() throws Exception {
		dat1 = loader("example1.txt", this);
		dat2 = loader("example2.txt", this);
	}

	@Test(expected = NullPointerException.class)
	public void testSmartScriptParser() {
		new SmartScriptParser(null);
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException1() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR 3 1 10 1 $}");
		test.getDocumentNode();
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException2() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR * \"1\" -10 \"1\" $}");
		test.getDocumentNode();
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException3() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR year @sin 10 $}");
		test.getDocumentNode();
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException4() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR year 1 10 \"1\" \"10\" $}");
		test.getDocumentNode();
	}

	@Test
	public void testEmptyEcho() {
		SmartScriptParser test = new SmartScriptParser("{$= $}");
		assertEquals("{$= $}", SmartScriptParser.createOriginalDocumentBody(test.getDocumentNode()));
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException5() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR year $}");
		test.getDocumentNode();
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException6() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR year 1 10 1 3 $}");
		test.getDocumentNode();
	}

	@Test(expected = SmartScriptParserException.class)
	public void testForException7() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR year 1 10 1 $} abc {$ FOR year 10 1 3 $} abc {$END$}");
		test.getDocumentNode();
	}
	
	@Test(expected = SmartScriptParserException.class)
	public void testForException8() {
		SmartScriptParser test = new SmartScriptParser("{$ FOR year 1 10 1 ");
		test.getDocumentNode();
	}

	@Test
	public void testNested() {
		String t = "{$ FOR i 1 10 1$} abc {$ FOR i 10 1 3$} abc {$END$} {$ FOR i 10 1 3$} abc {$END$} {$END$}";
		SmartScriptParser test = new SmartScriptParser(t);
		// System.out.println(SmartScriptParser.createOriginalDocumentBody(test.getDocumentNode()));
		assertEquals(t, SmartScriptParser.createOriginalDocumentBody(test.getDocumentNode()));
	}

	@Test(expected = SmartScriptParserException.class)
	public void testEndException() {
		SmartScriptParser test = new SmartScriptParser("{$END$}");
		test.getDocumentNode();
	}

	@Test
	public void testEscapingNextLine() {
		String text1 = "{$= \"\\n\" $}";
		SmartScriptParser test = new SmartScriptParser(text1);
		DocumentNode doc = test.getDocumentNode();
		String text2 = SmartScriptParser.createOriginalDocumentBody(doc);
		test = new SmartScriptParser(text2);
		doc = test.getDocumentNode();
		String text3 = SmartScriptParser.createOriginalDocumentBody(doc);
		assertEquals(text1, text2);
		assertEquals(text2, text3);
		assertEquals(text3, text1);
	}
	
	@Test
	public void testEsapingExample() {
		String simpleDoc = "A tag follows {$= \"Joe \\\"Long\\\" Smith\" $}.";
		SmartScriptParser test = new SmartScriptParser(simpleDoc);
		DocumentNode doc = test.getDocumentNode();
		String text = SmartScriptParser.createOriginalDocumentBody(doc);
		assertEquals(simpleDoc, text);
		assertTrue(doc.getChild(0) instanceof TextNode);
		assertTrue(doc.getChild(1) instanceof EchoNode);
		assertEquals("Joe \"Long\" Smith",
				((ElementString)((EchoNode)doc.getChild(1)).getElements()[0]).getValue());
	}

	@Test
	public void testGetDocumentNode() {
		SmartScriptParser test = new SmartScriptParser(dat1);
		DocumentNode doc = test.getDocumentNode();
		String s = SmartScriptParser.createOriginalDocumentBody(doc);
		assertEquals(dat1, s);
	}

	@Test
	public void testCreateOriginalDocumentBody() {
		DocumentNode doc = new SmartScriptParser(dat2).getDocumentNode();
		String s = SmartScriptParser.createOriginalDocumentBody(doc);
		DocumentNode docNew = new SmartScriptParser(s).getDocumentNode();
		String s2 = SmartScriptParser.createOriginalDocumentBody(docNew);
		assertEquals(s, s2);
	}
}
