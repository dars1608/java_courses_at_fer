package hr.fer.zemris.java.hw03;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

import static hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser.createOriginalDocumentBody;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * This is demonstration program for class {@link SmartScriptParser}. Program
 * requires one command line argument which represents path to some text
 * document which will be parsed.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartScriptTester {  //example text document is in ..\src\test\resources\example2.txt

	/**
	 * This method is called when program starts. Command line arguments are used
	 * for providing file path to text document.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException(
					"Program requires one command line arguments. " + args.length + " provided.");
		}

		String docBody = null;

		try {
			docBody = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}

		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(docBody);
		} catch (SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		DocumentNode document = parser.getDocumentNode();
		String newDocument = createOriginalDocumentBody(document);

		System.out.println("Parsed document:\n");
		System.out.println(newDocument);
		try {
			parser = new SmartScriptParser(newDocument);
		} catch (SmartScriptParserException e) {
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}

		document = parser.getDocumentNode();
		String newDocument2 = createOriginalDocumentBody(document);

		System.out.println("------------------------------------------------------");
		System.out.println("Reparsed document:\n");
		System.out.println(newDocument2);
		System.out.println("+----------------------------------------------------+");
		System.out.printf("Documents %s.", newDocument.equals(newDocument2) ? "are equal" : "aren't equal");
	}

}
