package hr.fer.zemris.java.custom.scripting.collections;

/**
 * This class represents an implementation of resizable array-backed collection
 * of objects. General contract of this collection is: duplicate elements are
 * allowed; storage of null references is not allowed.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ArrayIndexedCollection extends Collection {
	/**
	 * Default size of backed-array. Is set to 16.
	 * 
	 * @since 1.0
	 */
	private static final int DEFAULT_SIZE = 16;

	/**
	 * Collection size.
	 */
	private int size;
	/**
	 * Collection capacity.
	 */
	private int capacity;
	/**
	 * Backed-array of objects.
	 */
	private Object[] elements;

	{
		this.size = 0;
	}

	/**
	 * Default constructor for {@link ArrayIndexedCollection. Sets collection
	 * capacity to default size of 16, and allocates <code>Object[]</code> for that
	 * capacity.
	 */
	public ArrayIndexedCollection() {
		this.capacity = DEFAULT_SIZE;
		this.elements = new Object[DEFAULT_SIZE];
	}

	/**
	 * Constructor for for {@link ArrayIndexedCollection which sets the initial
	 * capacity. {@link Object}
	 * 
	 * @param initialCapacity
	 *            Initial value of collections capacity.
	 * @throws IllegalArgumentException
	 *             if initialCapacity is less than 1.
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		if (initialCapacity < 1)
			throw new IllegalArgumentException("Initial capacity must be greater than 0. Is " + initialCapacity);

		this.capacity = initialCapacity;
		this.elements = new Object[initialCapacity];
	}

	/**
	 * Constructor for for {@link ArrayIndexedCollection which creates a copy of
	 * given collection.
	 * 
	 * @param other
	 *            Collection which values are copied.
	 * @throws NullPointerException
	 *             if given collection reference is null.
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other.size()); // will throw NullPointerException if other == null
		this.addAll(other);
	}

	/**
	 * Constructor for for {@link ArrayIndexedCollection which creates a copy of
	 * given collection and also sets the capacity to initial value if the size of
	 * given collection is less than initial value.
	 * 
	 * @param other
	 *            Collection which values are copied.
	 * @param initialCapacity
	 *            Initial value of collections capacity.
	 * @throws NullPointerException
	 *             if given Collection reference is null.
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		this(initialCapacity > other.size() ? initialCapacity : other.size());
		this.addAll(other);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws NullPointerException
	 *             if given value is <code>null</code>.
	 */
	@Override
	public void add(Object value) {
		if (value == null) {
			throw new NullPointerException("This collection can't store null values");
		} else if (size == capacity) {
			resizeArray();
		}

		elements[size] = value;
		size++;

	}

	/**
	 * This method returns object at given index.
	 * 
	 * @param index
	 *            Index of an object which will be returned.
	 * @return Object which index is equal to given index.
	 * @throws IndexOutOfBoundsException
	 *             if index is greater than size or less than 0.
	 */
	public Object get(int index) {
		if (!inBounds(index))
			throw new IndexOutOfBoundsException("Index is out of bounds [0, " + size + "]. Is " + index + ".");
		return elements[index];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		for (int i = 0; i < size; i++) {
			elements[i] = null;
		}
		size = 0;
	}

	/**
	 * This method inserts object at given index.
	 * 
	 * @param value
	 *            Object which is inserted.
	 * @param position
	 *            Index at which object needs to be inserted.
	 * @throws IndexOutOfBoundsException
	 *             if given index is out of bounds.
	 * @throws NullPointerException if given value is
	 *             <code>null</code>.
	 */
	public void insert(Object value, int position) {
		if (value == null) {
			throw new NullPointerException("This collection can't store null values");
		} else if (position == size) {
			add(value);
			return;
		} else if (!inBounds(position)) {
			throw new IndexOutOfBoundsException("Index is out of bounds [0, " + size + "]. Is " + position + ".");
		} else if (size == capacity) {
			resizeArray();
		}

		Object temp = elements[position], temp2;
		elements[position] = value;
		for (int i = position + 1; i < size; i++) {
			temp2 = elements[i];
			elements[i] = temp;
			temp = temp2;
		}
		elements[size] = temp;
		size++;
	}

	/**
	 * This method is used to find index of objects first occurrence.
	 * 
	 * @param value
	 *            Object which index needs to be found.
	 * @return Index of given object, -1 if it's <code>null</code> or not in
	 *         collection.
	 */
	public int indexOf(Object value) {
		if (value == null) {
			return -1;
		}

		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value))
				return i;
		}
		return -1;
	}

	/**
	 * Removes element at given index.
	 * 
	 * @param index
	 *            Index of element which needs to be removed.
	 * 
	 * @throws IndexOutOfBoundsException
	 *             if index isn't from range [0, size].
	 */
	public void remove(int index) {
		if (!inBounds(index)) {
			throw new IndexOutOfBoundsException("Index is out of bounds [0, " + size + "]. Is " + index + ".");
		}

		for (int i = index; i < size - 1; i++) { // if i = size - 1, i+1 = size, which isn't secure index (may be
													// greater than capacity)
			elements[i] = elements[i + 1];
		}
		elements[size - 1] = null;

		size--;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws NullPointerException
	 *             if given processor reference is null.
	 */
	@Override
	public void forEach(Processor processor) {
		if (processor == null) {
			throw new NullPointerException("Processor reference can't be null");
		}
		for (Object element : elements) {
			if (element != null) {
				processor.process(element);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object value) {
		if (value == null) {
			return false;
		}

		for (Object element : elements) {
			if (element.equals(value))
				return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toArray() {
		if (size == 0) {
			return new Object[0];
		}
		return elements.clone();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object value) {
		int index = indexOf(value);
		if (index == -1)
			return false;
		remove(index);
		return true;
	}

	// These methods are used internally.

	/**
	 * This method is used internally for checking if given index is in range [0,
	 * size-1].
	 * 
	 * @param index
	 *            Index which is checked
	 * @return <code>true</code> if it's in that range, <code>false</code>
	 *         otherwise.
	 */
	private boolean inBounds(int index) {
		return index >= 0 && index < size;
	}

	/**
	 * This method is used internally for resizing of the backing array.
	 */
	private void resizeArray() {
		Object[] tempArray = elements.clone();
		elements = new Object[capacity * 2];
		for (int i = 0; i < capacity; i++) {
			elements[i] = tempArray[i];
		}
		capacity *= 2;
	}

	// This method is defined for testing only.

	/**
	 * This method is used for testing only.
	 * 
	 * @return Capacity of this collection.
	 */
	int getCapacity() {
		return capacity;
	}
}
