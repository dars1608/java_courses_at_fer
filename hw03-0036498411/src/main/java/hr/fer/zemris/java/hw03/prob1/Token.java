package hr.fer.zemris.java.hw03.prob1;

/**
 * @author Darko Britvec
 * @version 1.0
 */
public class Token {
	
	private final TokenType type;
	private final Object value;

	/**
	 * Constructor method for class {@link Token}
	 * 
	 * @param type Token type.
	 * @param value Token value.
	 * @throws IllegalArgumentException if type parameter is null.
	 */
	public Token(TokenType type, Object value) {
		if (type == null) {
			throw new IllegalArgumentException("Token type can not be null");
		}
		this.type = type;
		this.value = value;
	}

	/**
	 * Getter method for value of token.
	 * @return Token value.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Getter method for type of token.
	 * @return Token type.
	 */
	public TokenType getType() {
		return type;
	}
	
	/**
	 * Enumeration used for describing token type.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public enum TokenType {
		EOF, WORD, NUMBER, SYMBOL;
	}
}
