package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * @author Darko Britvec
 * @version 1.0
 */
public class SmartScriptToken {
	

	private final SSTokenType type;
	private final Object value;

	/**
	 * Constructor method for class {@link Token}
	 * 
	 * @param type Token type.
	 * @param value Token value.
	 * @throws IllegalArgumentException if type parameter is null.
	 */
	public SmartScriptToken(SSTokenType type, Object value) {
		if (type == null) {
			throw new IllegalArgumentException("Token type can not be null");
		}
		this.type = type;
		this.value = value;
	}

	/**
	 * Getter method for value of token.
	 * @return Token value.
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Getter method for type of token.
	 * @return Token type.
	 */
	public SSTokenType getType() {
		return type;
	}
}
