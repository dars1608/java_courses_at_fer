package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class extends {@link Element} class. It defines an integer constant.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ElementConstantInteger extends Element {
	/** Value of this element. */
	private final int value;

	/**
	 * Constructor method for class {@link ElementConstantInteger}.
	 * 
	 * @param value
	 *            Value of element.
	 */
	public ElementConstantInteger(int value) {
		super();
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.elems.Element#asText()
	 */
	@Override
	public String asText() {
		return Integer.toString(value);
	}
}
