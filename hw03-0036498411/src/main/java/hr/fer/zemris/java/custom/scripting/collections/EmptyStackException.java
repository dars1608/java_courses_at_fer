/**
 * 
 */
package hr.fer.zemris.java.custom.scripting.collections;

/**
 * @author Darko Britvec
 * @version 1.0
 */
public class EmptyStackException extends RuntimeException {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Detail message.
	 */
	private String message;

	/**
	 * Default constructor for class {@link EmptyStackException}.
	 */
	public EmptyStackException() {
	}

	/**
	 * Constructor method for class {@link EmptyStackException}.
	 * 
	 * @param message
	 *            The detail message string of this exception.
	 */
	public EmptyStackException(String message) {
		this.message = message;
	}

	/**
	 * Returns the detail message string of this exception.
	 * 
	 * @return The detail message string of this exception. (may be
	 *         <code>null</code>).
	 */
	@Override
	public String getMessage() {
		return message;
	}
}
