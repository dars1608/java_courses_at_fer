/**
 * This subpackage contains support
 * {@link hr.fer.zemris.java.custom.scripting.nodes.collection.ArrayIndexedCollection}
 * used for second assignment.
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.collections;