package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class extends {@link Element} class. It defines a double constant.
 * @author Darko Britvec
 * @version 1.0
 */
public class ElementConstantDouble extends Element {
	/** Value of this element. */
	private final double value;

	/**
	 * Constructor method for class {@link ElementConstantDouble}.
	 * 
	 * @param value
	 *            Value of element.
	 */
	public ElementConstantDouble(double value) {
		super();
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see hr.fer.zemris.java.custom.scripting.elems.Element#asText()
	 */
	@Override
	public String asText() {
		return Double.toString(value);
	}
}
