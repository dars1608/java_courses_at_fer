/**
 * This package contains first assignment, which is used as test example for
 * making simple language processor.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw03.prob1;