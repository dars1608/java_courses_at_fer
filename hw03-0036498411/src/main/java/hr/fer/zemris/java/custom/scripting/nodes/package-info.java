/**
 * This package contains classes used for the representation of structured
 * documents, which are part of solution of second assignment.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.nodes;