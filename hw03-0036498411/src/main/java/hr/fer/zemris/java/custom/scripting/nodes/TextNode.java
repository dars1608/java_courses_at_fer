package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * A node representing a piece of textual data. It inherits from {@link Node}
 * class.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TextNode extends Node {
	/** Text value */
	private final String text;

	/**
	 * Constructor method for {@link TextNode} class.
	 * 
	 * @param text
	 *            Text value
	 */
	public TextNode(String text) {
		super();
		this.text = text;
	}

	/**
	 * Getter method for text value.
	 * 
	 * @return the text
	 */
	public String getText() {
		return text;
	}
}
