package hr.fer.zemris.java.custom.scripting.collections;

/**
 * This class represents some general collection of objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Collection {

	/**
	 * This is default constructor for class {@link Collection}
	 */
	protected Collection(){
		
	}
	/**
	 * This method checks if the collection is empty.
	 * 
	 * @return <code>true</code> if collection contains no objects and
	 *         <code>false</code> otherwise.
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * This method returns the number of currently stored objects in this
	 * collection.
	 * 
	 * @return Number of currently stored objects
	 */
	public int size() {
		return 0;
	}

	/**
	 * This method adds the given object into this collection.
	 * 
	 * @param value
	 *            Object which needs to be added
	 */
	public void add(Object value) {
	}

	/**
	 * This method checks if given value is in this collection.
	 * 
	 * @param value
	 *            Checked object.
	 * @return Returns <code>true</code> only if the collection contains given
	 *         value, as determined by equals method, <code>false</code> otherwise.
	 */
	public boolean contains(Object value) {
		return false;
	}

	/**
	 * This method is used to delete given value form the collection.
	 * 
	 * @param value
	 *            Object which needs to be deleted.
	 * @return Returns <code>true</code> only if the collection contains given value
	 *         as determined by equals method and removes one occurrence of it.
	 */
	public boolean remove(Object value) {
		return false;
	}

	/**
	 * This method allocates new array with size equals to the size of this
	 * collections, fills it with collection content and returns the array. This
	 * method will never return null.
	 * 
	 * @return Array of objects from this collection.
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException(
				"Method hr.fer.zemris.java.custom.collections.Collection.toArray is not implemented.");
	}

	/**
	 * This method calls processor.process(.) for each element of this collection.
	 * The order in which elements will be sent is undefined in this class.
	 * 
	 * @param processor
	 *            Object that processes elements from this collection in some way
	 */
	public void forEach(Processor processor) {
	}

	/**
	 * This method adds into the current collection all elements from the given
	 * collection.
	 * 
	 * @param other
	 *            Collection from which elements are added to this collection.
	 */
	public void addAll(Collection other) {
		/**
		 * Local class which is used to load values from one collection to another.
		 * 
		 * @author Darko Britvec
		 * @version 1.0
		 */
		class CollectionLoader extends Processor {
			/**
			 * Collection which needs to be loaded.
			 */
			private Collection collection;

			/**
			 * Constructor used to initialize {@link CollectionLoader}.
			 * 
			 * @param collection
			 *            Collection in which the value are added.
			 * @throws NullPointerException if collection reference is
			 *             <code>null</code>.
			 */
			public CollectionLoader(Collection collection) {
				if (collection == null) {
					throw new NullPointerException("Collection reference can't be null");
				}
				this.collection = collection;
			}

			/**
			 * This method invokes method which adds values into the collection.
			 * 
			 * @param value
			 *            Object which needs to be added
			 */
			@Override
			public void process(Object value) {
				collection.add(value);
			}
		}

		other.forEach(new CollectionLoader(this));
	}

	/**
	 * This method moves all elements from this collection.
	 */
	public void clear() {
	}
}
