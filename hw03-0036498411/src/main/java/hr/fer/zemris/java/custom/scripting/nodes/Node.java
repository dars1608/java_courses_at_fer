package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.collections.ArrayIndexedCollection;

/**
 * Base class for all graph nodes
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Node {
	/** Collection of child nodes */
	private ArrayIndexedCollection childNodes;

	/**
	 * This method adds given <code>child</code> to an internally managed collection
	 * of children;
	 * 
	 * @param child
	 *            Child node which needs to be added.
	 * @throws IllegalArgumentException
	 *             if given child reference is <code>null</code>.
	 */
	public void addChildNode(Node child) {
		if (child == null) {
			throw new IllegalArgumentException("Child can't be null");
		}
		if (childNodes == null) {
			childNodes = new ArrayIndexedCollection();
		}

		childNodes.add(child);
	}

	/**
	 * This method is used for checking how many direct children does this node
	 * have.
	 * 
	 * @return Number of children.
	 */
	public int numberOfChildren() {
		if (childNodes == null)
			return 0;
		return childNodes.size();
	}

	/**
	 * This method is used for getting child node from internal managed collection
	 * of children.
	 * 
	 * @param index
	 *            Index of child in collection.
	 * @return Child at given index.
	 */
	public Node getChild(int index) {
		if (index < 0 || index >= childNodes.size()) {
			throw new IndexOutOfBoundsException("Index is out of range [0, " + childNodes.size() + "].");
		}

		return (Node) childNodes.get(index);
	}

}

/*
 * Node getChild(int index); – returns selected child or throws an appropriate
 * exception if the index is invalid.
 * 
 */