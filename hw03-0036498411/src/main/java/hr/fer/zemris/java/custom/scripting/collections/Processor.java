package hr.fer.zemris.java.custom.scripting.collections;

/**
 * This class describes an object which can do something with given object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Processor {

	/**
	 * Empty method, needs to be overridden.
	 * 
	 * @param value
	 *            Any {@link Object}.
	 */
	public void process(Object value) {
	}
}
