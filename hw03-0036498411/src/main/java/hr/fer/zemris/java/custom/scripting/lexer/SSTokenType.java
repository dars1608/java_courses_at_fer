package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enumeration used for describing token type.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public enum SSTokenType {
	/** Token describing the end of file. */
	EOF,
	/** Token describing some text. */
	TEXT,
	/** Token describing a variable. */
	VARIABLE, 
	/** Token describing an integer constant. */
	INTEGER,
	/** Token describing a double constant. */
	DOUBLE,
	/** Token describing a string constant. */
	STRING, 
	/** Token describing an operator. */
	OPERATOR, 
	/** Token describing some function. */
	FUNCTION,
	/** Token describing keyword for. */
	FOR,
	/** Token describing keyword echo. */
	EQUALS, 
	/** Token describing keyword end. */
	END, 
	//** Token describing a beginning of a tag. */
	BEGIN_TAG,
	/** Token describing an end of a tag. */
	END_TAG;
}
