package hr.fer.zemris.java.hw03.prob1;

/**
 * This class defines exception which is thrown in class {@link Lexer} if error
 * occurs while generating token from text.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LexerException extends RuntimeException {

	/**
	 * Generated serial version.
	 */
	private static final long serialVersionUID = -1173741942168406063L;

	/**
	 * Default constructor for {@link LexerException}.
	 */
	public LexerException() {

	}

	/**
	 * Constructor method for {@link LexerException}.
	 * 
	 * @param message
	 *            Detail message.
	 */
	public LexerException(String message) {
		super(message);
	}

	/**
	 * Constructor method for {@link LexerException}.
	 * 
	 * @param t Cause.
	 */
	public LexerException(Throwable t) {
		super(t);
	}
	
	/**
	 * Constructor method for {@link LexerException}.
	 * @param message Detail message.
	 * @param t Cause.
	 */
	public LexerException(String message,Throwable t) {
		super(message, t);
	}
}
