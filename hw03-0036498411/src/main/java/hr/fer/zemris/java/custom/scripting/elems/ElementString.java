package hr.fer.zemris.java.custom.scripting.elems;

/**
 * This class extends {@link Element} class. It defines a string element.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ElementString extends Element {
	/** Value of this element. */
	private final String value;

	/**
	 * Constructor method for class {@link ElementString}.
	 * 
	 * @param value
	 *            Value of element.
	 */
	public ElementString(String value) {
		if (value == null) {
			throw new IllegalArgumentException("Value cannot be null.");
		}

		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.java.custom.scripting.elems.Element#asText()
	 */
	@Override
	public String asText() {
		//We must add quoting signs and replace special chars to return
		//initial string which was parsed.
		return "\"" + value.replaceAll("\n", "\\\\n")
				.replaceAll("\r", "\\\\r")
				.replaceAll("\t", "\\\\t")
				.replaceAll("\\\\", "\\\\")
				.replaceAll("\"", "\\\\\"") + "\"";
	}

	/**
	 * Getter function for <code>value</code>.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	
}
