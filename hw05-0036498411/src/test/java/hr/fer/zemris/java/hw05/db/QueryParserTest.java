package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class QueryParserTest {
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDirectQuery() {
		String input = "    jmbag=    \"0036498411\"       ";
		QueryParser test = new QueryParser(input);
		assertTrue(test.isDirectQuery());
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.JMBAG, exp.getGetterStrategy());
		assertEquals("0036498411", exp.getLiteral());
		assertEquals(ComparisonOperators.EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testLessQuery() {
		String input = "    jmbag<    \"0036498411\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.JMBAG, exp.getGetterStrategy());
		assertEquals("0036498411", exp.getLiteral());
		assertEquals(ComparisonOperators.LESS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testLessOrEqualsQuery() {
		String input = "    jmbag <=    \"0036498411\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.JMBAG, exp.getGetterStrategy());
		assertEquals("0036498411", exp.getLiteral());
		assertEquals(ComparisonOperators.LESS_OR_EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testGreaterQuery() {
		String input = "    jmbag>    \"0036498411\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.JMBAG, exp.getGetterStrategy());
		assertEquals("0036498411", exp.getLiteral());
		assertEquals(ComparisonOperators.GREATER, exp.getComparisonStrategy());
	}
	
	@Test
	public void testGreaterOrEqualsQuery() {
		String input = "    jmbag >=    \"0036498411\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.JMBAG, exp.getGetterStrategy());
		assertEquals("0036498411", exp.getLiteral());
		assertEquals(ComparisonOperators.GREATER_OR_EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testNotEqualsQuery() {
		String input = "    jmbag !=    \"0036498411\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.JMBAG, exp.getGetterStrategy());
		assertEquals("0036498411", exp.getLiteral());
		assertEquals(ComparisonOperators.NOT_EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testGetFirstNameQuery() {
		String input = "    firstName <=    \"Darko\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.FIRST_NAME, exp.getGetterStrategy());
		assertEquals("Darko", exp.getLiteral());
		assertEquals(ComparisonOperators.LESS_OR_EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testLastNameQuery() {
		String input = "    lastName !=    \"Britvec\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.LAST_NAME, exp.getGetterStrategy());
		assertEquals("Britvec", exp.getLiteral());
		assertEquals(ComparisonOperators.NOT_EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testLikeQuery() {
		String input = "    lastName liKe    \"Britvec\"       ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.LAST_NAME, exp.getGetterStrategy());
		assertEquals("Britvec", exp.getLiteral());
		assertEquals(ComparisonOperators.LIKE, exp.getComparisonStrategy());
	}
	
	@Test
	public void testChainedQuery() {
		String input = "    lastName !=    \"Britvec\"   aNd firstName = \"Darko\" ";
		QueryParser test = new QueryParser(input);
		ConditionalExpression exp = test.getQuery().get(0);
		assertEquals(FieldValueGetters.LAST_NAME, exp.getGetterStrategy());
		assertEquals("Britvec", exp.getLiteral());
		assertEquals(ComparisonOperators.NOT_EQUALS, exp.getComparisonStrategy());
		exp = test.getQuery().get(1);
		assertEquals(FieldValueGetters.FIRST_NAME, exp.getGetterStrategy());
		assertEquals("Darko", exp.getLiteral());
		assertEquals(ComparisonOperators.EQUALS, exp.getComparisonStrategy());
	}
	
	@Test
	public void testBigInput() {
		StringBuilder sb = new StringBuilder();
		sb.append("");
		for(int i = 0; i<10; i++) {
			sb.append(" firstName");
			sb.append("!=");
			sb.append("\"");
			sb.append("darko");
			sb.append(i);
			sb.append("\"");
			if(i!=9) {
				sb.append(" AND ");
			}
		}
		
		QueryParser test = new QueryParser(sb.toString());
		assertFalse(test.isDirectQuery());
		List<ConditionalExpression> list = test.getQuery();
		assertEquals(10, list.size());
		for(int i = 0; i<10; i++) {
			ConditionalExpression exp = list.get(i);
			assertEquals(FieldValueGetters.FIRST_NAME, exp.getGetterStrategy());
			assertEquals("darko" + i, exp.getLiteral());
			assertEquals(ComparisonOperators.NOT_EQUALS, exp.getComparisonStrategy());
		}
		
	}
	
	
	

}
