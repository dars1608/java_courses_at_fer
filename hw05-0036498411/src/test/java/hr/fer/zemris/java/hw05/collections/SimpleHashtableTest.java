package hr.fer.zemris.java.hw05.collections;

import static org.junit.Assert.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class SimpleHashtableTest {
	private SimpleHashtable<String, String> test;

	@Before
	public void setUp() throws Exception {
		test = new SimpleHashtable<String, String>();
		for (int i = 0; i < 20; i++) {
			test.put("a" + i, "b" + i);
		}
	}

	@Test
	public void constructorTest() {
		SimpleHashtable<String, String> a = new SimpleHashtable<>();
		assertEquals(16, a.getTable().length);
	}

	@Test
	public void constructorCapacityTest() {
		SimpleHashtable<String, String> a = new SimpleHashtable<>(17);
		assertEquals(32, a.getTable().length);
	}

	@Test
	public void sizeTest() {
		assertEquals(20, test.size());
	}

	@Test
	public void containsKeyTest() {
		for (int i = 0; i < 20; i++) {
			assertTrue(test.containsKey("a" + i));
		}
	}

	@Test
	public void containsValueTest() {
		for (int i = 0; i < 20; i++) {
			assertTrue(test.containsValue("b" + i));
		}
	}

	@Test
	public void putTest() {
		test.put("x", "y");
		assertTrue(test.containsKey("x"));
		assertEquals("y", test.get("x"));
	}

	@Test
	public void putSameKeyTest() {
		test.put("a1", "newB1");
		assertTrue(test.containsKey("a1"));
		assertEquals("newB1", test.get("a1"));
		assertFalse(test.containsValue("b1"));

		test.put("a11", "newB11");
		assertTrue(test.containsKey("a11"));
		assertEquals("newB11", test.get("a11"));
		assertFalse(test.containsKey("b11"));
	}

	@Test
	public void sizeAfterPut() {
		test.put("x", "x1");
		test.put("y", "y1");
		test.put("z", "z1");
		assertEquals(23, test.size());
	}

	@Test
	public void sizeAfterPutSame() {
		test.put("a1", "x1");
		test.put("a2", "y1");
		test.put("a15", "z1");
		assertEquals(20, test.size());
	}

	@Test
	public void putNullValueTest() {
		test.put("null", null);
		assertTrue(test.containsKey("null"));
		assertTrue(test.containsValue(null));
		assertNull(test.get("null"));
	}

	@Test(expected = NullPointerException.class)
	public void putNullKeyTest() {
		test.put(null, "null");
	}

	public void getNullKeyTest() {
		assertNull(test.get(null));
	}

	@Test
	public void removeHeadTest() {
		test.remove("a0");
		assertFalse(test.containsKey("a0"));
		assertFalse(test.containsValue("b0"));
		for (int i = 1; i < 20; i++) {
			assertTrue(test.containsKey("a" + i));
			assertTrue(test.containsValue("b" + i));
		}
	}

	@Test
	public void removeTest() {
		test.remove("a5");
		assertFalse(test.containsKey("a5"));
		assertFalse(test.containsValue("b5"));
		for (int i = 0; i < 20; i++) {
			if (i == 5)
				continue;
			assertTrue(test.containsKey("a" + i));
			assertTrue(test.containsValue("b" + i));
		}
	}

	@Test
	public void sizeAfterRemoveTest() {
		test.remove("a0");
		test.remove("a5");
		test.remove("a19");
		assertEquals(17, test.size());
	}

	@Test
	public void isEmptyTest() {
		for (int i = 0; i < 20; i++) {
			test.remove("a" + i);
		}

		assertTrue(test.isEmpty());
	}

	@Test
	public void toStringTest() {
		SimpleHashtable<String, String> a = new SimpleHashtable<>();
		for (int i = 1; i <= 3; i++) {
			a.put("key" + i, "value" + i);
		}
		String expected = "[key1=value1, key2=value2, key3=value3]";
		assertEquals(expected, a.toString());
	}

	@Test
	public void loadFactorTest() {
		SimpleHashtable<String, String> a = new SimpleHashtable<>();
		for (int i = 0; i <= 50; i++) {
			a.put("key" + i, "value" + i);
		}
		assertEquals(128, a.getTable().length);
	}

	@Test
	public void testExampleFromPDF1() {
		SimpleHashtable<String, Integer> examMarks = new SimpleHashtable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana

		String[] keys = new String[] { "Ante", "Ivana", "Jasna", "Kristina"

		};

		Integer[] values = new Integer[] { 2, 5, 2, 5 };

		String[] testKeys = new String[4];
		Integer[] testValues = new Integer[4];
		int counter = 0;
		for (SimpleHashtable.TableEntry<String, Integer> pair : examMarks) {
			testKeys[counter] = pair.getKey();
			testValues[counter] = pair.getValue();
			counter++;
		}

		assertArrayEquals(keys, testKeys);
		assertArrayEquals(values, testValues);
	}

	@Test
	public void testExampleFromPDF2() {
		SimpleHashtable<String, Integer> examMarks = new SimpleHashtable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana

		String[] keys = new String[] { "Ante", "Ivana", "Jasna", "Kristina" };

		Integer[] values = new Integer[] { 2, 5, 2, 5 };

		int outerCounter = 0;
		for (SimpleHashtable.TableEntry<String, Integer> pair1 : examMarks) {
			int innerCounter = 0;
			for (SimpleHashtable.TableEntry<String, Integer> pair2 : examMarks) {
				assertEquals(keys[outerCounter], pair1.getKey());
				assertEquals(values[outerCounter], pair1.getValue());
				assertEquals(keys[innerCounter], pair2.getKey());
				assertEquals(values[innerCounter], pair2.getValue());
				innerCounter++;
			}
			outerCounter++;
		}
	}

	@Test
	public void testExampleFromPDF3() {
		SimpleHashtable<String, Integer> examMarks = new SimpleHashtable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana

		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while (iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			if (pair.getKey().equals("Ivana")) {
				iter.remove(); // sam iterator kontrolirano uklanja trenutni element
			}
		}

		assertFalse(examMarks.containsKey("Ivana"));
	}

	@Test(expected = IllegalStateException.class)
	public void testExampleFromPDF4() {
		SimpleHashtable<String, Integer> examMarks = new SimpleHashtable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana

		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while (iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			if (pair.getKey().equals("Ivana")) {
				iter.remove();
				iter.remove();
			}
		}
	}

	@Test(expected = ConcurrentModificationException.class)
	public void testExampleFromPDF5() {
		SimpleHashtable<String, Integer> examMarks = new SimpleHashtable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana

		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while (iter.hasNext()) {
			SimpleHashtable.TableEntry<String, Integer> pair = iter.next();
			if (pair.getKey().equals("Ivana")) {
				examMarks.remove("Ivana");
			}
		}
	}

	@Test
	public void testExampleFromPDF6() {
		SimpleHashtable<String, Integer> examMarks = new SimpleHashtable<>(2);
		// fill data:
		examMarks.put("Ivana", 2);
		examMarks.put("Ante", 2);
		examMarks.put("Jasna", 2);
		examMarks.put("Kristina", 5);
		examMarks.put("Ivana", 5); // overwrites old grade for Ivana

		Iterator<SimpleHashtable.TableEntry<String, Integer>> iter = examMarks.iterator();
		while (iter.hasNext()) {
			iter.next();
			iter.remove();
		}

		assertTrue(examMarks.isEmpty());
	}

}
