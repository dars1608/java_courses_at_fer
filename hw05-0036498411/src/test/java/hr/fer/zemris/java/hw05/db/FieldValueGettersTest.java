package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FieldValueGettersTest {
	private StudentRecord test;
	
	@Before
	public void setUp(){
		test = new StudentRecord("0036498411", "Darko", "Britvec", 5);
	}

	@Test
	public void testJmbag() {
		assertEquals("0036498411", FieldValueGetters.JMBAG.get(test));
	}
	
	@Test
	public void testFirstName() {
		assertEquals("Darko", FieldValueGetters.FIRST_NAME.get(test));
	}
	
	@Test
	public void testLastName() {
		assertEquals("Britvec", FieldValueGetters.LAST_NAME.get(test));
	}
	
	@Test(expected=NullPointerException.class)
	public void testNull() {
		FieldValueGetters.JMBAG.get(null);
	}
}
