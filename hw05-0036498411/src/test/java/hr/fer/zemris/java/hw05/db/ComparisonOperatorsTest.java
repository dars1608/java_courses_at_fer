package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import org.junit.Test;

public class ComparisonOperatorsTest {

	@Test
	public void testEquals() {
		assertTrue(ComparisonOperators.EQUALS.satisfied("abcd", "abcd"));
		assertFalse(ComparisonOperators.EQUALS.satisfied("abcd", "aabcd"));
	}
	
	@Test
	public void testNotEquals() {
		assertFalse(ComparisonOperators.NOT_EQUALS.satisfied("abcd", "abcd"));
		assertTrue(ComparisonOperators.NOT_EQUALS.satisfied("abcd", "aabcd"));
	}

	@Test
	public void testLessOrEquals() {
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("abcd", "abcd"));
		assertTrue(ComparisonOperators.LESS_OR_EQUALS.satisfied("aacd", "abcd"));
		assertFalse(ComparisonOperators.LESS_OR_EQUALS.satisfied("abcd", "aabcd"));
	}
	
	@Test
	public void testGreaterOrEquals() {
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("abcd", "abcd"));
		assertTrue(ComparisonOperators.GREATER_OR_EQUALS.satisfied("accd", "abcd"));
		assertFalse(ComparisonOperators.GREATER_OR_EQUALS.satisfied("abcd", "adbcd"));
	}
	
	@Test
	public void testGreater() {
		assertTrue(ComparisonOperators.GREATER.satisfied("accd", "abcd"));
		assertFalse(ComparisonOperators.GREATER.satisfied("abcd", "adbcd"));
	}
	
	@Test
	public void testLess() {
		assertTrue(ComparisonOperators.LESS.satisfied("aacd", "abcd"));
		assertFalse(ComparisonOperators.LESS.satisfied("abcd", "aabcd"));
	}
	
	@Test
	public void testLike() {
		assertTrue(ComparisonOperators.LIKE.satisfied("abcd", "abcd"));
		assertTrue(ComparisonOperators.LIKE.satisfied("abcd", "*bcd"));
		assertTrue(ComparisonOperators.LIKE.satisfied("abcd", "abc*"));
		assertTrue(ComparisonOperators.LIKE.satisfied("abcd", "*"));
		assertTrue(ComparisonOperators.LIKE.satisfied("2bcd", "*bcd"));
		assertTrue(ComparisonOperators.LIKE.satisfied("Žbcd", "*bcd"));
		assertTrue(ComparisonOperators.LIKE.satisfied("ćbcd", "*bcd"));
		assertTrue(ComparisonOperators.LIKE.satisfied("ab - cd", "ab*cd"));
		assertFalse(ComparisonOperators.LESS.satisfied("abcd", "aabcd"));
		assertFalse(ComparisonOperators.LESS.satisfied("abcd", "aa*cd"));
	}
	
	@Test(expected=NullPointerException.class)
	public void testNull() {
		ComparisonOperators.LESS.satisfied(null, null);
	}

}
