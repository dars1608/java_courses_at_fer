package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConditionalExpressionTest {
	private ConditionalExpression test;
	
	@Before
	public void setUp() throws Exception {
		test = new ConditionalExpression(FieldValueGetters.FIRST_NAME, "Darko", ComparisonOperators.EQUALS);
	}

	@Test
	public void test() {
		assertEquals(FieldValueGetters.FIRST_NAME, test.getGetterStrategy());
		assertEquals("Darko", test.getLiteral());
		assertEquals(ComparisonOperators.EQUALS, test.getComparisonStrategy());
	}

}
