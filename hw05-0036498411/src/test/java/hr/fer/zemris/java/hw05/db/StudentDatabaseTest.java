package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

public class StudentDatabaseTest {

	@Test
	public void testAllRecords() {
		final IFilter trueForAll = (i)->true;
		final IFilter falseForAll = (i)->false;
		
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get(PATH_NAME), StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("Error reading file");
			System.exit(1);
		}
		
		StudentDatabase database = new StudentDatabase(lines);	
		int sizeBefore = lines.size();
		List<StudentRecord> records = database.filter(trueForAll);
		int sizeAfter = records.size();
		
		Collections.sort(records, Comparator.comparing(StudentRecord::getJmbag));
		for(int i = 0, size = records.size(); i<size; i++) {
			StudentRecord record = records.get(i);
			assertTrue(lines.get(i), lines.get(i).contains(record.getJmbag()));
		}
		assertEquals(sizeBefore, sizeAfter);
		
		records = database.filter(falseForAll);
		assertEquals(0, records.size());
	}
	
	private static final String PATH_NAME = ".\\src\\main\\resources\\database.txt";

}
