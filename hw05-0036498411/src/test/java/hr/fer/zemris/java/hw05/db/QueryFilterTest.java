package hr.fer.zemris.java.hw05.db;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class QueryFilterTest {
	private QueryFilter test;
	private StudentRecord[] records;
	
	@Before
	public void setUp() throws Exception {
		List<ConditionalExpression> list = new LinkedList<>();
		records = new StudentRecord[20];
		
		for(int i = 0; i<20; i++) {
			list.add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "darko"+i, ComparisonOperators.LESS));
			records[i] = new StudentRecord("000000000"+i, "Marko"+i, "Marić", 5);
		}
		test = new QueryFilter(list);
	}

	@Test
	public void test() {
		for(int i = 0; i<20; i++) {
			assertTrue(test.accepts(records[i]));
		}
	}

}
