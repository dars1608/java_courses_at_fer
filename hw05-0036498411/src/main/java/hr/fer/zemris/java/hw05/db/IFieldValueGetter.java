package hr.fer.zemris.java.hw05.db;

/**
 * This interface defines strategy which is responsible for obtaining a
 * requested field value from given {@link StudentRecord}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IFieldValueGetter {
	
	/**
	 * This method is used to obtain a field value represented by this
	 * {@link IFieldValueGetter} from given {@link StudentRecord}.
	 * 
	 * @param record
	 *            {@link StudentRecord} from which field value is obtained
	 * @return Obtained field value
	 */
	public String get(StudentRecord record);

}
