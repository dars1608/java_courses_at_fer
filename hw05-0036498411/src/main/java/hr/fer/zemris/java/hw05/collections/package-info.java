/**
 * This package contains classes which represent parameterized collections, used
 * for solving fifth homework assignment.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw05.collections;