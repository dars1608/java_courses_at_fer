package hr.fer.zemris.java.hw05.db;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * This class represents parser used for parsing queries which are used for
 * fetching records from {@link StudentDatabase}. Query must start with a key
 * word "query" following one or many conditional expressions. Conditional
 * expression must consist of variable name, operator and string literal, after
 * which may or may not be a key word "AND" which is used for chaining
 * conditional expressions.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class QueryParser {
	
	/** Lexer used for generating {@link QueryToken} objects */
	private QueryLexer lexer;
	/** Query string */
	private String input;
	/** String for direct querry */
	private String jmbag;
	/** List of conditional expressions */
	private LinkedList<ConditionalExpression> query;
	/** Flag used for checking if input is already parsed*/
	private boolean isParsed;

	/**
	 * Constructor method for class {@link QueryParser}. It gets query string
	 * through constructor.
	 * 
	 * @param input
	 *            Query string
	 */
	public QueryParser(String input) {
		input = Objects.requireNonNull(input).trim();
		this.input = input;
		lexer = new QueryLexer(input);
		query = new LinkedList<>();
		isParsed = false;
	}

	/**
	 * This method must return true if query was of the form jmbag="xxx" (i.e. it
	 * must have only one comparison, on attribute jmbag, and operator must be
	 * equals). It also starts parsing process if input isn't already parsed.
	 * 
	 * @return <code>true</code> if query was direct, <code>false</code> otherwise.
	 */
	public boolean isDirectQuery() {
		if (jmbag == null) {
			if (!isParsed) {
				isParsed = true;
				parseQuery();
			}

			if (query.size() == 1) {
				ConditionalExpression ce = query.get(0);
				IComparisonOperator compOp = ce.getComparisonStrategy();
				IFieldValueGetter getter = ce.getGetterStrategy();

				if (compOp.equals(ComparisonOperators.EQUALS) && getter.equals(FieldValueGetters.JMBAG)) {
					jmbag = ce.getLiteral();
					return true;
				}
			}
			return false;
		} else {
			return true;
		}
	}

	/**
	 * This method must return the string jmbag which was given in equality
	 * comparison in direct query.
	 * 
	 * @return Queried jmbag
	 * @throws IllegalStateException
	 *             if the query was not direct one
	 */
	public String getQueriedJMBAG() {
		if (isDirectQuery()) {
			return jmbag;
		}

		throw new IllegalStateException("Query isn't direct");
	}

	/**
	 * This method returns list of {@link ConditionalExpression} objects generated
	 * based on parsed input.
	 * 
	 * @return List
	 */
	public List<ConditionalExpression> getQuery() {
		if (!isParsed) {
			parseQuery();
		}
		return query;
	}

	/**
	 * This method is used internally for parsing input query. It fills
	 * <code>query</code> list with parsed conditional expressions.
	 */
	private void parseQuery() {
		IFieldValueGetter getter = null;
		IComparisonOperator compOp = null;
		String literal = null;
		QueryToken token = null;

		while (true) {
			token = getNextToken();
			QueryTokenType type = token.getType();
			if (type == QueryTokenType.VARIABLE) {
				getter = parseGetter(token);
			} else {
				throw new ParserException(INVALID_INPUT + input);
			}

			token = getNextToken();
			type = token.getType();
			if (type == QueryTokenType.OPERATOR || type == QueryTokenType.LIKE) {
				compOp = parseComparisonOperator(token);
			} else {
				throw new ParserException(INVALID_INPUT + input);
			}

			token = getNextToken();
			type = token.getType();
			if (type == QueryTokenType.LITERAL) {
				literal = token.getValue();
			}else {
				throw new ParserException(INVALID_INPUT + input);
			}

			query.add(new ConditionalExpression(getter, literal, compOp));

			token = getNextToken();
			type = token.getType();
			if (type == QueryTokenType.EOL) {
				isParsed = true;
				break;
			} else if (type != QueryTokenType.AND) {
				throw new ParserException(INVALID_INPUT + input);
			}
		}

	}

	/**
	 * This method is used for parsing {@link IComparisonOperator} objects.
	 * 
	 * @param token
	 *            Current token
	 * @return parsed {@link IComparisonOperator} object
	 */
	private IComparisonOperator parseComparisonOperator(QueryToken token) {
		IComparisonOperator compOp = null;
		String operator = token.getValue();
		if (token.getType() == QueryTokenType.LIKE) {
			compOp = ComparisonOperators.LIKE;
		} else if (operator.equals(LESS)) {
			compOp = ComparisonOperators.LESS;
		} else if (operator.equals(LESS_OR_EQUALS)) {
			compOp = ComparisonOperators.LESS_OR_EQUALS;
		} else if (operator.equals(GREATER)) {
			compOp = ComparisonOperators.GREATER;
		} else if (operator.equals(GREATER_OR_EQUALS)) {
			compOp = ComparisonOperators.GREATER_OR_EQUALS;
		} else if (operator.equals(EQUALS)) {
			compOp = ComparisonOperators.EQUALS;
		} else if (operator.equals(NOT_EQUALS)) {
			compOp = ComparisonOperators.NOT_EQUALS;
		} else {
			throw new ParserException(INVALID_INPUT + input + ". Operator " + operator + " doesn't exist.");
		}
		return compOp;
	}

	/**
	 * This method is used for parsing {@link IFieldValueGetter} objects.
	 * 
	 * @param token
	 *            Current token
	 * @return parsed {@link IFieldValueGetter} object
	 */
	private IFieldValueGetter parseGetter(QueryToken token) {
		IFieldValueGetter getter = null;
		String name = token.getValue();
		if (name.equals(JMBAG_VAR)) {
			getter = FieldValueGetters.JMBAG;
		} else if (name.equals(FIRST_NAME_VAR)) {
			getter = FieldValueGetters.FIRST_NAME;
		} else if (name.equals(LAST_NAME_VAR)) {
			getter = FieldValueGetters.LAST_NAME;
		} else {
			throw new ParserException(INVALID_INPUT + input);
		}

		return getter;
	}

	/**
	 * This method is used internally for getting the next token from lexer.
	 * 
	 * @return next {@link QueryToken}
	 * @throws ParserException
	 *             if any {@link LexerException} occurs
	 */
	private QueryToken getNextToken() {
		try {
			return lexer.nextToken();
		} catch (LexerException | IllegalArgumentException e) {
			throw new ParserException(INVALID_INPUT + input + ". " + e.getLocalizedMessage(), e);
		}
	}
	
	/*-------------------constants------------------*/
	
	/** Message shown when parser finds an invalid input*/
	private static final String INVALID_INPUT = "Invalid input: ";
	/** Symbol representing comparison operator not equals*/
	private static final String NOT_EQUALS = "!=";
	/** Symbol representing comparison operator equals*/
	private static final String EQUALS = "=";
	/** Symbol representing comparison operator greater or equals*/
	private static final String GREATER_OR_EQUALS = ">=";
	/** Symbol representing comparison operator greater*/
	private static final String GREATER = ">";
	/** Symbol representing comparison operator less or equals*/
	private static final String LESS_OR_EQUALS = "<=";
	/** Symbol representing comparison operator less*/
	private static final String LESS = "<";
	/** String representing field name for last name*/
	private static final String LAST_NAME_VAR = "lastName";
	/** String representing field name for first name*/
	private static final String FIRST_NAME_VAR = "firstName";
	/** String representing field name for unique student number*/
	private static final String JMBAG_VAR = "jmbag";
}
