package hr.fer.zemris.java.hw05.collections;

import static java.lang.Math.log;
import static java.lang.Math.ceil;
import static java.lang.Math.pow;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

import static java.lang.Math.abs;

/**
 * This class represents simple implementation of a hash table structure. It
 * consists of entries of type {@link TableEntry} which represent pairs of keys
 * and values.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @param <K>
 *            type of key
 * @param <V>
 *            type of value
 */
public class SimpleHashtable<K, V> implements Iterable<SimpleHashtable.TableEntry<K, V>> {

	/** Backing array used for storing entries */
	private TableEntry<K, V>[] table;
	/** Number of stored entries */
	private int size;
	/** Current size and capacity ratio */
	private double loadFactor;
	/**
	 * Counter used for determine whether the collection has been changed while
	 * iterating.
	 */
	private int modificationCount;

	/**
	 * Constructor method for class {@link SimpleHashtable}. It sets initial
	 * capacity to closest power of 2 which is less or equal to given parameter.
	 *
	 * @param capacity
	 *            Initial capacity of hash table
	 * @throws IllegalArgumentException
	 *             if given capacity parameter is less than 1.
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int capacity) {
		if (capacity <= 0) {
			throw new IllegalArgumentException("Initial capaciti can't be less or equal to 0. Is " + capacity);
		}
		capacity = calculateCapacity(capacity);
		table = (TableEntry<K, V>[]) new TableEntry[capacity];
		size = 0;
	}

	/**
	 * Default constructor for class {@link SimpleHashtable}. It sets the capacity
	 * of the hash table to default value.
	 *
	 */
	public SimpleHashtable() {
		this(DEFAULT_CAPACITY);
	}

	/**
	 * This method is used for storing value at given key in hash table. Key must
	 * not be <code>null</code>, but value may be.
	 * 
	 * @param key
	 *            Key of an entry
	 * @param value
	 *            Value of an entry
	 * @throws NullPointerException
	 *             if given key reference is <code>null</code>.
	 */
	public void put(K key, V value) {
		setModified();
		Objects.requireNonNull(key);
		int slotIndex = getSlotIndex(key);
		TableEntry<K, V> slot = table[slotIndex];

		if (slot != null) {
			if (slot.key.equals(key)) {
				slot.value = value;
				return;
			}

			while (slot.next != null && !slot.next.key.equals(key)) {
				slot = slot.next;
			}

			if (slot.next != null && slot.next.key.equals(key)) {
				slot.next.value = value;
				return;

			} else {
				slot.next = new TableEntry<>(key, value);
			}

		} else {
			table[slotIndex] = new TableEntry<>(key, value);
		}

		size++;
		updateLoadFactor();
		if (checkLoadFactor()) {
			reallocateTable();
		}
	};

	/**
	 * This method is used for getting the value stored at given key. It returns
	 * <code>null</code> if there aren't any value stored under that key (it also
	 * returns <code>null</code> if <code>null</code> is stored under that key).
	 * 
	 * @param key
	 *            Key of an entry
	 * @return Value stored at given key
	 * @throws NullPointerException
	 *             if key reference is <code>null</code>
	 */
	public V get(Object key) {
		int slotIndex = getSlotIndex(key);
		if (slotIndex == NULL_KEY) {
			return null;
		}

		TableEntry<K, V> slot = findEntry(slotIndex, key);

		return slot == null ? null : slot.value;

	}

	/**
	 * This method checks how many entries are stored in hash table.
	 * 
	 * @return Number of stored entries
	 */
	public int size() {
		return size;
	}

	/**
	 * This method checks if given key is used in the hash table.
	 * 
	 * @param key
	 *            Checked key
	 * @return <code>true</code> if key is used, <code>false</code> otherwise
	 * @throws NullPointerException
	 *             if given key reference is null
	 */
	public boolean containsKey(Object key) {
		int slotIndex = getSlotIndex(key);
		if (slotIndex == NULL_KEY) {
			return false;
		}

		TableEntry<K, V> slot = findEntry(slotIndex, key);

		return slot == null ? false : true;
	}

	/**
	 * This method checks if given value is stored in the hash table.
	 * 
	 * @param value
	 *            Value which is checked
	 * @return <code>true</code> if value is stored in hash table, false otherwise
	 */
	public boolean containsValue(Object value) {
		for (int slotIndex = 0; slotIndex < table.length; slotIndex++) {
			TableEntry<K, V> slot = table[slotIndex];

			while (slot != null && slot.value != value && !slot.value.equals(value)) {
				slot = slot.next;
			}

			if (slot != null) {
				return true;
			}
		}

		return false;
	}

	/**
	 * This method removes entry with given key from the hash table. If there's no
	 * entry with given key, method does nothing.
	 * 
	 * @param key
	 *            Key of an entry which must be removed
	 * @throws NullPointerException
	 *             if given key reference is <code>null</code>.
	 */
	public void remove(Object key) {
		setModified();
		int slotIndex = getSlotIndex(key);
		if (slotIndex == NULL_KEY) {
			return;
		}

		TableEntry<K, V> slot = table[slotIndex];
		if (slot == null) {
			return;
		}
		if (slot.key.equals(key)) {
			table[slotIndex] = slot.next;
			size--;
			return;
		}

		while (slot.next != null && !slot.next.key.equals(key)) {
			slot = slot.next;
		}

		if (slot.next != null) {
			slot.next = slot.next.next;
			size--;
		}

		updateLoadFactor();
	}

	/**
	 * This method checks if hash table doesn't contain any entries.
	 * 
	 * @return <code>true</code> if hash table is empty, <code>false</code>
	 *         otherwise.
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("[");
		for (TableEntry<K, V> entry : table) {
			if (entry != null) {
				sb.append(entry);
				sb.append(", ");

				while (entry != null && entry.next != null) {
					entry = entry.next;
					sb.append(entry);
					sb.append(", ");
				}
			} 
		}
		sb.delete(sb.length() - 2, sb.length());
		sb.append(']');

		return sb.toString();

	}

	/**
	 * This method clears all entries from the hash table.
	 */
	public void clear() {
		setModified();
		for (int slotIndex = 0; slotIndex < table.length; slotIndex++) {
			table[slotIndex] = null;
		}
		size = 0;
		updateLoadFactor();
	}

	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}

	/**
	 * This class represents an entry of the hash table. It consists of a pair of
	 * key and value. Key cannot be <code>null</code>, but value can.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 * 
	 * @param <P>
	 *            type of key
	 * @param <V>
	 *            type of value
	 */
	public static class TableEntry<P, R> {
		/** Key of an entry */
		private final P key;
		/** Value of an entry */
		private R value;
		/** Reference pointing to next entry stored at the same slot as this */
		private TableEntry<P, R> next;

		/**
		 * Constructor method for class {@link TableEntry}. Key of an entry can't be
		 * null reference.
		 *
		 * @param key
		 *            Entry key
		 * @param value
		 *            Entry value
		 * @throws NullPointerException
		 *             if given key reference is <code>null</code>.
		 */
		public TableEntry(P key, R value) {
			this.key = Objects.requireNonNull(key);
			this.value = value;
			next = null;
		}

		/**
		 * Getter method for the value.
		 * 
		 * @return the value
		 */
		public R getValue() {
			return value;
		}

		/**
		 * Setter method for the value.
		 * 
		 * @param value
		 *            the value to set
		 */
		public void setValue(R value) {
			this.value = value;
		}

		/**
		 * Getter method for the key.
		 * 
		 * @return the key
		 */
		public P getKey() {
			return key;
		}

		@Override
		public String toString() {
			String vString = value==null?"null":value.toString();
			return String.format("%s=%s", key.toString(), vString);
		}

	}

	/**
	 * This class represents {@link Iterator} implementation for
	 * {@link SimpleHashtable}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>> {

		/** Index of current slot in hash table */
		private int currentSlot;
		/** Current entry */
		private TableEntry<K, V> currentEntry;
		/**
		 * Flag which determines whether method {@link Iterator#hasNext()} has been
		 * called.
		 */
		private boolean flagNext;
		/**
		 * Flag which determines whether method {@link Iterator#next()} has been called.
		 */
		private boolean flagRemove;
		/** Number of modifications made until creating an iterator */
		private int currentModifications;

		/**
		 * Default constructor for class {@link IteratorImpl}. It creates an
		 * implementation of an {@link Iterator} which can be used for iterating through
		 * {@link SimpleHashtable}.
		 */
		public IteratorImpl() {
			currentSlot = -1;
			currentEntry = null;
			flagNext = true;
			flagRemove = false;
			currentModifications = modificationCount;
		}

		/**
		 * This method checks whether the {@link Iterator} has next {@link TableEntry}
		 * object. It also sets <code>currentEntry</code> to next entry if method
		 * {@link IteratorImpl#next()} has been called.
		 * 
		 */
		public boolean hasNext() {
			if (currentModifications != modificationCount) {
				throw new ConcurrentModificationException("Collection has been modified.");
			}

			if (flagNext) {
				flagNext = false;

				if (currentEntry == null) {
					while (currentEntry == null && currentSlot < table.length) {
						currentSlot++;
						if (currentSlot < table.length) {
							currentEntry = table[currentSlot];
						}

					}

					if (currentSlot >= table.length) {
						return false;
					}

				} else {
					currentEntry = currentEntry.next;

					if (currentEntry == null) {
						flagNext = true;
						return hasNext();
					}
				}
			}
			return true;
		}

		@Override
		public SimpleHashtable.TableEntry<K, V> next() {
			if (hasNext()) {
				flagNext = true;
				flagRemove = true;
				return currentEntry;
			}

			throw new NoSuchElementException("Iterator came to an end");
		}

		@Override
		public void remove() {
			if (currentModifications != modificationCount) {
				throw new ConcurrentModificationException("Collection has been modified.");
			}
			if (flagRemove) {
				flagRemove = false;
				if (currentEntry != null) {
					SimpleHashtable.this.remove(currentEntry.key);
					currentModifications = modificationCount;
				}
				return;
			}

			throw new IllegalStateException("Method IteratorImpl#next() hasn't been called.");
		}
	}

	/*--------------------constants---------------------------*/

	/** Default capacity of hash table */
	private static final int DEFAULT_CAPACITY = 16;
	/** Label that */
	private static final int NULL_KEY = -1;
	/** Maximal ratio of size and capacity */
	private static final double MAX_LOAD_FACTOR = 0.75;

	/*--------------------helping methods----------------------*/

	/**
	 * This method is used for calculating new capacity of a hash table. New
	 * capacity must be less or equal to given parameter, and also be a power of
	 * number 2.
	 * 
	 * @param capacity
	 * @return
	 */
	private static int calculateCapacity(int capacity) {
		int pot2 = (int) ceil(log(capacity) / log(2));
		int newCapacity = (int) pow(2, pot2);

		return newCapacity;
	}

	/**
	 * This method is used for calculating index of a slot in hash table.
	 * 
	 * @param key
	 *            Key of an entry
	 * @return Index of a slot in a hash map
	 */
	private int getSlotIndex(Object key) {
		return key == null ? NULL_KEY : (abs(key.hashCode()) % table.length);
	}

	/**
	 * This method is used for finding entry on given slot index with provided key.
	 * 
	 * @param slotIndex
	 *            Slot index
	 * @param key
	 *            Key of an entry
	 * @return {@link TableEntry} under given key, <code>null</code> if there's no
	 *         such entry.
	 */
	private TableEntry<K, V> findEntry(int slotIndex, Object key) {
		TableEntry<K, V> slot = table[slotIndex];

		while (slot != null && !slot.key.equals(key)) {
			slot = slot.next;
		}

		return slot;
	}

	/**
	 * This method is used internally for updating <code>loadFactor</code>. This
	 * method is called whenever some entry is added or deleted from the table.
	 */
	private void updateLoadFactor() {
		loadFactor = size * 1.0 / table.length;
	}

	/**
	 * This method is used internally for checking if current load factor reached
	 * maximal load factor {@value #MAX_LOAD_FACTOR}.
	 * 
	 * @return <code>true</code> if <code>loadFactor</code> is greater or equal
	 *         {@value #MAX_LOAD_FACTOR}, <code>false</code> otherwise.
	 */
	private boolean checkLoadFactor() {
		return loadFactor >= MAX_LOAD_FACTOR;
	}

	/**
	 * This method is used internally to allocate new array of {@link TableEntry}
	 * objects, assign that array to field <code>table</code> and then fill that
	 * array with old values.
	 */
	@SuppressWarnings("unchecked")
	private void reallocateTable() {
		TableEntry<K, V>[] currentTable = table;
		size = 0;
		table = (TableEntry<K, V>[]) new TableEntry[table.length * 2];

		for (int slotIndex = 0; slotIndex < currentTable.length; slotIndex++) {
			TableEntry<K, V> entry = currentTable[slotIndex];
			if (entry == null) {
				continue;
			}

			put(entry.key, entry.value);
			while (entry.next != null) {
				entry = entry.next;
				put(entry.key, entry.value);
			}
		}
	}

	/**
	 * This method updates modification counter whenever the hash map is modified.
	 */
	private void setModified() {
		modificationCount++;
	}

	/*-----------methods used for testing-----------------------*/

	/**
	 * This method is used for testing only.
	 * 
	 * @return Backing array of the {@link SimpleHashtable}.
	 */
	TableEntry<K, V>[] getTable() {
		return table;
	}

}
