/**
 * This package contains classes used to simulate working with simple database.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw05.db;