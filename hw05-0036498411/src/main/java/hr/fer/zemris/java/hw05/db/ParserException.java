package hr.fer.zemris.java.hw05.db;

/**
 * This class represents exception which may occur while working with
 * {@link QueryParser}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ParserException extends RuntimeException {
	
	/** Generated serial version */
	private static final long serialVersionUID = 6911370659986981015L;

	/**
	 * Default constructor for {@link ParserException}.
	 */
	public ParserException() {

	}

	/**
	 * Constructor method for {@link ParserException}.
	 * 
	 * @param message
	 *            Detail message.
	 */
	public ParserException(String message) {
		super(message);
	}

	/**
	 * Constructor method for {@link ParserException}.
	 * 
	 * @param t
	 *            Cause.
	 */
	public ParserException(Throwable t) {
		super(t);
	}

	/**
	 * Constructor method for {@link ParserException}.
	 * 
	 * @param message
	 *            Detail message.
	 * @param t
	 *            Cause.
	 */
	public ParserException(String message, Throwable t) {
		super(message, t);
	}

}
