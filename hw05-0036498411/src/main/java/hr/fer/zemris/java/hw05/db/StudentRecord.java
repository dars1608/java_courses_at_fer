package hr.fer.zemris.java.hw05.db;

/**
 * This class represents a record of some student in database. It contains
 * informations such as: jmbag, first name, last name and final grade.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class StudentRecord {
	
	/** Students unique identification number */
	private final String jmbag;
	/** Students first name */
	private final String firstName;
	/** Students last name */
	private final String lastName;
	/** Students final grade */
	private final int finalGrade;

	/**
	 * Constructor method for class {@link StudentRecord}.
	 * 
	 * @param jmbag
	 *            Students unique identification number
	 * @param firstName
	 *            Students first name
	 * @param lastName
	 *            Students last name
	 * @param finalGrade
	 *            Students final grade
	 */
	public StudentRecord(String jmbag, String firstName, String lastName, int finalGrade) {
		super();
		this.jmbag = jmbag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.finalGrade = finalGrade;
	}

	/**
	 * Getter method for the <code>jmbag</code>.
	 * 
	 * @return the jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Getter method for the <code>firstName</code>.
	 * 
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Getter method for the <code>lastName</code>.
	 * 
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Getter method for the <code>finalGrade</code>.
	 * 
	 * @return the finalGrade
	 */
	public int getFinalGrade() {
		return finalGrade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StudentRecord other = (StudentRecord) obj;
		if (jmbag == null) {
			if (other.jmbag != null) {
				return false;
			}
		} else if (!jmbag.equals(other.jmbag)) {
			return false;
		}
		return true;
	}

}
