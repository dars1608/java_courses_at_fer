package hr.fer.zemris.java.hw05.db;

/**
 * This interface defines strategy for generating queries. Object which
 * implements this interface defines concrete strategy for each comparison
 * operator (>,<,>=, <=, like ect.)
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IComparisonOperator {
	
	/**
	 * This method checks whether the arguments satisfy the condition defined by
	 * this {@link IComparisonOperator}.
	 * 
	 * @param value1
	 *            Left string
	 * @param value2
	 *            Right string
	 * @return <code>true</code> if condition is satisfied, <code>false</code>
	 *         otherwise
	 */
	public boolean satisfied(String value1, String value2);
}
