package hr.fer.zemris.java.hw05.db;

/**
 * <p>
 * This enumeration represents types of tokens which are generated by
 * {@link QueryLexer}.
 * </p>
 * 
 * <p>
 * Enumeration labels are:
 * 
 * <li>{@link QueryTokenType#AND}
 * <li>{@link QueryTokenType#LIKE}
 * <li>{@link QueryTokenType#OPERATOR}
 * <li>{@link QueryTokenType#LITERAL}
 * <li>{@link QueryTokenType#EOL}
 * 
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public enum QueryTokenType {

	/** Operator token */
	OPERATOR,
	/** Operator used for matching with pattern */
	LIKE,
	/** Variable token */
	VARIABLE,
	/** Key word AND */
	AND,
	/** Quoted string */
	LITERAL,
	/** End of line */
	EOL
}
