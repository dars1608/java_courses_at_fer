package hr.fer.zemris.java.hw05.db;

/**
 * This class is effectively an enumeration of {@link IFieldValueGetter}
 * objects, used for obtaining field values from {@link StudentRecord} objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class FieldValueGetters {
	
	/**
	 * {@link IFieldValueGetter} used to obtain first name value from
	 * {@link StudentRecord}.
	 */
	public static IFieldValueGetter FIRST_NAME;
	/**
	 * {@link IFieldValueGetter} used to obtain last name value from
	 * {@link StudentRecord}.
	 */
	public static IFieldValueGetter LAST_NAME;
	/**
	 * {@link IFieldValueGetter} used to obtain jmbag value from
	 * {@link StudentRecord}.
	 */
	public static IFieldValueGetter JMBAG;

	static {
		FIRST_NAME = (r) -> r.getFirstName();
		LAST_NAME = (r) -> r.getLastName();
		JMBAG = (r) -> r.getJmbag();
	}

}
