package hr.fer.zemris.java.hw05.db;

/**
 * <p>
 * This class is effectively an enumeration of {@link IComparisonOperator}
 * objects, used to compare two strings.
 * </p>
 * 
 * <p>
 * Operators are described in this class as static fields:
 * 
 * <li>{@link ComparisonOperators#LESS}
 * <li>{@link ComparisonOperators#LESS_OR_EQUALS}
 * <li>{@link ComparisonOperators#GREATER}
 * <li>{@link ComparisonOperators#GREATER_OR_EQUALS}
 * <li>{@link ComparisonOperators#EQUALS}
 * <li>{@link ComparisonOperators#NOT_EQUALS}
 * <li>{@link ComparisonOperators#LIKE}
 * 
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class ComparisonOperators {

	/**
	 * Determines whether some string is less than other based on
	 * {@link String#compareTo(String)} method.
	 */
	public static IComparisonOperator LESS;
	/**
	 * Determines whether some string is less or equal than other based on
	 * {@link String#compareTo(String)} method.
	 */
	public static IComparisonOperator LESS_OR_EQUALS;
	/**
	 * Determines whether some string is greater than other based on
	 * {@link String#compareTo(String)} method.
	 */
	public static IComparisonOperator GREATER;
	/**
	 * Determines whether some string is greater or equal than other based on
	 * {@link String#compareTo(String)} method.
	 */
	public static IComparisonOperator GREATER_OR_EQUALS;
	/**
	 * Determines whether some string is equal based on
	 * {@link Object#equals(Object)} method.
	 */
	public static IComparisonOperator EQUALS;
	/**
	 * Determines whether some string is not equal based on
	 * {@link Object#equals(Object)} method.
	 */
	public static IComparisonOperator NOT_EQUALS;
	/**
	 * Determines whether some string is like based on
	 * {@link String#matches(String)} method.
	 */
	public static IComparisonOperator LIKE;

	static {
		LESS = (s1, s2) -> s1.compareTo(s2) < 0;
		
		LESS_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) <= 0;
		
		GREATER = (s1, s2) -> s1.compareTo(s2) > 0;
		
		GREATER_OR_EQUALS = (s1, s2) -> s1.compareTo(s2) >= 0;
		
		EQUALS = (s1, s2) -> s1.equals(s2);
		
		NOT_EQUALS = (s1, s2) -> !s1.equals(s2);
		
		LIKE = new IComparisonOperator() {

			@Override
			public boolean satisfied(String value1, String value2) {
				final String VALID_CHARS = "[a-z0-9\\sčćžđšČĆŽŠĐ-]*";
				StringBuilder sb = new StringBuilder();
				char[] parts = value2.toCharArray();
				boolean wildCard = true;

				sb.append('^');
				for (char c : parts) {
					if (c == '*' && wildCard) {
						wildCard = false;
						sb.append(VALID_CHARS);
					} else if (c == '*' && !wildCard) {
						throw new IllegalArgumentException("Too many wildcards in pattern " + value2);
					} else {
						sb.append(c);
					}
				}
				sb.append('$');

				return value1.toLowerCase().matches(sb.toString().toLowerCase());
			}

		};
	}

}
