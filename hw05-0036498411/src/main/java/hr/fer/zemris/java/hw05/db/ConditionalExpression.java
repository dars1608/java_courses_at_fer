package hr.fer.zemris.java.hw05.db;

import java.util.Objects;

/**
 * This class describes one conditional expression used for generating queries
 * for {@link StudentDatabase}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ConditionalExpression {

	/** Getter strategy */
	private final IFieldValueGetter getterStrategy;
	/** Literal used in comparison */
	private final String literal;
	/** Comparison strategy */
	private final IComparisonOperator comparisonStrategy;

	/**
	 * Constructor method for class {@link ConditionalExpression}.
	 * 
	 * @param getterStrategy
	 *            {@link IFieldValueGetter} object
	 * @param literal
	 *            string used in comparison
	 * @param comparisonStrategy
	 *            {@link IComparisonOperator} object
	 * @throws NullPointerException
	 *             if any argument is <code>null</code>
	 */
	public ConditionalExpression(IFieldValueGetter getterStrategy, String literal,
			IComparisonOperator comparisonStrategy) {
		super();
		this.getterStrategy = Objects.requireNonNull(getterStrategy);
		this.literal = Objects.requireNonNull(literal);
		this.comparisonStrategy = Objects.requireNonNull(comparisonStrategy);
	}

	/**
	 * Getter method for the <code>getterStrategy</code>.
	 * 
	 * @return the getterStrategy
	 */
	public IFieldValueGetter getGetterStrategy() {
		return getterStrategy;
	}

	/**
	 * Getter method for the <code>literal</code>.
	 * 
	 * @return the literal
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Getter method for the <code>comparisonStrategy</code>.
	 * 
	 * @return the comparisonStrategy
	 */
	public IComparisonOperator getComparisonStrategy() {
		return comparisonStrategy;
	}
}
