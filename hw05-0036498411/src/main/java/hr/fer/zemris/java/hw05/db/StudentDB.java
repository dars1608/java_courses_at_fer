package hr.fer.zemris.java.hw05.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * <p>
 * This program demonstrates functionality of {@link StudentDatabase} and all
 * other database components. It loads entries into the database from text file
 * "database.txt", and then it communicates with user via standard input/output.
 * </p>
 * 
 * <p>
 * User must write queries in order to obtain records from database. Query must
 * start with a key word "query" following one or many conditional expressions.
 * Conditional expression must consist of variable name, operator and string
 * literal, after which may or may not be a key word "AND" which is used for
 * chaining conditional expressions.
 * </p>
 * 
 * <p>
 * Type "exit" to quit the program.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class StudentDB {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		List<String> lines = null;

		try {
			lines = Files.readAllLines(Paths.get(PATH_NAME),
					StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println(ERROR_MESSAGE);
			System.exit(1);
		}

		StudentDatabase database = new StudentDatabase(
				lines);

		try (Scanner sc = new Scanner(System.in)) {
			while (true) {
				String line = null;

				System.out.println(INPUT_SYMBOL);
				try {
					line = sc.nextLine();
				} catch (Exception err) {
					err.printStackTrace();
					break;
				}

				if (line.toLowerCase()
						.equals(EXIT_KEYWORD)) {
					if (line.equals(EXIT_KEYWORD)) {
						System.out.println(EXIT_MESSAGE);
						break;
					} else {
						System.err.println(
								EXIT_ERROR_MESSAGE);
						continue;
					}
				}

				if (!line.startsWith(QUERY_KEYWORD)) {
					if (line.toLowerCase()
							.startsWith(QUERY_KEYWORD)) {
						System.err.println(
								QUERY_ERROR_MESSAGE);
					} else {
						System.err.println(
								INVALID_COMMAND_MESSAGE);
					}
					continue;
				}

				line = line.substring(5).trim();
				QueryParser parser = null;
				boolean isDirectQuery = false;
				try {
					parser = new QueryParser(line);
					isDirectQuery = parser.isDirectQuery();
				} catch (ParserException ex) {
					System.err.println(
							ex.getLocalizedMessage());
					continue;
				}

				if (isDirectQuery) {
					System.out.println(
							INDEX_RETRIVAL_MESSAGE);
					StudentRecord record = database
							.forJMBAG(parser
									.getQueriedJMBAG());

					printNumberOfRecords(
							record == null ? 0 : 1);
					if (record != null) {
						System.out.println(DIVIDER);
						printRecord(record);
						System.out.println(DIVIDER);
					}

				} else {
					IFilter filter = new QueryFilter(
							parser.getQuery());
					List<StudentRecord> list = database
							.filter(filter);
					int size = list.size();

					printNumberOfRecords(size);
					if (size != 0) {
						System.out.println(DIVIDER);
						for (StudentRecord record : list) {
							printRecord(record);
						}
						System.out.println(DIVIDER);
					}
				}
			}
		}

	}

	/**
	 * This class is used to print one {@link StudentRecord}. Space reserved for
	 * <code>firstName</code> and <code>lastName</code> is 20 characters, 10
	 * characters for <code>jmbag</code> and 1 character for
	 * <code>finalGrade</code>.
	 * 
	 * @param record
	 *            {@link StudentRecord} to print
	 */
	private static void printRecord(StudentRecord record) {
		String toPrint = String.format(
				"| %s | 20%s | 20%s | %1d |",
				record.getJmbag(), record.getLastName(),
				record.getFirstName(),
				record.getFinalGrade());

		System.out.println(toPrint);
	}

	/**
	 * This class is used to print a number of records obtained from database.
	 * 
	 * @param number
	 *            Number of obtained records
	 */
	private static void printNumberOfRecords(int number) {
		System.out.println(String
				.format("Records selected: %d", number));
	}

	/*-----------------constants------------------*/

	/** Error reading file message */
	private static final String ERROR_MESSAGE = "Error while reading from file.";
	/** Text which will be shown when exiting the program */
	private static final String EXIT_MESSAGE = "Goodbye!";
	/** Input which must be entered to exit the program */
	private static final String EXIT_KEYWORD = "exit";
	/** First word of every query */
	private static final String QUERY_KEYWORD = "query ";
	/** Symbol which indicates that program expects some input */
	private static final String INPUT_SYMBOL = "> ";
	/** Path name of the test file, which contains student records */
	private static final String PATH_NAME = "./src/main/resources/database.txt";
	/** String used for formated printing */
	private static final String DIVIDER = "+============+======================+======================+===+";
	/** Message shown when we access some record directly, by jmbag */
	private static final String INDEX_RETRIVAL_MESSAGE = "Using index for record retrieval.";
	/** Message shown when user types "query" in different casing */
	private static final String QUERY_ERROR_MESSAGE = "Query keyword is case sensitive. Type \""
			+ QUERY_KEYWORD
			+ "\" at the start of every query.";
	/** Message shown when user types "exit" in different casing */
	private static final String EXIT_ERROR_MESSAGE = "Exit command is case sensitive. Type \""
			+ EXIT_KEYWORD + "\" to quit.";
	/**
	 * Message shown when user types an invalid command (something other than query
	 * or exit)
	 */
	private static final String INVALID_COMMAND_MESSAGE = "Invalid command.";
}
