package hr.fer.zemris.java.hw05.db;

import java.util.List;
import java.util.Objects;

/**
 * This class is an implementation of {@link IFilter}, which is used for
 * filtering {@link StudentRecord} objects from {@link StudentDatabase}. Based
 * on list of {@link ComparisonOperators}, it accepts records which satisfy
 * conditions.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class QueryFilter implements IFilter {

	/** List of conditional expressions */
	private List<ConditionalExpression> conditions;

	/**
	 * Constructor method for class {@link QueryFilter}. It accepts list of
	 * {@link ConditionalExpression} object used to determine which
	 * {@link StudentRecord} objects are acceptable.
	 * 
	 * @param conditions
	 *            List of conditional expressions
	 * @throws NullPointerException
	 *             if list reference is <code>null</code>
	 */
	public QueryFilter(List<ConditionalExpression> conditions) {
		super();
		this.conditions = Objects.requireNonNull(conditions);
	}

	@Override
	public boolean accepts(StudentRecord record) {
		for (ConditionalExpression condition : conditions) {
			String value = condition.getGetterStrategy().get(record);
			String literal = condition.getLiteral();
			if (!condition.getComparisonStrategy().satisfied(value, literal)) {
				return false;
			}
		}
		return true;
	}

}
