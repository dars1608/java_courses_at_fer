package hr.fer.zemris.java.hw05.db;

/**
 * This interface describes functionality of some object used for filtering
 * {@link StudentRecord} objects in {@link StudentDatabase}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IFilter {

	/**
	 * This method is used to determine if given record undertakes the condition
	 * defined by this filter.
	 * 
	 * @param record
	 * @return
	 */
	public boolean accepts(StudentRecord record);
}
