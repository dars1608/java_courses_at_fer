package hr.fer.zemris.java.hw05.db;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.hw05.collections.SimpleHashtable;
import hr.fer.zemris.java.hw05.collections.SimpleHashtable.TableEntry;

/**
 * <p>This class describes simple database for storing some records of students.
 * Entries of this database are {@link StudentRecord} objects.</p>
 * 
 * <p> Primary key used for obtaining data directly is <code>jmbag</code>, unique
 * identification label of every student. We can also search entries by <code>
 * firstName</code> and <code>lastName</code>.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class StudentDatabase {

	/** Backing collection used for storing {@link StudentRecord} objects */
	private SimpleHashtable<String, StudentRecord> records = new SimpleHashtable<>();

	/**
	 * Constructor method for class {@link StudentDatabase}. It's parameter is
	 * {@link List} of strings representing one line of text which contains
	 * informations used to construct {@link StudentRecord} object. Lines are parsed
	 * and then stored in database.
	 * 
	 * @param lines
	 *            {@link List} of lines contains informations about some students
	 */
	public StudentDatabase(List<String> lines) {
		for (String line : lines) {
			Matcher matcher = PATTERN.matcher(line);
			if (matcher.find()) {
				String jmbag = matcher.group(1);
				String lastName = matcher.group(2);
				String firstName = matcher.group(3);
				int finalGrade = Integer.parseInt(matcher.group(4));
				records.put(jmbag, new StudentRecord(jmbag, firstName, lastName, finalGrade));
			}

		}
	}

	/**
	 * This method is used to obtain requested record in O(1); if record does not
	 * exists, the method returns <code>null</code>.
	 * 
	 * @param jmbag
	 *            Unique identification number of requested student
	 * @return {@link StudentRecord} with given jmbag
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return records.get(jmbag);
	}

	/**
	 * The method filter in {@link StudentDatabase} loops through all student
	 * records in its internal list; it calls {@link IFilter#accepts(StudentRecord)}
	 * method on given filter-object with current record; each record for which
	 * accepts returns <code>true</code> is added to temporary list and this list is
	 * then returned by the filter method.
	 * 
	 * @param filter {@link IFilter} object
	 * @return List of records filtered by given filter
	 */
	public List<StudentRecord> filter(IFilter filter) {
		ArrayList<StudentRecord> list = new ArrayList<>();
		
		for (TableEntry<String, StudentRecord> entry : records) {
			StudentRecord record = entry.getValue();
			if (filter.accepts(record)) {
				list.add(record);
			} 
		}

		return list;
	}

	
	/*-----------------constants-------------------------------*/
	
	/**  Regex used for parsing input.*/
	private static final String INPUT_REGEX = "\\s*([0-9]{10})" 
											+ "\\s+([a-z0-9\\u0020čćžšđČĆŽĐŠ-]+)"
											+ "\\s+([a-z0-9\\u0020čćžšđČĆŽĐŠ-]+)"
											+ "\\s+([1-5])\\s*$";
	
	/** {@link Pattern} used for parsing input */
	private static final Pattern PATTERN = Pattern.compile(INPUT_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
}
