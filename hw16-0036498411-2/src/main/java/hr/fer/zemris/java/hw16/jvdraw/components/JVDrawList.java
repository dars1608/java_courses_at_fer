package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.ListModel;

import hr.fer.zemris.java.hw16.jvdraw.actions.DeleteAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.DownAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.UpAction;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.components.list.GeometricalObjectEditor;

/**
 * This class defines custom {@link JList} used for showing info about geometric objects
 * currently added to {@link DrawingModel}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class JVDrawList extends JList<GeometricalObject> {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor method for class {@link JVDrawList}.
	 * @param listModel
	 * 			 list model
	 * @param drawingModel
	 * 			 drawing model
	 */
	public JVDrawList(ListModel<GeometricalObject> listModel, DrawingModel drawingModel) {
		super(listModel);
		
		InputMap inputMap = this.getInputMap();
		ActionMap actionMap = this.getActionMap();
		
		AbstractAction up = new UpAction(drawingModel);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "up");
		actionMap.put("up", up);
		
		AbstractAction down = new DownAction(drawingModel);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, 0), "down");
		actionMap.put("down", down);
		
		AbstractAction del = new DeleteAction(drawingModel);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "del");
		actionMap.put("del", del);
		
		this.addMouseListener(new ListMouseListener());
	}

	
	/**
	 * This class defines a mouse adapter used for enabling the edit of objects shown on {@link JList}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class ListMouseListener extends MouseAdapter {
		
		@Override
		@SuppressWarnings("unchecked")
		public void mouseClicked(MouseEvent evt) {
	        JList<GeometricalObject> list = (JList<GeometricalObject>) evt.getSource();
	        
	        if (evt.getClickCount() != 2) {
	        	return;
	        }
	        
            int index = list.locationToIndex(evt.getPoint());
            GeometricalObject o = list.getModel().getElementAt(index);
            GeometricalObjectEditor editor = o.createGeometricalObjectEditor();
            
            while(true) {
	            if(JOptionPane.showConfirmDialog(
	            		JVDrawList.this.getTopLevelAncestor(),
	            		editor, 
	            		"Edit", 
	            		JOptionPane.OK_CANCEL_OPTION,
	            		JOptionPane.PLAIN_MESSAGE) != JOptionPane.OK_OPTION) {
	            	
	            	break;
	            }
	            	
            	try {
	            	editor.checkEditing();
	            	editor.acceptEditing();
	            	break;
            	} catch(Exception ex) {
            		JOptionPane.showMessageDialog(
            				JVDrawList.this.getTopLevelAncestor(), 
            				ex.getMessage(), 
            				"Error", 
            				JOptionPane.OK_OPTION);
            	}
	             
	        }
	    }
	}
}
