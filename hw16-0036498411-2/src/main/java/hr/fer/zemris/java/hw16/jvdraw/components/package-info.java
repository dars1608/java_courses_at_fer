/**
 * This package contains custom {@link javax.swing.JComponent JComponents} used
 * in JVDraw program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.jvdraw.components;