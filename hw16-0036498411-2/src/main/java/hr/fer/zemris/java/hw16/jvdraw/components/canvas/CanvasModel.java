package hr.fer.zemris.java.hw16.jvdraw.components.canvas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectListener;

/**
 * This class represents an implementation of {@link DrawingModel} used as the
 * core of the {@link JDrawingCanvas} component.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CanvasModel implements DrawingModel, GeometricalObjectListener {

	/** List of {@link DrawingModelListener} objects */
	private List<DrawingModelListener> listeners = new ArrayList<>();
	/** List of {@link GeometricalObject} */
	private LinkedList<GeometricalObject> objects = new LinkedList<>();

	@Override
	public int getSize() {
		return objects.size();
	}

	@Override
	public GeometricalObject getObject(int index) {
		if (index < 0 || index >= objects.size()) {
			return null;
		} else {
			return objects.get(index);
		}
	}

	@Override
	public void add(GeometricalObject object) {
		objects.add(Objects.requireNonNull(object));
		object.addGeometricalObjectListener(this);
		listeners.forEach(l -> l.objectsAdded(this, objects.size() - 1, objects.size() - 1));

	}

	@Override
	public void addDrawingModelListener(DrawingModelListener l) {
		listeners.add(Objects.requireNonNull(l));
		l.objectsAdded(this, 0, objects.size() - 1);
	}

	@Override
	public void removeDrawingModelListener(DrawingModelListener l) {
		listeners.remove(Objects.requireNonNull(l));
	}

	@Override
	public void remove(GeometricalObject object) {
		int index = objects.indexOf(Objects.requireNonNull(object));
		if (index < 0) {
			throw new NoSuchElementException("Theres no such element in the model.");
		}

		objects.remove(index);
		listeners.forEach(l -> l.objectsRemoved(this, index, index));
	}

	@Override
	public void changeOrder(GeometricalObject object, int offset) {
		final int indexStart = objects.indexOf(Objects.requireNonNull(object));
		int index = indexStart;
		if (index < 0) {
			throw new NoSuchElementException("Theres no such element in the model.");
		}

		index += offset;
		final int indexEnd = index;
		if (index < 0 || index >= objects.size()) {
			throw new IndexOutOfBoundsException(
					String.format("offset = %d, index = %d, length = %d", offset, index, objects.size()));
		}

		objects.remove(object);
		objects.add(index, object);

		listeners.forEach(l -> l.objectsRemoved(this, indexStart, indexStart));
		listeners.forEach(l -> l.objectsAdded(this, indexEnd, indexEnd));
	}

	@Override
	public void geometricalObjectChanged(GeometricalObject o) {
		int index = objects.indexOf(Objects.requireNonNull(o));
		if (index < 0) {
			throw new NoSuchElementException("Theres no such element in the model.");
		}

		listeners.forEach(l -> l.objectsChanged(this, index, index));
	}

}