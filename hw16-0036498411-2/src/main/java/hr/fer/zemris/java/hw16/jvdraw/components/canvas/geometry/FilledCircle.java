package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;
import hr.fer.zemris.java.hw16.jvdraw.components.list.FilledCircleEditor;
import hr.fer.zemris.java.hw16.jvdraw.components.list.GeometricalObjectEditor;

/**
 * This class represents {@link GeometricalObject} which models a filled circle.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class FilledCircle extends Circle {

	/**
	 * Constructor method for class {@link FilledCircle}.
	 * 
	 * @param fgColor
	 *            foreground color
	 * @param bgColor
	 *            background color
	 * @param center
	 *            center
	 * @param radius
	 *            radius
	 */
	public FilledCircle(Color fgColor, Color bgColor, Point2D center, int radius) {
		super(fgColor, bgColor, center, radius);
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		Objects.requireNonNull(v).visit(this);
	}

	@Override
	public String toString() {
		return String.format("Filled circle (%d,%d), %d, #%02X%02X%02X", 
				super.getCenter().getX(), 
				super.getCenter().getY(),
				super.getRadius(), 
				super.getBgColor().getRed(),
				super.getBgColor().getGreen(),
				super.getBgColor().getBlue()
		);
	}
	
	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new FilledCircleEditor(this);
	}
}
