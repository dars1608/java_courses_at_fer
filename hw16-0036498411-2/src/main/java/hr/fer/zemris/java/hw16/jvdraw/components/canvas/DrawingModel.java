package hr.fer.zemris.java.hw16.jvdraw.components.canvas;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;

/**
 * This interface describes a model which manages a list of
 * {@link GeometricalObject}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface DrawingModel {

	/**
	 * This method returns a number of stored {@link GeometricalObject objects}.
	 * 
	 * @return the size
	 */
	public int getSize();

	/**
	 * This method returns a {@link GeometricalObject} at the given index.
	 * 
	 * @param index
	 *            index of the object
	 * @return the object at given index
	 */
	public GeometricalObject getObject(int index);

	/**
	 * This method adds {@link GeometricalObject} in the model.
	 * 
	 * @param object
	 *            object to be added
	 * @throws NullPointerException if the argument is {@code null}
	 */
	public void add(GeometricalObject object);

	/**
	 * This method removes {@link GeometricalObject} from the model.
	 * 
	 * @param object
	 *            object to be removed
	 * @throws NullPointerException if the argument is {@code null}
	 */
	void remove(GeometricalObject object);

	/**
	 * This method is used for changing order in the internal list of
	 * {@link GeometricalObject objects}.
	 * 
	 * @param object
	 *            object which position is modified
	 * @param offset
	 *            offset of the current position
	 * @throws NullPointerException if the argument is {@code null}
	 */
	void changeOrder(GeometricalObject object, int offset);

	/**
	 * This method is used for adding the {@link DrawingModelListener}.
	 * 
	 * @param l
	 *            listener to be added
	 * @throws NullPointerException if the argument is {@code null}
	 */
	public void addDrawingModelListener(DrawingModelListener l);

	/**
	 * This method is used for removing the {@link DrawingModelListener}.
	 * 
	 * @param l
	 *            listener to be removed
	 * @throws NullPointerException if the argument is {@code null}
	 */
	public void removeDrawingModelListener(DrawingModelListener l);
}