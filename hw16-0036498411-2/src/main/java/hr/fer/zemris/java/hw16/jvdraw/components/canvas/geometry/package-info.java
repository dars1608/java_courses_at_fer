/**
 * This package contains classes used for modeling and showing the objects on
 * the {@link hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;