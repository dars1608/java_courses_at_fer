package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;
import hr.fer.zemris.java.hw16.jvdraw.components.list.CircleEditor;
import hr.fer.zemris.java.hw16.jvdraw.components.list.GeometricalObjectEditor;

/**
 * This class represents {@link GeometricalObject} which models a circle.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Circle extends GeometricalObject {

	/** Center */
	private Point2D center;
	/** Radius */
	private int radius;

	/**
	 * Constructor method for class {@link Circle}.
	 * 
	 * @param fgColor
	 *            foreground color
	 * @param bgColor
	 *            background color
	 * @param center
	 *            center
	 * @param radius
	 *            radius
	 */
	public Circle(Color fgColor, Color bgColor, Point2D center, int radius) {
		super(fgColor, bgColor);

		if (radius < 0) {
			throw new IllegalArgumentException("Radius can't be negative integer.");
		}

		this.center = Objects.requireNonNull(center);
		this.radius = radius;
	}

	/**
	 * Getter method for the {@code center}.
	 * 
	 * @return the center
	 */
	public Point2D getCenter() {
		return center;
	}

	/**
	 * Setter method for the {@code center}.
	 * 
	 * @param center
	 *            the center to set
	 */
	public void setCenter(Point2D center) {
		this.center = Objects.requireNonNull(center);
		fire();
	}

	/**
	 * Getter method for the {@code radius}.
	 * 
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * Setter method for the {@code radius}.
	 * 
	 * @param radius
	 *            the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
		fire();
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		Objects.requireNonNull(v).visit(this);
	}

	@Override
	public String toString() {
		return String.format("Circle (%d,%d), %d", center.getX(), center.getY(), radius);
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new CircleEditor(this);
	}
}
