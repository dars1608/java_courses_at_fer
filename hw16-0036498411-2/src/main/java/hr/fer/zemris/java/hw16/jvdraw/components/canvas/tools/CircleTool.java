package hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools;

import java.awt.Graphics2D;

import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.Circle;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectPainter;
import hr.fer.zemris.java.hw16.jvdraw.components.color.IColorProvider;

/**
 * This class describes a {@link Tool} used for drawing the circle on the {@link JDrawingCanvas}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CircleTool extends AbstractTool {

	/**
	 * Constructor method for class {@link CircleTool}.
	 * 
	 * @param fgColorProvider
	 *            foreground color provider
	 * @param bgColorProvider
	 *            background color provider
	 * @param model
	 *            drawing model
	 * @throws NullPointerException
	 *             if any argument is {@code null}
	 */
	public CircleTool(IColorProvider fgColorProvider, IColorProvider bgColorProvider, DrawingModel model) {
		super(fgColorProvider, bgColorProvider, model);
	}

	@Override
	public void paint(Graphics2D g2d) {	
		if(start == null || end == null) {
			return;
		}
		
		int dx = start.getX() - end.getX();
		int dy = start.getY() - end.getY();
		int radius = (int) Math.sqrt(dx*dx + dy*dy);
		
		Circle c = new Circle(
				fgColorProvider.getCurrentColor(),
				bgColorProvider.getCurrentColor(),
				start, 
				radius
		);
		
		if(!isStarted) {
			model.add(c);
			restart();
		}

		c.accept(new GeometricalObjectPainter(g2d));
	}

}
