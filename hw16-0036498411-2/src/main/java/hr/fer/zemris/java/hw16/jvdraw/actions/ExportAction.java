package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

import hr.fer.zemris.java.hw16.jvdraw.DrawingCanvasChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectBBCalculator;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectPainter;

/**
 * This class represents {@link JVDrawFileAction} used for exporting the drawn
 * image in .png, .jpg or .gif format.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ExportAction extends JVDrawFileAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** File filter used for filtering .png files */
	private static final FileFilter PNG_FILTER = new FileFilter() {
		@Override
		public String getDescription() {
			return ".png";
		}

		@Override
		public boolean accept(File f) {
			return f.toString().endsWith(".png") || f.isDirectory();
		}
	};
	/** File filter used for filtering .gif files */
	private static final FileFilter GIF_FILTER = new FileFilter() {
		@Override
		public String getDescription() {
			return ".gif";
		}

		@Override
		public boolean accept(File f) {
			return f.toString().endsWith(".gif") || f.isDirectory();
		}
	};

	/** File filter used for filtering .jpg files */
	private static final FileFilter JPG_FILTER = new FileFilter() {
		@Override
		public String getDescription() {
			return ".jpg";
		}

		@Override
		public boolean accept(File f) {
			return f.toString().endsWith(".jpg") || f.isDirectory();
		}
	};

	/**
	 * Constructor method for class {@link ExportAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 * @param parent
	 *            parent {@link JFrame}
	 * @param changeListener
	 *            change listener
	 */
	public ExportAction(DrawingModel model, JFrame parent, DrawingCanvasChangeListener changeListener) {
		super(model, parent, changeListener);

		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control E"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_E);
		putValue(NAME, "Export");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (model.getSize() == 0) {
			JOptionPane.showMessageDialog(
					parent,
					"There's nothing to export. Operation aborted.",
					"Warning",
					JOptionPane.WARNING_MESSAGE
			);
			return;
		}

		BufferedImage image = prepareImage();

		JFileChooser jfc = new JFileChooser();
		jfc.addChoosableFileFilter(PNG_FILTER);
		jfc.addChoosableFileFilter(GIF_FILTER);
		jfc.setFileFilter(JPG_FILTER);
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setDialogTitle("Export document as...");

		if (jfc.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					parent, 
					"Nothing was saved.", 
					"Warning",
					JOptionPane.WARNING_MESSAGE
			);
			return;
		}

		String fileName = jfc.getSelectedFile().toString();
		String extension = jfc.getFileFilter().getDescription();
		
		if (!jfc.getSelectedFile().exists() && !fileName.endsWith(extension)) {
			fileName = fileName + extension;
		}

		Path openedFilePath = Paths.get(fileName);
		if (openedFilePath.toFile().exists()) {
			int res = JOptionPane.showConfirmDialog(
					parent,
					"File already exists. Do you want to overwrite it?",
					"Warning", 
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE
			);

			if (res == JOptionPane.NO_OPTION) {
				return;
			}
		}

		try {
			File f = openedFilePath.toFile();
			f.createNewFile();
			ImageIO.write(image, extension.substring(1), f);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					parent, 
					"File can't be exported.",
					"Error", 
					JOptionPane.ERROR_MESSAGE
			);
			return;
		}

		JOptionPane.showMessageDialog(
				parent,
				"Image was exported."
		);
	}

	/**
	 * This method is used for preparing the image for exporting.
	 * 
	 * @return prepared image
	 */
	private BufferedImage prepareImage() {
		GeometricalObjectBBCalculator bbcalc = new GeometricalObjectBBCalculator();
		for (int i = 0, len = model.getSize(); i < len; i++) {
			model.getObject(i).accept(bbcalc);
		}

		Rectangle box = bbcalc.getBoundingBox();
		BufferedImage image = new BufferedImage(
				box.width,
				box.height,
				BufferedImage.TYPE_3BYTE_BGR
		);
		Graphics2D g = image.createGraphics();

		AffineTransform af = g.getTransform();
		af.translate(-box.x, -box.y);
		g.setTransform(af);

		g.setColor(Color.WHITE);
		g.fillRect(box.x, box.y, 2*box.width, 2*box.height);
		GeometricalObjectPainter painter = new GeometricalObjectPainter(g);
		for (int i = 0, len = model.getSize(); i < len; i++) {
			model.getObject(i).accept(painter);
		}
		g.dispose();
		
		return image;
	}
}
