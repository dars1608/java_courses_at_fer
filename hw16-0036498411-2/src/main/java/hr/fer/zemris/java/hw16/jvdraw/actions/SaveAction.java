package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Path;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw16.jvdraw.DrawingCanvasChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.JVDFileUtil;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;

/**
 * This class represents {@link JVDrawFileAction} used for saving the current
 * image.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SaveAction extends SaveAsAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link SaveAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 * @param parent
	 *            {@link JFrame}
	 * @param changeListener
	 *            change listener
	 */
	public SaveAction(DrawingModel model, JFrame parent, DrawingCanvasChangeListener changeListener) {
		super(model, parent, changeListener);

		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_S);
		putValue(NAME, "Save");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Path cPath = changeListener.getCurrentPath();
		
		if(cPath == null) {
			super.actionPerformed(e);
			return;
		}
		
		try {
			JVDFileUtil.writeToFile(cPath, model);
			changeListener.setModified(false);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					parent,
					"File can't be saved.", 
					"Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
