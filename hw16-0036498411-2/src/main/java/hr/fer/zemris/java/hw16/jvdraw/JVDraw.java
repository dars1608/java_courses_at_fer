package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.ScrollPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.hw16.jvdraw.actions.ExitAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.ExportAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.OpenAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.SaveAction;
import hr.fer.zemris.java.hw16.jvdraw.actions.SaveAsAction;
import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.components.JColorStatus;
import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.components.JVDrawList;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.CircleTool;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.FilledCircleTool;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.LineTool;
import hr.fer.zemris.java.hw16.jvdraw.components.list.DrawingObjectListModel;

/**
 * JVDraw is a simple application for vector graphics.
 * 
 * <p>
 * Program offers:
 * <ul>
 * <li> Drawing lines, circles and filled circles
 * <li> Saving the creations in special .jvd format
 * <li> Loading the creations from the .jvd file
 * <li> Exporting the image in .jpg, .gif or .png format
 * </ul>
 * </p>
 * @author Darko Britvec
 * @version 1.0
 */
public class JVDraw extends JFrame {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	/** Default program title*/
	private static final String DEFAULT_TITLE = "JVDraw";
	
	/** Drawing canvas*/
	private JDrawingCanvas canvas;
	/** Drawing model*/
	private DrawingModel model;
	/** Background color chooser*/
	private JColorArea bg;
	/** Foreground color chooser*/
	private JColorArea fg;
	/** Exit action*/
	private Action exitAction;

	/**
	 * Constructor method for class {@link JVDraw}.
	 */
	public JVDraw() {
		setTitle(DEFAULT_TITLE);
		setLocation(50, 50);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		initGUI();
		
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitAction.actionPerformed(null);
			}
			
		});
		
		pack();
	}

	/**
	 * This method initializes GUI.
	 */
	private void initGUI() {		
		initToolbar();
		initStatusBar();
		initCanvas();
		initList();
		initMenuBar();
	}

	/**
	 * This method initializes menu bar.
	 */
	private void initMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		
		DrawingCanvasChangeListener changeListener = new DrawingCanvasChangeListener();
		model.addDrawingModelListener(changeListener);
		changeListener.setModified(false);
		
		JMenu file = new JMenu("File");
		file.add(new JMenuItem(new OpenAction(model, this, changeListener)));
		file.addSeparator();
		file.add(new JMenuItem(new SaveAction(model, this, changeListener)));
		file.add(new JMenuItem(new SaveAsAction(model, this, changeListener)));
		file.add(new JMenuItem(new ExportAction(model, this, changeListener)));
		file.addSeparator();
		
		exitAction = new ExitAction(model, this, changeListener);
		file.add(new JMenuItem(exitAction));
		menuBar.add(file);
		
		this.setJMenuBar(menuBar);		
	}

	/** 
	 * This method initializes status bar.
	 */
	private void initStatusBar() {
		JColorStatus status = new JColorStatus(fg, bg);
		this.add(status, BorderLayout.SOUTH);
		
	}

	/**
	 * This method initializes drawing canvas.
	 */
	private void initCanvas() {
		canvas = new JDrawingCanvas();
		model = canvas.getModel();
		canvas.setCurrentState(new LineTool(fg, bg, model));
		this.add(canvas, BorderLayout.CENTER);
		
	}

	/**
	 * This method initializes tool bar.
	 */
	private void initToolbar() {
		JToolBar toolBar = new JToolBar();
		
		bg = new JColorArea(Color.BLUE);
		fg = new JColorArea(Color.BLACK);
		
		toolBar.add(fg);
		toolBar.add(bg);
		this.add(toolBar, BorderLayout.NORTH);
		
		JToggleButton lineButton = new JToggleButton("Line");
		JToggleButton circleButton = new JToggleButton("Circle");
		JToggleButton fCircleButton = new JToggleButton("Filled circle");
		
		ButtonGroup group = new ButtonGroup();
		group.add(lineButton);
		group.add(circleButton);
		group.add(fCircleButton);
		
		lineButton.addActionListener(l -> {
			canvas.setCurrentState(new LineTool(fg, bg, model));
		});

		circleButton.addActionListener(l -> {
			canvas.setCurrentState(new CircleTool(fg, bg, model));
		});
		
		fCircleButton.addActionListener(l -> {
			canvas.setCurrentState( new FilledCircleTool(fg, bg, model));
		});
		
		toolBar.addSeparator();
		toolBar.add(lineButton);
		toolBar.add(circleButton);
		toolBar.add(fCircleButton);
		
		lineButton.setSelected(true);
	}

	/**
	 * This method initializes a list of drawn objects.
	 */
	private void initList() {
		JList<GeometricalObject> list = new JVDrawList(new DrawingObjectListModel(model), model);
		ScrollPane sPane = new ScrollPane(ScrollPane.SCROLLBARS_AS_NEEDED);
		
		sPane.add(list);
		sPane.setPreferredSize(new Dimension(200, 0));
		this.add(sPane, BorderLayout.EAST);
	}

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JVDraw().setVisible(true);
		});
	}
}
