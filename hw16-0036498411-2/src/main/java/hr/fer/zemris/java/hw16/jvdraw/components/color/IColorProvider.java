package hr.fer.zemris.java.hw16.jvdraw.components.color;

import java.awt.Color;

/**
 * This interface defines a color provider.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IColorProvider {

	/**
	 * Getter for the current color.
	 * 
	 * @return the current color
	 */
	public Color getCurrentColor();

	/**
	 * This method is used for adding the {@link ColorChangeListener}.
	 * 
	 * @param l
	 *            listener to be added
	 * @throws NullPointerException
	 *             if the argument is {@code null}
	 */
	public void addColorChangeListener(ColorChangeListener l);

	/**
	 * This method is used for removing the {@link ColorChangeListener}.
	 * 
	 * @param l
	 *            listener to be removed
	 * @throws NullPointerException
	 *             if the argument is {@code null}
	 */
	public void removeColorChangeListener(ColorChangeListener l);
}
