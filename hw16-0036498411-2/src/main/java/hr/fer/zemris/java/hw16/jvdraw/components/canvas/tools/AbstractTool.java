package hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.color.IColorProvider;

/**
 * This class defines basic implementation of the {@link Tool} interface.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class AbstractTool implements Tool {

	/** Start point */
	protected Point2D start;
	/** End point */
	protected Point2D end;
	/** Foreground color provider */
	protected IColorProvider fgColorProvider;
	/** Background color provider */
	protected IColorProvider bgColorProvider;
	/** Drawing model */
	protected DrawingModel model;
	/** Flag determining if the drawing has started */
	protected boolean isStarted = false;

	/**
	 * Constructor method for class {@link AbstractTool}.
	 * 
	 * @param fgColorProvider
	 *            foreground color provider
	 * @param bgColorProvider
	 *            background color provider
	 * @param model
	 *            drawing model
	 * @throws NullPointerException
	 *             if any argument is {@code null}
	 */
	protected AbstractTool(IColorProvider fgColorProvider, IColorProvider bgColorProvider, DrawingModel model) {
		this.fgColorProvider = Objects.requireNonNull(fgColorProvider);
		this.bgColorProvider = Objects.requireNonNull(bgColorProvider);
		this.model = Objects.requireNonNull(model);
	}

	/**
	 * Used for restarting the start and the end point.
	 */
	protected void restart() {
		start = null;
		end = null;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (isStarted) {
			end = new Point2D();
			end.setLocation(e.getX(), e.getY());
			isStarted = false;
		} else {
			start = new Point2D();
			end = new Point2D();
			start.setLocation(e.getX(), e.getY());
			end.setLocation(e.getX(), e.getY());
			isStarted = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if (isStarted) {
			end.setLocation(e.getX(), e.getY());
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		mouseMoved(e);
	}

	@Override
	public abstract void paint(Graphics2D g2d);
}
