package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.JList;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;

/**
 * This class represents basic {@link AbstractAction} used for manipulating
 * {@link JList} in the {@link JVDraw} program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class JVDrawListAction extends AbstractAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Drawing model */
	protected DrawingModel model;

	/**
	 * Constructor method for class {@link JVDrawListAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 */
	protected JVDrawListAction(DrawingModel model) {
		this.model = Objects.requireNonNull(model);
	}

	@Override
	public abstract void actionPerformed(ActionEvent e);

}
