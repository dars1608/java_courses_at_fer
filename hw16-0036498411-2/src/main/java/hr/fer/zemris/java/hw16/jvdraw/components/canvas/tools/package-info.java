/**
 * This package contains
 * {@link hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Tool tools} for
 * drawing on the
 * {@link hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas}.a
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools;