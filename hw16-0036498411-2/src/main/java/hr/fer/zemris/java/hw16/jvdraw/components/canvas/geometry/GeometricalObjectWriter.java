package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Color;
import java.io.PrintWriter;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;


/**
 * This class defines {@link GeometricalObjectVisitor} used for writing 
 * out the {@link GeometricalObject objects} in the .jvd file.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class GeometricalObjectWriter implements GeometricalObjectVisitor {
	
	/** Writer object*/
	private PrintWriter writer;
	
	/**
	 * Constructor method for class {@link GeometricalObjectWriter}.
	 * 
	 * @param writer print writer
	 */
	public GeometricalObjectWriter(PrintWriter writer) {
		this.writer = Objects.requireNonNull(writer);
	}

	@Override
	public void visit(Line line) {
		Point2D start = line.getStart();
		Point2D end = line.getEnd();
		Color col = line.getFgColor();
		writer.println(String.format("LINE %d %d %d %d %d %d %d", 
				start.getX(),
				start.getY(), 
				end.getX(),
				end.getY(),
				col.getRed(),
				col.getGreen(),
				col.getBlue())
		);

	}

	@Override
	public void visit(Circle circle) {
		Point2D center = circle.getCenter();
		Color col = circle.getFgColor();
		
		writer.println(String.format("CIRCLE %d %d %d %d %d %d", 
				center.getX(),
				center.getY(),
				circle.getRadius(),
				col.getRed(),
				col.getGreen(),
				col.getBlue()
		));

	}

	@Override
	public void visit(FilledCircle filledCircle) {
		Point2D center = filledCircle.getCenter();
		Color fgCol = filledCircle.getFgColor();
		Color bgCol = filledCircle.getBgColor();
		
		writer.println(String.format("FCIRCLE %d %d %d %d %d %d %d %d %d", 
				center.getX(),
				center.getY(),
				filledCircle.getRadius(),
				fgCol.getRed(),
				fgCol.getGreen(),
				fgCol.getBlue(),
				bgCol.getRed(),
				bgCol.getGreen(),
				bgCol.getBlue()
		));

	}

}
