package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.components.list.GeometricalObjectEditor;

/**
 * This class models a basic object which can be drawn on the
 * {@link JDrawingCanvas} component.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class GeometricalObject {

	/** Foreground color */
	private Color bgColor;
	/** Backgroun color */
	private Color fgColor;
	/** List of {@link GeometricalObjectListener} objects */
	private List<GeometricalObjectListener> listeners = new ArrayList<>();

	/**
	 * Constructor method for class {@link GeometricalObject}.
	 * 
	 * @param fgColor
	 *            foreground color
	 * @param bgColor
	 *            background color
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	protected GeometricalObject(Color fgColor, Color bgColor) {
		this.fgColor = Objects.requireNonNull(fgColor);
		this.bgColor = Objects.requireNonNull(bgColor);
	}

	/**
	 * Getter method for the {@code bgColor}.
	 * 
	 * @return the bgColor
	 */
	public Color getBgColor() {
		return bgColor;
	}

	/**
	 * Setter method for the {@code bgColor}.
	 * 
	 * @param bgColor
	 *            the bgColor to set
	 */
	public void setBgColor(Color bgColor) {
		this.bgColor = Objects.requireNonNull(bgColor);
		fire();
	}

	/**
	 * Getter method for the {@code fgColor}.
	 * 
	 * @return the fgColor
	 */
	public Color getFgColor() {
		return fgColor;
	}

	/**
	 * Setter method for the {@code fgColor}.
	 * 
	 * @param fgColor
	 *            the fgColor to set
	 */
	public void setFgColor(Color fgColor) {
		this.fgColor = Objects.requireNonNull(fgColor);
		fire();
	}

	/**
	 * This method is used for accepting the {@link GeometricalObjectVisitor}.
	 * 
	 * @param v
	 *            visitor
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public abstract void accept(GeometricalObjectVisitor v);

	/**
	 * This method is used for creating the frame which shows a form for editing the
	 * informations about the object.
	 * 
	 * @return a {@link GeometricalObjectEditor} panel
	 */
	public abstract GeometricalObjectEditor createGeometricalObjectEditor();

	/**
	 * This method is used for adding the {@link GeometricalObjectListener}.
	 * 
	 * @param l
	 *            listener to be added
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public void addGeometricalObjectListener(GeometricalObjectListener l) {
		listeners.add(Objects.requireNonNull(l));
		l.geometricalObjectChanged(this);
	}

	/**
	 * This method is used for removing the {@link GeometricalObjectListener}.
	 * 
	 * @param l
	 *            listener to be removed
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public void removeGeometricalObjectListener(GeometricalObjectListener l) {
		listeners.add(Objects.requireNonNull(l));
	}

	/**
	 * This method is used for notifying the listeners about the changing of the
	 * object.
	 */
	protected void fire() {
		listeners.forEach(l -> l.geometricalObjectChanged(this));
	}

}
