package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

/**
 * This interface defines a {@link GeometricalObject} listener.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface GeometricalObjectListener {
	
	/**
	 * This method is called whenever the change of the {@link GeometricalObject}
	 * occurs.
	 * 
	 * @param o
	 *            changed object
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public void geometricalObjectChanged(GeometricalObject o);
}
