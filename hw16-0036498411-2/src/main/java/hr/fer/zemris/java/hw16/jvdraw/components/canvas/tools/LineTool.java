package hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools;

import java.awt.Graphics2D;

import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectPainter;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.Line;
import hr.fer.zemris.java.hw16.jvdraw.components.color.IColorProvider;

/**
 * This class describes a {@link Tool} used for drawing the line on the {@link JDrawingCanvas}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LineTool extends AbstractTool {

	/**
	 * Constructor method for class {@link LineTool}.
	 * 
	 * @param fgColorProvider
	 *            foreground color provider
	 * @param bgColorProvider
	 *            background color provider
	 * @param model
	 *            drawing model
	 * @throws NullPointerException
	 *             if any argument is {@code null}
	 */
	public LineTool(IColorProvider fgColorProvider, IColorProvider bgColorProvider, DrawingModel model) {
		super(fgColorProvider, bgColorProvider, model);
	}

	@Override
	public void paint(Graphics2D g2d) {
		if(start == null || end == null) {
			return;
		}
		
		Line line = new Line(
				fgColorProvider.getCurrentColor(),
				bgColorProvider.getCurrentColor(),
				start, 
				end
		);
		
		if(!isStarted) {
			model.add(line);
			restart();
		}
		
		line.accept(new GeometricalObjectPainter(g2d));
	}

}
