package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.JColorChooser;
import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.components.color.ColorChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.components.color.IColorProvider;

/**
 * This class is used as color chooser button with dimensions of 15x15 pixels.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class JColorArea extends JComponent implements IColorProvider {

	/** Serial version */
	private static final long serialVersionUID = 1;

	/** Current color */
	private Color selectedColor;
	/** Listeners */
	private List<ColorChangeListener> listeners = new ArrayList<>();

	/**
	 * Constructor method for class {@link JColorArea}.
	 * 
	 * @param color
	 *            initial color
	 */
	public JColorArea(Color color) {
		this.selectedColor = Objects.requireNonNull(color);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Color newColor = JColorChooser.showDialog(
						JColorArea.this.getTopLevelAncestor(),
						"Choose color", 
						color
				);

				if (newColor != null) {
					JColorArea.this.setCurrentColor(newColor);
				}
			}
		});
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(15, 15);
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(15, 15);
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(15, 15);
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(selectedColor);
		g.fillRect(0, 0, 15, 15);
		g.setColor(Color.WHITE);
		g.drawRect(0, 0, 15, 15);
	}

	@Override
	public void addColorChangeListener(ColorChangeListener listener) {
		listeners.add(Objects.requireNonNull(listener));
		listeners.forEach(l -> l.newColorSelected(this, selectedColor, selectedColor));
	}

	@Override
	public void removeColorChangeListener(ColorChangeListener listener) {
		listeners.remove(Objects.requireNonNull(listener));
	}

	@Override
	public Color getCurrentColor() {
		return selectedColor;
	}

	/**
	 * This method is used internally for setting the current color
	 * 
	 * @param newColor
	 *            color to be set
	 */
	private void setCurrentColor(Color newColor) {
		listeners.forEach(l -> l.newColorSelected(this, selectedColor, Objects.requireNonNull(newColor)));
		selectedColor = newColor;
		repaint();
	}

}
