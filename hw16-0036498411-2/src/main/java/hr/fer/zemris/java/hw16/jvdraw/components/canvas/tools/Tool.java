package hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas;

/**
 * This interface defines a state of {@link JDrawingCanvas}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface Tool {
	
	/**
	 * This method defines an action which is performed on the press of the mouse.
	 * 
	 * @param e event
	 */
	public void mousePressed(MouseEvent e);

	/**
	 * This method defines an action which is performed on the release of the mouse.
	 * 
	 * @param e event
	 */
	public void mouseReleased(MouseEvent e);

	/**
	 * This method defines an action which is performed on the click of the mouse.
	 * 
	 * @param e event
	 */
	public void mouseClicked(MouseEvent e);

	/**
	 * This method defines an action which is performed on the mouse moving.
	 * 
	 * @param e event
	 */
	public void mouseMoved(MouseEvent e);

	/**
	 * This method defines an action which is performed on the mouse dragging.
	 * 
	 * @param e event
	 */
	public void mouseDragged(MouseEvent e);

	/**
	 * This method is used for painting on the graphics object.
	 * 
	 * @param g2d graphics object
	 * @throws NullPointerException if the argument is {@code null}
	 */
	public void paint(Graphics2D g2d);
}