package hr.fer.zemris.java.hw16.jvdraw.components.list;

import java.util.Objects;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.ListModel;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;

/**
 * This class defines a custom {@link ListModel} used as adapter of
 * {@link DrawingModel}. This way the {@link DrawingModel} can be the source of
 * data for {@link JList} object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DrawingObjectListModel extends AbstractListModel<GeometricalObject> {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Drawing model */
	private DrawingModel model;

	/**
	 * Constructor method for class {@link DrawingObjectListModel}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 */
	public DrawingObjectListModel(DrawingModel model) {
		this.model = Objects.requireNonNull(model);

		model.addDrawingModelListener(new DrawingModelListener() {

			@Override
			public void objectsRemoved(DrawingModel source, int index0, int index1) {
				fireIntervalRemoved(source, index0, index1);

			}

			@Override
			public void objectsChanged(DrawingModel source, int index0, int index1) {
				fireContentsChanged(source, index0, index1);

			}

			@Override
			public void objectsAdded(DrawingModel source, int index0, int index1) {
				fireIntervalAdded(source, index0, index1);
			}
		});
	}

	@Override
	public int getSize() {
		return model.getSize();
	}

	@Override
	public GeometricalObject getElementAt(int index) {
		return model.getObject(index);
	}

}
