/**
 * This package contains {@link javax.swing.AbstractAction} objects used in
 * JVDraw program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.jvdraw.actions;