package hr.fer.zemris.java.hw16.jvdraw.components.list;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.FilledCircle;

/**
 * This class represents {@link GeometricalObjectEditor} which shows a form for
 * editing the {@link FilledCircle} object.
 *  
 * @author Darko Britvec
 * @version 1.0
 */
public class FilledCircleEditor extends CircleEditor{

	/** Serial version */
	private static final long serialVersionUID = 1L;
	
	/** Background color chooser*/
	private JColorArea cArea;
	/** Filled circle object*/
	private FilledCircle filledCircle;

	/**
	 * Constructor method for class {@link FilledCircleEditor}.
	 * @param filledCircle {@link FilledCircle} object
	 */
	public FilledCircleEditor(FilledCircle filledCircle) {
		super(filledCircle);
		
		this.filledCircle = filledCircle;
		initGUI2();
	}

	/**
	 * This method is used for initializing the GUI.
	 */
	private void initGUI2() {
		JLabel color = new JLabel("Choose background color: ", SwingConstants.RIGHT);
		cArea = new JColorArea(filledCircle.getBgColor());
		
		this.add(color);
		this.add(cArea);
	}

	@Override
	public void checkEditing() {
		super.checkEditing();
	}

	@Override
	public void acceptEditing() {
		super.acceptEditing();

		filledCircle.setBgColor(cArea.getCurrentColor());
	}

}
