package hr.fer.zemris.java.hw16.jvdraw.components.list;

import javax.swing.JPanel;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;

/**
 * This class represents a basic form for editing the {@link GeometricalObject}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class GeometricalObjectEditor extends JPanel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * This method is called to check the values inserted in the form.
	 * 
	 * @throws IllegalArgumentException
	 *             if the inserted arguments are illegal
	 */
	public abstract void checkEditing();

	/**
	 * This method is called to accept the inserted values.
	 * 
	 * @throws IllegalStateException
	 *             if the {@link #checkEditing()} isn't called
	 */
	public abstract void acceptEditing();
}