package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

/**
 * This interface defines a visitor of the {@link GeometricalObject}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface GeometricalObjectVisitor {

	/**
	 * This method is used for visiting the {@link Line} object.
	 * 
	 * @param line
	 *            line to be visited
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public abstract void visit(Line line);

	/**
	 * This method is used for visiting the {@link Circle} object.
	 * 
	 * @param circle
	 *            circle to be visited
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public abstract void visit(Circle circle);

	/**
	 * This method is used for visiting the {@link FilledCircle} object.
	 * 
	 * @param filledCircle
	 *            filledCircle to be visited
	 * @throws NullPointerException
	 *             if the argument is{@code null}
	 */
	public abstract void visit(FilledCircle filledCircle);
}