/**
 * This package contains classes used for modeling a list of drawn object in the
 * JVDraw program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.jvdraw.components.list;