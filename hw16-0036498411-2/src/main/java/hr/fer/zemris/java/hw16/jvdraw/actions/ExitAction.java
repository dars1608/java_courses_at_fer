package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw16.jvdraw.DrawingCanvasChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;

/**
 * This class represents an {@link AbstractAction} used for exiting the JVDraw program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ExitAction extends SaveAction {

	
	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link ExitAction}.
	 * @param model
	 * 			  {@link DrawingModel} object
	 * @param parent
	 * 			  parent {@link JFrame}
	 * @param changeListener
	 *            change listener
	 */
	public ExitAction(DrawingModel model, JFrame parent, DrawingCanvasChangeListener changeListener) {
		super(model, parent, changeListener);

		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_X);
		putValue(NAME, "Exit");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (JOptionPane.showConfirmDialog(
				parent,
				"Do you want to exit the program?", 
				"Closing program...",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION) {
			
			return;
		}
		
		if(changeListener.isModified()) {
			while(changeListener.isModified()) {
				if (JOptionPane.showConfirmDialog(
						parent,
						"There's some unsaved modifications. Do you want to save it?",
						"Warning",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
					
					parent.dispose();
					return;
				}
				
				super.actionPerformed(e);
			}
		}
		
		parent.dispose();
	}
}
