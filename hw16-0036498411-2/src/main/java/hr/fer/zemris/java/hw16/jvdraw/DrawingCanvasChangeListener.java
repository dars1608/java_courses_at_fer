package hr.fer.zemris.java.hw16.jvdraw;

import java.nio.file.Path;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModelListener;

/**
 * This class represents {@link DrawingModelListener} who keeps track of the
 * activities of {@link DrawingModel} - modifications and changing of the path
 * which is in relation with the model.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DrawingCanvasChangeListener implements DrawingModelListener {

	/** Flag determining if the canvas has been edited */
	private boolean isModified;
	/** Current file path */
	private Path currentPath;

	/**
	 * Getter method for the {@code isModified}.
	 * 
	 * @return the isModified
	 */
	public boolean isModified() {
		return isModified;
	}

	/**
	 * Setter method for the {@code isModified}.
	 * 
	 * @param isModified
	 *            the isModified to set
	 */
	public void setModified(boolean isModified) {
		this.isModified = isModified;
	}

	/**
	 * Getter method for the {@code currentPath}.
	 * 
	 * @return the currentPath
	 */
	public Path getCurrentPath() {
		return currentPath;
	}

	/**
	 * Setter method for the {@code currentPath}.
	 * 
	 * @param currentPath
	 *            the currentPath to set
	 */
	public void setCurrentPath(Path currentPath) {
		this.currentPath = currentPath;
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		isModified = true;
	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		isModified = true;
	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		isModified = true;
	}

}
