package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Graphics2D;
import java.util.Objects;

/**
 * This class represents {@link GeometricalObjectVisitor} used for paining
 * {@link GeometricalObject objects} on the graphics object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class GeometricalObjectPainter implements GeometricalObjectVisitor {

	/** Graphics object */
	private Graphics2D g2d;

	/**
	 * Constructor method for class {@link GeometricalObjectPainter}.
	 * 
	 * @param g2d
	 *            graphics object
	 */
	public GeometricalObjectPainter(Graphics2D g2d) {
		this.g2d = Objects.requireNonNull(g2d);
	}

	@Override
	public void visit(Line line) {
		g2d.setColor(line.getFgColor());

		int x1 = line.getStart().getX();
		int y1 = line.getStart().getY();
		int x2 = line.getEnd().getX();
		int y2 = line.getEnd().getY();

		g2d.drawLine(x1, y1, x2, y2);

	}

	@Override
	public void visit(Circle circle) {
		int width = circle.getRadius() * 2;
		int height = width;
		int x = circle.getCenter().getX() - width / 2;
		int y = circle.getCenter().getY() - height / 2;

		g2d.setColor(circle.getFgColor());
		g2d.drawOval(x, y, width, height);
	}

	@Override
	public void visit(FilledCircle filledCircle) {
		int width = filledCircle.getRadius() * 2;
		int height = width;
		int x = filledCircle.getCenter().getX() - width / 2;
		int y = filledCircle.getCenter().getY() - height / 2;

		g2d.setColor(filledCircle.getBgColor());
		g2d.fillOval(x, y, width, height);
		g2d.setColor(filledCircle.getFgColor());
		g2d.drawOval(x, y, width, height);

	}

}
