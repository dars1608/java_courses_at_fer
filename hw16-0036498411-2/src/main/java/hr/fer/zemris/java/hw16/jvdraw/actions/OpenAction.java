package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Stack;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw16.jvdraw.BadJVDFileFormatException;
import hr.fer.zemris.java.hw16.jvdraw.DrawingCanvasChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.JVDFileUtil;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;

/**
 * This class represents {@link JVDrawFileAction} used for opening the .jvd file
 * and loading it into the current {@link DrawingModel}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class OpenAction extends SaveAction{

	/** Serial version */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor method for class {@link OpenAction}.
	 * 
	 * @param model 
	 * 			{@link DrawingModel} object
	 * @param parent
	 * 			parent {@link JFrame}
	 * @param changeListener
	 *            change listener
	 */
	public OpenAction(DrawingModel model, JFrame parent, DrawingCanvasChangeListener changeListener) {
		super(model, parent, changeListener);

		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_O);
		putValue(NAME, "Open");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(changeListener.isModified()) {
			if (JOptionPane.showConfirmDialog(
					parent,
					"There's some unsaved modifications. Do you want to save it?",
					"Warning",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
				
				super.actionPerformed(e);
			}
		}
		
		
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(JVD_FILTER);
		fc.setDialogTitle("Open file");
		if (fc.showOpenDialog(parent) != JFileChooser.APPROVE_OPTION) {
			return;
		}

		File fileName = fc.getSelectedFile();
		Path filePath = fileName.toPath();
		if (!Files.isReadable(filePath)) {
			JOptionPane.showMessageDialog(
					parent, 
					String.format("File %s can't be read.", fileName.getAbsolutePath()),
					"Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		Stack<GeometricalObject> temp = new Stack<>();
		for(int i = model.getSize() - 1; i >= 0; i--) {
			GeometricalObject o = model.getObject(i);
			temp.add(o);
			model.remove(o);
		}
		
		try {
			JVDFileUtil.readFromFile(filePath, model);
			changeListener.setCurrentPath(filePath);
			changeListener.setModified(false);
		} catch (IOException | BadJVDFileFormatException e1) {
			JOptionPane.showMessageDialog(
					parent, 
					String.format("File can't be read.\n Explanation: %s", e1.getMessage()),
					"Error", 
					JOptionPane.ERROR_MESSAGE);
			
			while(!temp.isEmpty()) {
				model.add(temp.pop());
			}
		}
	}
}
