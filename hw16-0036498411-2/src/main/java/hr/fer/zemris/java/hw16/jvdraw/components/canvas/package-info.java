/**
 * This package contains interfaces which describe the details for the logic of
 * {@link hr.fer.zemris.java.hw16.jvdraw.components.JDrawingCanvas} component.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.jvdraw.components.canvas;