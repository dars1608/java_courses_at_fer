package hr.fer.zemris.java.hw16.jvdraw;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.Circle;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.FilledCircle;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectVisitor;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectWriter;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.Line;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;

/**
 * This is utility class used for reading and writing .jvd files.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class JVDFileUtil {

	/** Pattern used for extracting informations about {@link Line} objects */
	private static final Pattern LINE_PATTERN = Pattern
			.compile("LINE (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+)");
	/** Pattern used for extracting informations about {@link Circle} objects */
	private static final Pattern CIRCLE_PATTERN = Pattern.compile("CIRCLE (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+)");
	/**
	 * Pattern used for extracting informations about {@link FilledCircle} objects
	 */
	private static final Pattern FCIRCLE_PATTERN = Pattern
			.compile("FCIRCLE (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+) (\\d+)");

	/**
	 * This method is used for writing the {@link GeometricalObject}s from the
	 * {@link DrawingModel} into the .jvd file.
	 * 
	 * @param openedFilePath
	 *            file path
	 * @param model
	 *            drawing model
	 * @throws IOException
	 *             if any error while writing occurs
	 * @throws NullPointerException
	 *             if any of the arguments is {@code null}
	 */
	public static void writeToFile(Path filePath, DrawingModel model) throws IOException {
		Objects.requireNonNull(filePath);
		Objects.requireNonNull(model);
		PrintWriter pw = new PrintWriter(Files.newBufferedWriter(filePath));

		GeometricalObjectVisitor writer = new GeometricalObjectWriter(pw);
		for (int i = 0, len = model.getSize(); i < len; i++) {
			model.getObject(i).accept(writer);
		}

		pw.close();
	}

	/**
	 * This method is used for reading from the .jvd file, creating
	 * {@link GeometricalObject}s and adding them into the {@link DrawingModel}.
	 * 
	 * @param filePath
	 *            path of the file
	 * @param model
	 *            drawing model
	 * @throws IOException
	 *             if any error occurs while reading
	 * @throws NullPointerException
	 *             if any of the arguments is {@code null}
	 */
	public static void readFromFile(Path filePath, DrawingModel model) throws IOException {
		Objects.requireNonNull(filePath);
		Objects.requireNonNull(model);
		BufferedReader br = Files.newBufferedReader(filePath);

		while (br.ready()) {
			String line = br.readLine().trim();
			Matcher m = LINE_PATTERN.matcher(line);
			if (m.matches()) {
				int x1 = Integer.parseInt(m.group(1));
				int y1 = Integer.parseInt(m.group(2));
				int x2 = Integer.parseInt(m.group(3));
				int y2 = Integer.parseInt(m.group(4));
				int r = Integer.parseInt(m.group(5));
				int g = Integer.parseInt(m.group(6));
				int b = Integer.parseInt(m.group(7));

				model.add(new Line(new Color(r, g, b), Color.WHITE, new Point2D(x1, y1), new Point2D(x2, y2)));
				continue;
			}

			m = CIRCLE_PATTERN.matcher(line);
			if (m.matches()) {
				int xC = Integer.parseInt(m.group(1));
				int yC = Integer.parseInt(m.group(2));
				int radius = Integer.parseInt(m.group(3));
				int r = Integer.parseInt(m.group(4));
				int g = Integer.parseInt(m.group(5));
				int b = Integer.parseInt(m.group(6));

				model.add(new Circle(new Color(r, g, b), Color.WHITE, new Point2D(xC, yC), radius));
				continue;
			}

			m = FCIRCLE_PATTERN.matcher(line);
			if (m.matches()) {
				int xC = Integer.parseInt(m.group(1));
				int yC = Integer.parseInt(m.group(2));
				int radius = Integer.parseInt(m.group(3));
				int rf = Integer.parseInt(m.group(4));
				int gf = Integer.parseInt(m.group(5));
				int bf = Integer.parseInt(m.group(6));
				int rb = Integer.parseInt(m.group(7));
				int gb = Integer.parseInt(m.group(8));
				int bb = Integer.parseInt(m.group(9));

				model.add(new FilledCircle(new Color(rf, gf, bf), new Color(rb, gb, bb), new Point2D(xC, yC), radius));
				continue;
			}

			throw new BadJVDFileFormatException("File " + filePath + " isn't of .jvd format.");
		}
	}

}
