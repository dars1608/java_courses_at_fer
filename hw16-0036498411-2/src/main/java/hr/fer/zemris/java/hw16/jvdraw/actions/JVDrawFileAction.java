package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;

import hr.fer.zemris.java.hw16.jvdraw.DrawingCanvasChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;

/**
 * This class represents basic {@link AbstractAction} used in File menu of
 * JVDraw program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class JVDrawFileAction extends AbstractAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	/** File filter for filtering .jvd files */
	protected static final FileFilter JVD_FILTER = new FileFilter() {

		@Override
		public String getDescription() {
			return ".jvd";
		}

		@Override
		public boolean accept(File f) {
			return f.toString().endsWith(".jvd") || f.isDirectory();
		}
	};

	/** Drawing model */
	protected DrawingModel model;
	/** Parent frame */
	protected JFrame parent;
	/** Change listener*/
	protected DrawingCanvasChangeListener changeListener;

	/**
	 * Constructor method for class {@link JVDrawFileAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 * @param parent
	 *            parent {@link JFrame}
	 */
	public JVDrawFileAction(DrawingModel model, JFrame parent, DrawingCanvasChangeListener changeListener) {
		this.model = Objects.requireNonNull(model);
		this.parent = Objects.requireNonNull(parent);
		this.changeListener = Objects.requireNonNull(changeListener);
	}

	@Override
	public abstract void actionPerformed(ActionEvent e);

}
