package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Rectangle;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;

/**
 * This class represents {@link GeometricalObjectVisitor} used for calculating
 * the bounding box of the {@link DrawingModel}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class GeometricalObjectBBCalculator implements GeometricalObjectVisitor {

	/** Maximal x coordinate */
	private int maxX;
	/** Minimal x coordinate */
	private int minX = Integer.MAX_VALUE;
	/** Maximal y coordinate */
	private int maxY;
	/** Minimal y coordinate */
	private int minY = Integer.MAX_VALUE;

	/**
	 * This method returns a {@link Rectangle} object representing the bounding box
	 * of the {@link DrawingModel}.
	 * 
	 * @return the bounding box
	 */
	public Rectangle getBoundingBox() {
		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	@Override
	public void visit(Line line) {
		int x1 = line.getStart().getX();
		int x2 = line.getEnd().getX();
		int y1 = line.getStart().getY();
		int y2 = line.getEnd().getY();

		update(x1, x2, y1, y2);
	}

	@Override
	public void visit(Circle circle) {
		int xC = circle.getCenter().getX();
		int yC = circle.getCenter().getY();
		int radius = circle.getRadius();

		int x1 = xC - radius;
		int x2 = xC + radius;
		int y1 = yC - radius;
		int y2 = yC + radius;

		update(x1, x2, y1, y2);
	}

	@Override
	public void visit(FilledCircle filledCircle) {
		visit((Circle) filledCircle);
	}

	/**
	 * This method is used for updating the values of the bounding box.
	 * 
	 * @param x1
	 *            x1 coordinate
	 * @param x2
	 *            x2 coordinate
	 * @param y1
	 *            y1 coordinate
	 * @param y2
	 *            y2 coordinate
	 */
	private void update(int x1, int x2, int y1, int y2) {
		if (maxX < x1) {
			maxX = x1;
		}

		if (maxX < x2) {
			maxX = x2;
		}

		if (minX > x1) {
			minX = x1;
		}

		if (minX > x2) {
			minX = x2;
		}

		if (maxY < y1) {
			maxY = y1;
		}

		if (maxY < y2) {
			maxY = y2;
		}

		if (minY > y1) {
			minY = y1;
		}

		if (minY > y2) {
			minY = y2;
		}
	}
}
