package hr.fer.zemris.java.hw16.jvdraw.components.list;

import java.awt.GridLayout;
import java.util.Objects;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.Line;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;

/**
 * This class represents {@link GeometricalObjectEditor} which shows a form for
 * editing the {@link Line} object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LineEditor extends GeometricalObjectEditor {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	
	/** Line object*/
	private Line line;
	
	/** xStart text field*/
	private JTextField xStart;
	/** yStart text field*/
	private JTextField yStart;
	/** xEnd text field*/
	private JTextField xEnd;
	/** yEnd text field*/
	private JTextField yEnd;
	/** Color chooser*/
	private JColorArea cArea;
	
	/** Final xStart value*/
	private Integer xStartValue;
	/** Final yStart value*/
	private Integer yStartValue;
	/** Final xEnd value*/
	private Integer xEndValue;
	/** Final xEnd value*/
	private Integer yEndValue;

	/**
	 * Constructor method for class {@link LineEditor}.
	 * @param line {@link Line} object
	 */
	public LineEditor(Line line) {
		this.line = Objects.requireNonNull(line);
		
		initGUI();
	}

	/**
	 * This method is used for initializing the GUI.
	 */
	private void initGUI() {
		this.setLayout(new GridLayout(0, 2));
		
		JLabel xStartLabel = new JLabel("x1: ", SwingConstants.RIGHT);
		xStart = new JTextField(Integer.toString(line.getStart().getX()), 4);
		JLabel yStartLabel = new JLabel("y1: ", SwingConstants.RIGHT);
		yStart = new JTextField(Integer.toString(line.getStart().getY()), 4);
		JLabel xEndLabel = new JLabel("x2: ", SwingConstants.RIGHT);
		xEnd =  new JTextField(Integer.toString(line.getEnd().getX()), 4);
		JLabel yEndLabel = new JLabel("y2: ", SwingConstants.RIGHT);
		yEnd = new JTextField(Integer.toString(line.getEnd().getY()), 4);
		JLabel color = new JLabel("Choose color:", SwingConstants.RIGHT);
		cArea = new JColorArea(line.getFgColor());
		
		this.add(xStartLabel);
		this.add(xStart);
		this.add(yStartLabel);
		this.add(yStart);
		this.add(xEndLabel);
		this.add(xEnd);
		this.add(yEndLabel);
		this.add(yEnd);
		this.add(color);
		this.add(cArea);		
	}

	@Override
	public void checkEditing() {
		try {
			xStartValue = Integer.valueOf(xStart.getText());
			yStartValue = Integer.valueOf(yStart.getText());
			xEndValue = Integer.valueOf(xEnd.getText());
			yEndValue = Integer.valueOf(yEnd.getText());
		} catch(NumberFormatException ex) {
			throw new IllegalArgumentException("All values must be integers.");
		}

	}

	@Override
	public void acceptEditing() {
		if(xStartValue == null || xEndValue == null || yStartValue == null || yEndValue == null) {
			throw new IllegalStateException("Values aren't ready. Call checkEditing()");
		}

		line.setStart(new Point2D(xStartValue, yStartValue));
		line.setEnd(new Point2D(xEndValue, yEndValue));
		line.setFgColor(cArea.getCurrentColor());
	}

}
