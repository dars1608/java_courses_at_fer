package hr.fer.zemris.java.hw16.jvdraw.components.list;

import java.awt.GridLayout;
import java.util.Objects;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import hr.fer.zemris.java.hw16.jvdraw.components.JColorArea;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.Circle;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;

/**
 * This class represents {@link GeometricalObjectEditor} which shows a form for
 * editing the {@link Circle} object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CircleEditor extends GeometricalObjectEditor {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Circle object*/
	private Circle circle;
	/** xCenter text field*/
	private JTextField xCenter;
	/** yCenter text field*/
	private JTextField yCenter;
	/** radius text field*/
	private JTextField radius;
	/** color chooser*/
	private JColorArea cArea;

	/** Final xCenter value*/
	private Integer xCenterValue;
	/** Final yCenter value*/
	private Integer yCenterValue;
	/** Final radius value*/
	private Integer radiusValue;

	/**
	 * Constructor method for class {@link CircleEditor}.
	 * @param circle {@link Circle} object
	 */
	public CircleEditor(Circle circle) {
		this.circle = Objects.requireNonNull(circle);
		initGUI();
	}

	/**
	 * This method is used for initializing the GUI.
	 */
	private void initGUI() {
		this.setLayout(new GridLayout(0, 2));

		JLabel xCenterLabel = new JLabel("x: ", SwingConstants.RIGHT);
		xCenter = new JTextField(Integer.toString(circle.getCenter().getX()), 4);
		JLabel yCenterLabel = new JLabel("y: ", SwingConstants.RIGHT);
		yCenter = new JTextField(Integer.toString(circle.getCenter().getY()), 4);
		JLabel radiusLabel = new JLabel("radius: ", SwingConstants.RIGHT);
		radius = new JTextField(Integer.toString(circle.getRadius()), 4);
		JLabel color = new JLabel("Choose outline color:", SwingConstants.RIGHT);
		cArea = new JColorArea(circle.getFgColor());

		this.add(xCenterLabel);
		this.add(xCenter);
		this.add(yCenterLabel);
		this.add(yCenter);
		this.add(radiusLabel);
		this.add(radius);
		this.add(color);
		this.add(cArea);
	}

	@Override
	public void checkEditing() {
		try {
			xCenterValue = Integer.valueOf(xCenter.getText());
			yCenterValue = Integer.valueOf(yCenter.getText());
			radiusValue = Integer.valueOf(radius.getText());
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("All values must be integers.");
		}

	}

	@Override
	public void acceptEditing() {
		if (xCenterValue == null || yCenterValue == null || radiusValue == null) {
			throw new IllegalStateException("Values aren't ready. Call checkEditing()");
		}

		circle.setCenter(new Point2D(xCenterValue, yCenterValue));
		circle.setRadius(radiusValue);
		circle.setFgColor(cArea.getCurrentColor());

	}

}
