package hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Point2D;
import hr.fer.zemris.java.hw16.jvdraw.components.list.GeometricalObjectEditor;
import hr.fer.zemris.java.hw16.jvdraw.components.list.LineEditor;

/**
 * This class represents {@link GeometricalObject} which models a line.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Line extends GeometricalObject {

	/** Starting point */
	private Point2D start;
	/** End point */
	private Point2D end;

	/**
	 * Constructor method for class {@link Line}.
	 * 
	 * @param fgColor
	 *            foreground color
	 * @param bgColor
	 *            background color
	 * @param start
	 *            start point
	 * @param end
	 *            end point
	 */
	public Line(Color fgColor, Color bgColor, Point2D start, Point2D end) {
		super(fgColor, bgColor);

		this.start = Objects.requireNonNull(start);
		this.end = Objects.requireNonNull(end);
	}

	/**
	 * Getter method for the {@code start}.
	 * 
	 * @return the start
	 */
	public Point2D getStart() {
		return start;
	}

	/**
	 * Setter method for the {@code start}.
	 * 
	 * @param start
	 *            the start to set
	 */
	public void setStart(Point2D start) {
		this.start = Objects.requireNonNull(start);
		fire();
	}

	/**
	 * Getter method for the {@code end}.
	 * 
	 * @return the end
	 */
	public Point2D getEnd() {
		return end;
	}

	/**
	 * Setter method for the {@code end}.
	 * 
	 * @param end
	 *            the end to set
	 */
	public void setEnd(Point2D end) {
		this.end = Objects.requireNonNull(end);
		fire();
	}

	@Override
	public void accept(GeometricalObjectVisitor v) {
		Objects.requireNonNull(v).visit(this);
	}

	@Override
	public String toString() {
		return String.format("Line (%d,%d)-(%d,%d)", start.getX(), start.getY(), end.getX(), end.getY());
	}

	@Override
	public GeometricalObjectEditor createGeometricalObjectEditor() {
		return new LineEditor(this);
	}
}
