package hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools;

/**
 * This is utility class used for representing the dots on the screen.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Point2D {

	/** X component */
	private int x;
	/** Y component */
	private int y;

	/**
	 * Constructor method for class {@link Point2D}.
	 */
	public Point2D() {
	}

	/**
	 * Constructor method for class {@link Point2D}.
	 * 
	 * @param x
	 *            x component
	 * @param y
	 *            y component
	 */
	public Point2D(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Setter method for the point location.
	 * 
	 * @param x
	 *            x component
	 * @param y
	 *            y component
	 */
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Getter method for the {@code x}.
	 * 
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Setter method for the {@code x}.
	 * 
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Getter method for the {@code y}.
	 * 
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * Setter method for the {@code y}.
	 * 
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

}