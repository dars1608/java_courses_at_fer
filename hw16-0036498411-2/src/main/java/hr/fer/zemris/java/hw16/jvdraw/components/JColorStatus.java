package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Color;

import javax.swing.JLabel;

import hr.fer.zemris.java.hw16.jvdraw.components.color.IColorProvider;

/**
 * This class represent simple derivate of {@link JLabel} used for showing the
 * current foreground and background color value.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class JColorStatus extends JLabel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Current foreground color */
	private Color fg = Color.WHITE;
	/** Current background color */
	private Color bg = Color.WHITE;

	/**
	 * Constructor method for class {@link JColorStatus}.
	 * 
	 * @param fg
	 *            foreground color provider
	 * @param bg
	 *            background color provider
	 */
	public JColorStatus(IColorProvider fg, IColorProvider bg) {
		fg.addColorChangeListener((provider, oldColor, newColor) -> {
			setFGColor(newColor);
		});

		bg.addColorChangeListener((provider, oldColor, newColor) -> {
			setBGColor(newColor);
		});
	}

	/**
	 * This method is used internally for setting the current foreground color
	 * 
	 * @param color
	 *            color to be set
	 */
	private void setFGColor(Color fg) {
		this.fg = fg;
		updateText();
	}

	/**
	 * This method is used internally for setting the current background color
	 * 
	 * @param color
	 *            color to be set
	 */
	private void setBGColor(Color bg) {
		this.bg = bg;
		updateText();
	}

	/**
	 * This method is used internally for updating the displayed text.a
	 */
	private void updateText() {
		this.setText(String.format("Foreground color: (%d, %d, %d), background color: (%d, %d, %d).", 
				fg.getRed(), fg.getGreen(), fg.getBlue(), bg.getRed(), bg.getGreen(), bg.getBlue()));
	}
}
