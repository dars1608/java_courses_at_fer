package hr.fer.zemris.java.hw16.jvdraw.components;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

import javax.swing.JComponent;

import hr.fer.zemris.java.hw16.jvdraw.JVDraw;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.CanvasModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModelListener;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectPainter;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObjectVisitor;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.tools.Tool;

/**
 * This class is used as drawing canvas of {@link JVDraw} application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class JDrawingCanvas extends JComponent implements DrawingModelListener {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	/** Preferred dimension*/
	private static final Dimension PREF_DIM = new Dimension(300, 200);

	/** Current {@link Tool} which is used */
	private Tool currentState;
	/** Drawing model */
	private DrawingModel model;

	/**
	 * Constructor method for class {@link JDrawingCanvas}.
	 */
	public JDrawingCanvas() {
		model = new CanvasModel();
		model.addDrawingModelListener(this);

		this.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				currentState.mouseReleased(e);
				repaint();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				currentState.mousePressed(e);
				repaint();
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				currentState.mouseClicked(e);
				repaint();
			}

		});

		this.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				currentState.mouseMoved(e);
				repaint();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				currentState.mouseDragged(e);
				repaint();
			}
		});
		
		this.setPreferredSize(PREF_DIM);
	}

	/**
	 * Getter method for the {@code currentState}.
	 * 
	 * @return the currentState
	 */
	public Tool getCurrentState() {
		return currentState;
	}

	/**
	 * Setter method for the {@code currentState}.
	 * 
	 * @param currentState
	 *            the currentState to set
	 */
	public void setCurrentState(Tool currentState) {
		this.currentState = Objects.requireNonNull(currentState);
	}

	/**
	 * Getter method for the {@code model}.
	 * 
	 * @return the model
	 */
	public DrawingModel getModel() {
		return model;
	}

	@Override
	protected void paintComponent(Graphics g) {
		GeometricalObjectVisitor painter = new GeometricalObjectPainter((Graphics2D) g);
		for(int i = 0, len = model.getSize(); i < len; i++) {
			model.getObject(i).accept(painter);
		}

		if (currentState != null) {
			currentState.paint((Graphics2D) g);
		}
	}

	@Override
	public void objectsAdded(DrawingModel source, int index0, int index1) {
		repaint();

	}

	@Override
	public void objectsRemoved(DrawingModel source, int index0, int index1) {
		repaint();

	}

	@Override
	public void objectsChanged(DrawingModel source, int index0, int index1) {
		repaint();
	}
}
