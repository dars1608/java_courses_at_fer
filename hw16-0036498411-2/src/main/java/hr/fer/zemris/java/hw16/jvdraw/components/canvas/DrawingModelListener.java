package hr.fer.zemris.java.hw16.jvdraw.components.canvas;

/**
 * This interface defines a listener of {@link DrawingModel} object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface DrawingModelListener {

	/**
	 * This method is called when the object is added to the {@link DrawingModel}.
	 * 
	 * @param source
	 *            observed model
	 * @param index0
	 *            first added index
	 * @param index1
	 *            last added index
	 * @throws NullPointerException
	 *             if the argument is {@code null}
	 */
	public void objectsAdded(DrawingModel source, int index0, int index1);

	/**
	 * This method is called when the object is removed to the {@link DrawingModel}.
	 * 
	 * @param source
	 *            observed model
	 * @param index0
	 *            first removed index
	 * @param index1
	 *            last removed index
	 * @throws NullPointerException
	 *             if the argument is {@code null}
	 */
	public void objectsRemoved(DrawingModel source, int index0, int index1);

	/**
	 * This method is called when the object is changed to the {@link DrawingModel}.
	 * 
	 * @param source
	 *            observed model
	 * @param index0
	 *            first changed index
	 * @param index1
	 *            last changed index
	 * @throws NullPointerException
	 *             if the argument is {@code null}
	 */
	public void objectsChanged(DrawingModel source, int index0, int index1);
}