package hr.fer.zemris.java.hw16.jvdraw;

/**
 * This class represents an {@link RuntimeException} which is thrown whenever
 * the badly formatted .jvd file is read.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class BadJVDFileFormatException extends RuntimeException {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link BadJVDFileFormatException}.
	 */
	public BadJVDFileFormatException() {
	}

	/**
	 * Constructor method for class {@link BadJVDFileFormatException}.
	 * 
	 * @param message
	 *            detail message
	 */
	public BadJVDFileFormatException(String message) {
		super(message);
	}

	/**
	 * Constructor method for class {@link BadJVDFileFormatException}.
	 * 
	 * @param cause
	 *            cause of the exception
	 */
	public BadJVDFileFormatException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor method for class {@link BadJVDFileFormatException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            cause of the exception
	 */
	public BadJVDFileFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor method for class {@link BadJVDFileFormatException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            cause of the exception
	 * @param enableSuppression
	 *            if suppression is enabled
	 * @param writableStackTrace
	 *            if stack trace is writable
	 */
	public BadJVDFileFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
