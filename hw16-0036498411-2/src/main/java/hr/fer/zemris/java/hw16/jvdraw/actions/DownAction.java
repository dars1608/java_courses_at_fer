package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;

import javax.swing.JList;

import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.geometry.GeometricalObject;

/**
 * This class represents {@link JVDrawListAction} used for shifting the
 * {@link GeometricalObject} one place down in the {@link JList}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DownAction extends JVDrawListAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link DownAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 */
	public DownAction(DrawingModel model) {
		super(model);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		JList<GeometricalObject> list = (JList<GeometricalObject>) e.getSource();
		int index = list.getSelectedIndex();
		
		try {
			model.changeOrder(model.getObject(index), 1);
			list.setSelectedIndex(index + 1);
		} catch (IndexOutOfBoundsException ignorable) {
		}

	}

}
