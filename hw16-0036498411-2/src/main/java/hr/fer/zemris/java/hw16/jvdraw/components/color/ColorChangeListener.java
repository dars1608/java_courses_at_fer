package hr.fer.zemris.java.hw16.jvdraw.components.color;

import java.awt.Color;

/**
 * This interface defines a listener of {@link IColorProvider} object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface ColorChangeListener {

	/**
	 * This method is called whenever the new color is selected.
	 * 
	 * @param source
	 *            color provider
	 * @param oldColor
	 *            old color
	 * @param newColor
	 *            new color
	 * @throws NullPointerException if any of the arguments are {@code null}
	 */
	void newColorSelected(IColorProvider source, Color oldColor, Color newColor);
}
