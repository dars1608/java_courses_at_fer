package hr.fer.zemris.java.hw16.jvdraw.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw16.jvdraw.DrawingCanvasChangeListener;
import hr.fer.zemris.java.hw16.jvdraw.JVDFileUtil;
import hr.fer.zemris.java.hw16.jvdraw.components.canvas.DrawingModel;

/**
 * This class represents {@link JVDrawFileAction} used for saving file as.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SaveAsAction extends JVDrawFileAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link SaveAsAction}.
	 * 
	 * @param model
	 *            {@link DrawingModel} object
	 * @param paretn
	 *            parent {@link JFrame}
	 * @param changeListener
	 *            change listener
	 */
	public SaveAsAction(DrawingModel model, JFrame parent, DrawingCanvasChangeListener changeListener) {
		super(model, parent, changeListener);

		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("control alt S"));
		putValue(MNEMONIC_KEY, KeyEvent.VK_A);
		putValue(NAME, "Save as...");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(JVD_FILTER);
		jfc.setDialogTitle("Save document as...");
		
		if (jfc.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					parent,
					"Nothing was saved.", 
					"Warning",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		String fileName = jfc.getSelectedFile().toString();
		String extension = jfc.getFileFilter().getDescription();
		
		if(!jfc.getSelectedFile().exists() && extension.equals(".jvd")) {
			fileName = fileName + extension;
		}
		
		Path openedFilePath = Paths.get(fileName);
		if (openedFilePath.toFile().exists()) {
			if(JOptionPane.showConfirmDialog(
					parent,
					"File already exists. Do you want to overwrite it?",
					"Warning",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE)  == JOptionPane.NO_OPTION) {
				
				return;
			}
		}

		try {
			JVDFileUtil.writeToFile(openedFilePath, model);
			changeListener.setCurrentPath(openedFilePath);
			changeListener.setModified(false);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					parent,
					"File can't be saved.", 
					"Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

}
