<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"
 import="java.util.List,hr.fer.zemris.java.hw14.model.PollOption"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
table.rez td {
	text-align: center;
}
</style>
<title>Rezultati glasanja</title>
</head>
<body>
		<h1>Rezultati glasanja</h1>
		<p>Ovo su rezultati glasanja. Vrati se na <a href="index.html">početnu stranicu</a> ili <a href="glasanje?pollID=<%= request.getParameter("pollID")%>">glasuj ponovo.</a></p>
		<table border="1" cellspacing="0" class="rez">
				<thead><tr><th>Opcija</th><th>Broj glasova</th></tr></thead>
				<tbody>
					<c:forEach var="result" items="${results}">
						<tr><td>${result.optionTitle}</td><td>${result.votesCount }</td></tr>
					</c:forEach>
				</tbody>
		</table>
		<h2>Grafički prikaz rezultata</h2>
		<img alt="Pie-chart" src="glasanje-grafika?pollID=<%= request.getParameter("pollID")%>" width="500" height="270" />
		<h2>Rezultati u XLS formatu</h2>
		<p>
				Rezultati u XLS formatu dostupni su <a href="glasanje-xls?pollID=<%= request.getParameter("pollID")%>">ovdje</a>
		</p>
		<h2>Razno</h2>
		<p>Linkovi pobjednika:</p>
		
		<%
		@SuppressWarnings("unchecked")
		List<PollOption> results = (List<PollOption>) request.getAttribute("results");
		long maxResult = -1L;
		for(int i = 0, len = results.size(); i < len; i++){
			PollOption result = results.get(i);
			if(result.getVotesCount() >= maxResult){
				maxResult = result.getVotesCount();%>
				<a href = "<%= result.getOptionLink() %>"><%= result.getOptionTitle() %> </a><br>
				<iframe width="420" height="315" src="<%= result.getOptionLink() %>"> </iframe><br><br>
				<%
			} else {
				break;
			}
		}%>
		

</body>
</html>