<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
</style>
<title>${title}</title>
</head>
<body>
		<h1>${message}</h1>
		
		Vidi <a href="glasanje-rezultati?pollID=<%= request.getParameter("pollID")%>">rezultate</a> ili se vrati na <a href="index.html">početnu stranicu</a>.
		
				<c:forEach var="option" items="${optionList}">
						<li><a href="glasanje-glasaj?id=${option.ID}&pollID=${option.pollID}">${option.optionTitle}</a> - <a href="${option.optionLink }">više na...</a></li>
				</c:forEach>
		
</body>
</html>