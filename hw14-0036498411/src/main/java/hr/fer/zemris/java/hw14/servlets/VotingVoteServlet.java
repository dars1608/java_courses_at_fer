package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mchange.v1.util.UnexpectedException;

import hr.fer.zemris.java.hw14.dao.DAOPollOptionsProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class represents {@link HttpServlet} used for saving the vote.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/glasanje-glasaj")
public class VotingVoteServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pollID = req.getParameter("pollID");
		String vote = req.getParameter("id");

		if (pollID == null || vote == null) {
			resp.sendError(400);
			return;
		}

		long fid = Long.valueOf(pollID);
		long id = Long.valueOf(vote);

		PollOption option = DAOPollOptionsProvider.getDao().getByIDAndFID(id, fid);

		if (option == null) { // if there's no artist with that id
			resp.sendError(400);
			return;
		}

		option.setVotesCount(option.getVotesCount() + 1L);
		try {
			DAOPollOptionsProvider.getDao().save(option);
		} catch (SQLException e) {
			throw new UnexpectedException(e);
		}

		resp.sendRedirect(req.getContextPath() + "/servleti/glasanje-rezultati?pollID=" + fid);
	}
}
