/**
 * This package contains defined entities used in voting-app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw14.model;