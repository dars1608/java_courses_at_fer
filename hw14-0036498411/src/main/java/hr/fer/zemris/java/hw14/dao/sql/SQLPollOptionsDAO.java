package hr.fer.zemris.java.hw14.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOException;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class describes a {@link DAO} object which works with the table
 * "PollOptions" from the database.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SQLPollOptionsDAO implements DAO<PollOption> {

	@Override
	public List<PollOption> getList(String sortBy) throws DAOException {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(
					"SELECT id, optionTitle, optionLink, pollId, votesCount FROM pollOptions ORDER BY " + sortBy);
			return getWithStatement(pst);
		} catch (Exception ex) {
			throw new DAOException("Error while reading the list of poll options.", ex);
		}
	}

	@Override
	public boolean save(PollOption entry) throws SQLException {
		Objects.requireNonNull(entry);

		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(
					"UPDATE pollOptions SET optionTitle = ?, optionLink = ?, votesCount = ? WHERE id = ? AND pollID = ?");
			pst.setString(1, entry.getOptionTitle());
			pst.setString(2, entry.getOptionLink());
			pst.setLong(3, entry.getVotesCount());
			pst.setLong(4, entry.getID());
			pst.setLong(5, entry.getPollID());

			return pst.executeUpdate() == 1;
		} catch (SQLException e) {
			throw e;
		}

	}

	@Override
	public List<PollOption> getByForeignID(long fid, String sortBy) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(
					"SELECT id, optionTitle, optionLink, pollId, votesCount FROM pollOptions WHERE pollid = ? ORDER BY "
							+ sortBy);
			pst.setLong(1, fid);
			return getWithStatement(pst);
		} catch (Exception ex) {
			throw new DAOException("Error while reading the list of poll options.", ex);
		}
	}

	@Override
	public PollOption getByIDAndFID(long id, long fid) throws DAOException {
		PollOption entry = null;
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement(
					"SELECT id, optionTitle, optionLink, pollId, votesCount FROM pollOptions WHERE id=? AND pollID=?");
			pst.setLong(1, id);
			pst.setLong(2, fid);

			try {
				ResultSet rs = pst.executeQuery();
				try {
					if (rs != null && rs.next()) {
						entry = new PollOption();
						entry.setID(rs.getLong(1));
						entry.setOptionTitle(rs.getString(2));
						entry.setOptionLink(rs.getString(3));
						entry.setPollID(rs.getLong(4));
						entry.setVotesCount(rs.getLong(5));
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception ex) {
			throw new DAOException("Error while obtaining poll option. (ID = " + id + ").", ex);
		}

		return entry;
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * <b>NOTE:</b>Unimplemented.
	 * </p>
	 */
	@Override
	public PollOption getByID(long id) throws DAOException {
		throw new UnsupportedOperationException("PollOptions table has combined key");
	}

	/**
	 * This method is used internally for obtaining list of entries from database
	 * with prepared statement.
	 * 
	 * @param pst
	 *            {@link PreparedStatement} object
	 * @return list of {@link PollOption} objects
	 * @throws SQLException
	 *             if error while working with database occurs
	 */
	private List<PollOption> getWithStatement(PreparedStatement pst) throws SQLException {
		List<PollOption> entries = new ArrayList<>();

		try {
			ResultSet rs = pst.executeQuery();
			try {
				while (rs != null && rs.next()) {
					PollOption entry = new PollOption();

					entry.setID(rs.getLong(1));
					entry.setOptionTitle(rs.getString(2));
					entry.setOptionLink(rs.getString(3));
					entry.setPollID(rs.getLong(4));
					entry.setVotesCount(rs.getLong(5));

					entries.add(entry);
				}
			} finally {
				try {
					rs.close();
				} catch (Exception ignorable) {
				}
			}
		} finally {
			try {
				pst.close();
			} catch (Exception ignorable) {
			}
		}

		return entries;

	}
}
