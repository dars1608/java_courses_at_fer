package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import hr.fer.zemris.java.hw14.dao.DAOPollOptionsProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class represents {@link HttpServlet} used for preparing the png image
 * which visualizes the voting results (creates simple pie-chart).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/glasanje-grafika")
public class VotingGraphicsServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pollID = req.getParameter("pollID");
		if (pollID == null) {
			resp.sendError(400);
			return;
		}

		long fid = Long.valueOf(pollID);
		List<PollOption> results = DAOPollOptionsProvider.getDao().getByForeignID(fid, "votesCount DESC");
		
		JFreeChart chart = createChart(createDataset(results), "Rezultati glasanja");
		resp.setContentType("image/png");

		int width = intFromString(req.getParameter("width"), 500);
		int height = intFromString(req.getParameter("height"), 270);

		ChartUtils.writeChartAsPNG(resp.getOutputStream(), chart, width, height);
	}

	/**
	 * This method is used internally for generating dataset which will be shown on
	 * the pie-chart.
	 * 
	 * @param results
	 *            list of results to be shown
	 * @return {@link DefaultPieDataset} object
	 */
	private PieDataset createDataset(List<PollOption> results) {
		DefaultPieDataset dataset = new DefaultPieDataset();

		results.forEach(result -> {
			if (result.getVotesCount() != 0) {
				dataset.setValue(result.getOptionTitle(), result.getVotesCount());
			}
		});

		return dataset;
	}
	
	/**
	 * This method is used for creating pie chart.
	 * 
	 * @param dataset
	 *            data to be shown on chart
	 * @param title
	 *            chart title
	 * @return {@link JFreeChart} object
	 */
	private static JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, true, true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);

		return chart;
	}
	
	/**
	 * This method is used obtaining the integer value from string.
	 * 
	 * @param s
	 *            string representation of integer
	 * @param defaultValue
	 *            default value, if parsing fails
	 * @return parsed integer
	 */
	private static Integer intFromString(String s, int defaultValue) {
		if (s != null) {
			try {
				return Integer.valueOf(s);
			} catch (NumberFormatException ignorable) {
				return defaultValue;
			}
		}

		return defaultValue;
	}
}
