package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOPollOptionsProvider;
import hr.fer.zemris.java.hw14.dao.DAOPollsProvider;
import hr.fer.zemris.java.hw14.model.Poll;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class represents {@link HttpServlet} used for loading the voting list.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/glasanje")
public class VotingServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pollID = req.getParameter("pollID");
		if (pollID == null) {
			resp.sendError(400);
			return;
		}

		long fid = Long.valueOf(pollID);
		List<PollOption> list = DAOPollOptionsProvider.getDao().getByForeignID(fid, "optionTitle");
		Poll poll = DAOPollsProvider.getDao().getByID(fid);

		if( poll == null || list == null) {
			resp.sendError(404);
			return;
		}
		
		req.setAttribute("title", poll.getTitle());
		req.setAttribute("message", poll.getMessage());
		req.setAttribute("optionList", list);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}
}
