package hr.fer.zemris.java.hw14.dao;

import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class represents {@link DAO} provider for communicating with table "PollOptions".
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class DAOPollOptionsProvider {
	
	/** DAO singleton object*/
	private static DAO<PollOption> dao;

	static {
		dao = (DAO<PollOption>) DAO.loadDao("pollOptions");									
	}

	/**
	 * This method returns the current implementation of {@link DAO}.
	 * @return DAO object
	 */
	public static DAO<PollOption> getDao() {
		return dao;
	}
}
