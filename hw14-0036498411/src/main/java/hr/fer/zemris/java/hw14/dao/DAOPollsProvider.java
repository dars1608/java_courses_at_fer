package hr.fer.zemris.java.hw14.dao;

import hr.fer.zemris.java.hw14.model.Poll;

/**
 * This class represents {@link DAO} provider for communicating with table "Polls".
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class DAOPollsProvider {
	
	/** DAO singleton object*/
	private static DAO<Poll> dao;

	static {
		dao = (DAO<Poll>) DAO.loadDao("polls");
	}

	/**
	 * This method returns the DAO
	 * @return DAO object
	 */
	public static DAO<Poll> getDao() {
		return dao;
	}
}
