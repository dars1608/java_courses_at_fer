package hr.fer.zemris.java.hw14.dao;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

/**
 * This interface defines the communication between the application and the data
 * persistence layer. See <a href="https://en.wikipedia.org/wiki/Data_access_object">this</a>
 * for more informations.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface DAO<T> {

	/**
	 * This method is used for loading all of the entries from the table.
	 * 
	 * @return list of entries, {@code null} if there's no such entries
	 * @throws DAOException
	 *             if error while reading occurs
	 */
	List<T> getList(String sortBy) throws DAOException;

	/**
	 * This method is used for loading all of the entries with the given primary
	 * key.
	 * 
	 * @param id
	 *            primary key
	 * @return list of entries, {@code null} if there's no such entries
	 * @throws DAOException
	 *             if error while reading occurs
	 */
	T getByID(long id) throws DAOException;

	/**
	 * This method is used for loading the entry by primary and foreign.
	 * 
	 * @param id
	 *            primary key
	 * @param fid
	 *            foreign key
	 * @return the entry, {@code null} if there's no such entry
	 * @throws DAOException
	 *             if error while reading occurs
	 */
	T getByIDAndFID(long id, long fid) throws DAOException;

	/**
	 * This method is used for loading all of the entries with the given foreigh
	 * key.
	 * 
	 * @param fid
	 *            foreign key
	 * @return list of entries, {@code null} if there's no such entries
	 * @throws DAOException
	 *             if error while reading occurs
	 */
	List<T> getByForeignID(long fid, String sortBy);

	/**
	 * This method stores the entry into the table.
	 * 
	 * @param entry
	 *            entry to be stored
	 * @return {@code true} if operation performed successfully, {@code false}
	 *         otherwise
	 * @throws SQLException
	 *             if error in database occurs
	 * @throws NullPointerException
	 *             if the argument entry is {@code null} reference
	 */
	boolean save(T entry) throws SQLException;

	/**
	 * This is the helper method used for loading the {@link DAO} object into the
	 * memory.
	 * 
	 * @param name
	 *            name of the object in the "dao.properties" file
	 * @return initialized DAO object
	 */
	static DAO<?> loadDao(String name) {
		DAO<?> dao = null;
		InputStream is = DAO.class.getResourceAsStream("dao.properties");
		Properties p = new Properties();

		try {
			p.load(is);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		String fqcn = p.getProperty(name);

		try {
			Class<?> referenceToClass = DAO.class.getClassLoader().loadClass(fqcn);
			dao = (DAO<?>) referenceToClass.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException ex) {

			ex.printStackTrace();
			System.exit(1);
		}

		return dao;
	}
}
