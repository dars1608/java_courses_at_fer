package hr.fer.zemris.java.hw14.model;

/**
 * This class models a poll option used in poles of voting-app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PollOption {

	/** Poll option id */
	private long ID;
	/** Option title */
	private String optionTitle;
	/** Web link describing the option */
	private String optionLink;
	/** Poll id */
	private long pollID;
	/** Number of votes */
	private long votesCount;

	/**
	 * Constructor method for class {@link PollOption}.
	 * 
	 * @param ID
	 *            poll option id
	 * @param optionTitle
	 *            option title
	 * @param optionLink
	 *            web link describing the option
	 * @param pollID
	 *            poll id
	 * @param votesCount
	 *            number of votes
	 */
	public PollOption(long ID, String optionTitle, String optionLink, long pollID, long votesCount) {
		this.ID = ID;
		this.optionTitle = optionTitle;
		this.optionLink = optionLink;
		this.pollID = pollID;
		this.votesCount = votesCount;
	}

	/**
	 * Default constructor method for class {@link PollOption}.
	 */
	public PollOption() {
		this(0L, null, null, 0L, 0L);
	}

	/**
	 * Getter method for the {@code iD}.
	 * 
	 * @return the iD
	 */
	public long getID() {
		return ID;
	}

	/**
	 * Setter method for the {@code iD}.
	 * 
	 * @param iD
	 *            the iD to set
	 */
	public void setID(long iD) {
		ID = iD;
	}

	/**
	 * Getter method for the {@code optionTitle}.
	 * 
	 * @return the optionTitle
	 */
	public String getOptionTitle() {
		return optionTitle;
	}

	/**
	 * Setter method for the {@code optionTitle}.
	 * 
	 * @param optionTitle
	 *            the optionTitle to set
	 */
	public void setOptionTitle(String optionTitle) {
		this.optionTitle = optionTitle;
	}

	/**
	 * Getter method for the {@code optionLink}.
	 * 
	 * @return the optionLink
	 */
	public String getOptionLink() {
		return optionLink;
	}

	/**
	 * Setter method for the {@code optionLink}.
	 * 
	 * @param optionLink
	 *            the optionLink to set
	 */
	public void setOptionLink(String optionLink) {
		this.optionLink = optionLink;
	}

	/**
	 * Getter method for the {@code pollID}.
	 * 
	 * @return the pollID
	 */
	public long getPollID() {
		return pollID;
	}

	/**
	 * Setter method for the {@code pollID}.
	 * 
	 * @param pollID
	 *            the pollID to set
	 */
	public void setPollID(long pollID) {
		this.pollID = pollID;
	}

	/**
	 * Getter method for the {@code votesCount}.
	 * 
	 * @return the votesCount
	 */
	public long getVotesCount() {
		return votesCount;
	}

	/**
	 * Setter method for the {@code votesCount}.
	 * 
	 * @param votesCount
	 *            the votesCount to set
	 */
	public void setVotesCount(long votesCount) {
		this.votesCount = votesCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ID ^ (ID >>> 32));
		result = prime * result + (int) (pollID ^ (pollID >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PollOption other = (PollOption) obj;
		if (ID != other.ID) {
			return false;
		}
		if (pollID != other.pollID) {
			return false;
		}
		return true;
	}

}
