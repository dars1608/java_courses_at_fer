package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import hr.fer.zemris.java.hw14.dao.DAOPollOptionsProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class represents {@link HttpServlet} used for generating .xls file based
 * on the provided voting results.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/glasanje-xls")
public class VotingXLSServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pollID = req.getParameter("pollID");
		if (pollID == null) {
			resp.sendError(400);
			return;
		}

		long fid = Long.valueOf(pollID);
		List<PollOption> results = DAOPollOptionsProvider.getDao().getByForeignID(fid, "votesCount DESC");
		HSSFWorkbook hwb = createXLS(results);

		resp.setContentType("application/vnd.ms-excel");
		resp.addHeader("Content-Disposition", "form-data; name=\"fieldName\"; filename=\"glasanje#"+fid+".xls\"");
		hwb.write(resp.getOutputStream());
	}

	/**
	 * This method is used internally for generating xls file.
	 * 
	 * @param list
	 *            list of results to be written
	 * @return {@link HSSFWorkbook} object
	 */
	private HSSFWorkbook createXLS(List<PollOption> list) {
		HSSFWorkbook hwb = new HSSFWorkbook();

		HSSFSheet sheet = hwb.createSheet("Sheet 1");

		HSSFRow rowhead = sheet.createRow((short) 0);
		rowhead.createCell((short) 0).setCellValue("Name");
		rowhead.createCell((short) 1).setCellValue("Result");

		for (int i = 1, len = list.size(); i <= len; i++) {
			PollOption r = list.get(i - 1);
			HSSFRow row = sheet.createRow((short) i);
			row.createCell((short) 0).setCellValue(r.getOptionTitle());
			row.createCell((short) 1).setCellValue(r.getVotesCount());
		}

		return hwb;
	}
}
