/**
 * This package contains classes used for modeling SQL data access layer.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw14.dao.sql;