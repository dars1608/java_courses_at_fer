package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOPollOptionsProvider;
import hr.fer.zemris.java.hw14.model.PollOption;

/**
 * This class represents {@link HttpServlet} used for preparing the voting
 * results which will be shown on screen.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/glasanje-rezultati")
public class VotingResultsServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pollID = req.getParameter("pollID");
		if (pollID == null) {
			resp.sendError(400);
			return;
		}

		long fid = Long.valueOf(pollID);
		List<PollOption> results = DAOPollOptionsProvider.getDao().getByForeignID(fid, "votesCount DESC");

		req.setAttribute("results", results);
		req.getRequestDispatcher("/WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	}
}
