package hr.fer.zemris.java.hw14;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

/**
 * This class describes a {@link ServletContextListener} used for initializing
 * the connection pool and the database (if needed), and destroying the
 * connection pool before the application shuts down.
 *
 */
@WebListener
public class Initialization implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Properties dbProperties = new Properties();
		Properties tableProperties = new Properties();

		InputStream dbConfig = sce.getServletContext().getResourceAsStream("/WEB-INF/dbsettings.properties");
		InputStream tableConfig = sce.getServletContext().getResourceAsStream("/WEB-INF/sql.properties");

		try {
			dbProperties.load(dbConfig);
			tableProperties.load(tableConfig);
			dbConfig.close();
			tableConfig.close();
		} catch (IOException e) {
			throw new RuntimeException("Error while loading configurations.", e);
		}

		String host = dbProperties.getProperty("host");
		String port = dbProperties.getProperty("port");
		String dbName = dbProperties.getProperty("name");
		String user = dbProperties.getProperty("user");
		String password = dbProperties.getProperty("password");

		if (host == null || port == null || dbName == null || user == null || password == null) {
			throw new RuntimeException("Configuration file is invalid.");
		}

		String connectionURL = String.format("jdbc:derby://%s:%s/%s;user=%s;password=%s", host, port, dbName, user,
				password);
		ComboPooledDataSource cpds = new ComboPooledDataSource();

		try {
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
		} catch (PropertyVetoException e1) {
			throw new RuntimeException("Error while initializing connection pool.", e1);
		}
		cpds.setJdbcUrl(connectionURL);

		Connection con;
		try {
			con = cpds.getConnection(user, password);
		} catch (SQLException e) {
			throw new RuntimeException("Database user doesn't exist.", e);
		}

		try {
			createIfNotExists(con, "polls", tableProperties);
			createIfNotExists(con, "pollOptions", tableProperties);
		} catch (SQLException e) {
			throw new RuntimeException("Internal database error.", e);
		}

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource) sce.getServletContext()
				.getAttribute("hr.fer.zemris.dbpool");

		if (cpds != null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method is used for creating the tables if needed.
	 * 
	 * @param con
	 *            connection with database
	 * @param table
	 *            table name
	 * @param p
	 *            properties object which holds the informations used for creating
	 *            the tables
	 * @throws SQLException
	 *             if database error occurs
	 */
	private static void createIfNotExists(Connection con, String table, Properties p) throws SQLException {
		String createSql = p.getProperty(table + ".create");
		
		DatabaseMetaData meta = con.getMetaData();
		ResultSet res = meta.getTables(null, null, table.toUpperCase(), 
		     new String[] {"TABLE"});
		
		if(!res.next()) {
			con.prepareStatement(createSql).execute();
		}

		ResultSet rs = con.prepareStatement("SELECT * FROM " + table).executeQuery();
		if (!rs.next()) {
			if (table.equals("polls")) {
				initPolls(con, p);
			} else if (table.equals("pollOptions")) {
				initPollOptions(con, p);
			}
		} else {
			rs.close();
		}

	}

	/**
	 * This method is used for initializing the table "Polls" used for storing the
	 * informations about the active polls.
	 * 
	 * @param con
	 *            database connection
	 * @param p
	 *            properties object which holds the informations used for creating
	 *            the tables
	 * @throws SQLException
	 *             if database error occurs
	 */
	private static void initPolls(Connection con, Properties p) throws SQLException {
		String[] valueSet = p.getProperty("polls.values").split("\\|");

		PreparedStatement s = con.prepareStatement("INSERT INTO polls (title, message) VALUES (?,?)",
				Statement.RETURN_GENERATED_KEYS);
		con.setAutoCommit(false);
		for (String values : valueSet) {
			String[] parts = values.split("\t");

			s.setString(1, parts[0]);
			s.setString(2, parts[1]);

			s.addBatch();
		}

		s.executeBatch();
		con.commit();
		con.setAutoCommit(true);
		s.close();
	}

	/**
	 * This method is used for initializing the table "PollOptions" used for storing
	 * the informations about the active poll options.
	 * 
	 * @param con
	 *            database connection
	 * @param p
	 *            properties object which holds the informations used for creating
	 *            the tables
	 * @throws SQLException
	 *             if database error occurs
	 * @param con
	 * @param p
	 * @throws SQLException
	 */
	private static void initPollOptions(Connection con, Properties p) throws SQLException {
		String[] valueSet = p.getProperty("pollOptions.values").split("\\|");

		PreparedStatement s = con.prepareStatement(
				"INSERT INTO polloptions (optionTitle, optionLink, pollID, votesCount) VALUES (?,?,?,?)");
		con.setAutoCommit(false);
		for (String values : valueSet) {
			String[] parts = values.split("\t");

			PreparedStatement idS = con.prepareStatement("SELECT ID FROM polls WHERE title=?");
			idS.setString(1, parts[2]);
			ResultSet rs = idS.executeQuery();

			long id;
			if (rs.next()) {
				id = rs.getLong(1);
				rs.close();
			} else {
				rs.close();
				continue;
			}

			idS.close();

			s.setString(1, parts[0]);
			s.setString(2, parts[1]);
			s.setLong(3, id);
			s.setLong(4, 0L);

			s.addBatch();
		}

		s.executeBatch();
		con.commit();
		con.setAutoCommit(true);
		s.close();
	}
}
