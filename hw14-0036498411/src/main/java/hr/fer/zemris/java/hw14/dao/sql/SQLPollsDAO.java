package hr.fer.zemris.java.hw14.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw14.dao.DAO;
import hr.fer.zemris.java.hw14.dao.DAOException;
import hr.fer.zemris.java.hw14.model.Poll;

/**
 * This class describes a {@link DAO} object which works with the table "Polls"
 * from the database.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SQLPollsDAO implements DAO<Poll> {

	@Override
	public List<Poll> getList(String sortBy) throws DAOException {
		List<Poll> entries = new ArrayList<>();
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement("SELECT id, title, message FROM polls ORDER BY " + sortBy);

			try {
				ResultSet rs = pst.executeQuery();
				try {
					while (rs != null && rs.next()) {
						Poll entry = new Poll(rs.getLong(1), rs.getString(2), rs.getString(3));
						entries.add(entry);
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception ex) {
			throw new DAOException("Error while reading the list of polls descriptions.", ex);
		}
		return entries;
	}

	@Override
	public Poll getByID(long id) throws DAOException {
		Poll entry = null;
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement("SELECT id, title, message FROM polls WHERE id=?");
			pst.setLong(1, Long.valueOf(id));

			try {
				ResultSet rs = pst.executeQuery();
				try {
					if (rs != null && rs.next()) {
						entry = new Poll(rs.getLong(1), rs.getString(2), rs.getString(3));
					}
				} finally {
					try {
						rs.close();
					} catch (Exception ignorable) {
					}
				}
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception ex) {
			throw new DAOException("Error while obtaining poll description. (ID = " + id + ").", ex);
		}

		return entry;
	}

	@Override
	public boolean save(Poll entry) {
		long id = Objects.requireNonNull(entry).getID();

		Poll test = getByID(id);
		if (test == null) {
			return executeInsert(entry);
		} else {
			return executeUpdate(entry);
		}

	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * <b>NOTE:</b>Unimplemented.
	 * </p>
	 */
	@Override
	public List<Poll> getByForeignID(long fid, String sortBy) {
		throw new UnsupportedOperationException("Poll table does't have foreign key.");
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * <b>NOTE:</b>Unimplemented.
	 * </p>
	 */
	@Override
	public Poll getByIDAndFID(long id, long fid) throws DAOException {
		throw new UnsupportedOperationException("Poll table does't have foreign key.");
	}

	/**
	 * This method is used internally for executing the insert into table.
	 * 
	 * @param entry
	 *            entry to be inserted
	 * @return {@code true} if operation succeeded, {@code false} otherwise
	 */
	private boolean executeInsert(Poll entry) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("INSERT	INTO pollOptions (title, message) VALUES (?,?)");
			pst.setString(1, entry.getTitle());
			pst.setString(2, entry.getMessage());

			return pst.executeUpdate() == 1;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * This method is used internally for executing the update of the table.
	 * 
	 * @param entry
	 *            entry to be updated
	 * @return {@code true} if operation succeeded, {@code false} otherwise
	 */
	private boolean executeUpdate(Poll entry) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("UPDATE pollOptions (id, title, message) = (?,?,?) WHERE id = ?");
			pst.setLong(1, entry.getID());
			pst.setString(2, entry.getTitle());
			pst.setString(3, entry.getMessage());
			pst.setLong(4, entry.getID());

			return pst.executeUpdate() == 1;
		} catch (SQLException e) {
			return false;
		}

	}

}
