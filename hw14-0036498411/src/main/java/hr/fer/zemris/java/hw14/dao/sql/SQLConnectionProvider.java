package hr.fer.zemris.java.hw14.dao.sql;

import java.sql.Connection;

/**
 * This class maintains a static map of database connections which stored by the
 * id of thread which asked for the connection.
 * 
 * 
 * @author marcupic
 *
 */
public class SQLConnectionProvider {

	/** Map of connections */
	private static ThreadLocal<Connection> connections = new ThreadLocal<>();

	/**
	 * This method sets the connection for the current thread (or deletes the entry
	 * from the map if argument is <code>null</code>).
	 * 
	 * @param con
	 *            database connection
	 */
	public static void setConnection(Connection con) {
		if (con == null) {
			connections.remove();
		} else {
			connections.set(con);
		}
	}

	/**
	 * This method obtains the connection which is used by this thread.
	 * 
	 * @return database connection
	 */
	public static Connection getConnection() {
		return connections.get();
	}

}