/**
 * This is a root package of the web application "croz-jokes".
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw14;