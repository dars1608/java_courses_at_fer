package hr.fer.zemris.java.hw14.model;

import java.util.Objects;

/**
 * This class models a poll description used in voting-app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Poll {

	/** Unique poll id */
	private long ID;
	/** Poll title */
	private String title;
	/** Description message */
	private String message;

	/**
	 * Constructor method for class {@link Poll}.
	 * 
	 * @param ID
	 *            unique poll id
	 * @param title
	 *            poll title
	 * @param message
	 *            description message
	 */
	public Poll(long ID, String title, String message) {
		this.ID = ID;
		this.title = title;
		this.message = message;
	}

	/**
	 * Getter method for the {@code iD}.
	 * 
	 * @return the iD
	 */
	public long getID() {
		return ID;
	}

	/**
	 * Setter method for the {@code iD}.
	 * 
	 * @param iD
	 *            the iD to set
	 */
	public void setID(long iD) {
		ID = iD;
	}

	/**
	 * Getter method for the {@code title}.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Setter method for the {@code title}.
	 * 
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Getter method for the {@code message}.
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Setter method for the {@code message}.
	 * 
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Poll other = (Poll) obj;

		return this.ID == other.ID;
	}

}
