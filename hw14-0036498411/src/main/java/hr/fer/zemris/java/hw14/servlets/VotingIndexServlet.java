package hr.fer.zemris.java.hw14.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw14.dao.DAOPollsProvider;
import hr.fer.zemris.java.hw14.model.Poll;

/**
 * This class describes a {@link HttpServlet} used for fetching and passing the
 * list of {@link Poll} objects to the view.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = { "/servleti/index.html" })
public class VotingIndexServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Poll> polls = DAOPollsProvider.getDao().getList("id");

		if (polls == null) {
			resp.sendError(500);
			return;
		}

		req.setAttribute("polls", polls);
		req.getRequestDispatcher("/WEB-INF/pages/index.jsp").forward(req, resp);
	}

}
