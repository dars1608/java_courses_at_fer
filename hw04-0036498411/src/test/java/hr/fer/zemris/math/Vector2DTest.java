package hr.fer.zemris.math;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector2DTest {

	@Test
	public void testTranslate() {
		Vector2D test = new Vector2D(2, -4.5);
		Vector2D offset = new Vector2D(1, 1.5);
		test.translate(offset);
		assertEquals(3, test.getX(), 1e-10);
		assertEquals(-3, test.getY(), 1e-10);
	}

	@Test
	public void testRotate() {
		Vector2D test = new Vector2D(1, 1);
		test.rotate(45);
		assertEquals(0, test.getX(), 1e-10);
		assertEquals(Math.sqrt(2), test.getY(), 1e-10);
		test.rotate(45);
		assertEquals(-1, test.getX(), 1e-10);
		assertEquals(1, test.getY(), 1e-10);
		test.rotate(45);
		assertEquals(-Math.sqrt(2), test.getX(), 1e-10);
		assertEquals(0, test.getY(), 1e-10);
		test.rotate(45);
		assertEquals(-1, test.getX(), 1e-10);
		assertEquals(-1, test.getY(), 1e-10);
		test.rotate(45);
		assertEquals(0, test.getX(), 1e-10);
		assertEquals(-Math.sqrt(2), test.getY(), 1e-10);
		test.rotate(45);
		assertEquals(1, test.getX(), 1e-10);
		assertEquals(-1, test.getY(), 1e-10);
	}

	@Test
	public void testScale() {
		Vector2D test = new Vector2D(1, 1);
		test.scale(2.5);
		assertEquals(2.5, test.getX(), 1e-10);
		assertEquals(2.5, test.getY(), 1e-10);
	}

	@Test
	public void testCopy() {
		Vector2D test = new Vector2D(1, 1);
		Vector2D copy = test.copy();
		assertEquals(test.getX(), copy.getX(), 1e-10);
		assertEquals(test.getY(), copy.getY(), 1e-10);
	}
	
	@SuppressWarnings( value ="unused")
	@Test(expected=NullPointerException.class)
	public void testOffsetNullReference() {
		Vector2D test = new Vector2D(1,1).translated(null);
	}

}
