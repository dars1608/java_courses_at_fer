package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.*;

import java.util.Objects;

import org.junit.Before;
import org.junit.Test;

public class DictionaryTest {
	private Dictionary test;

	@Before
	public void setUp() throws Exception {
		test = new Dictionary();
		for(int i = 0; i<10; i++) {
			test.put(i, "a"+i);
		}
	}

	@Test
	public void testIsEmpty() {
		Dictionary d = new Dictionary();
		assertTrue(d.isEmpty());
	}

	@Test
	public void testSize() {
		assertEquals(10, test.size());
	}

	@Test
	public void testClear() {
		test.clear();
		assertTrue(test.isEmpty());
	}

	@Test
	public void testPut() {
		for(int i = 5; i<10; i++) {
			test.put(i, "b"+i);
		}
		for(int i = 0; i<5; i++) {
			assertTrue(Objects.equals("a"+i, test.get(i)));
		}
		for(int i = 5; i<10; i++) {
			assertTrue(Objects.equals("b"+i, test.get(i)));
		}
	}
	
	@Test(expected=NullPointerException.class)
	public void testNullKey() {
		test.put(null, "x");
	}
	
	@Test(expected=NullPointerException.class)
	public void testNullKey2() {
		test.get(null);
	}

	@Test
	public void testGet() {
		for(int i = 0; i<10; i++) {
			assertTrue(Objects.equals("a"+i, test.get(i)));
		}
	}

}
