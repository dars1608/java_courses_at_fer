package demo;

import hr.fer.zemris.lsystems.gui.LSystemViewer;
import hr.fer.zemris.lsystems.impl.LSystemBuilderImpl;

/**
 * Demonstration program for class
 * {@link hr.fer.zemris.lsystems.impl.LSystemBuilderImpl}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Glavni3 {	//note: some other test files are added, to test other commands (skip, scale)

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		LSystemViewer.showLSystem(LSystemBuilderImpl::new);

	}

}
