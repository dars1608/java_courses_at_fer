/**
 * This package contains classes used to describe 2D space.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.math;