package hr.fer.zemris.math;

import static java.lang.Math.atan;
import static java.lang.Math.PI;
import static java.lang.Math.sqrt;

import java.util.Objects;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 *
 * @author Darko Britvec
 * @version 1.0
 */
public class Vector2D {

	/** X coordinate */
	private double x;
	/** Y coordinate */
	private double y;

	/**
	 * Constructor method for class Vector2D.
	 * 
	 * @param x
	 *            value of x coordinate
	 * @param y
	 *            value of y coordinate
	 */
	public Vector2D(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * Getter method for <code>x</code> value.
	 * 
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Getter method for <code>y</code> value.
	 * 
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * This method is used for translating this vector at given offset.
	 * 
	 * @param offset
	 * @throws NullPointerException
	 *             if given offset reference is <code>null</code>.
	 */
	public void translate(Vector2D offset) {
		Objects.requireNonNull(offset);

		this.x += offset.x;
		this.y += offset.y;
	}

	/**
	 * This method creates a copy of this vector translated at given offset.
	 * 
	 * @param offset
	 * @return copy of translated vector
	 * @throws NullPointerException
	 *             if given offset reference is <code>null</code>.
	 */
	public Vector2D translated(Vector2D offset) {
		Vector2D ret = copy();
		ret.translate(offset);

		return ret;
	}

	/**
	 * This method is used for rotating this vector by given angle.
	 * 
	 * @param angle
	 *            angle of rotation in degrees
	 * @throws NullPointerException
	 *             if given offset reference is <code>null</code>.
	 */
	public void rotate(double angle) {
		double currentAngle = getAngle();
		double length = getLength();

		currentAngle = (currentAngle + angle / 180.0 * PI) % (2 * PI);

		this.x = length * cos(currentAngle);
		this.y = length * sin(currentAngle);

	}

	/**
	 * This method returns copy of this vector rotated by an angle.
	 * 
	 * @param angle
	 *            angle of rotation in degrees
	 * @return rotated vector
	 */
	public Vector2D rotated(double angle) {
		Vector2D ret = copy();
		ret.rotate(angle);

		return ret;

	}

	/**
	 * This method is used for scaling the vector by given parameter.
	 * 
	 * @param scaler
	 *            Parameter used for scaling.
	 */
	public void scale(double scaler) {
		double angle = getAngle();
		double currentLengt = getLength();

		this.x = scaler * currentLengt * cos(angle);
		this.y = scaler * currentLengt * sin(angle);
	}

	/**
	 * This method returns a copy of this vector scaled by given parameter.
	 * 
	 * @param scaler
	 *            Parameter used for scaling
	 * @return scaled vector
	 */
	public Vector2D scaled(double scaler) {
		Vector2D ret = copy();
		ret.scale(scaler);

		return ret;
	}

	/**
	 * This method is used for creating a copy of this vector.
	 * 
	 * @return copy of this vector
	 */
	public Vector2D copy() {
		return new Vector2D(x, y);
	}

	/**
	 * This method is used internally for calculating normed angle of this vector.
	 * 
	 * @return angle in radians.
	 */
	private double getAngle() {
		double currentAngle;
		if (x == 0) {
			currentAngle = y > 0 ? PI / 2 : 3 * PI / 2;
		} else {
			currentAngle = atan(y / x);
			if (x < 0 && y > 0 || x < 0 && y < 0) {
				currentAngle += PI;
			} else if (x > 0 && y < 0) {
				currentAngle += 2 * PI;
			}
		}
		return currentAngle;
	}

	/**
	 * This method is used internally for calculating lenght of this vector.
	 * 
	 * @return lenght
	 */
	private double getLength() {
		return sqrt(x * x + y * y);
	}
}
