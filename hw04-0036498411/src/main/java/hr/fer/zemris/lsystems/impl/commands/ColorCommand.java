package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * This class describes {@link Command} which modifies color of current
 * {@link TurtleState}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ColorCommand implements Command {
	
	/** Turtle color */
	private Color color;

	/**
	 * Constructor method for class {@link ColorCommand}.
	 * 
	 * @param color
	 */
	public ColorCommand(Color color) {
		super();
		this.color = color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.lsystems.impl.Command#execute(hr.fer.zemris.lsystems.impl.
	 * Context, hr.fer.zemris.lsystems.Painter)
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currentState = ctx.getCurrentState();
		currentState.setColor(color);

	}

}
