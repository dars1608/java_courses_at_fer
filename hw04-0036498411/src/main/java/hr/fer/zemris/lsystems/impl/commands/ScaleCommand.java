package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * This class describes {@link Command} which modifies effective lenght of
 * current {@link TurtleState}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ScaleCommand implements Command {
	
	/** Factor of scaling */
	private double factor;

	/**
	 * Constructor method for class {@link ScaleCommand}.
	 * @param factor
	 */
	public ScaleCommand(double factor) {
		super();
		this.factor = factor;
	}

	/*
	 * (non-Javadoc)
	 * @see hr.fer.zemris.lsystems.impl.Command#execute(hr.fer.zemris.lsystems.impl.Context, hr.fer.zemris.lsystems.Painter)
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState currentState = ctx.getCurrentState();
		currentState.setEffectiveLenght(factor*currentState.getEffectiveLenght());
		
	}
	
	
}
