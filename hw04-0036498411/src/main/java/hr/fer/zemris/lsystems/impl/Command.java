package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * This interface describes one method which is performed on {@link Painter}.
 * It also modifies {@link Context}.
 * @author Darko Britvec
 * @version 1.0
 */
public interface Command {
	
	/**
	 * This method executes command represented by this class.
	 * @param ctx {@link Context} of painter
	 * @param painter {@link Painter} object
	 */
	void execute(Context ctx, Painter painter);
}
