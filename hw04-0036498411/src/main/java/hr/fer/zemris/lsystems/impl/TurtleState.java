package hr.fer.zemris.lsystems.impl;

import java.awt.Color;
import java.util.Objects;

import hr.fer.zemris.math.Vector2D;

/**
 * This class describes state of turtle used for drawing on screen.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TurtleState {
	
	/** Current position in coordinate system*/
	private Vector2D currentPosition;
	/** Direction vector*/
	private Vector2D currentDirection;
	/** Current color*/
	private Color color;
	/** Current effective length */
	private double effectiveLenght;

	/**
	 * Constructor method for class {@link TurtleState}.
	 * 
	 * @param currentPosition
	 * @param currentDirection
	 * @param color
	 * @param effectiveLenght
	 */
	public TurtleState(Vector2D currentPosition, Vector2D currentDirection, Color color, double effectiveLenght) {
		super();
		this.currentPosition = Objects.requireNonNull(currentPosition);
		this.currentDirection = Objects.requireNonNull(currentDirection);
		this.color = Objects.requireNonNull(color);
		this.effectiveLenght = effectiveLenght;
	}
	
	/**
	 * This method is used for copying current {@link TurtleState}.
	 * 
	 * @return current {@link TurtleState}
	 */
	public TurtleState copy() {
		return new TurtleState(currentPosition.copy(), currentDirection.copy(), color, effectiveLenght);
	}

	/**
	 * Getter method for <code>currentPosition</code>.
	 * 
	 * @return the currentPosition
	 */
	public Vector2D getCurrentPosition() {
		return currentPosition;
	}

	/**
	 * Setter method for <code>currentPosition</code>.
	 * 
	 * @param currentPosition
	 *            the currentPosition to set
	 */
	public void setCurrentPosition(Vector2D currentPosition) {
		this.currentPosition = currentPosition;
	}

	/**
	 * Getter method for <code>currentDirection</code>.
	 * 
	 * @return the currentDirection
	 */
	public Vector2D getCurrentDirection() {
		return currentDirection;
	}

	/**
	 * Setter method for <code>currentDirection</code>.
	 * 
	 * @param currentDirection
	 *            the currentDirection to set
	 */
	public void setCurrentDirection(Vector2D currentDirection) {
		this.currentDirection = currentDirection;
	}

	/**
	 * Getter method for <code>color</code>.
	 * 
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Setter method for <code>color</code>.
	 * 
	 * @param color
	 *            the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Getter method for <code>effectiveLenght</code>.
	 * 
	 * @return the effectiveLenght
	 */
	public double getEffectiveLenght() {
		return effectiveLenght;
	}

	/**
	 * Setter method for <code>effectiveLenght</code>.
	 * 
	 * @param effectiveLenght
	 *            the effectiveLenght to set
	 */
	public void setEffectiveLenght(double effectiveLenght) {
		this.effectiveLenght = effectiveLenght;
	}

}
