/**
 * This package contains classes used for drawing Lindenmayer systems. An
 * L-system or Lindenmayer system is a parallel rewriting system and a type of
 * formal grammar. An L-system consists of an alphabet of symbols that can be
 * used to make strings, a collection of production rules that expand each
 * symbol into some larger string of symbols, an initial "axiom" string from
 * which to begin construction, and a mechanism for translating the generated
 * strings into geometric structures.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.lsystems.impl;