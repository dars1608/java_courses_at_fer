package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;

/**
 * This class describes {@link Command} which translates turtle on
 * {@link Painter} without drawing a line, and modifies {@link Context}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SkipCommand implements Command {
	
	/** Length of translation */
	private double step;

	/**
	 * Constructor method for class {@link SkipCommand}.
	 * 
	 * @param step
	 */
	public SkipCommand(double step) {
		super();
		this.step = step;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.lsystems.impl.Command#execute(hr.fer.zemris.lsystems.impl.
	 * Context, hr.fer.zemris.lsystems.Painter)
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState nextState = ctx.getCurrentState();
		Vector2D transVector = nextState.getCurrentDirection();
		
		transVector = transVector.scaled(step*ctx.getCurrentState().getEffectiveLenght());
		nextState.getCurrentPosition().translate(transVector);
	}

}
