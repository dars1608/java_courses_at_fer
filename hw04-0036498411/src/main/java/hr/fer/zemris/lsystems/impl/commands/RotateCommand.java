package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;

/**
 * This class describes {@link Command} used for rotating current
 * {@link TurtleState}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class RotateCommand implements Command {
	
	/** Angle of rotation */
	private double angle;

	/**
	 * Default constructor for class {@link RotateCommand}.
	 * 
	 * @param angle
	 *            angle of rotation
	 */
	public RotateCommand(double angle) {
		this.angle = angle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see hr.fer.zemris.lsystems.impl.Command#execute(hr.fer.zemris.lsystems.impl.
	 * Context, hr.fer.zemris.lsystems.Painter)
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		ctx.getCurrentState().getCurrentDirection().rotate(angle);
	}

}
