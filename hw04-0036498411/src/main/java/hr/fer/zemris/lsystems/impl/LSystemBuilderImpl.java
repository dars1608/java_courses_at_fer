package hr.fer.zemris.lsystems.impl;

import static java.lang.Math.abs;
import static java.lang.Math.pow;

import java.awt.Color;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.custom.collections.Dictionary;
import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.commands.ColorCommand;
import hr.fer.zemris.lsystems.impl.commands.DrawCommand;
import hr.fer.zemris.lsystems.impl.commands.PopCommand;
import hr.fer.zemris.lsystems.impl.commands.PushCommand;
import hr.fer.zemris.lsystems.impl.commands.RotateCommand;
import hr.fer.zemris.lsystems.impl.commands.ScaleCommand;
import hr.fer.zemris.lsystems.impl.commands.SkipCommand;
import hr.fer.zemris.math.Vector2D;

/**
 * This class describes implementation of {@link LSystemBuilder}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LSystemBuilderImpl implements LSystemBuilder {

	/** Registered commands */
	private Dictionary commands;
	/** Registered productions */
	private Dictionary productions;
	/** Initial position of a turtle */
	private Vector2D origin;
	/** Initial direction angle of a turtle */
	private double angle;
	/** Initial effective length of a turtle */
	private double unitLength;
	/** Scaler used for drawing fractals of constant size */
	private double unitLengthDegreeScaler;
	/** Initial command */
	private String axiom;

	// Initialization block
	{
		commands = new Dictionary();
		productions = new Dictionary();
		unitLength = DEFAULT_UNIT_LENGTH;
		unitLengthDegreeScaler = DEFAULT_SCALER;
		origin = DEFAULT_ORIGIN;
		angle = DEFAULT_ANGLE;
		axiom = DEFAULT_AXIOM;
	}

	/**
	 * This method is used for creating an implementation of @link {@link LSystem}.
	 * 
	 * @return an implementation of @link {@link LSystem}
	 */
	@Override
	public LSystem build() {
		return new LSystemImpl();
	}

	/**
	 * This method is used for configuring {@link LSystemBuilder}
	 * 
	 * @param directives
	 *            configure directives
	 * @return this {@link LSystemBuilder}
	 * @throws NullPointerException
	 *             if given array reference, or any reference stored in array are
	 *             <code>null</code>
	 * @throws IllegalArgumentException
	 * 			   if some directive is illegal.
	 */
	@Override
	public LSystemBuilder configureFromText(String[] directives) {
		Objects.requireNonNull(directives);
		
		for (String input : directives) {
			Objects.requireNonNull(input);
			if (input.equals(DEFAULT_AXIOM)) {
				continue;
			}

			input = input.trim();

			Pattern p = Pattern.compile(AXIOM_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			Matcher m = p.matcher(input);
			if (m.find()) {
				setAxiom(m.group(1));
				continue;
			}

			p = Pattern.compile(COMMAND_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				registerCommand(m.group(1).charAt(0), m.group(2));
				continue;
			}

			p = Pattern.compile(PRODUCTION_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				registerProduction(m.group(1).charAt(0), m.group(2));
				continue;
			}

			p = Pattern.compile(ANGLE_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				setAngle(Double.parseDouble(m.group(1)));
				continue;
			}

			p = Pattern.compile(UNIT_LENGHT_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				setUnitLength(Double.parseDouble(m.group(1)));
				continue;
			}

			p = Pattern.compile(UNIT_SCALER1_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				double a = Double.parseDouble(m.group(1));
				double b = Double.parseDouble(m.group(2));
				if (b == 0) {
					throw new ArithmeticException("Division by zero");
				}
				setUnitLengthDegreeScaler(a / b);
				continue;
			}

			p = Pattern.compile(UNIT_SCALER2_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				setUnitLengthDegreeScaler(Double.parseDouble(m.group(1)));
				continue;
			}

			p = Pattern.compile(ORIGIN_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			m = p.matcher(input);
			if (m.find()) {
				double x = Double.parseDouble(m.group(1));
				double y = Double.parseDouble(m.group(2));
				setOrigin(x, y);
				continue;
			}
			
			throw new IllegalArgumentException("Invalid directive: \"" + input + "\"");
		}
		return this;
	}

	/**
	 * This method is used for assigning command of this {@link LSystemBuilder}.
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            command
	 * @return this {@link LSystemBuilder}
	 * @throws NullPointerException
	 *             if value parameter is <code>null</code>
	 */
	@Override
	public LSystemBuilder registerCommand(char key, String value) {
		Objects.requireNonNull(value);
		value = value.trim();
		
		Pattern p = Pattern.compile(PUSH_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher m = p.matcher(value);
		if (m.find()) {
			commands.put(key, new PushCommand());
			return this;
		}
		
		p = Pattern.compile(POP_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(value);
		if(m.find()) {
			commands.put(key, new PopCommand());
			return this;
		}
		
		p = Pattern.compile(DRAW_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(value);
		if(m.find()) {
			double step = Double.parseDouble(m.group(1));
			commands.put(key, new DrawCommand(step));
			return this;
		}
		
		p = Pattern.compile(SKIP_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(value);
		if(m.find()) {
			double step = Double.parseDouble(m.group(1));
			commands.put(key, new SkipCommand(step));
			return this;
		}
		
		p = Pattern.compile(ROTATE_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(value);
		if(m.find()) {
			double angle = Double.parseDouble(m.group(1));
			commands.put(key, new RotateCommand(angle));
			return this;
		}
		
		p = Pattern.compile(SCALE_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(value);
		if(m.find()) {
			double factor = Double.parseDouble(m.group(1));
			commands.put(key, new ScaleCommand(factor));
			return this;
		}
		
		p = Pattern.compile(COLOR_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		m = p.matcher(value);
		if(m.find()) {
			commands.put(key, new ColorCommand(Color.decode("#" + m.group(1))));
			return this;
		}

		throw new IllegalArgumentException("Operation \"" + value + "\" can't be parsed");
	}

	/**
	 * This method is used for assigning production of this {@link LSystemBuilder}.
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            production
	 * @return this {@link LSystemBuilder}
	 * @throws NullPointerException
	 *             if value parameter is <code>null</code>
	 */
	@Override
	public LSystemBuilder registerProduction(char key, String production) {
		Objects.requireNonNull(production);
		production = production.trim();

		productions.put(key, production);
		return this;
	}

	/**
	 * This method is used for setting an angle of this {@link LSystemBuilder}.
	 * 
	 * @param angle
	 *            angle in degrees
	 * @return this {@link LSystemBuilder}
	 */
	@Override
	public LSystemBuilder setAngle(double angle) {
		angle = angle % 360.0;
		this.angle = angle > 0 ? angle : angle + 360;
		return this;
	}

	/**
	 * This method is used for assigning axiom of this {@link LSystemBuilder}.
	 * 
	 * @param axiom
	 *            axiom
	 * @return this {@link LSystemBuilder}
	 * @throws NullPointerException
	 *             if axiom reference is <code>null</code>
	 */
	@Override
	public LSystemBuilder setAxiom(String axiom) {
		this.axiom = Objects.requireNonNull(axiom);
		return this;
	}

	/**
	 * This method is used for setting an initial position of turtle. Coordinates
	 * must be set in range [0,1].
	 * 
	 * @param x
	 *            x-coordinate
	 * @param y
	 *            y-coordinate
	 * @return this {@link LSystemBuilder}
	 * @throws IllegalArgumentException
	 *             if coordinates are out of range
	 */
	@Override
	public LSystemBuilder setOrigin(double x, double y) {
		if (abs(x) > 1 || abs(y) > 1) {
			throw new IllegalArgumentException(
					String.format("Origin of coordinates must be 0 <= x,y <= 1. Are x=%d, y=%d.", x, y));
		}
		this.origin = new Vector2D(x, y);
		return this;
	}

	/**
	 * This method sets unit length of coordinate system.
	 * 
	 * @param unitLength
	 *            unit length
	 * @return this {@link LSystemBuilder}
	 * @throws IllegalArgumentException
	 *             if length is 0 or negative
	 */
	@Override
	public LSystemBuilder setUnitLength(double unitLenght) {
		if (unitLenght <= 0) {
			throw new IllegalArgumentException("Unit lenght can't be negative.");
		}
		this.unitLength = unitLenght;
		return this;
	}

	/**
	 * This method sets unit length degree scaler.
	 * 
	 * @param unitLengthDegreeScaler
	 *            scaler
	 * @return this {@link LSystemBuilder}
	 * @throws IllegalArgumentException
	 *             if scaler is 0 or negative.
	 */
	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double scaler) {
		if (scaler <= 0) {
			throw new IllegalArgumentException("Unit lenght scaler can't be negative.");
		}

		this.unitLengthDegreeScaler = scaler;
		return this;
	}

	/**
	 * This class represents generator of Lindenmayers systems. It implements
	 * interface {@link LSystem}.
	 *
	 * @author Darko Britvec
	 * @version 1.0
	 */
	class LSystemImpl implements LSystem {

		/**
		 * This method is used for drawing the Lindenmayer system of given level on
		 * provided {@link Painter}.
		 * 
		 * @param level
		 *            fractal level
		 * @param painter
		 *            {@link Painter}
		 * 
		 * @throws IllegalArgumentException
		 *             if level is less than 0
		 */
		@Override
		public void draw(int level, Painter painter) {
			Objects.requireNonNull(painter);
			if (level < 0) {
				throw new IllegalArgumentException("Level can't be negative. Is " + level);
			}
			
			Context context = new Context();
			context.pushState(
						new TurtleState(origin.copy(), 
						new Vector2D(1, 0).rotated(angle),
						Color.BLACK,
						unitLength * (pow(unitLengthDegreeScaler, level))));
			
			char[] sequence = generate(level).toCharArray();
			for (char key : sequence) {
				Command c = (Command) commands.get(key);
				if (c != null) {
					c.execute(context, painter);
				}
			}
		}

		/**
		 * This method is used for generating string representation of the Lindenmayer
		 * system.
		 * 
		 * @param level
		 *            fractal level
		 * @return string representation of a fractal
		 * @throws IllegalArgumentException
		 * 			  if level parameter is negative
		 */
		@Override
		public String generate(int level) {
			if(level<0) {
				throw new IllegalArgumentException("Level can't be negative. Is " + level);
			}
			StringBuilder sb = new StringBuilder(axiom);

			for (int i = 1; i <= level; i++) {
				char[] temp = sb.toString().toCharArray();
				sb = new StringBuilder();
				
				for (int j = 0; j < temp.length; j++) {
					char c = temp[j];
					String production = (String) productions.get(c);
					
					if (production != null) {
						sb.append(production);
					} else {
						sb.append(c);
					}
				}
			}

			return sb.toString();
		}

	}
	
	// --------------------------------constants---------------------------------------------------

	private static final String COLOR_REGEX = "color\\s+([a-f0-9]{6})\\s*$";
	private static final String POP_REGEX = "pop\\s*$";
	private static final String PUSH_REGEX = "push\\s*$";
	private static final String ROTATE_REGEX = "rotate\\s+([+-]?[0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String SCALE_REGEX = "scale\\s+([0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String SKIP_REGEX = "skip\\s+([0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String DRAW_REGEX = "draw\\s+([0-9]+[\\.]?[0-9]*)\\s*$";

	private static final String AXIOM_REGEX = "axiom\\s+(.+)";
	private static final String PRODUCTION_REGEX = "production\\s+(.)\\s+(.+)";
	private static final String COMMAND_REGEX = "command\\s+(.)\\s+(.+)";
	private static final String UNIT_SCALER2_REGEX = "unitLengthDegreeScaler\\s+([0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String UNIT_SCALER1_REGEX = "unitLengthDegreeScaler\\s+([0-9]+[\\.]?[0-9]*)\\s*[/]\\s*([0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String UNIT_LENGHT_REGEX = "unitLength\\s+([0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String ANGLE_REGEX = "angle\\s+([+-]?[0-9]+[\\.]?[0-9]*)\\s*$";
	private static final String ORIGIN_REGEX = "origin\\s+([0-9]+[\\.]?[0-9]*)\\s+([0-9]+[\\.]?[0-9]*)\\s*$";
	
	private static final String DEFAULT_AXIOM = "";
	private static final int DEFAULT_ANGLE = 0;
	private static final int DEFAULT_SCALER = 1;
	private static final double DEFAULT_UNIT_LENGTH = 0.1;
	private static final Vector2D DEFAULT_ORIGIN = new Vector2D(0, 0);
}
