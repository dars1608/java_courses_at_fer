package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 *
 * @author Darko Britvec
 * @version 1.0
 */
public class Context {
	
	/** Stack on which we put current {@link TurtleState}*/
	private ObjectStack stack;
	
	/**
	 * Constructor method for class {@link Context}.
	 */
	public Context() {
		this.stack = new ObjectStack();
	}
	
	/**
	 * This method is used for fetching current {@link TurtleState}.
	 * @return current TurtleState
	 */
	public TurtleState getCurrentState() {
		if(stack.isEmpty()) {
			return null;
		}
		return (TurtleState) stack.peek();
	}
	
	/**
	 * This method is used for pushing new {@link TurtleState} on internal stack.
	 * @param state TurtleState to be pushed on stack
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * This method removes last pushed {@link TurtleState} from internal stack.
	 */
	public void popState() {
		if(!stack.isEmpty()) {
			stack.pop();
		}
	}
}
