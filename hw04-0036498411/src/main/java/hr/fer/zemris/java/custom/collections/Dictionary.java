package hr.fer.zemris.java.custom.collections;

import java.util.Objects;

/**
 * This class represents a model of an collection which stores entries (it is
 * also called map or associative array). Every entry has its key, which can't
 * be null, and value, which can be null.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Dictionary {
	/** Backing collection used for storing data */
	private ArrayIndexedCollection data;

	/**
	 * Default constructor for class {@link Dictionary}
	 */
	public Dictionary() {
		this.data = new ArrayIndexedCollection();
	}

	/**
	 * This method checks if there's no entries stored inside the
	 * {@link Dictionary}.
	 * 
	 * @return <code>true</code> if it's empty, <code>false</code> otherwise.
	 */
	public boolean isEmpty() {
		return data.isEmpty();
	}

	/**
	 * This method is used to check how many entries are stored inside the
	 * {@link Dictionary}.
	 * 
	 * @return number of entries
	 */
	public int size() {
		return data.size();
	}

	/**
	 * This method deletes all the entries stored inside the {@link Dictionary}.
	 */
	public void clear() {
		data.clear();
	}

	/**
	 * This method is used for putting an entry into the {@link Dictionary}.
	 * 
	 * @param key
	 *            entry key
	 * @param value
	 *            entry value
	 * @throws NullPointerException
	 *             if given key is <code>null</code>.
	 */
	public void put(Object key, Object value) {
		Object tempKey = Objects.requireNonNull(key);
		int index = data.indexOf(new Entry(tempKey, value));
		if (index == -1) {
			data.add(new Entry(tempKey, value));
		} else {
			data.remove(index);
			data.insert(new Entry(tempKey, value), index);
		}
	}

	/**
	 * This method is used for acquiring value stored at given key. It returns
	 * <code>null</code> if there aren't any value stored under that key (it also
	 * returns <code>null</code> if <code>null</code> is stored under that key).
	 * 
	 * @param key
	 *            an object representing entry key
	 * @return value stored at given key, if there isn't any value stored at that
	 *         key, returns <code>null</code>
	 * @throws NullPointerException
	 *             if key reference is <code>null</code>.
	 */
	public Object get(Object key) {
		if (key == null) {
			return null;
		}
		int index = data.indexOf(new Entry(key, null));
		return index == -1 ? null : ((Entry) data.get(index)).getValue();
	}

	/**
	 * Private class used to model an entry of this dictionary.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private static class Entry {
		/** Entry key*/
		private final Object key;
		/** Entry value*/
		private final Object value;

		public Entry(Object key, Object value) {
			this.key = Objects.requireNonNull(key);
			this.value = value;
		}

		/**
		 * Getter method for value of an {@link Entry}.
		 * 
		 * @return the value
		 */
		public Object getValue() {
			return value;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			return Objects.hash(key);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (obj == this)
				return true;
			if (getClass() != obj.getClass())
				return false;
			return Objects.equals(this.key, ((Entry) obj).key);
		}
	}

}
