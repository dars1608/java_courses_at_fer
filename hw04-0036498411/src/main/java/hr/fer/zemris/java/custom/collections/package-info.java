/**
 * This package contains collections developed earlier for storing data.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections;