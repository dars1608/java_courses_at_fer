origin                 0.1 0.5
angle                  0
unitLength             0.8
unitLengthDegreeScaler 1.0 / 3

command F draw 1
command G draw 1
command + rotate 90
command - rotate -90

axiom F

production F F+F-F-F-G+F+F+F-F
production G GGG