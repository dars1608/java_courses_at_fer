origin                 0.5 0.5
angle                  0
unitLength             0.05
unitLengthDegreeScaler 1.0 / 2

command X draw 1
command Y draw 1
command + rotate 90
command - rotate -90

axiom XYXYXYX+XYXYXYX+XYXYXYX+XYXYXYX

production X X+X+XY-Y-
production Y +X+XY-Y-Y