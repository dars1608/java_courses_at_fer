origin                 0.5 0.75
angle                  0
unitLength             0.2
unitLengthDegreeScaler 1.0 / 2.0

command A draw 1
command B draw 1
command + rotate 60
command - rotate -60

axiom A

production A A-B--B+A++AA+B-
production B +A-BB--B-A++A+B

