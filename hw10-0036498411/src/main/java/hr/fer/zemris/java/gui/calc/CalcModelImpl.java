package hr.fer.zemris.java.gui.calc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.DoubleBinaryOperator;

/**
 * This class represents the implementation of {@link CalcModel} interface.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CalcModelImpl implements CalcModel {

	/** Maximal number of digits*/
	private static final int MAX_NUM_OF_DIGITS = 308;
	
	/** List of listeners */
	private List<CalcValueListener> listeners = new ArrayList<>();
	/** Stored value */
	private String stringValue = null;
	/** Pending binary operator */
	private DoubleBinaryOperator pendingOperator = null;
	/** Active operand */
	private double activeOperand;
	/** Flag determining if the active operand is set */
	private boolean isActiveOperandSet = false;
	/** Flag determining if calculator is ready for new operator*/
	private boolean ready = true;

	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(Objects.requireNonNull(l));
		l.valueChanged(this);

	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(Objects.requireNonNull(l));
	}

	@Override
	public double getValue() {
		if (stringValue == null) {
			return 0.0;
		} else {
			return Double.parseDouble(stringValue);
		}
	}

	@Override
	public void setValue(double value) {
		stringValue = Double.toString(value);
		
		notifyAllListeners();
	}

	@Override
	public void clear() {
		stringValue = null;

		notifyAllListeners();
	}

	@Override
	public void clearAll() {
		stringValue = null;
		pendingOperator = null;
		isActiveOperandSet = false;

		notifyAllListeners();
	}

	@Override
	public void swapSign() {
		if (stringValue == null) {
			return;
		} else if (stringValue.startsWith("-")) {
			stringValue = stringValue.substring(1);
		} else {
			stringValue = "-" + stringValue;
		}

		notifyAllListeners();

	}

	@Override
	public void insertDecimalPoint() {
		if (stringValue == null) {
			stringValue = "0.";
		} else if (!stringValue.contains(".")) {
			stringValue = stringValue + ".";
			notifyAllListeners();
		}
	}

	@Override
	public void insertDigit(int digit) {
		if (stringValue == null) {
			stringValue = Integer.toString(digit);
			notifyAllListeners();
			return;
		}
		
		
		if(!ready) {
			stringValue = "";
			ready = true;
		}
		
		if(stringValue.length() >= MAX_NUM_OF_DIGITS && ready) {
			return;
		}

		if (stringValue.equals("0") && digit == 0) {
			return;
		}

		if (stringValue.equals("0") && digit != 0) {
			stringValue = Integer.toString(digit);
		} else {
			stringValue = stringValue + Integer.toString(digit);
		}

		notifyAllListeners();
	}

	@Override
	public boolean isActiveOperandSet() {
		return isActiveOperandSet;
	}

	@Override
	public double getActiveOperand() {
		if (!isActiveOperandSet) {
			throw new IllegalStateException("There's no active operand.");
		}

		return activeOperand;
	}

	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;
		isActiveOperandSet = true;
	}

	@Override
	public void clearActiveOperand() {
		isActiveOperandSet = false;
	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperator;
	}

	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		if (!isActiveOperandSet) {
			isActiveOperandSet = true;
			setActiveOperand(getValue());
		} else {
			if(!ready) {
				return;
			}
			
			setActiveOperand(pendingOperator.applyAsDouble(activeOperand, getValue()));
			setValue(activeOperand);
		}
		
		this.pendingOperator = Objects.requireNonNull(op);
		ready = false;
	}

	@Override
	public String toString() {
		if (stringValue == null) {
			return Integer.toString(0);
		} else {
			return stringValue;
		}
	}
	

	/**
	 * This method is used internally to notify all active listeners about change of
	 * value.
	 */
	private void notifyAllListeners() {
		for (CalcValueListener l : listeners) {
			l.valueChanged(this);
		}
	}
}
