package hr.fer.zemris.java.gui.calc;

/**
 * This interface describes the listener of the {@link CalcModel}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@FunctionalInterface
public interface CalcValueListener {

	/**
	 * This method is called by the {@link CalcModel} object whenever the observed
	 * value is changed.
	 * 
	 * @param model
	 */
	void valueChanged(CalcModel model);
}