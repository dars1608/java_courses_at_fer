/**
 * This package contains implementations of
 * {@link hr.fer.zemris.java.gui.calc.CalcBinaryOperator} objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.gui.calc.binaryoperators;