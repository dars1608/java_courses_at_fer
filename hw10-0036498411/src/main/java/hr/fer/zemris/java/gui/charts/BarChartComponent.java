package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.util.Comparator;
import java.util.Objects;

import javax.swing.JComponent;

/**
 * This class models {@link JComponent} used for showing bar chart based on
 * given {@link BarChart} model passed through constructor.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class BarChartComponent extends JComponent {

	/** Serial version */
	private static final long serialVersionUID = -2290688286165650352L;
	
	/** Default bar color */
	private static final Color BAR_COLOR = Color.decode("#F47748");
	/** Default line color */
	private static final Color LINE_COLOR = Color.decode("#EEDCBB");
	/** Default component size*/
	private static final Dimension DEFAULT_SIZE = new Dimension(640, 480);

	/** Offset from the edge of the component */
	private static final int OFFSET_FROM_EDGE = 15;
	/** Offset between numbers and axis */
	private static final int OFFSET_SMALL = 3;
	/** Offset between descriptions and numbers */
	private static final int OFFSET_BIG = 5;

	/** Bar chart model */
	private BarChart model;
	/** Numbers to be placed alongside y-axis */
	private String[] yNumbers;
	/** Maximum length of the number which will be placed alongside y-axis */
	private int maxNumLength = 0;

	/**
	 * Constructor method for class {@link BarChartComponent}.
	 * 
	 * @param model
	 *            bar chart model
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	public BarChartComponent(BarChart model) {
		this.model = Objects.requireNonNull(model);

		this.model.getValues().sort(Comparator.comparing(XYValue::getX));
		initNumbers(model.getyMin(), model.getyMax(), model.getStep());
		
		setPreferredSize(DEFAULT_SIZE);
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D painter = (Graphics2D) g;
		Dimension dim = getSize();

		painter.setFont(Font.decode("Arial-13")); // font for the descriptions alongside axis
		FontMetrics fm = g.getFontMetrics();
		
		//gathering info which will later be used for drawing
		int textHeight = fm.getHeight();
		int yDescWidth = fm.stringWidth(model.getYDescription());
		int xDescWidth = fm.stringWidth(model.getXDescription());
		int numOfXNum = model.getValues().size();
		int yOfXAxis = dim.height - (OFFSET_FROM_EDGE + 2 * textHeight + OFFSET_BIG + OFFSET_SMALL);
		int xOfYAxis = OFFSET_FROM_EDGE + textHeight + OFFSET_BIG + maxNumLength + OFFSET_SMALL;
		int xStepLength = (dim.width - OFFSET_FROM_EDGE - xOfYAxis) / numOfXNum;
		int yStepLength = (yOfXAxis - OFFSET_FROM_EDGE) / yNumbers.length;

		drawLabels(painter, dim, xOfYAxis, yOfXAxis, xDescWidth, yDescWidth);
		drawShadows(painter, xOfYAxis, yOfXAxis, xStepLength, yStepLength);
		drawGrid(painter, dim, xOfYAxis, yOfXAxis, xStepLength, yStepLength, numOfXNum);
		drawBars(painter, xOfYAxis, yOfXAxis, xStepLength, yStepLength);

	}

	/**
	 * This method is used internally to initialize numbers which will be printed
	 * alongside y-axis.
	 * 
	 * @param min
	 *            minimal number
	 * @param max
	 *            maximal number
	 * @param step
	 *            difference between two numbers
	 */
	private void initNumbers(int min, int max, int step) {
		yNumbers = new String[(max - min) / step + 1];

		for (int num = min, offset = 0; num <= max; num += step) {
			yNumbers[offset] = Integer.toString(num);

			if (yNumbers[offset].length() > maxNumLength) {
				maxNumLength = yNumbers[offset].length();
			}

			offset++;
		}

	}

	/**
	 * This method is used internally for drawing the labels (description of the
	 * axis).
	 * 
	 * @param painter
	 *            {@link Graphics2D} object
	 * @param xOfYAxis
	 *            relative position of x axis
	 * @param yOfXAxis
	 *            relative position of y axis
	 * @param xDescWidth
	 *            width of description alongside x-axis
	 * @param yDescWidth
	 *            width of description alongside y-axis
	 */
	private void drawLabels(Graphics2D painter, Dimension dim, int xOfYAxis, int yOfXAxis, int xDescWidth,
			int yDescWidth) {

		// performing rotation to write label alongside y-axis
		AffineTransform oldTransform = painter.getTransform();
		AffineTransform at = new AffineTransform(oldTransform);
		at.rotate(-Math.PI / 2);
		painter.setTransform(at);

		// centering and drawing the y-axis label
		int yDescPos = (yOfXAxis + OFFSET_FROM_EDGE - yDescWidth) / 2 + yDescWidth;
		painter.drawString(model.getYDescription(), -(dim.height - yDescPos), OFFSET_FROM_EDGE);
		painter.setTransform(oldTransform); // return state to previous

		// centering and drawing the x-axis label
		int xDescPos = (dim.width + xOfYAxis - xDescWidth) / 2;
		painter.drawString(model.getXDescription(), xDescPos, dim.height - OFFSET_FROM_EDGE);
	}

	/**
	 * This method is used internally for drawing shadows of bars
	 * 
	 * @param painter
	 *            {@link Graphics2D} object
	 * @param xOfYAxis
	 *            relative position of x axis
	 * @param yOfXAxis
	 *            relative position of y axis
	 * @param xStepLength
	 *            effective step length (horizontal direction)
	 * @param yStepLength
	 *            effective step length (vertical direction)
	 */
	private void drawShadows(Graphics2D painter, int xOfYAxis, int yOfXAxis, int xStepLength, int yStepLength) {
		painter.setColor(Color.LIGHT_GRAY);
		int offset = 0;
		final int TRANSLATION = 5;

		for (XYValue xy : model.getValues()) {

			int yValue = xy.getY();
			if (yValue < model.getyMin()) {
				yValue = model.getyMin();
			} else if (yValue > model.getyMax()) {
				yValue = model.getyMax();
			}

			int x = xOfYAxis + offset * xStepLength + TRANSLATION;
			int y = yOfXAxis - (yValue - model.getyMin()) * yStepLength / model.getStep() + TRANSLATION;

			int[] rectangleX = { x, x, x + xStepLength, x + xStepLength };
			int[] rectangleY = { y, yOfXAxis, yOfXAxis, y };

			Polygon rectangle = new Polygon(rectangleX, rectangleY, 4);
			painter.fillPolygon(rectangle);
			offset++;
		}
	}

	/**
	 * This method is used internally for drawing the grid on the screen.
	 * 
	 * @param painter
	 *            {@link Graphics2D} object
	 * @param dim
	 *            {@link Dimension} object
	 * @param xOfYAxis
	 *            relative position of x axis
	 * @param yOfXAxis
	 *            relative position of y axis
	 * @param xStepLength
	 *            effective step length (horizontal direction)
	 * @param yStepLength
	 *            effective step length (vertical direction)
	 * @param numOfXNum
	 *            number of bars
	 */
	private void drawGrid(Graphics2D painter, Dimension dim, int xOfYAxis, int yOfXAxis, int xStepLength,
			int yStepLength, int numOfXNum) {
		FontMetrics fm = painter.getFontMetrics();
		int textHeight = fm.getHeight();
		final int LINE_END = 2;

		painter.setFont(Font.decode("Arial-Bold-14"));
		painter.setPaint(Color.BLACK);

		// drawing numbers alongside y-axis parallel with horizontal lines
		for (int y = yOfXAxis, numOfSteps = 0; numOfSteps < yNumbers.length; y -= yStepLength) {

			int x = xOfYAxis - OFFSET_SMALL - fm.stringWidth(yNumbers[numOfSteps]);
			painter.drawString(yNumbers[numOfSteps], x, y + textHeight / 4);

			if (numOfSteps != 0) {
				painter.setColor(LINE_COLOR); //when drawing other lines
			} else {
				painter.setColor(Color.GRAY); // when drawing the axis
			}

			painter.drawLine(
					xOfYAxis - LINE_END, 
					y, 
					dim.width - OFFSET_FROM_EDGE + OFFSET_SMALL,
					y);
			
			painter.setColor(Color.BLACK);

			numOfSteps++;
		}

		// drawing numbers alongside y-axis parallel with vertical lines
		int xStart = xOfYAxis + xStepLength / 2;

		for (int x = xStart, numOfSteps = 0; numOfSteps < numOfXNum; x += xStepLength) {
			String s = Integer.toString(model.getValues().get(numOfSteps).getX());
			painter.drawString(s, x - fm.stringWidth(s) / 2,
					dim.height - (OFFSET_FROM_EDGE + textHeight + OFFSET_SMALL));

			if (numOfSteps != 0) {
				painter.setColor(LINE_COLOR);
			} else {
				painter.setColor(Color.GRAY);
			}

			painter.drawLine(
					x - xStepLength / 2, 
					yOfXAxis + LINE_END, 
					x - xStepLength / 2, 
					OFFSET_FROM_EDGE + yStepLength - OFFSET_SMALL);
			
			painter.setColor(Color.BLACK);

			numOfSteps++;
		}

		// drawing the arrow at the end of the x-axis
		int[] triangleX = new int[] {
				dim.width - OFFSET_FROM_EDGE + OFFSET_SMALL,
				dim.width - OFFSET_FROM_EDGE + OFFSET_SMALL,
				dim.width - OFFSET_FROM_EDGE + 4 * OFFSET_SMALL
		};

		int[] triangleY = new int[] {
				yOfXAxis - 2 * OFFSET_SMALL,
				yOfXAxis + 2 * OFFSET_SMALL,
				yOfXAxis 
		};

		painter.setColor(Color.GRAY);

		Polygon triangle = new Polygon(triangleX, triangleY, 3);
		painter.fillPolygon(triangle);

		// drawing the arrow at the end of the y-axis
		triangleX = new int[] {
				xOfYAxis - 2 * OFFSET_SMALL,
				xOfYAxis + 2 * OFFSET_SMALL,
				xOfYAxis
		};

		triangleY = new int[] {
				OFFSET_FROM_EDGE + yStepLength - OFFSET_SMALL,
				OFFSET_FROM_EDGE + yStepLength - OFFSET_SMALL,
				OFFSET_FROM_EDGE + yStepLength -  4 * OFFSET_SMALL
		};

		triangle = new Polygon(triangleX, triangleY, 3);
		painter.fillPolygon(triangle);
	}

	/**
	 * This method is used internally to draw bars on the grid.
	 * 
	 * @param painter
	 *            {@link Graphics2D} object
	 * @param xOfYAxis
	 *            relative position of x axis
	 * @param yOfXAxis
	 *            relative position of y axis
	 * @param xStepLength
	 *            effective step length (horizontal direction)
	 * @param yStepLength
	 *            effective step length (vertical direction)
	 */
	private void drawBars(Graphics2D painter, int xOfYAxis, int yOfXAxis, int xStepLength, int yStepLength) {
		painter.setColor(BAR_COLOR);
		int offset = 0;
		for (XYValue xy : model.getValues()) {

			int yValue = xy.getY();
			if (yValue < model.getyMin()) {
				yValue = model.getyMin();
			} else if (yValue > model.getyMax()) {
				yValue = model.getyMax();
			}

			int x = xOfYAxis + offset * xStepLength;
			int y = yOfXAxis - (yValue - model.getyMin()) * yStepLength / model.getStep();

			int[] rectangleX = { x, x, x + xStepLength, x + xStepLength };
			int[] rectangleY = { y, yOfXAxis, yOfXAxis, y };

			Polygon rectangle = new Polygon(rectangleX, rectangleY, 4);
			painter.fillPolygon(rectangle);

			painter.setColor(Color.WHITE);
			for (int i = 0; i < rectangleX.length; i++) { // drawing the outlines
				painter.drawLine(
						rectangleX[i], 
						rectangleY[i], 
						rectangleX[i], 
						rectangleY[(i + 1) % rectangleY.length]
				);
			}

			painter.setColor(BAR_COLOR);
			offset++;
		}
	}
}
