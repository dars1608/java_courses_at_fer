/**
 * This package contains classes used for creating simple
 * {@linkplain hr.fer.zemris.java.gui.calc.Calculator Calculator}, like the one
 * used on Windows XP.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.gui.calc;