package hr.fer.zemris.java.gui.calc.unaryoperators;

import static java.lang.Math.log;
import static java.lang.Math.pow;
import static java.lang.Math.E;

import hr.fer.zemris.java.gui.calc.CalcUnaryOperator;

/**
 * This class is an implementation of the {@link CalcUnaryOperator} used for
 * calculating the natural logarithm of given argument.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Ln implements CalcUnaryOperator {

	@Override
	public double applyAsDouble(double arg, boolean isInverse) {
		if(isInverse) {
			return pow(E, arg);
		} else {
			return log(arg);
		}
	}

	@Override
	public String getSymbol() {
		return "ln";
	}

}
