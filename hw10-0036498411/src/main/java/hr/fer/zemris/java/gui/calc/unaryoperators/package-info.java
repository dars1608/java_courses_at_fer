/**
 * This package contains implementations of
 * {@link hr.fer.zemris.java.gui.calc.CalcUnaryOperator} objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.gui.calc.unaryoperators;