package hr.fer.zemris.java.gui.calc;

import java.util.function.DoubleBinaryOperator;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * This interface defines the binary operator used in the program {@link Calculator}
 * program. It extends interface {@link DoubleBinaryOperator} which provides the
 * method {@link DoubleBinaryOperator}{@link #applyAsDouble(double, double)} to
 * apply defined operation. The implementation must provide string symbol used
 * for mapping the operator objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface CalcBinaryOperator extends DoubleBinaryOperator {

	/**
	 * Getter method for the operator symbol.
	 * 
	 * @return the symbol
	 */
	String getSymbol();
}
