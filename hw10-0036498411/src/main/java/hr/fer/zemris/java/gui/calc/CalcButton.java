package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JButton;

/**
 * This class represents a button used in program {@link Calculator}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CalcButton extends JButton {

	/** Serial version */
	private static final long serialVersionUID = 8461450247119040418L;
	/** Null insets */
	private static final Insets NULL_INSETS = new Insets(0, 0, 0, 0);
	/** Default button dimension */
	private static final Dimension BUTTON_DIMENSION = new Dimension(60, 60);

	/**
	 * Constructor method for class {@link CalcButton}.
	 * 
	 * @param name
	 */
	public CalcButton(String name) {
		setText(name);
		setMargin(NULL_INSETS);
		setPreferredSize(BUTTON_DIMENSION);
		setFont(Calculator.DEFAULT_FONT);
		setForeground(Color.BLACK);
		setBackground(Calculator.DEFAULT_BUTTON_COLOR);
	}

}
