package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

/**
 * This class represents the main frame of the {@link PrimDemo} program.
 * 
 * @see PrimDemo
 * @author Darko Britvec
 * @version 1.0
 */
public class PrimFrame extends JFrame {

	/** Serial version */
	private static final long serialVersionUID = -94686193916158648L;
	
	/**
	 * Constructor method for class {@link PrimFrame}.
	 */
	public PrimFrame() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("PrimDemo");
		setLocation(20, 20);
		setSize(100, 200);
		
		initGUI();
		setVisible(true);
	}

	/**
	 * This method is used internally for initializing GUI components.
	 */
	private void initGUI() {
		PrimListModel model = new PrimListModel();
		
		JList<Integer> list1 = new JList<>(model);
		JList<Integer> list2 = new JList<>(model);
		
		JScrollPane sPanel1 = new JScrollPane(list1);
		JScrollPane sPanel2 = new JScrollPane(list2);
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 2));
		panel.add(sPanel1);
		panel.add(sPanel2);
		
		JButton next = new JButton("Next");
		next.addActionListener(l -> model.next());
		
		getContentPane().add(panel, BorderLayout.CENTER);
		getContentPane().add(next, BorderLayout.SOUTH);
	}

}
