package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * This program describes simple calculator (like the one used on Windows XP
 * operation system). It provides basic functionalities like every other
 * calculator (unary and binary operators). It also provides simplest operations
 * with stack (push and pop) to store intermediate results. Frame layout manager
 * of the program is modeled by class {@link CalcLayout}, while model which
 * represents the core of the calculator is modeled by {@link CalcModel}.
 * Calculators default angle units are radians. Program shows dialog window when
 * invalid result is obtained.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Calculator extends JFrame {

	/** Serial version */
	private static final long serialVersionUID = 5016122500695877672L;
	/** Spaces between buttons and labels on the frame */
	private static final int SPACE_BETWEEN_COMPONENTS = 5;
	/** Default font (Arial-Bold-20) */
	static final Font DEFAULT_FONT = Font.decode("Arial-Bold-20");
	/** Default button color */
	static final Color DEFAULT_BUTTON_COLOR = Color.decode("#729FCF");

	/** Map of binary operators */
	private Map<String, CalcBinaryOperator> binaryOperators = new HashMap<>();
	/** Map of unary operators */
	private Map<String, CalcUnaryOperator> unaryOperators = new HashMap<>();
	/** Stack for storing intermediate result */
	private Stack<Double> stack = new Stack<>();
	/** Calculator model */
	private CalcModel model = new CalcModelImpl();
	/** Flag determining if the operation x^n / nth root is selected */
	// private boolean isXN = false;

	/** Main panel */
	private JPanel panel = new JPanel(new CalcLayout(SPACE_BETWEEN_COMPONENTS));
	/** Label where the result is displayed */
	private JLabel result = new CalcScreen();
	/** Check-box determining if the inversion operations are enabled or not */
	private JCheckBox inversion = new CalcInvButon();

	/**
	 * Constructor method for class {@link Calculator}.
	 */
	public Calculator() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Calculator");
		setLocation(20, 20);

		if (!initOperators() || !initGUI()) {
			JOptionPane.showMessageDialog(
					this,
					"Program failed to initialize",
					"Initialization error",
					JOptionPane.ERROR_MESSAGE);

			System.exit(1);
		}

		getContentPane().add(panel);
		setVisible(true);
		pack();
	}

	/**
	 * This method is used internally for initializing map of operators which are
	 * used in {@link Calculator} program. There are two types of operands; first
	 * one is described by {@link CalcUnaryOperator} interface, while second is
	 * described by {@link CalcBinaryOperator} interface. It uses
	 * {@link ClassLoader} to load classes, instantiate them and store them into
	 * map, under its symbols.
	 */
	private boolean initOperators() {
		try {
			File root = new File("src/main/java");
			URL[] urls = new URL[] { root.toURI().toURL() };
			URLClassLoader cl = new URLClassLoader(urls);

			File dir = new File(
					Thread.currentThread()
					.getContextClassLoader()
					.getResource("hr.fer.zemris.java.gui.calc.unaryoperators".replace('.', '/'))
					.getFile());
			File[] files = dir.listFiles();

			for (File file : files) {
				System.out.println(file);
				if (file.getName().equals("package-info.class"))
					continue;

				Class<?> cls = cl.loadClass("hr.fer.zemris.java.gui.calc.unaryoperators." 
									+ file.getName().split("\\.")[0]);

				CalcUnaryOperator op = (CalcUnaryOperator) cls.getConstructor().newInstance();
				unaryOperators.put(op.getSymbol(), op);
			}

			dir = new File(
					Thread.currentThread()
					.getContextClassLoader()
					.getResource("hr.fer.zemris.java.gui.calc.binaryoperators".replace('.', '/'))
					.getFile());
			files = dir.listFiles();

			for (File file : files) {
				System.out.println(file);
				if (file.getName().equals("package-info.class"))
					continue;

				Class<?> cls = cl.loadClass("hr.fer.zemris.java.gui.calc.binaryoperators."
									+ file.getName().split("\\.")[0]);

				CalcBinaryOperator op = (CalcBinaryOperator) cls.getConstructor().newInstance();
				binaryOperators.put(op.getSymbol(), op);
			}

			cl.close();

		} catch (Exception ignorable) {
		}

		return true;
	}

	/**
	 * This method is used internally for initializing GUI components.
	 * 
	 * @return <code>true</code> if initialization completed correctly,
	 *         <code>false</code> otherwise
	 */
	private boolean initGUI() {
		List<String> buttonLabels = null;

		try {
			buttonLabels = Files.readAllLines(Paths.get("src/main/resources/buttonLabels.txt"));
		} catch (IOException e) {
			return false;
		}

		panel.add(result, new RCPosition(1, 1));
		model.addCalcValueListener(m -> result.setText(m.toString()));
		panel.add(inversion, new RCPosition(5, 7));

		ActionListener digitListener = l -> {
			JButton b = (JButton) l.getSource();

			model.insertDigit(Integer.parseInt(b.getText()));

		};

		ActionListener unaryOperatorListener = l -> {
			JButton b = (JButton) l.getSource();

			double arg = model.getValue();
			CalcUnaryOperator op = unaryOperators.get(b.getText());
			double res = op.applyAsDouble(arg, inversion.isSelected());

			if (isUnacceptableResult(res)) {
				JOptionPane.showMessageDialog(
						this, 
						"Unacceptable result.", 
						"Error", 
						JOptionPane.ERROR_MESSAGE);
			} else {
				model.setValue(res);
			}
			

		};

		ActionListener binaryOperatorListener = l -> {
			JButton b = (JButton) l.getSource();

			model.setPendingBinaryOperation(binaryOperators.get(b.getText()));
		
		};

		for (int row = 1,  offset = 0; row <= RCPosition.MAX_ROW; row++) {
			for (int column = 1; column <= RCPosition.MAX_COLUMN; column++) {

				if (row == 1 && column <= 5)
					continue; // place for the calculator "screen"
				if (row == 5 && column == 7)
					continue; // place for "Inv" check-box

				String name = buttonLabels.get(offset);
				JButton button = new CalcButton(name);

				if (isDigit(name)) {
					button.addActionListener(digitListener);

				} else if (name.equals("x^n")) {
					button.addActionListener(l -> {
						if (inversion.isSelected()) {
							model.setPendingBinaryOperation(binaryOperators.get("nroot"));
						} else {
							model.setPendingBinaryOperation(binaryOperators.get("x^n"));
						}
					});

				} else if (isUnaryOperator(name)) {
					button.addActionListener(unaryOperatorListener);

				} else if (isBinaryOperator(name)) {
					button.addActionListener(binaryOperatorListener);

				} else if (name.equals("=")) {
					button.addActionListener(l -> {
						DoubleBinaryOperator op = model.getPendingBinaryOperation();

						if (op != null && model.isActiveOperandSet()) {
							double res = op.applyAsDouble(model.getActiveOperand(), model.getValue());
							if (isUnacceptableResult(res)) {
								JOptionPane.showMessageDialog(
										this, 
										"Unacceptable result.", 
										"Error",
										JOptionPane.ERROR_MESSAGE);
							} else {
								model.setValue(res);
								model.clearActiveOperand();
							}
						}
					});

				} else if (name.equals(".")) {
					button.addActionListener(l -> model.insertDecimalPoint());

				} else if (name.equals("+/-")) {
					button.addActionListener(l -> model.swapSign());

				} else if (name.equals("clr")) {
					button.addActionListener(l -> model.clear());

				} else if (name.equals("res")) {
					button.addActionListener(l -> model.clearAll());

				} else if (name.equals("push")) {
					button.addActionListener(l -> stack.push(model.getValue()));

				} else if (name.equals("pop")) {
					button.addActionListener(l -> {
						if (!stack.isEmpty()) {
							model.setValue(stack.pop());
						} else {
							JOptionPane.showMessageDialog(this, "Stack is empty", "Warning",
									JOptionPane.WARNING_MESSAGE);
						}
					});
				}

				panel.add(button, new RCPosition(row, column));
				offset++;
			}
		}

		return true;
	}

	/**
	 * This method is used internally to determine if the result of the operation is
	 * unacceptable (infinity or NaN).
	 * 
	 * @param res
	 *            result of some operation
	 * @return <code>true</code> if it's unacceptable, <code>false</code> otherwise
	 */
	private static boolean isUnacceptableResult(double res) {
		return Double.isNaN(res) || Double.isInfinite(res);
	}

	/**
	 * This method is used internally to determine if the given string argument
	 * represents a digit.
	 * 
	 * @param arg
	 *            string to be checked
	 * @return <code>true</code> if it's digit, <code>false</code> otherwise
	 */
	private boolean isDigit(String arg) {
		return "0123456789".contains(arg);
	}

	/**
	 * This method is used internally to check if the given string argument
	 * represents binary operator.
	 * 
	 * @param arg
	 *            string to be checked
	 * @return <code>true</code> if it's a binary operator, <code>false</code>
	 *         otherwise
	 */
	private boolean isBinaryOperator(String arg) {
		return binaryOperators.containsKey(arg);
	}

	/**
	 * This method is used internally to check if the given string argument
	 * represents unary operator.
	 * 
	 * @param arg
	 *            string to be checked
	 * @return <code>true</code> if it's a unary operator, <code>false</code>
	 *         otherwise
	 */
	private boolean isUnaryOperator(String name) {
		return unaryOperators.containsKey(name);
	}

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new Calculator();
		});

	}
}
