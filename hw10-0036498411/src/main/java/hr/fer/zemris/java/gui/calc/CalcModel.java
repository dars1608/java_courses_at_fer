package hr.fer.zemris.java.gui.calc;

import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;

/**
 * This interface describes a model used as the core of {@link Calculator}
 * program. Every implementation of this interface must provide field of type
 * {@link String} which will represent current input, then field of type
 * {@link BinaryOperator} where pending binary operator is stored. It must also
 * have a list where the possible listeners will be stored. Listeners are
 * modeled as {@link CalcValueListener} objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface CalcModel {

	/**
	 * This method is used for adding {@link CalcValueListener} to list of
	 * listeners.
	 * 
	 * @param l
	 *            listener object to be added
	 */
	void addCalcValueListener(CalcValueListener l);

	/**
	 * This method is used for removing {@link CalcValueListener} from the list of
	 * observers.
	 * 
	 * @param l
	 *            listener object to be removed
	 */
	void removeCalcValueListener(CalcValueListener l);

	@Override
	String toString();

	/**
	 * This method parses and returns input value. If input is empty, returns 0.
	 * 
	 * @return current value
	 */
	double getValue();

	/**
	 * This method sets the input value, and converts it into string.
	 * 
	 * @param value
	 *            value to be set
	 */
	void setValue(double value);

	/**
	 * This method clears currently stored input.
	 */
	void clear();

	/**
	 * This method clears currently stored input, active operand and pending
	 * operation.
	 */
	void clearAll();

	/**
	 * This method swaps sign of the stored input number.
	 */
	void swapSign();

	/**
	 * This method inserts decimal point (appends it on the end of current input).
	 */
	void insertDecimalPoint();

	/**
	 * This method inserts digit (appends it on the end of current input).
	 * 
	 * @param digit
	 *            digit to be set
	 */
	void insertDigit(int digit);

	/**
	 * This method checks if the active operand has been set.
	 * 
	 * @return <code>true</code> if it is set, <code>false</code> otherwise
	 */
	boolean isActiveOperandSet();

	/**
	 * Getter method for the active operand.
	 * 
	 * @return active operand
	 */
	double getActiveOperand();

	/**
	 * Setter method for the active operand.
	 * 
	 * @param activeOperand
	 *            active operand to be set
	 */
	void setActiveOperand(double activeOperand);

	/**
	 * This method clears the active operand.
	 */
	void clearActiveOperand();

	/**
	 * Getter method for the pending binary operation.
	 * 
	 * @return pending {@link DoubleBinaryOperator} object describing pending
	 *         operation
	 */
	DoubleBinaryOperator getPendingBinaryOperation();

	/**
	 * Setter method for the pending binary operation.
	 * 
	 * @param op
	 *            {@link DoubleBinaryOperator} object describing pending operation
	 *            to be set
	 */
	void setPendingBinaryOperation(DoubleBinaryOperator op);
}