package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * This class represents the "screen" of the calculator used in program
 * {@link Calculator}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CalcScreen extends JLabel {

	/** Default screen color */
	private static final Color DEFAULT_SCREEN_COLOR = Color.decode("#FFD320");
	/** Default font (Arial-Bold-20) */
	private static final Font DEFAULT_FONT = Font.decode("Arial-Bold-20");
	/** Serial version */
	private static final long serialVersionUID = 2960396167707979048L;

	/**
	 * Constructor method for class {@link CalcScreen}.
	 */
	public CalcScreen() {
		setText("");
		setFont(DEFAULT_FONT.deriveFont(30f));
		setForeground(Color.BLACK);
		setBackground(DEFAULT_SCREEN_COLOR);
		setOpaque(true);
		setHorizontalAlignment(SwingConstants.RIGHT);
	}
}
