/**
 * This package conains classes for drawing bar chart on the screen. To see what
 * bar chart is click on the
 * <a href="https://en.wikipedia.org/wiki/Bar_chart">link</a>.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.gui.charts;