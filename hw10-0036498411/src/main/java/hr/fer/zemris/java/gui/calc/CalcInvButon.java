package hr.fer.zemris.java.gui.calc;

import java.awt.Color;

import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

/**
 * This class models check box used in {@link Calculator} program as inverter of
 * unary operators: when is selected, it turns the mode of unary operators to
 * inverse operation.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CalcInvButon extends JCheckBox {

	/** Serial version */
	private static final long serialVersionUID = 8046889749473664366L;

	/**
	 * Constructor method for class {@link CalcInvButon}.
	 */
	public CalcInvButon() {
		super("Inv");
		
		setFont(Calculator.DEFAULT_FONT);
		setBackground(Calculator.DEFAULT_BUTTON_COLOR);
		setForeground(Color.BLACK);
		setHorizontalAlignment(SwingConstants.CENTER);
	}

}
