package hr.fer.zemris.java.gui.calc.binaryoperators;

import hr.fer.zemris.java.gui.calc.CalcBinaryOperator;

/**
 * This class represents of {@link CalcBinaryOperator} used for multiplying two
 * doubles.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Multiplication implements CalcBinaryOperator {

	@Override
	public double applyAsDouble(double arg1, double arg2) {
		return arg1 * arg2;
	}

	@Override
	public String getSymbol() {
		return "*";
	}

}
