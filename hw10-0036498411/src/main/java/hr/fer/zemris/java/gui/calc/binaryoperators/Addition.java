package hr.fer.zemris.java.gui.calc.binaryoperators;

import hr.fer.zemris.java.gui.calc.CalcBinaryOperator;

/**
 * This class represents of {@link CalcBinaryOperator} used for adding two
 * doubles.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Addition implements CalcBinaryOperator {

	@Override
	public double applyAsDouble(double left, double right) {
		return left + right;
	}

	@Override
	public String getSymbol() {
		return "+";
	}
}
