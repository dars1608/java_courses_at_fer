package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * This class represents simple {@link LayoutManager} used for modeling the GUI
 * of the {@link Calculator} program. It contains 31 slots, where the first slot
 * (at the location 1,1) is 5 slots combined together. The other slots are the
 * same size. Size of the slot is determined by the maximum size of the
 * components.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CalcLayout implements LayoutManager2 {

	/**
	 * Map containing {@link Component} objects stored under the key represented by
	 * {@link RCPosition} objects
	 */
	private Map<RCPosition, Component> components = new HashMap<>();
	/** Preferred dimension of the slot */
	private Dimension dimension = new Dimension(0, 0);
	/** Space between every row and column */
	private final int space;

	/**
	 * Constructor method for class {@link CalcLayout}.
	 * 
	 * @param space
	 *            between every row and column (in pixels)
	 */
	public CalcLayout(int space) {
		if (space < 0) {
			throw new IllegalArgumentException("Argument can't be negative number.");
		}

		this.space = space;
	}

	/**
	 * Default constructor method for class {@link CalcLayout}. Sets space between
	 * every row and column to 0 pixels.
	 */
	public CalcLayout() {
		this(0);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <b>NOTE</b>: method isn't supported in {@link CalcLayout}, instead throws
	 * {@link UnsupportedOperationException}
	 */
	@Override
	public void addLayoutComponent(String name, Component comp) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		Objects.requireNonNull(comp);

		RCPosition keyToBeRemoved = null;
		for (Map.Entry<RCPosition, Component> c : components.entrySet()) {
			if (c.getValue().equals(comp)) {
				keyToBeRemoved = c.getKey();
				break;
			}
		}

		if (keyToBeRemoved != null) {
			components.remove(keyToBeRemoved);
		}
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		Objects.requireNonNull(parent);

		Dimension d = calculateDimension(parent, c -> c.getPreferredSize(), true);
		
		return d;
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		Objects.requireNonNull(parent);

		Dimension d = calculateDimension(parent, c -> c.getMinimumSize(), true);

		return d;
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		Objects.requireNonNull(target);

		Dimension d = calculateDimension(target, c -> c.getMaximumSize(), false);

		return d;
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		Objects.requireNonNull(comp);
		Objects.requireNonNull(constraints);

		RCPosition constr = null;

		if (constraints instanceof RCPosition) {
			constr = (RCPosition) constraints;
		} else if (constraints instanceof String) {
			constr = RCPosition.parse((String) constraints);
		} else {
			throw new CalcLayoutException("Illegal constraints argument");
		}

		if (components.containsKey(constr) && comp != components.get(constr)) {
			throw new CalcLayoutException("Slot is already taken by the other component.");
		}

		if (constr.equals(RCPosition.FIRST_SLOT)) {
			Dimension size = comp.getPreferredSize();
			comp.setPreferredSize(new Dimension((size.width - 4 * space) / 5, size.height));
		}
		components.put(constr, comp);
	}

	@Override
	public void layoutContainer(Container parent) {
		Dimension parentDim = parent.getSize();

		int newWidth = (parentDim.width - (RCPosition.MAX_COLUMN - 1) * space) / RCPosition.MAX_COLUMN;
		int newHeight = (parentDim.height - (RCPosition.MAX_ROW - 1) * space) / RCPosition.MAX_ROW;

		dimension.setSize(newWidth, newHeight);

		updateDimension();
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {
		dimension = new Dimension(0, 0);
	}

	/**
	 * This method is used internally for calculating the dimension of container
	 * specified by the {@code func} argument (minimal, maximal or preferred)
	 * 
	 * @param cont
	 *            {@link Container} object
	 * @param func
	 *            {@link Function} used for getting the needed size
	 * @return {@link Dimension} object representing the dimension of the layout
	 */
	private Dimension calculateDimension(Container cont, Function<Component, Dimension> func, boolean biggestValue) {
		int width = 0;
		int heigth = 0;

		for (Component c : cont.getComponents()) {
			Dimension d = func.apply(c);
			if (d == null)
				continue;

			if (biggestValue) {
				if ((d.width > width)) {
					width = d.width;
				}

				if (d.height > heigth) {
					heigth = d.height;
				}
			} else {
				if ((d.width < width)) {
					width = d.width;
				}

				if (d.height < heigth) {
					heigth = d.height;
				}
			}
		}

		return new Dimension(width * RCPosition.MAX_COLUMN + space * (RCPosition.MAX_COLUMN - 1),
				heigth * RCPosition.MAX_ROW + space * (RCPosition.MAX_ROW - 1));
	}

	/**
	 * This method is used internally for updating the layout whenever the component
	 * is added.
	 */
	private void updateDimension() {

		for (Map.Entry<RCPosition, Component> e : components.entrySet()) {
			Point p = getLocation(e.getKey());

			if (e.getKey().equals(RCPosition.FIRST_SLOT)) {
				e.getValue().setSize(getFirstSlotSize());

			} else {
				e.getValue().setSize(dimension);
			}

			e.getValue().setLocation(p);
		}

	}

	/**
	 * This method is used internally for calculating location of the component
	 * based on its constraints.
	 * 
	 * @param rcp
	 *            constraints represented by {@link RCPosition} object
	 * @return {@link Point} object representing location of the component
	 */
	private Point getLocation(RCPosition rcp) {
		int row = rcp.getRow();
		int column = rcp.getColumn();

		int x = (column - 1) * (dimension.width + space);
		int y = (row - 1) * (dimension.height + space);

		return new Point(x, y);
	}

	/**
	 * This method is used for calculating the size of the first (special) slot.
	 * 
	 * @return {@link Dimension} object representing the size of the first slot
	 */
	private Dimension getFirstSlotSize() {
		return new Dimension(5 * dimension.width + 4 * space, dimension.height);
	}
}
