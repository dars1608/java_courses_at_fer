package hr.fer.zemris.java.gui.layouts;

/**
 * This class represents an {@link RuntimeException} which is thrown whenever
 * the exception is generated in classes used for modeling {@link CalcLayout}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CalcLayoutException extends RuntimeException {

	/** Serial version */
	private static final long serialVersionUID = 7932812220542367420L;

	/**
	 * Default constructor method for class {@link CalcLayoutException}.
	 */
	public CalcLayoutException() {
	}

	/**
	 * Constructor method for class {@link CalcLayoutException}.
	 * 
	 * @param message
	 *            detail message
	 */
	public CalcLayoutException(String message) {
		super(message);
	}

	/**
	 * Constructor method for class {@link CalcLayoutException}.
	 * 
	 * @param cause
	 *            {@link Throwable} object which caused the exception
	 */
	public CalcLayoutException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructor method for class {@link CalcLayoutException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            {@link Throwable} object which caused the exception
	 */
	public CalcLayoutException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor method for class {@link CalcLayoutException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            {@link Throwable} object which caused the exception
	 * @param enableSuppression
	 *            whether or not the suppression is enabled
	 * @param writableStackTrace
	 *            whether or not the stack trace is writable
	 */
	public CalcLayoutException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
