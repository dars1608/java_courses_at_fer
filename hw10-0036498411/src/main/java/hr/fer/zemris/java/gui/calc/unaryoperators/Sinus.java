package hr.fer.zemris.java.gui.calc.unaryoperators;

import static java.lang.Math.asin;
import static java.lang.Math.sin;

import hr.fer.zemris.java.gui.calc.CalcUnaryOperator;

/**
 * This class is an implementation of the {@link CalcUnaryOperator} used for
 * calculating the sinus value of given argument.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Sinus implements CalcUnaryOperator {

	@Override
	public double applyAsDouble(double arg, boolean isInverse) {
		if(isInverse) {
			return asin(arg);
		} else {
			return sin(arg);
		}
	}

	@Override
	public String getSymbol() {
		return "sin";
	}

}
