package hr.fer.zemris.java.gui.calc.unaryoperators;

import static java.lang.Math.acos;
import static java.lang.Math.cos;

import hr.fer.zemris.java.gui.calc.CalcUnaryOperator;

/**
 * This class is an implementation of the {@link CalcUnaryOperator} used for
 * calculating the cosinus value of given argument.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Cosinus implements CalcUnaryOperator {

	@Override
	public double applyAsDouble(double arg, boolean isInverse) {
		if (isInverse) {
			return acos(arg);
		} else {
			return cos(arg);
		}
	}

	@Override
	public String getSymbol() {
		return "cos";
	}

}
