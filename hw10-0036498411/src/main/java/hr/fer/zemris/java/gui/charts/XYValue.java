package hr.fer.zemris.java.gui.charts;

/**
 * This class models simple unmodifiable tuple with 2 members (x,y).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class XYValue {
	
	/** X component*/
	private final int x;
	/** Y component*/
	private final int y;
	
	/**
	 * Constructor method for class {@link XYValue}.
	 * @param x x component
	 * @param y y component
	 */
	public XYValue(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Getter method for the {@code x}.
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Getter method for the {@code y}.
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		XYValue other = (XYValue) obj;
		if (x != other.x) {
			return false;
		}
		if (y != other.y) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "XYValue [x=" + x + ", y=" + y + "]";
	}
	
	
}
