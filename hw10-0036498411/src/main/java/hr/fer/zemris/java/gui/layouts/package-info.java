/**
 * This package contains classes used for modeling custom layout manager for gui
 * of program {@linkplain hr.fer.zemris.java.gui.calc.Calculator Calculator}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.gui.layouts;