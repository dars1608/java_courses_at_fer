package hr.fer.zemris.java.gui.layouts;

import java.util.Objects;

/**
 * This class models constraints used in {@link CalcLayout} manager. CalcLayout
 * manager uses 5x7 matrix where first five slots in the first row are merget and
 * form the big slot at position (1,1).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class RCPosition {

	/** Max row count */
	public static final int MAX_ROW = 5;
	/** Max column count */
	public static final int MAX_COLUMN = 7;
	/** Special first slot*/
	public static final RCPosition FIRST_SLOT = new RCPosition(1, 1);

	/** Row number */
	private final int row;
	/** Column number */
	private final int column;

	/**
	 * Constructor method for class {@link RCPosition}.
	 * 
	 * @param row
	 *            row number
	 * @param column
	 *            column number
	 * @throws CalcLayoutException
	 *             if arguments are illegal
	 */
	public RCPosition(int row, int column) {
		checkArguments(row, column);

		this.row = row;
		this.column = column;
	}

	/**
	 * Getter method for the {@code row}.
	 * 
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Getter method for the {@code column}.
	 * 
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * This method is used for parsing string into {@link RCPosition} object. String
	 * must be of format "rowNum,columnNum".
	 * 
	 * @param constraints
	 *            string to be parsed
	 * @return constraints parsed from given argument
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 * @throws CalcLayoutException
	 *             if given argument is of invalid format
	 */
	public static RCPosition parse(String constraints) {
		Objects.requireNonNull(constraints);
		String[] parts = constraints.split(",");

		if (parts.length != 2) {
			throw new CalcLayoutException("Illegal constraints format.");
		}

		int row = 0;
		int column = 0;

		try {
			row = Integer.parseInt(parts[0]);
			column = Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			throw new CalcLayoutException("Illegal constraints format.");
		}

		return new RCPosition(row, column);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RCPosition other = (RCPosition) obj;
		if (column != other.column) {
			return false;
		}
		if (row != other.row) {
			return false;
		}
		
		return true;
	}

	/**
	 * This method is used internally for checking validity of the arguments
	 * 
	 * @param row
	 *            row number
	 * @param column
	 *            column number
	 * @throws CalcLayoutException
	 *             if arguments are illegal
	 */
	private void checkArguments(int row, int column) {
		if (row <= 0 || column <= 0) {
			throw new CalcLayoutException("Arguments must be greater than 0");
		}

		if (row == 1 && column > 1 && column < 6) {
			throw new CalcLayoutException("Positions (1,a), where a is in range from 2 do 5, are unused.");
		}

		if (row > MAX_ROW) {
			throw new CalcLayoutException("Argument row is a number in range from 1 to 5");
		}

		if (column > MAX_COLUMN) {
			throw new CalcLayoutException("Argument collumn is a number in range from 1 to 7");
		}
	}
}
