package hr.fer.zemris.java.gui.charts;

import java.util.List;
import java.util.Objects;

/**
 * This class represents the model of simple
 * <a href="https://en.wikipedia.org/wiki/Bar_chart">bar chart</a>, which can
 * show the statistic from provided data. Constructor accepts the list of
 * {@link XYValue} objects, based on which the bars are drawn (x component
 * determines the ordinal number of the bar, while y component represents the
 * bar height).
 * 
 * <p>
 * Every bar chart must have minimal and maximal value, step between numbers on
 * y-axis, descriptions of axis and defined values of every bar (modeled as
 * {@link XYValue} objects).
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class BarChart {

	/**
	 * List of {@link XYValue} objects representing info about every bar on the
	 * chart
	 */
	private final List<XYValue> values;
	/** Description to be written beside x axis */
	private final String xDescription;
	/** Description to be written beside y axis */
	private final String yDescription;
	/** Minimal vertical boundary */
	private final int yMin;
	/** Maximal vertical boundary */
	private final int yMax;
	/** Distance between two numbers on y axis */
	private final int step;

	/**
	 * Constructor method for class {@link BarChart}.
	 * 
	 * @param values
	 *            list of {@link XYValue} objects
	 * @param xDescription
	 *            description to be written beside x axis
	 * @param yDescription
	 *            description to be written beside y axis
	 * @param yMin
	 *            minimal vertical boundary
	 * @param yMax
	 *            maximal vertical boundary
	 * @param step
	 *            distance between two numbers on y axis
	 */
	public BarChart(List<XYValue> values, String xDescription, String yDescription, int yMin, int yMax, int step) {
		this.values = Objects.requireNonNull(values);
		this.xDescription = Objects.requireNonNull(xDescription);
		this.yDescription = Objects.requireNonNull(yDescription);

		if (yMin > yMax) {
			throw new IllegalArgumentException("Minimal vertical boundary can't be greater than maximal.");
		}

		if (step <= 0) {
			throw new IllegalArgumentException("Step argument must be positive number");
		}

		this.yMin = yMin;
		this.yMax = yMax;
		this.step = (yMax - yMin) % step == 0 ? step : step + 1;
	}

	/**
	 * Getter method for the {@code values}.
	 * 
	 * @return the values
	 */
	public List<XYValue> getValues() {
		return values;
	}

	/**
	 * Getter method for the {@code xDescription}.
	 * 
	 * @return the xDescription
	 */
	public String getXDescription() {
		return xDescription;
	}

	/**
	 * Getter method for the {@code yDescription}.
	 * 
	 * @return the yDescription
	 */
	public String getYDescription() {
		return yDescription;
	}

	/**
	 * Getter method for the {@code yMin}.
	 * 
	 * @return the yMin
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * Getter method for the {@code yMax}.
	 * 
	 * @return the yMax
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * Getter method for the {@code step}.
	 * 
	 * @return the step
	 */
	public int getStep() {
		return step;
	}

}
