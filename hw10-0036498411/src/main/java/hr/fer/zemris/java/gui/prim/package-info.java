/**
 * This package contains simple prime numbers generator program
 * {@link hr.fer.zemris.java.gui.prim.PrimDemo}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.gui.prim;