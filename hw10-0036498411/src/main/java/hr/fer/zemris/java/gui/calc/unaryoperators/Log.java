package hr.fer.zemris.java.gui.calc.unaryoperators;

import static java.lang.Math.log10;
import static java.lang.Math.pow;

import hr.fer.zemris.java.gui.calc.CalcUnaryOperator;

/**
 * This class is an implementation of the {@link CalcUnaryOperator} used for
 * calculating the logarithm of given argument.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Log implements CalcUnaryOperator {

	@Override
	public double applyAsDouble(double arg, boolean isInverse) {
		if (isInverse) {
			return pow(10, arg);
		} else {
			return log10(arg);
		}
	}

	@Override
	public String getSymbol() {
		return "log";
	}

}
