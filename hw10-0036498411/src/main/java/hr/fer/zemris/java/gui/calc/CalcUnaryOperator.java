package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.Calculator;

/**
 * This interface defines the unary operator used in the program
 * {@link Calculator}. It provides the method
 * {@link CalcUnaryOperator}{@link #applyAsDouble(double, boolean)} used for
 * applying defined operand on the provided argument. It also accepts the
 * boolean flag used for determining if the operation is inverse (f(x)^-1).
 * Every implementation must also provide the getter method for the symbol of
 * defined unary operator.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface CalcUnaryOperator {

	/**
	 * This methd is used for applying defined operand on the provided argument. It
	 * also accepts the boolean flag used for determining if the method is inverted
	 * (f(x)^-1)
	 * 
	 * @param arg
	 *            operation argument
	 * @param isInverse
	 *            flag determining if the operator is inverse
	 * @return the result
	 */
	double applyAsDouble(double arg, boolean isInverse);

	/**
	 * Getter method for the symbol of the operator.
	 * 
	 * @return the symbol
	 */
	String getSymbol();
}
