package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * This class models simple {@link ListModel} which generates and stores prime
 * numbers. Prime number is generated on the every call of the method
 * {@link PrimListModel#next()}. The model notifies all current listeners about
 * any change of content (in this case, only appending of next prime number in
 * the internal list).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PrimListModel implements ListModel<Integer> {

	/** List where prime numbers are stored when generated */
	private List<Integer> data = new ArrayList<>();
	/** List of current listeners */
	private List<ListDataListener> listeners = new ArrayList<>();
	/** Last generated prime */
	private int lastPrime = 1;

	/**
	 * Constructor method for class {@link PrimListModel}.
	 */
	public PrimListModel() {
		data.add(1);
	}

	@Override
	public int getSize() {
		return data.size();
	}

	@Override
	public Integer getElementAt(int index) {
		return data.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(Objects.requireNonNull(l));
		l.contentsChanged(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, 0, data.size() - 1));

	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(Objects.requireNonNull(l));
	}

	/**
	 * This method generates the next prime number and stores it in the internal
	 * list. Then all of the listeners are notified about changing the content.
	 */
	public void next() {
		while (true) {
			lastPrime++;
			if (isPrime(lastPrime)) {
				break;
			}
		}

		data.add(lastPrime);
		notifyAllListeners();
	}

	/**
	 * This method is used internally for notifying all listeners about changing the
	 * content.
	 */
	private void notifyAllListeners() {
		ListDataEvent event = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, data.size() - 2, data.size() - 1);
		for (ListDataListener l : listeners) {
			l.intervalAdded(event);
		}

	}

	/**
	 * This method is used internally to check if an integer number is prime or not.
	 * 
	 * @return <code>true</code> if number is prime, <code>false</code> otherwise
	 */
	private static boolean isPrime(int number) {
		if (number == 2) {
			return true;
		}

		if (number == 3) {
			return true;
		}

		if (number % 2 == 0) {
			return false;
		}

		if (number % 3 == 0) {
			return false;
		}

		int sqrt = (int) Math.sqrt(number) + 1;

		for (int i = 2; i < sqrt; i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;
	}
}
