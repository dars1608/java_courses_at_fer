package hr.fer.zemris.java.gui.calc.binaryoperators;

import static java.lang.Math.pow;

import hr.fer.zemris.java.gui.calc.CalcBinaryOperator;

/**
 * This class represents of {@link CalcBinaryOperator} used for calculating n-th
 * root of x.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class NRoot implements CalcBinaryOperator {

	@Override
	public double applyAsDouble(double left, double right) {
		return pow(left, 1/right);
	}

	@Override
	public String getSymbol() {
		return "nroot";
	}

}
