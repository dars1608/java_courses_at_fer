package hr.fer.zemris.java.gui.calc.unaryoperators;

import static java.lang.Math.atan;
import static java.lang.Math.tan;

import hr.fer.zemris.java.gui.calc.CalcUnaryOperator;

/**
 * This class is an implementation of the {@link CalcUnaryOperator} used for
 * calculating the cotangent value of given argument.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Cotangent implements CalcUnaryOperator {

	@Override
	public double applyAsDouble(double arg, boolean isInverse) {
		if (isInverse) {
			return atan(1 / arg);
		} else {
			return 1 / tan(arg);
		}
	}

	@Override
	public String getSymbol() {
		return "ctg";
	}

}
