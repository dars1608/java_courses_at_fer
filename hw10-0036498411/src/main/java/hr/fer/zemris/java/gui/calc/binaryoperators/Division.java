package hr.fer.zemris.java.gui.calc.binaryoperators;

import hr.fer.zemris.java.gui.calc.CalcBinaryOperator;

/**
 * This class represents of {@link CalcBinaryOperator} used for dividing two
 * doubles.s
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Division implements CalcBinaryOperator {

	@Override
	public double applyAsDouble(double arg1, double arg2) {
		return arg1 / arg2;
	}

	@Override
	public String getSymbol() {
		return "/";
	}
}
