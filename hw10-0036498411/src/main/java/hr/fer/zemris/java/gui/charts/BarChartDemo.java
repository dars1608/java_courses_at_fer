package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * This is program demonstrates functionalities of {@link BarChart} and
 * {@link BarChartComponent} used for drawing bar charts. Program accepts one
 * command line argument; the path of text file which must be formated as
 * following:
 * 
 * <ul>
 * <li>first row: description of x-axis
 * <li>second row: description of y-axis
 * <li>third row: width and height of every bar (<i>width,height</i>) separated
 * by spaces)
 * <li>forth row: minimal y value
 * <li>fifth row: maximal y value
 * <li>sixth row: distance between two numbers on the y-axis
 * </ul>
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see BarChart
 * @see BarChartComponent
 */
public class BarChartDemo { // test example: src/test/resources/barChartTestExample.txt

	/**
	 * This method is called when program starts.
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.err.println("Invalid number of arguments. (1 is expected).");
			return;
		}

		List<String> lines;
		Path path = Paths.get(args[0]);

		try {
			lines = Files.readAllLines(path);
		} catch (IOException ex) {
			System.err.println("Provided path is invalid.");
			return;
		}

		if (lines.size() < 5) {
			System.err.println("Invalid file format.");
			return;
		}

		BarChart model;
		try {
			model = parseBarChart(lines);
		} catch (IllegalArgumentException ex) {
			System.err.println(ex.getMessage());
			return;
		}

		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame();
			BarChartComponent comp = new BarChartComponent(model);
			JLabel label = new JLabel(path.toAbsolutePath().toString(), SwingConstants.CENTER);

			frame.add(comp, BorderLayout.CENTER);
			frame.add(label, BorderLayout.NORTH);

			frame.setLocation(20, 20);
			frame.pack();

			frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

			frame.setVisible(true);
		});
	}

	/**
	 * This method is used internally for parsing {@link BarChart} model from list
	 * of strings.
	 * 
	 * @param lines
	 *            list of strings containing informations about the model
	 * @return the model
	 * @throws IllegalArgumentException
	 *             if file format is illegal
	 */
	private static BarChart parseBarChart(List<String> lines) {
		String xDescription = lines.get(0);
		String yDescription = lines.get(1);

		String[] tuples = lines.get(2).split(" ");
		List<XYValue> xyv = new ArrayList<>();

		for (String t : tuples) {
			String[] parts = t.split(",");
			if (parts.length != 2) {
				throw new IllegalArgumentException("Invalid file format.");
			}

			try {
				xyv.add(new XYValue(Integer.parseInt(parts[0]), Integer.parseInt(parts[1])));
			} catch (NumberFormatException ex) {
				throw new IllegalArgumentException("Invalid file format.");
			}
		}

		int min = 0;
		int max = 0;
		int step = 0;

		try {
			min = Integer.parseInt(lines.get(3));
			max = Integer.parseInt(lines.get(4));
			step = Integer.parseInt(lines.get(5));
		} catch (NumberFormatException ex) {
			throw new IllegalArgumentException("Invalid file format.");
		}

		return new BarChart(xyv, xDescription, yDescription, min, max, step);
	}

}
