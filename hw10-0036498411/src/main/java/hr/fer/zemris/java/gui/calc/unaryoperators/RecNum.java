package hr.fer.zemris.java.gui.calc.unaryoperators;

import hr.fer.zemris.java.gui.calc.CalcUnaryOperator;

/**
 * This class is an implementation of the {@link CalcUnaryOperator} used for
 * calculating the reciprocal value of given argument.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class RecNum implements CalcUnaryOperator{

	@Override
	public double applyAsDouble(double arg, boolean isInverse) {
		return 1/arg;
	}

	@Override
	public String getSymbol() {
		return "1/x";
	}

}
