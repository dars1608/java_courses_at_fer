package hr.fer.zemris.java.gui.layouts;

import static org.junit.Assert.*;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.junit.Test;

public class CalcLayoutTest {

	@Test
	public void testImplementation() {
		JPanel p = new JPanel(new CalcLayout(2));
		JLabel l1 = new JLabel(""); l1.setPreferredSize(new Dimension(108,15));
		JLabel l2 = new JLabel(""); l2.setPreferredSize(new Dimension(16,30));
		p.add(l1, new RCPosition(1,1));
		p.add(l2, new RCPosition(3,3));
		Dimension dim = p.getPreferredSize();
		
		assertEquals(152, dim.width);
		assertEquals(158, dim.height);
	}
	
	@Test(expected = CalcLayoutException.class)
	public void testException1() {
		new CalcLayout(2).addLayoutComponent(new JLabel(), new RCPosition(0, 6));
	}
	
	@Test(expected = CalcLayoutException.class)
	public void testException2() {
		new CalcLayout(2).addLayoutComponent(new JLabel(), new RCPosition(6, 6));
	}
	
	@Test(expected = CalcLayoutException.class)
	public void testException3() {
		new CalcLayout(2).addLayoutComponent(new JLabel(), new RCPosition(3, 0));
	}
	
	@Test(expected = CalcLayoutException.class)
	public void testException4() {
		new CalcLayout(2).addLayoutComponent(new JLabel(), new RCPosition(3, 8));
	}
	
	@Test(expected = CalcLayoutException.class)
	public void testException5() {
		new CalcLayout(2).addLayoutComponent(new JLabel(), new RCPosition(1, 4));
	}
	
	@Test(expected = CalcLayoutException.class)
	public void testException6() {
		CalcLayout test = new CalcLayout(2);
		test.addLayoutComponent(new JLabel(), new RCPosition(2,2));
		test.addLayoutComponent(new JLabel(), new RCPosition(2,2));
		
	}
	
	@Test
	public void testAddTwoSameComponentsOnTheSamePosition() {
		CalcLayout test = new CalcLayout(2);
		JLabel label = new JLabel();
		
		test.addLayoutComponent(label, new RCPosition(1, 1));
		test.addLayoutComponent(label, new RCPosition(1,1));
	}
	

}
