package hr.fer.zemris.java.gui.prim;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.junit.Test;

public class PrimListModelTest {

	@Test
	public void testNext() throws IOException {
		MockListener mockListener = new MockListener();
		PrimListModel model = new PrimListModel();
		
		model.addListDataListener(mockListener);
		List<String> primes = Files.readAllLines(Paths.get("src/test/resources/PrimeNumbers.txt"));
		
		for(String p: primes) {
			int prime = Integer.parseInt(p);
			model.next();
			assertEquals(prime, mockListener.getA());
		}
	}

	
	private static class MockListener implements ListDataListener{
		private int a;
		
		public int getA() {
			return a;
		}

		@Override
		public void intervalAdded(ListDataEvent e) {
			a = ((PrimListModel)e.getSource()).getElementAt(e.getIndex1());
			
		}

		@Override
		public void intervalRemoved(ListDataEvent e) {
			
		}

		@Override
		public void contentsChanged(ListDataEvent e) {
			
		}
		
	}

}
