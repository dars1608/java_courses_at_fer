package hr.fer.zemris.java.hw01;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.hw01.Factorial.factorial;

import org.junit.Test;

/**
 * This class tests functionality of program Factorial which calculates
 * factorial of given argument.
 * 
 * @author Darko Britveca
 *
 */
public class FactorialTest {

	@Test
	public void testFactorialSmallValue() {
		assertTrue(1L == factorial(1));
	}

	@Test
	public void testFactorialNormalValue() {
		assertTrue(3628800L == factorial(10));
	}

	@Test
	public void testFactorialBigValue() {
		assertTrue(2432902008176640000L == factorial(20));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialOutOfRange1() {
		factorial(-10);
		fail();
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFactorialOutOfRange2() {
		factorial(30);
		fail();
	}
}
