/**
 * 
 */
package hr.fer.zemris.java.hw01;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.hw01.Rectangle.area;
import static hr.fer.zemris.java.hw01.Rectangle.perimiter;

import org.junit.Test;

/**
 * This class tests functionality of program Rectangle which calculates area and
 * diameter of a rectangle with given size.
 * 
 * @author Darko Britvec
 */
public class RectangleTest {

	@Test
	public void testAreaNormal() {
		assertTrue(Math.abs(area(2.2, 4.2) - 9.24) < 10e-10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAreaInvalidArgument() {
		area(-2, 5);
		fail();
	}

	@Test
	public void testPerimiterNormal() {
		assertTrue(Math.abs(perimiter(2.2, 4.2) - 12.8) < 10e-10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPerimiterInvalidArgument() {
		perimiter(-2, 5);
		fail();
	}

}
