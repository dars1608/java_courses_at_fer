/**
 * 
 */
package hr.fer.zemris.java.hw01;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import hr.fer.zemris.java.hw01.UniqueNumbers.TreeNode;
import static hr.fer.zemris.java.hw01.UniqueNumbers.*;

/**
 * This class tests functionality of program UniqueNumbers which adds numbers in
 * tree structure.
 * 
 * @author Darko Britvec
 */
public class UniqueNumbersTest {
	TreeNode root = null;

	@Before
	public void setUp() {
		root = null;
		root = addNode(root, 42);
		root = addNode(root, 76);
		root = addNode(root, 21);
		root = addNode(root, 76);
		root = addNode(root, 35);
	}

	@Test
	public void testTreeSize() {
		assertTrue(treeSize(root) == 4);
	}

	@Test
	public void testContainsalue() {
		assertTrue(containsValue(root, 21));
		assertFalse(containsValue(root, 0));
	}

	@Test
	public void testAddNode() {
		root = addNode(root, 21);
		root = addNode(root, 123);
		root = addNode(root, 234);
		assertTrue(containsValue(root, 123));
		assertTrue(containsValue(root, 21));
		assertTrue(containsValue(root, 234));
		assertTrue(treeSize(root) == 6);
	}
}
