package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * This program is written as solution for the first homework for Java
 * Course@FER. Program simulates creating collection of unique numbers (type
 * int) using tree structure. User enters values from standard input until the
 * keyword "kraj" is written. Then program prints values on standard output in
 * ascending and descending order.
 * 
 * @author Darko Britvec
 */
public class UniqueNumbers {

	/**
	 * @author Darko Britvec This class is used internally for creating tree
	 *         structure
	 */
	public static class TreeNode {
		int value;
		TreeNode left, right;

		public TreeNode(int value) {
			this.value = value;
			this.left = null;
			this.right = null;
		}
	}

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		TreeNode root = null;
		String inputText = "";

		System.out.println("Za kraj programa unesite \"kraj\".");
		try (Scanner scanner = new Scanner(System.in)) {
			do {
				System.out.print("Unesite broj > ");
				inputText = scanner.nextLine().trim();

				if (!inputText.equals("kraj")) {
					try {
						int inputNumber = Integer.parseInt(inputText);
						if (!containsValue(root, inputNumber)) {
							root = addNode(root, inputNumber);
							System.out.println("Dodano.");
							
						} else {
							System.out.println("Broj već postoji. Preskačem.");
						}

					} catch (NumberFormatException ex) {
						System.out.println("'" + inputText + "' nije cijeli broj.");
					}
				}
			} while (!inputText.equals("kraj"));
		}

		System.out.print("Ispis od najmanjeg:");
		printInorder(root);
		System.out.println();
		System.out.print("Ispis od najvećeg:");
		printReversed(root);
	}

	/**
	 * This method is used for adding node in tree structure.
	 * 
	 * @param root
	 *            TreeNode element representing the root of tree structure.
	 * @param value
	 *            Value to be added to tree structure.
	 */
	public static TreeNode addNode(TreeNode root, int value) {
		if (root == null) {
			root = new TreeNode(value);
		} else if (root.value > value) {
			root.left = addNode(root.left, value);
		} else if (root.value < value) {
			root.right = addNode(root.right, value);
		}
		return root;
	}

	/**
	 * This method is used for counting entries in tree structure.
	 * 
	 * @param root
	 *            TreeNode element representing the root of tree structure.
	 * @return Number of entries.
	 */
	public static int treeSize(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return 1 + treeSize(root.left) + treeSize(root.right);
	}

	/**
	 * This method is used to check if the value is stored in tree structure.
	 * 
	 * @param root
	 *            TreeNode element representing the root of tree structure.
	 * @param value
	 *            Value which is searched.
	 * @return true if the value is stored in tree structure, false otherwise.
	 */
	public static boolean containsValue(TreeNode root, int value) {
		if (root == null) {
			return false;
		} else if (root.value == value) {
			return true;
		} else if (root.value > value) {
			return containsValue(root.left, value);
		} else {
			return containsValue(root.right, value);
		}
	}

	/**
	 * This method prints values in ascending order.
	 * 
	 * @param root
	 *            TreeNode element representing the root of tree structure.
	 */
	public static void printInorder(TreeNode root) {
		if (root == null) {
			return;
		}

		printInorder(root.left);
		System.out.print(" " + root.value);
		printInorder(root.right);
	}

	/**
	 * This method prints values in descending order.
	 * 
	 * @param root
	 *            TreeNode element representing the root of tree structure.
	 */
	public static void printReversed(TreeNode root) {
		if (root == null) {
			return;
		}

		printReversed(root.right);
		System.out.print(" " + root.value);
		printReversed(root.left);
	}
}
