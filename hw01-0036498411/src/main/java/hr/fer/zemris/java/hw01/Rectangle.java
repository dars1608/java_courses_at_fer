package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * This program is written as solution for the first homework for Java
 * Course@FER. Program calculates diameter and area of a rectangle which
 * dimensions are obtained from command line.
 * 
 * @author Darko Britvec
 */
public class Rectangle {

	/**
	 * This method is called when program starts. Command line arguments determine
	 * dimensions of a rectangle which diameter and area user wants to calculate
	 * First argument is width, second argument is height. If there's no argument,
	 * user must enter dimensions trough standard input.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		double width = 0.0;
		double height = 0.0;
		if (args.length > 0) {
			if (args.length != 2) {
				System.out.println("Krivi broj argumenata!");
				return;
			} else {
				try {
					width = Double.parseDouble(args[0].replaceAll(",", ".")); // making sure that decimal number
																				// is divided with a dot, not
																				// with comma
					height = Double.parseDouble(args[1].replaceAll(",", "."));
				} catch (NumberFormatException ex) {
					System.out.println("Argumenti nisu valjani.");
				}
			}
		} else {
			try (Scanner scanner = new Scanner(System.in)) {
				width = inputDouble(scanner, "širinu");
				height = inputDouble(scanner, "visinu");
			}
		}

		try {
			System.out.println("Pravokutnik širine " + width + " i visine " + height + " ima površinu "
					+ area(width, height) + " te opseg " + perimiter(width, height) + ".");
		} catch (IllegalArgumentException ex) {
			System.out.println("Argumenti širina = " + width + ", visina = " + height + " nisu valjani.");
		}
	}

	/**
	 * This method is used to calculate perimiter of a rectangle.
	 * 
	 * @param width
	 *            Rectangle width.
	 * @param height
	 *            Rectangle height
	 * @return Rectangle diameter
	 * @throws IllegalArgumentException
	 *             If arguments are negative or zero.
	 */
	public static double perimiter(double width, double height) throws IllegalArgumentException {
		if (width <= 0 || height <= 0) {
			throw new IllegalArgumentException("( " + width + ", " + height + " ) are wrong dimensions of a rectangle");
		}
		return 2 * width + 2 * height;
	}

	/**
	 * This method is used to calculate area of a rectangle.
	 * 
	 * @param width
	 *            Rectangle width.
	 * @param height
	 *            Rectangle height
	 * @return Rectangle area
	 * @throws IllegalArgumentException
	 *             If arguments are negative or zero.
	 */
	public static double area(double width, double height) throws IllegalArgumentException {
		if (width <= 0 || height <= 0) {
			throw new IllegalArgumentException("( " + width + ", " + height + " ) are wrong dimensions of a rectangle");
		}
		return width * height;
	}

	/**
	 * This method is used internally to simplify entering positive doubles from
	 * standard input.
	 * 
	 * @param scanner
	 *            Scanner object used for reading from standard input.
	 * @param argument
	 *            Parameter which user must enter.
	 * @return Entered value.
	 */
	private static double inputDouble(Scanner scanner, String argument) {
		double returnValue = 0.0;
		String textInput = "";
		do {
			try {
				System.out.print("Unesite " + argument + " > ");
				textInput = scanner.nextLine().trim();
				textInput.replaceFirst(",", ".");
				returnValue = Double.parseDouble(textInput);

				if (returnValue == 0) {
					System.out.println("Unijeli ste nulu");
				} else if (returnValue < 0) {
					System.out.println("Unijeli ste negativnu vrijednost");
				}

			} catch (NumberFormatException ex) {
				System.out.println("'" + textInput + "' se ne može protumačiti kao broj");
			}
		} while (returnValue <= 0);

		return returnValue;
	}
}
