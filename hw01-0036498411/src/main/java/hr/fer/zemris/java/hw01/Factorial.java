package hr.fer.zemris.java.hw01;

import java.util.Scanner;

/**
 * This program is written as solution for the first homework for Java
 * Course@FER. Program calculates factorial of numbers (range 1-20) from
 * standard input. User must type "kraj" to end program execution.
 * 
 * @author Darko Britvec
 */
public class Factorial {
	// This constants define range of arguments for factorial function.
	private static final int MIN_RANGE = 1;
	private static final int MAX_RANGE = 20;

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {
			String inputText = "";
			int inputNumber = 0;

			System.out.println("Unesite cijeli broj u rasponu od " + MIN_RANGE + " do " + MAX_RANGE
					+ ". Za kraj unesite \"kraj\".");
			do {
				System.out.print("Unesite broj > ");
				inputText = scanner.nextLine().trim(); // in case there's unnecessary blank spaces

				if (inputText.equals("kraj")) {
					System.out.println("Doviđenja.");
				} else {
					try {
						inputNumber = Integer.parseInt(inputText);

						if (inputNumber < MIN_RANGE || inputNumber > MAX_RANGE) {
							System.out.println("'" + inputText + "' nije u dozvoljenom rasponu.");
						} else {
							System.out.println(inputText + "! = " + factorial(inputNumber));
						}
					} catch (NumberFormatException ex) {
						System.out.println("'" + inputText + "' nije cijeli broj.");
					}
				}

			} while (!inputText.equals("kraj"));
		}
	}

	/**
	 * This method is used internally to calculate factorial from given argument.
	 * 
	 * @param argument
	 *            Argument of factorial function (range [1,20]).
	 * @return Result of factorial function.
	 * @throws IllegalArgumentException if argument is out of range.
	 */
	public static long factorial(int argument) throws IllegalArgumentException {
		long result = 1L;
		
		if (argument < MIN_RANGE || argument > MAX_RANGE) {
			throw new IllegalArgumentException("Argument out of range [1,20]");
		} else {
			while (argument > 0) {
				result *= argument;
				argument--;
			}
		}
		return result;
	}
}
