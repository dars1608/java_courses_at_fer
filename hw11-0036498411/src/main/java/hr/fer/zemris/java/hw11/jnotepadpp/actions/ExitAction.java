package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.nio.file.Path;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for closing the
 * {@link JNotepadPP} application.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class ExitAction extends ActionWithMultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link ExitAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @param parent
	 * 			  parent frame
	 */
	public ExitAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);
		
		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift X"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/exit_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/exit_icon_small.png", this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (JOptionPane.showConfirmDialog(parent,
				LocalizationProvider.getInstance().getString("closingProgram"), 
				"",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION) {
			return;
		}

		boolean isFirstToSave = true;
		for (int i = 0, len = tabbedPanel.getNumberOfDocuments(); i < len; i++) {
			SingleDocumentModel document = tabbedPanel.getDocument(i);

			if (document.isModified()) {
				if (isFirstToSave) {
					isFirstToSave = false;
					if (JOptionPane.showConfirmDialog(parent,
							LocalizationProvider.getInstance().getString("unsaved"),
							LocalizationProvider.getInstance().getString("warning"),
							JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE) == JOptionPane.NO_OPTION) {
						parent.dispose();
						return;
					}
				}
				Path path = document.getFilePath();

				do {
					if (path == null) {
						JFileChooser jfc = new JFileChooser();
						jfc.setDialogTitle(String.format(
								LocalizationProvider.getInstance().getString("savingDocumentAt"), i + 1));
	
						if (jfc.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
							JOptionPane.showMessageDialog(
									parent,
									LocalizationProvider.getInstance().getString("nothingWasSaved"), 
									LocalizationProvider.getInstance().getString("warning"),
									JOptionPane.INFORMATION_MESSAGE);
							break;
							
						} else { 
							path = jfc.getSelectedFile().toPath();
						}
					}
					
					tabbedPanel.saveDocument(document, path);
					path = null;
					
				} while (document.isModified());
			}
		}

		parent.dispose();

	}

}
