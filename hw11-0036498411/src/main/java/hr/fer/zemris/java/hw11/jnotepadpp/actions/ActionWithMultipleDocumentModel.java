package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;

/**
 * This class is abstract implementation of {@link AbstractAction} which works
 * with {@link MultipleDocumentModel}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class ActionWithMultipleDocumentModel extends AbstractAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Multiple tab panel */
	protected MultipleDocumentModel tabbedPanel;
	/** Parent frame */
	protected JFrame parent;

	/**
	 * Constructor method for class {@link ActionWithMultipleDocumentModel}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab panel
	 * @param parent
	 *            parent frame
	 * @throws NullPointerException
	 *             if arguments are {@code null}
	 */
	public ActionWithMultipleDocumentModel(MultipleDocumentModel tabbedPanel, JFrame parent) {
		this.tabbedPanel = Objects.requireNonNull(tabbedPanel);
		this.parent = Objects.requireNonNull(parent);
	}

}
