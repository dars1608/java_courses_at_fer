package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents {@link AbstractAction} used for showing statistics
 * about the text in active editor.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class StatisticsAction extends ActionWithMultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link StatisticsAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab panel
	 * @param parent
	 * 			  parent frame
	 */
	public StatisticsAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control I"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/statistic_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/statistic_icon_small.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		char[] data = tabbedPanel.getCurrentDocument().getTextComponent().getText().toCharArray();

		int numOfChars = 0;
		int numOfNonBlank = 0;
		int numOfLines = 0;

		for (char c : data) {
			numOfChars++;

			if (!Character.isWhitespace(c)) {
				numOfNonBlank++;
			}

			if (c == '\n') {
				numOfLines++;
			}
		}

		JOptionPane.showMessageDialog(parent,
				String.format(LocalizationProvider.getInstance().getString("statisticsOutput"), 
						numOfChars, numOfNonBlank, numOfLines),
				LocalizationProvider.getInstance().getString("Statistics.name"),
				JOptionPane.INFORMATION_MESSAGE);

	}

}
