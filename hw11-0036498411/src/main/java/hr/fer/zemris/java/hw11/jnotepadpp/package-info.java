/**
 * This package and all of its subpackages contain classes used to create simple
 * text editor {@link hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP JNotepad++}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw11.jnotepadpp;