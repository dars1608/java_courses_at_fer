package hr.fer.zemris.java.hw11.jnotepadpp.multipledocument;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * This interface describes an object which represents a model of single
 * document, having information about file path from which document was loaded
 * (can be {@code null} for new document), document modification status and reference to
 * Swing component which is used for editing (each document has its own editor
 * component).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface SingleDocumentModel {
	
	/**
	 * Getter method for the {@code textComponent} value.
	 * @return the {@code textComponent}
	 */
	JTextArea getTextComponent();

	/**
	 * Getter method for the {@code filePath} value.
	 * @return the {@code filePath}
	 */
	Path getFilePath();

	/**
	 * Setter method for the {@code filePath} value.
	 * 
	 * @param path path to be set
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null
	 */
	void setFilePath(Path path);

	/**
	 * This method checks if the document is modified
	 * 
	 * @return {@code true} if it's modified, {@code false} otherwise
	 */
	boolean isModified();

	/**
	 * This method sets the {@code modified} flag to the given value.
	 * 
	 * @param modified flag to be set
	 */
	void setModified(boolean modified);

	/**
	 * This method registers the lister to this model.
	 * 
	 * @param l listener to be added
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null}
	 */
	void addSingleDocumentListener(SingleDocumentListener l);

	/**
	 * This removes listener from the list of listeners of this model.
	 * 
	 * @param l
	 *            listener to be removed
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null}
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
}
