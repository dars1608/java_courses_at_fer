package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for opening the new empty
 * editor in the new tab of the multiple tab panel.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class NewDocumentAction extends AbstractAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Multiple tab model */
	private MultipleDocumentModel tabbedPanel;

	/**
	 * Constructor method for class {@link NewDocumentAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @throws NullPointerException
	 *            if argument is {@code null}
	 */
	public NewDocumentAction(MultipleDocumentModel tabbedPanel) {
		this.tabbedPanel = Objects.requireNonNull(tabbedPanel);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/new_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/new_icon_small.png", this));

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		tabbedPanel.createNewDocument();
	}

}
