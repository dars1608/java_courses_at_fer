package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * This interface describes a listener of {@link ILocalizationProvider}
 * object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@FunctionalInterface
public interface ILocalizationListener {

	/**
	 * This method is called whenever the {@link ILocalizationProvider} changes the
	 * current language to inform all registered listeners about the change.
	 */
	void localizationChanged();
}
