package hr.fer.zemris.java.hw11.jnotepadpp.swing;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProviderBridge;

/**
 * This class represents an implementation of {@link LocalizationProviderBridge}
 * which can disconnect itself when the frame (provided through constructor) is
 * closed.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Constructor method for class {@link FormLocalizationProvider}.
	 * 
	 * @param parent
	 *            provider
	 * @param frame
	 *            frame which uses the provider
	 */
	public FormLocalizationProvider(ILocalizationProvider parent, JFrame frame) {
		super(parent);

		frame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosed(WindowEvent e) {
				disconnect();
			}
			
		});
	}

}
