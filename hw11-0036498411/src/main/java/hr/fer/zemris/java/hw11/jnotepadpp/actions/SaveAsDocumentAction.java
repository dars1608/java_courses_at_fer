package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.nio.file.Path;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for saving the document
 * under the new name, type and path.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class SaveAsDocumentAction extends ActionWithMultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link SaveAsDocumentAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @param parent
	 * 			  parent frame
	 */
	public SaveAsDocumentAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control alt S"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/save_as_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/save_as_icon_small.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Path openedFilePath = tabbedPanel.getCurrentDocument().getFilePath();
		
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Save document");
		if (jfc.showSaveDialog(parent) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					parent, 
					LocalizationProvider.getInstance().getString("nothingWasSaved"),
					LocalizationProvider.getInstance().getString("warning"),
					JOptionPane.WARNING_MESSAGE);
			return;
		}

		openedFilePath = jfc.getSelectedFile().toPath();

		if (openedFilePath.toFile().exists()) {
			int res = JOptionPane.showConfirmDialog(
					parent,
					LocalizationProvider.getInstance().getString("fileAlreadyExists"),
					LocalizationProvider.getInstance().getString("warning"),
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (res == JOptionPane.NO_OPTION) {
				return;
			}
		}

		tabbedPanel.saveDocument(tabbedPanel.getCurrentDocument(), openedFilePath);
	}

}
