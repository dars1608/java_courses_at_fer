package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for pasting the data from
 * the system clipboard to the active text editor.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class PasteAction extends ActionWithMultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;


	/**
	 * Constructor method for class {@link PasteAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @param parent
	 * 		      parent frame
	 */
	public PasteAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/paste_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/paste_icon_small.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		tabbedPanel.getCurrentDocument().getTextComponent().paste();
	}
}
