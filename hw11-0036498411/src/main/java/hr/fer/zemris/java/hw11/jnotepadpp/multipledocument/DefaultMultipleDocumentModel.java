package hr.fer.zemris.java.hw11.jnotepadpp.multipledocument;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an implementation of {@link MultipleDocumentModel}. It
 * extends {@link JTabbedPane}, so it represents a multiple tab panel containing
 * editors in every panel.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** {@link Icon} object representing icon shown when document is modified */
	private final Icon MODIFIED_ICON = IconLoader.initIcon("../icons/modified_icon.png", this);
	/** {@link Icon} object representing icon shown when document isn't modified */
	private final Icon UNMODIFIED_ICON = IconLoader.initIcon("../icons/unmodified_icon.png", this);
	/** Listener of the documents */
	private final SingleDocumentListener listener = new SingleDocumentListener() {
		@Override
		public void documentModifyStatusUpdated(SingleDocumentModel model) {
			if (model.isModified()) {
				setIconAt(getSelectedIndex(), MODIFIED_ICON);
			} else {
				setIconAt(getSelectedIndex(), UNMODIFIED_ICON);
			}
		}

		@Override
		public void documentFilePathUpdated(SingleDocumentModel model) {
			String newName = model.getFilePath().getFileName().toString();
			setTitleAt(getSelectedIndex(), newName);
			setToolTipTextAt(getSelectedIndex(), model.getFilePath().toAbsolutePath().toString());
		}
	};

	/** List of stored documents */
	private List<SingleDocumentModel> documents = new ArrayList<>();
	/** Current document */
	private SingleDocumentModel currentDocument;
	/** List of registered listeners */
	private List<MultipleDocumentListener> listeners = new ArrayList<>();
	
	/**
	 * Constructor method for class {@link DefaultMultipleDocumentModel}.
	 */
	public DefaultMultipleDocumentModel() {
		addChangeListener(l -> {
			SingleDocumentModel oldDocument = currentDocument;
			currentDocument = getSelectedIndex() == -1 ? null : documents.get(getSelectedIndex());

			listeners.forEach(k -> k.currentDocumentChanged(oldDocument, currentDocument));
		});
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel document = new DefaultSingleDocumentModel();
		documents.add(document);

		currentDocument = document;

		if (currentDocument != null) {
			currentDocument.removeSingleDocumentListener(listener);
		}

		document.addSingleDocumentListener(listener);

		add("Untitled", 
				new JScrollPane(document.getTextComponent(),
					JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));

		SingleDocumentModel oldDocument = currentDocument;
		currentDocument = document;

		listeners.forEach(l -> {
			l.documentAdded(document);
			l.currentDocumentChanged(oldDocument, document);
		});

		setSelectedIndex(documents.size() - 1);
		setToolTipTextAt(getSelectedIndex(), "Untitled");
		setIconAt(getSelectedIndex(), UNMODIFIED_ICON);

		return document;
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return currentDocument;
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		Objects.requireNonNull(path);
		
		for (int i = 0, len = documents.size(); i < len; i++) {
			SingleDocumentModel model = documents.get(i);

			if (path.equals(model.getFilePath())) {
				setSelectedIndex(i);

				listeners.forEach(l -> l.currentDocumentChanged(currentDocument, model));
				currentDocument = model;

				return currentDocument;
			}
		}

		SingleDocumentModel document = getDocumentFromFile(path);
		if (document == null) {
			return null;
		}

		documents.add(document);
		document.addSingleDocumentListener(listener);
		
		this.add(path.getFileName().toString(), new JScrollPane(document.getTextComponent(),
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));

		SingleDocumentModel oldDocument = currentDocument;
		currentDocument = document;

		listeners.forEach(l -> {
			l.documentAdded(document);
			l.currentDocumentChanged(oldDocument, document);
		});

		setSelectedIndex(documents.size() - 1);
		setToolTipTextAt(getSelectedIndex(), path.toAbsolutePath().toString());
		setIconAt(getSelectedIndex(), UNMODIFIED_ICON);

		return document;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		String content = Objects.requireNonNull(model).getTextComponent().getText();

		Path path = model.getFilePath();
		if (path == null && newPath == null) {
			JOptionPane.showMessageDialog(
					getParent(), 
					"File can't be saved", 
					"ERROR", 
					JOptionPane.ERROR_MESSAGE
			);
			return;
		}

		if (newPath != null) {
			if(!newPath.toAbsolutePath().equals(path)) {
				for(SingleDocumentModel d: documents) {
					if(newPath.equals(d.getFilePath())) {
						JOptionPane.showMessageDialog(
								getParent(), 
								LocalizationProvider.getInstance().getString("alreadyOpened"), 
								LocalizationProvider.getInstance().getString("error"), 
								JOptionPane.ERROR_MESSAGE
						);
						
						return;
					}
				}
			}
			
			
			path = newPath;
			model.setFilePath(path);
		}

		byte[] data = content.getBytes(StandardCharsets.UTF_8);

		try {
			if(path.toFile().exists()) {
				Files.write(path, data, StandardOpenOption.TRUNCATE_EXISTING);
			} else {
				Files.write(path, data, StandardOpenOption.CREATE);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		model.setModified(false);
		listeners.forEach(l -> l.currentDocumentChanged(model, model));
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
		documents.remove(model);

		remove(getSelectedIndex());

		if (documents.size() == 0) {
			currentDocument = null;
		} else {
			setSelectedIndex(0);
			currentDocument = documents.get(0);
		}
		
		listeners.forEach(l -> l.documentRemoved(currentDocument));
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(Objects.requireNonNull(l));

	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);

	}

	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return documents.get(index);
	}

	/**
	 * This method is used internally for reading the content of the file from the
	 * disk.
	 * 
	 * @param path
	 *            path of the file to be read
	 * @return model of document (file) to be read
	 */
	private SingleDocumentModel getDocumentFromFile(Path path) {
		byte[] data;
		try {
			data = Files.readAllBytes(path);
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(getParent(),
					LocalizationProvider.getInstance().getString("errorWhileReading")
						+ path.toAbsolutePath().toString(),
					LocalizationProvider.getInstance().getString("error"),
					JOptionPane.ERROR_MESSAGE);
			return null;
		}

		return new DefaultSingleDocumentModel(path, new String(data, StandardCharsets.UTF_8));
	}

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documents.iterator();
	}

}
