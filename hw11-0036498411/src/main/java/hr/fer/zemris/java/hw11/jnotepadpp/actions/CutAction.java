package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for cutting the text from
 * the editor opened in {@link DefaultMultipleDocumentModel} tab to the system
 * clipboard.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class CutAction extends ActionWithMultipleDocumentModel{

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link CutDocumentAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @param parent
	 * 		      parent frame
	 */
	public CutAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/cut_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/cut_icon_small.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		tabbedPanel.getCurrentDocument().getTextComponent().cut();
	}

}
