package hr.fer.zemris.java.hw11.jnotepadpp;

/**
 * This class is used as collection of constants (key words) which are used in
 * {@link JNotepadPP} aplication.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
abstract class KeyWords {

	/** value = {@value#NEW} */
	public static final String NEW = "New";
	/** value = {@value#OPEN} */
	public static final String OPEN = "Open";
	/** value = {@value#CLOSE} */
	public static final String CLOSE = "Close";
	/** value = {@value#SAVE} */
	public static final String SAVE = "Save";
	/** value = {@value#SAVE_AS} */
	public static final String SAVE_AS = "Saveas";
	/** value = {@value#EXIT} */
	public static final String EXIT = "Exit";
	/** value = {@value#COPY} */
	public static final String COPY = "Copy";
	/** value = {@value#CUT} */
	public static final String CUT = "Cut";
	/** value = {@value#PASTE} */
	public static final String PASTE = "Paste";
	/** value = {@value#STATISTICS} */
	public static final String STATISTICS = "Statistics";
	/** value = {@value#UNIQUE} */
	public static final String UNIQUE = "Unique";
	/** value = {@value#FILE} */
	public static final String FILE = "file";
	/** value = {@value#EDIT} */
	public static final String EDIT = "edit";
	/** value = {@value#INFO} */
	public static final String INFO = "info";
	/** value = {@value#LANGUAGE} */
	public static final String LANGUAGE = "language";
	/** value = {@value#TOOLS} */
	public static final String TOOLS = "tools";
	/** value = {@value#EN} */
	public static final String EN = "en";
	/** value = {@value#DE} */
	public static final String DE = "de";
	/** value = {@value#HR} */
	public static final String HR = "hr";
	/** value = {@value#ES} */
	public static final String ES = "es";
	/** value = {@value#CHANGE_CASE} */
	public static final String CHANGE_CASE = "changeCase";
	/** value = {@value#UPPER} */
	public static final String UPPER = "upper";
	/** value = {@value#LOWER} */
	public static final String LOWER = "lower";
	/** value = {@value#INVERT} */
	public static final String INVERT = "invert";
	/** value = {@value#LENGTH} */
	public static final String LENGTH = "length";
	/** value = {@value#SORT} */
	public static final String SORT = "sort";
	/** value = {@value#ASC} */
	public static final String ASC = "ascending";
	/** value = {@value#DESC} */
	public static final String DESC = "descending";
	/** value = {@value#CARET} */
	public static final String CARET = "caret";

}
