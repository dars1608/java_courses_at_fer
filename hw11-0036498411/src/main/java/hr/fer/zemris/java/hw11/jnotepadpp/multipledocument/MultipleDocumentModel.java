package hr.fer.zemris.java.hw11.jnotepadpp.multipledocument;

import java.nio.file.Path;

/**
 * This interface an object which represents a model capable of holding zero,
 * one or more documents, where each document and having a concept of current
 * document – the one which is shown to the user and on which user works.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel>{

	/**
	 * This method is used for creating new document model.
	 * 
	 * @return {@link SingleDocumentModel} object
	 */
	SingleDocumentModel createNewDocument();

	/**
	 * Getter method for {@code currentDocument} value.
	 * 
	 * @return the current document model
	 */
	SingleDocumentModel getCurrentDocument();

	/**
	 * This method is used to load the text file into a document model.
	 * 
	 * @param path
	 *            path of the file
	 * @return {@link SingleDocumentModel} object
	 * @throws NullPointerException
	 *             if {@code path} argument is {@code null}
	 */
	SingleDocumentModel loadDocument(Path path);

	/**
	 * This method is used for saving the document into a file which path is
	 * described by the newPath argument. {@code newPath} can be {@code null}; if
	 * {@code null}, document is saved using path associated from document,
	 * otherwise, new path is used and after saving is completed, document’s path is
	 * updated to newPath.
	 * 
	 * @param model
	 *            document model to be saved
	 * @param newPath
	 *            path of the file
	 * @throws NullPointerException
	 *             if {@code model} argument is {@code null}
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);

	/**
	 * This method removes specified document from this model.
	 * 
	 * @param model
	 *            model to be removed
	 * @throws NullPointerException
	 *             if {@code model} argument is {@code null}
	 */
	void closeDocument(SingleDocumentModel model);

	/**
	 * This registers listener to this model.
	 * 
	 * @param l
	 *            listener to be added
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null}
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);

	/**
	 * This removes listener from the list of listeners of this model.
	 * 
	 * @param l
	 *            listener to be removed
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null}
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);

	/**
	 * This method is used for checking the number of documents currently stored in
	 * this model.
	 * 
	 * @return number of documents
	 */
	int getNumberOfDocuments();

	/**
	 * This method is used for getting the document at the given index.
	 * 
	 * @param index
	 *            index of a document
	 * @return document at given index
	 * @throws IllegalArgumentException
	 *             if index is out of bounds.
	 */
	SingleDocumentModel getDocument(int index);
}
