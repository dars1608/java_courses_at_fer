package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class represents an abstract implementation of
 * {@link ILocalizationProvider} used only for registering and unregistering of
 * listeners.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {

	/** List of registered listeners */
	private List<ILocalizationListener> listeners = new ArrayList<>();

	@Override
	public void addLocalizationListener(ILocalizationListener l) {
		listeners.add(Objects.requireNonNull(l));

	}

	@Override
	public void removeLocalizationListener(ILocalizationListener l) {
		listeners.remove(l);

	}

	@Override
	public abstract String getString(String s);

	@Override
	public abstract String getCurrentLanguage();

	/**
	 * This method is used for notifying all registered listeners about the change
	 * of the current language.
	 */
	protected void fire() {
		listeners.forEach(l -> l.localizationChanged());
	}

}
