package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for copying the text
 * from the editor opened in {@link DefaultMultipleDocumentModel} tab to the
 * system clipboard.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class CopyAction extends ActionWithMultipleDocumentModel{

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link CopyDocumentAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @param parent 
	 * 			  parent frame
	 */
	public CopyAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
		putValue(Action.LARGE_ICON_KEY,  IconLoader.initIcon("../icons/copy_icon.png", this));
		putValue(Action.SMALL_ICON,  IconLoader.initIcon("../icons/copy_icon_small.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		tabbedPanel.getCurrentDocument().getTextComponent().copy();
	}

}
