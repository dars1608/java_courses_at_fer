package hr.fer.zemris.java.hw11.jnotepadpp.util;

import java.io.IOException;
import java.io.InputStream;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * This is utility class used for loading icons from file.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class IconLoader {

	/**
	 * This method is used for loading icons from file. Path is resolved relatively
	 * from {@code reference} argument.
	 * 
	 * @param fileName file name
	 * @return loaded icon
	 */
	public static Icon initIcon(String fileName, Object reference) {
		InputStream is = reference.getClass().getResourceAsStream(fileName);
		if (is == null) {
			throw new IllegalArgumentException("File " + fileName + " doesn't exist");
		}

		byte[] bytes = null;
		try {
			bytes = is.readAllBytes();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new ImageIcon(bytes);
	}

}
