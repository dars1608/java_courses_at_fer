package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Objects;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;

import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.swing.SimpleClock;

/**
 * This class represents a status bar of {@link JNotepadPP} application. It
 * contains informations about length of current document, carret position and
 * timestamp.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class StatusBar extends JPanel implements ILocalizationListener, MultipleDocumentListener {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Label showing current document length */
	private JLabel length;
	/** Label showing position of caret */
	private JLabel caretInfo;
	/** Label showing current date-time */
	private SimpleClock clock;
	/** Localization provider */
	private ILocalizationProvider provider = LocalizationProvider.getInstance();
	/** Multiple tab panel */
	private MultipleDocumentModel tabbedPanel;

	/** Listener for determining the length of text currently in the editor */
	private SingleDocumentListener lengthListener = new SingleDocumentListener() {
		@Override
		public void documentModifyStatusUpdated(SingleDocumentModel model) {
			if (model == null)
				return;
			length.setText(String.format("%s:%d", provider.getString(KeyWords.LENGTH),
					model.getTextComponent().getText().length()));

		}

		@Override
		public void documentFilePathUpdated(SingleDocumentModel model) {
		}
	};
	/** Listener of the current editors caret */
	private CaretListener caretListener;

	/**
	 * Constructor method for class {@link StatusBar}.
	 * @param tabbedPanel
	 * @param frame
	 */
	public StatusBar(MultipleDocumentModel tabbedPanel, JFrame frame) {
		this.tabbedPanel = Objects.requireNonNull(tabbedPanel);
		setLayout(new GridLayout(1, 3));

		length = new JLabel();
		caretInfo = new JLabel();
		clock = new SimpleClock();
		
		add(length);
		add(caretInfo);
		add(clock);
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				clock.stopClock();
			}
		});

	}

	@Override
	public void localizationChanged() {
		currentDocumentChanged(null, tabbedPanel.getCurrentDocument());
	}

	@Override
	public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
		if (previousModel != null) {
			previousModel.removeSingleDocumentListener(lengthListener);
			if (caretListener != null) {
				previousModel.getTextComponent().removeCaretListener(caretListener);
			}
		}
		if (currentModel == null) {
			length.setVisible(false);
			caretInfo.setVisible(false);
		} else {
			length.setText(String.format("%s:%d", provider.getString(KeyWords.LENGTH),
					currentModel.getTextComponent().getText().length()));
			length.setVisible(true);

			caretInfo.setVisible(true);
			try {
				updateCaretInfo(currentModel.getTextComponent());
			} catch (BadLocationException e2) {
				e2.printStackTrace();
			}

			currentModel.addSingleDocumentListener(lengthListener);

			caretListener = new CaretListener() {
				@Override
				public void caretUpdate(CaretEvent e) {
					try {
						updateCaretInfo(currentModel.getTextComponent());
					} catch (BadLocationException e1) {
						e1.printStackTrace();
					}

				}
			};

			currentModel.getTextComponent().addCaretListener(caretListener);
		}

	}

	@Override
	public void documentAdded(SingleDocumentModel model) {
	}

	@Override
	public void documentRemoved(SingleDocumentModel model) {
	}

	/**
	 * This method is used internally for updating the caret info.
	 * 
	 * @param ta current text area
	 * @throws BadLocationException ignorable
	 */
	private void updateCaretInfo(JTextArea ta) throws BadLocationException {
		int ln = ta.getLineOfOffset(ta.getCaret().getDot());
		int col = ta.getCaret().getDot() - ta.getLineStartOffset(ln);
		int sel = Math.abs(ta.getCaret().getDot() - ta.getCaret().getMark());

		caretInfo.setText(String.format(provider.getString(KeyWords.CARET), ln, col, sel));
	}
}
