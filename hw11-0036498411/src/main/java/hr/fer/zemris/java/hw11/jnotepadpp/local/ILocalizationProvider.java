package hr.fer.zemris.java.hw11.jnotepadpp.local;

/**
 * This interface describes a Subject which offers the translation of given
 * text. Every time the current text is changed, the listeners must be notified.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface ILocalizationProvider {

	/**
	 * This method is used for registering a new listener to this Subject.
	 * 
	 * @param l
	 *            listener to be registered
	 */
	void addLocalizationListener(ILocalizationListener l);

	/**
	 * This method is used for removing a listener from the internal list of
	 * listeners.
	 * 
	 * @param l
	 *            listener to be removed
	 */
	void removeLocalizationListener(ILocalizationListener l);

	/**
	 * This method is used for translating the string based on current language.
	 * 
	 * @param s
	 *            string to be translated
	 * @return translated string
	 */
	String getString(String s);

	/**
	 * This method is used for fetching the current language of this localization
	 * provider.
	 * 
	 * @return string representing a code of current language
	 */
	String getCurrentLanguage();
}
