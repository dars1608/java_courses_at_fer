/**
 * This package contains utility classes used in
 * {@link hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP JNotepadPP} application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw11.jnotepadpp.util;