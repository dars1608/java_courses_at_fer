package hr.fer.zemris.java.hw11.jnotepadpp.multipledocument;

/**
 * This interface describes a listener of the {@link SingleDocumentModel}.
 * Listener must subscribe to the Subject of interest. When is subscribed, it
 * will be notified whenever the change in the Subject is made.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface SingleDocumentListener {
	
	/**
	 * This method is called whenever the model has changed the modified status.
	 * 
	 * @param model observed model
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);

	/**
	 * This method is called whenever the model has changed its file path.
	 * 
	 * @param model observed models
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}
