package hr.fer.zemris.java.hw11.jnotepadpp.multipledocument;

/**
 * This method describes a listener of the {@link MultipleDocumentModel}.
 * Listener must subscribe to the Subject of interest. When is subscribed, it
 * will be notified whenever the change in the Subject is made.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface MultipleDocumentListener {
	
	/**
	 * This method is called whenever the current document in the model has been changed.
	 * 
	 * @param previousModel model of the previous document
	 * @param currentModel model of current document
	 * @throws NullPointerException
	 *             if {@code l} arguments are both {@code null}

	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);

	/**
	 * This method is called whenever the new document is added to the observed model.
	 * 
	 * @param model model of the document to be added
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null}
	 */
	void documentAdded(SingleDocumentModel model);

	/**
	 * This method is called whenever the new document is removed to the observed model.
	 * 
	 * @param model model of the document to be removed
	 * @throws NullPointerException
	 *             if {@code l} argument is {@code null}
	 */
	void documentRemoved(SingleDocumentModel model);

}
