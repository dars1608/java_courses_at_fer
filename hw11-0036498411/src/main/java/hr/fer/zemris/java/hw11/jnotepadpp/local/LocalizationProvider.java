package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * This class represents the real implementation of {@link ILocalizationProvider}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LocalizationProvider extends AbstractLocalizationProvider {

	/** Package where .properties files are placed */
	private static final String DEFAULT_PACKAGE = "hr.fer.zemris.java.hw11.jnotepadpp.local.lang";
	/** Singleton instance of {@code LocalizationProvider} */
	private static LocalizationProvider instance;

	/** Current language */
	private String language;
	/** Current bundle */
	private ResourceBundle bundle;

	static {
		instance = new LocalizationProvider("en");
	}

	/**
	 * Private constructor method for class {@link LocalizationProvider}.
	 * 
	 * @param language
	 *            language to be set
	 */
	private LocalizationProvider(String language) {
		setLanguage(language);
	}

	/**
	 * This method is used for retrieving the instance of current
	 * {@code LocalizationProvider}.
	 * 
	 * @return current {@code LocalizationProvider}
	 */
	public static LocalizationProvider getInstance() {
		return instance;
	}

	/**
	 * Setter method for current language of the provider.
	 * 
	 * @param language
	 *            language to be set as current
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public void setLanguage(String language) {
		if(Objects.requireNonNull(language).equals(this.language)) {
			return;
		}

		this.language = language;
		this.bundle = ResourceBundle.getBundle(DEFAULT_PACKAGE, Locale.forLanguageTag(language));
		
		fire();
	}

	@Override
	public String getString(String s) {
		return bundle.getString(Objects.requireNonNull(s));
	}

	@Override
	public String getCurrentLanguage() {
		return language;
	}

}
