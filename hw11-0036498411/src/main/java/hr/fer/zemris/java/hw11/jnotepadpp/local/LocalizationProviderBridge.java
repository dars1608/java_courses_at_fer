package hr.fer.zemris.java.hw11.jnotepadpp.local;

import java.util.Objects;

/**
 * This class modelates a proxy object used as bridge from
 * {@link ILocalizationListener} objects to real implementation of
 * {@link ILocalizationProvider}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

	/** Provider */
	private ILocalizationProvider parent;
	/** Flag determining if the proxy is connected or not */
	private boolean connected;
	/** Language which was current before the disconnection from the provider */
	private String lastLanguage;

	/** Listener of the provider */
	private ILocalizationListener listener = new ILocalizationListener() {
		@Override
		public void localizationChanged() {
			fire();
		}
	};

	/**
	 * Constructor method for class {@link LocalizationProviderBridge}.
	 * 
	 * @param parent
	 *            provider of the proxy object
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	public LocalizationProviderBridge(ILocalizationProvider parent) {
		this.parent = Objects.requireNonNull(parent);
	}

	/**
	 * This method is used for connecting the proxy to its provider.
	 */
	public void connect() {
		if (connected)
			return;

		parent.addLocalizationListener(listener);
		connected = true;

		if (!parent.getCurrentLanguage().equals(lastLanguage)) {
			fire();
		}
	}

	/**
	 * This method is used to disconnect proxy from its provider.
	 */
	public void disconnect() {
		if (!connected)
			return;

		lastLanguage = parent.getCurrentLanguage();
		parent.removeLocalizationListener(listener);
		connected = false;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws IllegalStateException
	 *             if proxy is not connected
	 * @throws NullPointerException
	 *             if argument is {@code null}
	 */
	@Override
	public String getString(String s) {
		if (!connected) {
			throw new IllegalStateException("Proxy is not connected");
		}

		return parent.getString(Objects.requireNonNull(s));
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws IllegalStateException
	 *             if proxy is not connected
	 */
	@Override
	public String getCurrentLanguage() {
		if (!connected) {
			throw new IllegalStateException("Proxy is not connected");
		}

		return parent.getCurrentLanguage();
	}

}
