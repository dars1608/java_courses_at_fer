/**
 * This package contains {@link javax.swing.AbstractAction actions} used in
 * {@link hr.fer.zemris.java.hw11.jnotepadpp.JNotepadPP JNotepad++} application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw11.jnotepadpp.actions;