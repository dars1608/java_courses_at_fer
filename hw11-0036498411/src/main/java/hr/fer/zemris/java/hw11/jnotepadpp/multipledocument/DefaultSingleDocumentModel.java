package hr.fer.zemris.java.hw11.jnotepadpp.multipledocument;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * This class represents default implementation of the
 * {@code SingleDocumentModel}.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see SingleDocumentModel
 * @see SingleDocumentListener
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {

	/** Path of the file */
	private Path filePath;
	/** {@link JTextArea} object */
	private JTextArea textComponent;
	/** Flag determining if the document is modified */
	private boolean modified;
	/** List of registered listeners */
	private List<SingleDocumentListener> listeners = new ArrayList<>();

	/**
	 * Constructor method for class {@link DefaultSingleDocumentModel}.
	 * 
	 * @param filePath
	 *            path of the file where this document is located
	 * @param textContent
	 *            initial content of the document
	 */
	public DefaultSingleDocumentModel(Path filePath, String textContent) {
		this.filePath = filePath;
		textComponent = new JTextArea(textContent == null ? "" : textContent);

		textComponent.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				setModified(true);
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				setModified(true);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				setModified(true);
			}
		});
	}
	
	/**
	 * Default constructor method for class {@link DefaultSingleDocumentModel}.
	 */
	public DefaultSingleDocumentModel() {
		this(null, null);
	}

	@Override
	public JTextArea getTextComponent() {
		return textComponent;
	}

	@Override
	public Path getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(Path path) {
		filePath = Objects.requireNonNull(path);
		
		listeners.forEach(l -> l.documentFilePathUpdated(this));
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		this.modified = modified;

		listeners.forEach(l -> l.documentModifyStatusUpdated(this));
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(Objects.requireNonNull(l));
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(Objects.requireNonNull(l));

	}

}
