package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for opening the
 * document from the disk into a new tab of the multiple tab panel.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class OpenDocumentAction extends ActionWithMultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link OpenDocumentAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab model
	 * @param parent
	 * 			  parent frame
	 */
	public OpenDocumentAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/open_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/open_icon_small.png", this));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(LocalizationProvider.getInstance().getString("openFile"));
		if (fc.showOpenDialog(parent) != JFileChooser.APPROVE_OPTION) {
			return;
		}

		File fileName = fc.getSelectedFile();
		Path filePath = fileName.toPath();
		if (!Files.isReadable(filePath)) {
			JOptionPane.showMessageDialog(
					parent,
					String.format(
							LocalizationProvider.getInstance().getString("fileCantBeRead"),
							fileName.getAbsolutePath()),
					LocalizationProvider.getInstance().getString("error"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		tabbedPanel.loadDocument(filePath);
	}
}
