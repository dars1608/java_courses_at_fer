package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents {@link AbstractAction} used for removing same lines
 * from the editor.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentMode
 */
public class UniqueLinesAction extends ActionWithMultipleDocumentModel {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link UniqueLinesAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab panel
	 * @param parent
	 *            parent frame
	 */
	public UniqueLinesAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift Q"));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/unique_icon.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextArea editor = tabbedPanel.getCurrentDocument().getTextComponent();
		Caret caret = editor.getCaret();
		Document doc = editor.getDocument();

		String text;
		int line1Offset;
		int line2Offset;

		try {
			int line1 = editor.getLineOfOffset(caret.getDot());
			int line2 = editor.getLineOfOffset(caret.getMark());

			if (line1 == line2) {
				return;
			} else if (line1 < line2) {
				line1Offset = editor.getLineStartOffset(line1);
				line2Offset = editor.getLineEndOffset(line2);
			} else {
				line1Offset = editor.getLineStartOffset(line2);
				line2Offset = editor.getLineEndOffset(line1);
			}

			text = doc.getText(line1Offset, line2Offset - line1Offset);

		} catch (BadLocationException e1) {
			e1.printStackTrace();
			return;
		}

		List<String> lines = new ArrayList<>(new LinkedHashSet<>(Arrays.asList(text.split("\n"))));

		StringBuilder sb = new StringBuilder();
		lines.forEach(l -> sb.append(l).append('\n'));

		try {
			doc.remove(line1Offset, line2Offset - line1Offset);
			doc.insertString(line1Offset, sb.toString(), null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}

}
