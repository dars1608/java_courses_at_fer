package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import hr.fer.zemris.java.hw11.jnotepadpp.actions.CaseSwitchAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CloseDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CopyAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.CutAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.ExitAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.NewDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.OpenDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.PasteAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SaveAsDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SaveDocumentAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.SortAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.StatisticsAction;
import hr.fer.zemris.java.hw11.jnotepadpp.actions.UniqueLinesAction;
import hr.fer.zemris.java.hw11.jnotepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.swing.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * JNotepad++ is a simple text editor with multiple tabs. It supports functionalities
 * of <i>Windows Notepad</i> (like creating new file, opening existing file, saving file (as), 
 * copy, cut and paste text). It also supports some additional functionalities.
 * For instance:
 * <ul>
 * <li>showing text statistics (number of characters, non-blank characters and lines)
 * <li>changing and toggling the case of selected text
 * <li>sorting selected lines of the text
 * <li>extracting unique lines from selected ones
 * <li>current date-time
 * <li>current length of text in active editor
 * <li>number of current line, column and selected characters
 * </ul>
 * Application provides the localization for English, Croatian, German and Spanish language.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class JNotepadPP extends JFrame {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	/** Application title */
	private static final String DEFAULT_TITLE = "JNotepad++";

	/** Multiple tab panel */
	private MultipleDocumentModel tabbedPanel = new DefaultMultipleDocumentModel();
	/** Localization provider */
	private LocalizationProvider provider;
	/** Localization provider bridge */
	private FormLocalizationProvider bridge;
	/** Status bar*/
	private StatusBar statusBar;

	/** Listener of the current editors caret */
	private CaretListener caretListener;

	/** Map of actions */
	private Map<String, Action> actions = new HashMap<>();
	/** Map of components which support localization */
	private Map<String, JComponent> components = new HashMap<>();

	/**
	 * Constructor method for class {@link JNotepadPP}. It initializes
	 * the GUI of the <i>JNotepad++</i> program.
	 */
	public JNotepadPP() {
		setTitle(DEFAULT_TITLE);
		setLocation(50, 50);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		
		provider = LocalizationProvider.getInstance();
		bridge = new FormLocalizationProvider(provider, this);

		initActions();
		initGUI();
		
		addWindowListener(new WindowAdapter() {			
			@Override
			public void windowClosing(WindowEvent e) {
				actions.get(KeyWords.EXIT).actionPerformed(null);
			}
		});
		
		bridge.addLocalizationListener(new ILocationListenerImpl());
		bridge.connect();

		tabbedPanel.addMultipleDocumentListener(new MultipleDocumentListenerImpl());
		
		pack();
		setSize(getSize().width, 550);
	}

	/**
	 * This method is used internally for initialization of graphical interface.
	 */
	private void initGUI() {
		this.getContentPane().setLayout(new BorderLayout());

		initMenues();
		initToolBar();
		initStatusBar();
	}

	/**
	 * This method is used internally for creating and initializing menu bar.
	 */
	private void initMenues() {
		JMenuBar menuBar = new JMenuBar();

		initFileMenu(menuBar);
		initEditMenu(menuBar);
		initInfoMenu(menuBar);
		initLanguagesMenu(menuBar);
		initToolsMenu(menuBar);

		setJMenuBar(menuBar);
	}

	/**
	 * This method is used internally for initializing <i>File</i> menu.
	 * 
	 * @param menuBar 
	 * 				menu bar where <i>File</i> menu will be placed
	 */
	private void initFileMenu(JMenuBar menuBar) {
		JMenu file = new JMenu();
		file.add(new JMenuItem(actions.get(KeyWords.NEW)));
		file.add(new JMenuItem(actions.get(KeyWords.CLOSE)));
		file.add(new JMenuItem(actions.get(KeyWords.OPEN)));
		file.addSeparator();
		file.add(new JMenuItem(actions.get(KeyWords.SAVE)));
		file.add(new JMenuItem(actions.get(KeyWords.SAVE_AS)));
		file.addSeparator();
		file.add(new JMenuItem(actions.get(KeyWords.EXIT)));

		menuBar.add(file);
		components.put(KeyWords.FILE, file);	
	}
	
	/**
	 * This method is used internally for initializing <i>Edit</i> menu.
	 * 
	 * @param menuBar 
	 * 				menu bar where <i>Edit</i> menu will be placed
	 */
	private void initEditMenu(JMenuBar menuBar) {
		JMenu edit = new JMenu();
		edit.add(new JMenuItem(actions.get(KeyWords.COPY)));
		edit.add(new JMenuItem(actions.get(KeyWords.CUT)));
		edit.add(new JMenuItem(actions.get(KeyWords.PASTE)));
		
		menuBar.add(edit);
		components.put(KeyWords.EDIT, edit);
	}
	
	/**
	 * This method is used internally for initializing <i>Info</i> menu.
	 * 
	 * @param menuBar 
	 * 				menu bar where <i>Info</i> menu will be placed
	 */
	private void initInfoMenu(JMenuBar menuBar) {
		JMenu info = new JMenu();
		info.add(new JMenuItem(actions.get(KeyWords.STATISTICS)));
		
		menuBar.add(info);
		components.put(KeyWords.INFO, info);
	}
	
	/**
	 * This method is used internally for initializing <i>Languages</i> menu.
	 * 
	 * @param menuBar 
	 * 				menu bar where <i>Languages</i> menu will be placed
	 */
	private void initLanguagesMenu(JMenuBar menuBar) {
		JMenu language = new JMenu();
		
		JMenuItem en = new JMenuItem();
		en.addActionListener(l -> {
			provider.setLanguage(KeyWords.EN);
		});
		en.setIcon(IconLoader.initIcon("icons/britain_icon.png", this));
		
		JMenuItem hr = new JMenuItem();
		hr.addActionListener(l -> {
			provider.setLanguage(KeyWords.HR);
		});
		hr.setIcon(IconLoader.initIcon("icons/croatia_icon.png", this));
		
		JMenuItem de = new JMenuItem();
		de.addActionListener(l -> {
			provider.setLanguage(KeyWords.DE);
		});
		de.setIcon(IconLoader.initIcon("icons/germany_icon.png", this));
		
		JMenuItem es = new JMenuItem();
		es.addActionListener(l -> {
			provider.setLanguage(KeyWords.ES);
		});
		es.setIcon(IconLoader.initIcon("icons/spain_icon.png", this));
		
		language.add(en);
		language.add(hr);
		language.add(de);
		language.add(es);
		
		menuBar.add(language);
		
		components.put(KeyWords.LANGUAGE, language);
		components.put(KeyWords.EN, en);
		components.put(KeyWords.DE, de);
		components.put(KeyWords.HR, hr);
		components.put(KeyWords.ES, es);
	}
	
	/**
	 * This method is used internally for initializing <i>Tools</i> menu.
	 * 
	 * @param menuBar 
	 * 				menu bar where <i>Tools</i> menu will be placed
	 */
	private void initToolsMenu(JMenuBar menuBar) {
		JMenu tools = new JMenu();
		
		JMenu changeCase = new JMenu();
		JMenuItem upper = new JMenuItem(
				new CaseSwitchAction(tabbedPanel, CaseSwitchAction.Mode.UPPERCASE));
		JMenuItem lower = new JMenuItem(
				new CaseSwitchAction(tabbedPanel, CaseSwitchAction.Mode.LOWERCASE));
		JMenuItem invert = new JMenuItem(
				new CaseSwitchAction(tabbedPanel, CaseSwitchAction.Mode.INVERTCASE));
		
		changeCase.add(upper);
		changeCase.add(lower);
		changeCase.add(invert);
		changeCase.setEnabled(false);
		changeCase.setIcon(IconLoader.initIcon("icons/change_case_icon.png", this));
		tools.add(changeCase);
		
		JMenu sort = new JMenu();
		JMenuItem ascending = new JMenuItem(new SortAction(tabbedPanel, false));
		JMenuItem descending = new JMenuItem(new SortAction(tabbedPanel, true));
		
		sort.add(ascending);
		sort.add(descending);
		sort.setEnabled(false);
		sort.setIcon(IconLoader.initIcon("icons/sort_icon.png", this));
		tools.add(sort);
		
		JMenuItem unique = new JMenuItem(actions.get(KeyWords.UNIQUE));
		tools.add(unique);
		
		menuBar.add(tools);

		components.put(KeyWords.TOOLS, tools);
		components.put(KeyWords.CHANGE_CASE, changeCase);
		components.put(KeyWords.UPPER, upper);
		components.put(KeyWords.LOWER, lower);
		components.put(KeyWords.INVERT, invert);
		components.put(KeyWords.SORT, sort);
		components.put(KeyWords.ASC, ascending);
		components.put(KeyWords.DESC, descending);
	}

	/**
	 * This method is used internally for creating and initializing tool bar.
	 * For every button in the tool bar, action text is hidden just like in
	 * Notepad++, Eclipse ect. 
	 */
	private void initToolBar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(true);

		JButton newDocument = new JButton(actions.get(KeyWords.NEW));
		newDocument.setHideActionText(true);
		JButton openDocument = new JButton(actions.get(KeyWords.OPEN));
		openDocument.setHideActionText(true);
		JButton saveDocument = new JButton(actions.get(KeyWords.SAVE));
		saveDocument.setHideActionText(true);
		JButton saveAsDocument = new JButton(actions.get(KeyWords.SAVE_AS));
		saveAsDocument.setHideActionText(true);
		JButton closeDocument = new JButton(actions.get(KeyWords.CLOSE));
		closeDocument.setHideActionText(true);

		JButton copy = new JButton(actions.get(KeyWords.COPY));
		copy.setHideActionText(true);
		JButton cut = new JButton(actions.get(KeyWords.CUT));
		cut.setHideActionText(true);
		JButton paste = new JButton(actions.get(KeyWords.PASTE));
		paste.setHideActionText(true);

		JButton statistic = new JButton(actions.get(KeyWords.STATISTICS));
		statistic.setHideActionText(true);

		JButton exit = new JButton(actions.get(KeyWords.EXIT));
		exit.setHideActionText(true);

		toolBar.add(newDocument);
		toolBar.add(openDocument);
		toolBar.add(saveDocument);
		toolBar.add(saveAsDocument);
		toolBar.add(closeDocument);
		toolBar.add(copy);
		toolBar.add(cut);
		toolBar.add(paste);
		toolBar.add(statistic);
		toolBar.add(exit);
		
		this.getContentPane().add(toolBar, BorderLayout.NORTH);
	}

	/**
	 * This method is used for initializing the status bar which shows some basic 
	 * info about the active editor.
	 */
	private void initStatusBar() {
		statusBar = new StatusBar(tabbedPanel, this);
		statusBar.setVisible(true);
		tabbedPanel.addMultipleDocumentListener(statusBar);
		bridge.addLocalizationListener(statusBar);


		
		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.add(statusBar, BorderLayout.SOUTH);
		centerPanel.add((JTabbedPane) tabbedPanel, BorderLayout.CENTER);
		this.getContentPane().add(centerPanel, BorderLayout.CENTER);
	}

	/**
	 * This method is used internally for initializing map of actions which are used
	 * in {@link JNotepadPP} program. It uses {@link ClassLoader} to load classes,
	 * instantiate them and store them into map, under its names.
	 */
	private void initActions() {
		actions.put(KeyWords.CLOSE, new CloseDocumentAction(tabbedPanel, this));
		actions.put(KeyWords.COPY, new CopyAction(tabbedPanel, this));
		actions.put(KeyWords.CUT, new CutAction(tabbedPanel, this));
		actions.put(KeyWords.EXIT, new ExitAction(tabbedPanel, this));
		actions.put(KeyWords.NEW, new NewDocumentAction(tabbedPanel));
		actions.put(KeyWords.OPEN, new OpenDocumentAction(tabbedPanel, this));
		actions.put(KeyWords.PASTE, new PasteAction(tabbedPanel, this));
		actions.put(KeyWords.SAVE_AS, new SaveAsDocumentAction(tabbedPanel, this));
		actions.put(KeyWords.SAVE, new SaveDocumentAction(tabbedPanel, this));
		actions.put(KeyWords.STATISTICS, new StatisticsAction(tabbedPanel, this));
		actions.put(KeyWords.UNIQUE, new UniqueLinesAction(tabbedPanel, this));
	}

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JNotepadPP().setVisible(true);
		});

	}

	/**
	 * This class represents an implementation of {@link MultipleDocumentListener}
	 * which handles the changes occurred in {@link DefaultMultipleDocumentModel}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class MultipleDocumentListenerImpl implements MultipleDocumentListener {
		
		@Override
		public void documentRemoved(SingleDocumentModel model) {
			if (tabbedPanel.getNumberOfDocuments() == 0) {
				actions.get(KeyWords.SAVE).setEnabled(false);
				actions.get(KeyWords.SAVE_AS).setEnabled(false);
				actions.get(KeyWords.CLOSE).setEnabled(false);
				actions.get(KeyWords.STATISTICS).setEnabled(false);
			}
		}

		@Override
		public void documentAdded(SingleDocumentModel model) {
			actions.get(KeyWords.SAVE).setEnabled(true);
			actions.get(KeyWords.SAVE_AS).setEnabled(true);
			actions.get(KeyWords.CLOSE).setEnabled(true);
			actions.get(KeyWords.STATISTICS).setEnabled(true);
		}

		@Override
		public void currentDocumentChanged(
				SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
			if (previousModel != null) {
				if (caretListener != null) {
					previousModel.getTextComponent().removeCaretListener(caretListener);
				}
			}
			if (currentModel == null) {
				actions.get(KeyWords.COPY).setEnabled(false);
				actions.get(KeyWords.CUT).setEnabled(false);
				actions.get(KeyWords.PASTE).setEnabled(false);

				setTitle(DEFAULT_TITLE);
				
				components.get(KeyWords.CHANGE_CASE).setEnabled(false);
				components.get(KeyWords.SORT).setEnabled(false);
				actions.get(KeyWords.UNIQUE).setEnabled(false);
			} else {
				actions.get(KeyWords.PASTE).setEnabled(true);

				String title = currentModel.getFilePath() == null ? "Untitled"
						: currentModel.getFilePath().toAbsolutePath().toString();
				setTitle(title + " - " + DEFAULT_TITLE);

				caretListener = new CaretListener() {
					@Override
					public void caretUpdate(CaretEvent e) {
						
						int sel = Math.abs(e.getDot() - e.getMark());
						
						if(sel > 0) {
							components.get(KeyWords.CHANGE_CASE).setEnabled(true);
							components.get(KeyWords.SORT).setEnabled(true);
							actions.get(KeyWords.UNIQUE).setEnabled(true);
							actions.get(KeyWords.COPY).setEnabled(true);
							actions.get(KeyWords.CUT).setEnabled(true);
						} else {
							components.get(KeyWords.CHANGE_CASE).setEnabled(false);
							components.get(KeyWords.SORT).setEnabled(false);
							actions.get(KeyWords.UNIQUE).setEnabled(false);
							actions.get(KeyWords.COPY).setEnabled(false);
							actions.get(KeyWords.CUT).setEnabled(false);
						}
					}
				};

				currentModel.getTextComponent().addCaretListener(caretListener);
			}
		}
	}

	/**
	 * This class represents an implementation of {@link ILocalizationListener} used
	 * for tracking current language of {@link JNotepadPP} application.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class ILocationListenerImpl implements ILocalizationListener {

		@Override
		public void localizationChanged() {
			actions.forEach((key, action) -> {
				action.putValue(Action.NAME, provider.getString(key + ".name"));
				action.putValue(Action.SHORT_DESCRIPTION, provider.getString(key + ".desc"));
				action.putValue(Action.MNEMONIC_KEY,
						KeyEvent.getExtendedKeyCodeForChar(
								provider.getString(key + ".mnemonic").charAt(0)));
			});

			components.forEach((key, component) -> {
				if (component instanceof JMenu) {
					((JMenu) component).setText(provider.getString(key + ".name"));
					((JMenu) component).setMnemonic(
							KeyEvent.getExtendedKeyCodeForChar(
									provider.getString(key + ".mnemonic").charAt(0)));
				} else if (component instanceof JMenuItem) {
					((JMenuItem) component).setText(provider.getString(key + ".name"));
					((JMenuItem) component).setMnemonic(
							KeyEvent.getExtendedKeyCodeForChar(
									provider.getString(key + ".mnemonic").charAt(0)));
				}
			});
		}
	}
}
