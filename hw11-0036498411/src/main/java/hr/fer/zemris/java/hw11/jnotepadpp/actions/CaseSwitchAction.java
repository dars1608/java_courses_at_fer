package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents {@link AbstractAction} which modifies text from the
 * current editor of the {@link DefaultMultipleDocumentModel}. Action can have
 * three different modes: {@code UPPERCASE, LOWERCASE, INVERTCASE}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CaseSwitchAction extends AbstractAction {

	/** Serial number */
	private static final long serialVersionUID = 1L;

	/** Multiple tab panel */
	private MultipleDocumentModel tabbedPanel;
	/** Mode of work */
	private CaseSwitchAction.Mode mode;

	/**
	 * Constructor method for class {@link CaseSwitchAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab panel
	 * @param mode
	 *            mode of work ({@code UPPERCASE, LOWERCASE, INVERTCASE})
	 */
	public CaseSwitchAction(MultipleDocumentModel tabbedPanel, CaseSwitchAction.Mode mode) {
		this.tabbedPanel = tabbedPanel;
		this.mode = mode;

		switch (mode) {
		case UPPERCASE:
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift U"));
			putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/uppercase_icon.png", this));
			break;
		case LOWERCASE:
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift L"));
			putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/lowercase_icon.png", this));
			break;
		case INVERTCASE:
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift I"));
			putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/sort_icon.png", this));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextArea editor = tabbedPanel.getCurrentDocument().getTextComponent();
		Document doc = editor.getDocument();

		int len = Math.abs(editor.getCaret().getDot() - editor.getCaret().getMark());
		int offset = Math.min(editor.getCaret().getDot(), editor.getCaret().getMark());

		try {
			String text = doc.getText(offset, len);

			switch (mode) {
			case UPPERCASE:
				text = text.toUpperCase();
				break;
			case LOWERCASE:
				text = text.toLowerCase();
				break;
			default:
				text = changeCase(text);
			}

			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * This method is used internally to toggle case of given text.
	 * 
	 * @param text
	 *            text which casing is inverted
	 * @return result text
	 */
	private String changeCase(String text) {
		char[] symbols = text.toCharArray();
		for (int i = 0; i < symbols.length; i++) {
			char c = symbols[i];

			if (Character.isLowerCase(c)) {
				symbols[i] = Character.toUpperCase(c);
			} else if (Character.isUpperCase(c)) {
				symbols[i] = Character.toLowerCase(c);
			}
		}

		return new String(symbols);

	}

	/**
	 * Enumeration used for setting up the mode of the {@link CaseSwitchAction}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public enum Mode {
		/** To uppercase */
		UPPERCASE,
		/** To lowercase */
		LOWERCASE,
		/** Invert case */
		INVERTCASE
	}

}
