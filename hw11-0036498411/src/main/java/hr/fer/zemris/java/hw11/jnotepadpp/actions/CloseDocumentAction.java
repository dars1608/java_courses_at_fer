package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.DefaultMultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} used for closing the document
 * which was opened in {@link DefaultMultipleDocumentModel} tab.
 * 
 * @author Darko Britvec
 * @version 1.0
 * @see ActionWithMultipleDocumentModel
 */
public class CloseDocumentAction extends SaveDocumentAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link CloseDocumentAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab panel
	 * @param parent
	 *            parent frame
	 */
	public CloseDocumentAction(MultipleDocumentModel tabbedPanel, JFrame parent) {
		super(tabbedPanel, parent);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		putValue(Action.LARGE_ICON_KEY, IconLoader.initIcon("../icons/close_icon.png", this));
		putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/close_icon_small.png", this));
		setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SingleDocumentModel currentDocument = tabbedPanel.getCurrentDocument();

		if (currentDocument.isModified()) {
			int res = JOptionPane.showConfirmDialog(
					parent,
					LocalizationProvider.getInstance().getString("unsaved"),
					LocalizationProvider.getInstance().getString("closingTab"),
					JOptionPane.YES_NO_CANCEL_OPTION, 
					JOptionPane.QUESTION_MESSAGE
		    );

			if (res == JOptionPane.CANCEL_OPTION) {
				return;
			}

			if (res == JOptionPane.YES_OPTION) {
				super.actionPerformed(e);
				if (tabbedPanel.getCurrentDocument() != null
						&& tabbedPanel.getCurrentDocument().isModified()) {
					return;
				}
			}
		}

		tabbedPanel.closeDocument(currentDocument);
	}

}
