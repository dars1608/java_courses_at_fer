package hr.fer.zemris.java.hw11.jnotepadpp.actions;

import java.awt.event.ActionEvent;
import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.multipledocument.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.util.IconLoader;

/**
 * This class represents an {@link AbstractAction} which is used for sorting
 * selected lines based on the current language.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SortAction extends AbstractAction {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Flag determining if the sort is descending or not */
	private boolean isDescending;
	/** Multiple tab panel */
	private MultipleDocumentModel tabbedPanel;

	/**
	 * Constructor method for class {@link SortAction}.
	 * 
	 * @param tabbedPanel
	 *            multiple tab panel
	 * @param locProvider
	 * 			  localization provider
	 * @param isDescending
	 *            flag determining if the sort is descending or not
	 * @throws NullPointerException
	 *             if the arguments {@code tabbedPanel or locProvider} are
	 *             {@code null}
	 */
	public SortAction(MultipleDocumentModel tabbedPanel,boolean isDescending) {
		this.tabbedPanel = Objects.requireNonNull(tabbedPanel);
		this.isDescending = isDescending;
		
		if(isDescending) {
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift D"));
			putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/descending_icon.png", this));
		} else {
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt shift A"));
			putValue(Action.SMALL_ICON, IconLoader.initIcon("../icons/ascending_icon.png", this));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JTextArea editor = tabbedPanel.getCurrentDocument().getTextComponent();
		Caret caret = editor.getCaret();
		Document doc = editor.getDocument();

		String text;
		int line1Offset;
		int line2Offset;
		
		try {
			int line1 = editor.getLineOfOffset(caret.getDot());
			int line2 = editor.getLineOfOffset(caret.getMark());

			if (line1 == line2) {
				return;
			} else if (line1 < line2) {
				line1Offset = editor.getLineStartOffset(line1);
				line2Offset = editor.getLineEndOffset(line2);
			} else {
				line1Offset = editor.getLineStartOffset(line2);
				line2Offset = editor.getLineEndOffset(line1);
			}

			text = doc.getText(line1Offset, line2Offset - line1Offset);

		} catch (BadLocationException e1) {
			e1.printStackTrace();
			return;
		}
		
		List<String> lines = Arrays.asList(text.split("\n"));

		Locale locale = new Locale(LocalizationProvider.getInstance().getCurrentLanguage());
		Collator collator = Collator.getInstance(locale);

		Comparator<String> comp = (s1, s2) -> collator.compare(s1, s2) * (isDescending ? -1 : 1);
		lines.sort(comp);

		StringBuilder sb = new StringBuilder();
		lines.forEach(l -> sb.append(l).append('\n'));

		try {
			doc.remove(line1Offset, line2Offset - line1Offset);
			doc.insertString(line1Offset, sb.toString(), null);
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
	}
}
