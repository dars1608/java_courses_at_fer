package hr.fer.zemris.java.hw11.jnotepadpp.swing;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * This class represents a {@link JLabel} with functionalities of
 * simple clock with a calendar. Format of the date which is show is:
 * "yyyy/mm/dd HH:mm:ss".
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SimpleClock extends JLabel {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	/** Formatter object used for formating date-time string */
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("uuuu/mm/dd HH:mm:ss");

	/** String representing current time */
	private volatile String timeString;
	/** Flag used to determine when the clock must stop */
	private volatile boolean stop;

	/**
	 * Constructor method for class {@link SimpleClock}.
	 */
	public SimpleClock() {
		super.setHorizontalAlignment(JLabel.RIGHT);
		updateTime();

		Thread t = new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(500);
				} catch (Exception ex) {
				}

				if (stop)
					break;

				SwingUtilities.invokeLater(() -> {
					updateTime();
				});
			}
		});

		t.setDaemon(true);
		t.start();
	}

	/**
	 * This method stops the active clock.
	 */
	public void stopClock() {
		stop = true;
	}

	/**
	 * This method is used internally for updating {@code timeString} value to current time.
	 */
	private void updateTime() {
		timeString = FORMATTER.format(LocalDateTime.now());
		setText(timeString);
	}
}
