/**
 * This package and its subpackages contain classes which are necessary to solve second homework
 * assignment for Java Course @FER 2017/18.
 */
/**
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java;