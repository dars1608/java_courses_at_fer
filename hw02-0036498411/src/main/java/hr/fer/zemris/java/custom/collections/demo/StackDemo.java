/**
 * 
 */
package hr.fer.zemris.java.custom.collections.demo;

import java.util.Arrays;

import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * This program uses {@link ObjectStack} to calculate mathematical expression
 * which must be postfix representation. Expression must be entered as one
 * command line argument (enclose whole expression into quotation marks).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class StackDemo {
	
	private static final String MOD = "%";
	private static final String MUL = "*";
	private static final String DIV = "/";
	private static final String MINUS = "-";
	private static final String PLUS = "+";

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments.
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException(
					"Program accepts single argument. " + args.length + " provided");
		}

		try {
			System.out.println("Expression evaluates to " + expressionSolver(args[0]));
		} catch (ArithmeticException ex){
			System.out.println("Expression contains division by zero.");
		} catch (IllegalArgumentException ex) {
			System.out.println("Expression is illegal and can't be solved");
		}
	}

	/**
	 * This method calculates mathematical expression written in postfix notation.
	 * 
	 * @param expression
	 *            String which contains operators and operands of an expression
	 *            which needs to be solved.
	 * @return Solution of given expression.
	 * @throws <li>{@link
	 *             IllegalArgumentException} if expression cannot be solved.
	 *         <li> - {@link
	 *             NullPointerException} if given string value is null.
	 */
	public static int expressionSolver(String line) {
		if (line == null) {
			throw new NullPointerException("Line cant be null.");
		}
		String[] expression = line.trim().replaceAll("\\s\\s+", " ").split(" ");
		ObjectStack stack = new ObjectStack();

		for (String element : expression) {
			try {
				stack.push(Integer.parseInt(element));

			} catch (NumberFormatException ex) {
				if (isOperator(element)) {
					try {
						stack.push(operation(element, (int) stack.pop(), (int) stack.pop()));
					} catch (EmptyStackException ex2) {
						throw new IllegalArgumentException(
								"Expression: " + Arrays.toString(expression) + " is not legal.");
					}
				}
			}
		}

		if (stack.size() != 1) {
			throw new IllegalArgumentException(
					"Expression: " + Arrays.toString(expression) + " is not legal.");
		}

		return (int) stack.pop();
	}

	/**
	 * This method is used internally to calculate an expression based on given
	 * arguments.
	 * 
	 * @param element
	 *            String which represents an operator.
	 * @param rightOperand
	 *            Right operand.
	 * @param leftOperand
	 *            Left operand.
	 * @return Operation result.
	 * @throws <li>{@link
	 *             IllegalArgumentException} if given element is illegal.
	 *         <li> - {@link
	 *             NullPointerException} if given element is null.
	 */
	private static Object operation(String element, int rightOperand, int leftOperand) {
		if (element == null) {
			throw new NullPointerException("String argument can't be null");
		}
		
		if(leftOperand == 0 && element.equals(DIV)) {
			throw new ArithmeticException("Division by zero.");
		}

		switch (element) {
		case PLUS:
			return leftOperand + rightOperand;
		case MINUS:
			return leftOperand - rightOperand;
		case DIV:
			return leftOperand / rightOperand;
		case MUL:
			return leftOperand * rightOperand;
		case MOD:
			return leftOperand % rightOperand;
		default:
			throw new IllegalArgumentException("Operand " + element + " is illegal.");
		}
	}

	/**
	 * This method is used internally to check whether the given string element is
	 * an operator.
	 * 
	 * @param element
	 *            Checked string.
	 * @return <code>true</code> if it is, <code>false</code> otherwise.
	 */
	private static boolean isOperator(String element) {
		return element.equals(PLUS) || element.equals(MINUS) || element.equals(DIV) || element.equals(MUL)
				|| element.equals(MOD);
	}
}
