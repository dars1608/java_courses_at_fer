/**
 * This package contains {@link ComplexNumber} class used fifth homework
 * assignment.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw02;