/**
 * This package contains demonstration program for {@link ObjectStack}
 * class.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections.demo;