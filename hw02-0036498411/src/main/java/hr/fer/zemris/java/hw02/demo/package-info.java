/**
 * This package contains demonstration class which demonstrates usage of
 * {@link ComplexNumber} class.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw02.demo;