package hr.fer.zemris.java.custom.collections;

/**
 * This class represents an implementation of linked list-backed collection of
 * objects. General contract of this collection is: duplicate elements are
 * allowed; storage of null references is not allowed.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LinkedListIndexedCollection extends Collection {

	/**
	 * This class is used internally to form linked list.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private static class ListNode {
		/**
		 * Previous node in linked list.
		 */
		ListNode previous;
		/**
		 * Next node in linked list.
		 */
		ListNode next;
		/**
		 * Value stored in node.
		 */
		Object value;

		/**
		 * Constructor for class {@link ListNode}.
		 * 
		 * @param previous
		 *            Reference to previous node in linked list.
		 * @param next
		 *            Reference to next node in linked list.
		 * @param value
		 *            Reference to an object which is stored in list.
		 */
		public ListNode(ListNode previous, ListNode next, Object value) {
			this.previous = previous;
			this.next = next;
			this.value = value;
		}
	}

	/**
	 * Collection size.
	 */
	private int size;
	/**
	 * Reference to lists first node.
	 */
	private ListNode first;
	/**
	 * Reference to lists last node.
	 */
	private ListNode last;

	{
		size = 0;
		first = null;
		last = null;
	}

	/**
	 * Default constructor for {@link LinkedListIndexedCollection}.
	 */
	public LinkedListIndexedCollection() {
	}

	/**
	 * Constructor for {@link LinkedListIndexedCollection} which copies values from
	 * given collection.
	 * 
	 * @param other
	 *            Collection which values are copied.
	 * @throws <li>{@link
	 * 			NullPointerException} if argument is null.
	 */
	public LinkedListIndexedCollection(Collection other) {
		if (other == null)
			throw new NullPointerException("Argument other can't be null reference");

		this.addAll(other);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws <li>{@link
	 * 			NullPointerException} if given value is <code>null</code>.
	 */
	@Override
	public void add(Object value) {
		if (value == null) {
			throw new NullPointerException(
					"LinkedListIndexedCollection can't store null values.");
		}

		if (first == null && last == null) {
			first = new ListNode(null, null, value);
			last = first;
		} else {
			last.next = new ListNode(last, null, value);
			last = last.next;
		}
		size++;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object value) {
		ListNode temp = first;

		while (temp != null) {
			if (temp.value.equals(value))
				return true;
			temp = temp.next;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object value) {
		if (first == null || value == null) {
			return false;
		} else if (first.value.equals(value)) {
			first = first.next;
			first.previous.value = null;
			first.previous = null;
		} else if (last.value.equals(value)) {
			last = last.previous;
			last.next.value = null;
			last.next = null;
		} else {
			ListNode temp = first;
			while (temp.next != null && !temp.next.value.equals(value)) {
				temp = temp.next;
			}
			if (temp.next != null) {
				temp.next.value = null;
				if (temp.next.next == null) {
					last = temp;
				} else {
					temp.next.next.previous = temp;
				}
				temp.next = temp.next.next;
			} else {
				return false;
			}
		}
		size--;
		return true;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toArray() {
		Object[] returnArray = new Object[size];
		ListNode temp = first;
		for (int i = 0; i < size; i++) {
			returnArray[i] = temp.value;
			temp = temp.next;
		}
		return returnArray;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void forEach(Processor processor) {
		ListNode temp = first;
		while (temp != null) {
			processor.process(temp.value);
			temp = temp.next;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		first = null;
		last = null;
		size = 0;

	}

	/**
	 * This method returns the object that is stored in linked list at position
	 * index. Valid indexes are 0 to size-1.
	 * 
	 * @param index
	 *            Index of an object which needs to be returned.
	 * @return Object at given index.
	 * @throws <li>{@link
	 * 			IndexOutOfBoundsException} if index isn't in range [0, size-1].
	 */
	public Object get(int index) {
		if (!inBounds(index)) {
			throw new IndexOutOfBoundsException(
					"Requested index is out of range [0, " + (size - 1) + "]. Is " + index + ".");
		}

		return findIndex(index).value;
	}

	/**
	 * This method inserts object at given index.
	 * 
	 * @param value
	 *            Object which is inserted.
	 * @param position
	 *            Index at which object needs to be inserted.
	 * @throws <li>{@link
	 * 			IndexOutOfBoundsException} if given index is out of bounds
	 *         <li> - {@link NullPointerException} if given value is
	 *             <code>null</code>.
	 */
	public void insert(Object value, int position) {
		if (value == null) {
			throw new NullPointerException("This collection can't store null values");
		}
		if (position == size) {
			add(value);
			return;
		} else if (!inBounds(position)) {
			throw new IndexOutOfBoundsException(
					"Index is out of bounds [0, " + size + "]. Is " + position + ".");
		} else {
			if (position == 0) {
				first.previous = new ListNode(null, first, value);
				first = first.previous;
				size++;
				return;
			} else {
				ListNode temp = findIndex(position - 1);

				temp.next.previous = new ListNode(temp, temp.next, value);
				temp.next = temp.next.previous;
			}

			size++;
		}
	}

	/**
	 * This method is used to find index of objects first occurrence.
	 * 
	 * @param value
	 *            Object which index needs to be found.
	 * @return Index of given object, -1 if it's <code>null</code> or not in
	 *         collection.
	 */
	public int indexOf(Object value) {
		if (value == null) {
			return -1;
		}

		ListNode temp = first;
		for (int i = 0; temp != null; temp = temp.next, i++) {
			if (temp.value.equals(value))
				return i;
		}
		return -1;
	}

	/**
	 * Removes element at given index.
	 * 
	 * @param index
	 *            Index of element which needs to be removed.
	 * 
	 * @throws <li>{@link
	 * 			  IndexOutOfBoundsException} if index isn't from range [0,
	 *             size].
	 */
	public void remove(int index) {
		if (!inBounds(index)) {
			throw new IndexOutOfBoundsException(
					"Index is out of bounds [0, " + size + "]. Is " + index + ".");
		}

		if (index == 0) {
			remove(first.value);
		} else if (index == size - 1) {
			remove(last.value);
		} else {
			ListNode temp = findIndex(index - 1);
			temp.next.value = null;
			temp.next = temp.next.next;
			if (temp.next != null) {
				temp.next.previous = temp;
			}

			size--;
		}
	}

	// These methods are used internally.

	/**
	 * This method is used internally for checking if given index is in range [0,
	 * size-1]
	 * 
	 * @param index
	 *            Index which is checked
	 * @return <code>true</code> if it's in that range, <code>false</code>
	 *         otherwise.
	 */
	private boolean inBounds(int index) {
		return index >= 0 && index < size;
	}

	/**
	 * This method is used internally for finding object at given index. It checks
	 * if the index is near 0 or size of collection. Isn't safe for public usage
	 * (Could return {@link NullPointerException}).
	 * 
	 * @param index
	 *            Index of an object which needs to be returned.
	 * @return ListNode at given index.
	 */
	private ListNode findIndex(int index) {
		ListNode temp;
		if (index < size / 2) {
			temp = first;
			for (int i = 0; i < index; i++) {
				temp = temp.next;
			}
		} else {
			temp = last;
			for (int i = size - 1; i > index; i--) {
				temp = temp.previous;
			}
		}
		return temp;
	}

	// These methods are defined only for testing.

	/**
	 * This method is used for testing only.
	 * 
	 * @return First {@link ListNode} of this list.
	 */
	ListNode getFirst() {
		return first;
	}

	/**
	 * This method is used for testing only.
	 * 
	 * @return Last {@link ListNode} of this list.
	 */
	ListNode getLast() {
		return last;
	}
}
