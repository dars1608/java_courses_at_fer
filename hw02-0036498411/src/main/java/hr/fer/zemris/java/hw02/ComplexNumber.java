	/**
 * 
 */
package hr.fer.zemris.java.hw02;

import static java.lang.Math.sqrt;
import static java.lang.Math.atan;
import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.abs;
import static java.lang.Math.PI;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used to represent complex number and some of the functions
 * defined for complex numbers.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ComplexNumber {
	/**
	 * Default precision used for determine whether the two doubles are the same. Is
	 * set to 1e-10.
	 */
	private static final double DEFAULT_PRECISION = 1E-10;

	private final double realPart;
	private final double imaginaryPart;
	private final double magnitude;
	private final double angle;

	/**
	 * Default constructor for class {@link ComplexNumber}.
	 * 
	 * @param realPart
	 *            Real part.
	 * @param imaginary
	 *            Part Imaginary part.
	 */
	public ComplexNumber(double realPart, double imaginaryPart) {
		this.realPart = realPart;
		this.imaginaryPart = imaginaryPart;
		this.magnitude = sqrt(realPart * realPart + imaginaryPart * imaginaryPart);
		
		double tempAngle = atan(imaginaryPart / realPart);
		if (realPart <= 0 && imaginaryPart >= 0 || realPart <= 0 && imaginaryPart<=0) {
			tempAngle += PI;
		}
		this.angle = tempAngle;
	}

	/**
	 * This method is used for conversion from real number to complex number.
	 * 
	 * @param real
	 *            Real number.
	 * @return Complex representation of given number.
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}

	/**
	 * This method is used for conversion from imaginary to complex number.
	 * 
	 * @param imaginary
	 *            Imaginary number
	 * @return Complex representation of given number.
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}

	/**
	 * This method is used for conversion from trigonometric notation to of complex
	 * number to classic notation.
	 * 
	 * @param magnitude
	 *            Magnitude of given complex number.
	 * @param angle
	 *            Angle of given complex number.
	 * @return Complex representation of given number.
	 * @throws <li> {@link 
	 * 				 IllegalArgumentException} if given magnitude is less than zero.
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		if (magnitude<0) {
			throw new IllegalArgumentException("Amplitude must be 0 or positive number. Is " + magnitude +".");
		}
		return new ComplexNumber(magnitude * cos(angle), magnitude * sin(angle));
	}

	/**
	 * This method is used for parsing complex number form a string.
	 * 
	 * @param s
	 *            String which is parsed.
	 * @return Parsed complex number.
	 * @throws <li> {@link
	 * 				NullPointerException} if given argument is <code>null</code>.
	 * 		   <li> - {@link
	 * 				IllegalArgumentException} if string argument cannot be parsed.
	 */
	public static ComplexNumber parse(String s) {
		if(s==null) {
			throw new NullPointerException("Argument can't be null.");
		}
		double real = 0;
		double imaginary = 0;

		s = s.replaceAll("\\s", "");

		if (s.equals("i")) {
			imaginary = 1;
		}else if (s.equals("-i")){
			imaginary = -1;
		} else {
			Pattern patternReImag = Pattern.compile(
					"([-]?[0-9[.]]+\\.?[0-9[.]]?)([-|+]+[0-9[.]]+\\.?[0-9[.]]?)[i$]+");
			Pattern patternRe = Pattern.compile("([-]?[0-9[.]]+\\.?[0-9[.]]?)$");
			Pattern patternImag = Pattern.compile("([-]?[0-9[.]]+\\.?[0-9[.]]?)[i$]");

			Matcher matcherReImag = patternReImag.matcher(s);
			Matcher matcherRe = patternRe.matcher(s);
			Matcher matcherImag = patternImag.matcher(s);

			if (matcherReImag.find()) {
				real = Double.parseDouble(matcherReImag.group(1));
				imaginary = Double.parseDouble(matcherReImag.group(2));
			} else if (matcherRe.find()) {
				real = Double.parseDouble(matcherRe.group(1));
				imaginary = 0;
			} else if (matcherImag.find()) {
				real = 0;
				imaginary = Double.parseDouble(matcherImag.group(1));
			} else {
				throw new IllegalArgumentException(
						"String " + s + " cannot be parsed to complex number.");
			}
		}

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * This method returns real part of this complex number.
	 * 
	 * @return Real part.
	 */
	public double getRealPart() {
		return realPart;
	}

	/**
	 * This method returns imaginary part of this complex number.
	 * 
	 * @return Imaginary part.
	 */
	public double getImaginaryPart() {
		return imaginaryPart;
	}

	/**
	 * This method returns magnitude of this complex number.
	 * 
	 * @return Magnitude.
	 */
	public double getMagnitude() {
		return magnitude;
	}

	/**
	 * This method returns angle of this complex number.
	 * 
	 * @return Angle.
	 */
	public double getAngle() {
		return angle;
	}

	/**
	 * This method adds this complex number with other.
	 * 
	 * @param c
	 *            Other complex number.
	 * @return Result of adding.
	 * @throws <li>{@link
	 *             NullPointerException} if argument is null.
	 */
	public ComplexNumber add(ComplexNumber c) {
		if (c == null) {
			throw new NullPointerException("Argument can't be null");
		}
		return new ComplexNumber(this.realPart + c.realPart, this.imaginaryPart + c.imaginaryPart);
	}

	/**
	 * This method subtracts this complex number with other.
	 * 
	 * @param c
	 *            Other complex number.
	 * @return Result of subtraction.
	 * @throws <li>{@link
	 *             NullPointerException} if argument is null.
	 */
	public ComplexNumber sub(ComplexNumber c) {
		if (c == null) {
			throw new NullPointerException("Argument can't be null");
		}
		return new ComplexNumber(this.realPart - c.realPart, this.imaginaryPart - c.imaginaryPart);
	}

	/**
	 * This method multiplies this complex number with other.
	 * 
	 * @param c
	 *            Other complex number.
	 * @return Result of multiplication.
	 * @throws <li>{@link
	 *             NullPointerException} if argument is null.
	 */
	public ComplexNumber mul(ComplexNumber c) {
		if (c == null) {
			throw new NullPointerException("Argument can't be null");
		}
		return fromMagnitudeAndAngle(this.magnitude * c.magnitude, this.angle + c.angle);
	}

	/**
	 * This method divides this complex number with other.
	 * 
	 * @param c
	 *            Other complex number.
	 * @return Result of division.
	 * @throws <li>{@link
	 *             NullPointerException} if argument is null.
	 * 		   <li> - {@link
	 *             IllegalArgumentException} if arguments magnitude is 0.
	 */
	public ComplexNumber div(ComplexNumber c) {
		if (c == null) {
			throw new NullPointerException("Argument can't be null");
		} else if (c.magnitude < DEFAULT_PRECISION) {
			throw new IllegalArgumentException("Arguments magnitude can't be 0.");
		}
		return fromMagnitudeAndAngle((this.magnitude / c.magnitude), (this.angle - c.angle));
	}

	/**
	 * This method returns the value of this complex number raised to the power of
	 * argument n.
	 * 
	 * @param n
	 *            exponent
	 * @return Result of (this complex number)^n.
	 * @throws <li>{@link
	 *             IllegalArgumentException} if provided argument is negative.
	 */
	public ComplexNumber power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Argument must be non negative number. Is " + n);
		}
		return fromMagnitudeAndAngle(pow(this.magnitude, n), this.angle * n);
	}

	/**
	 * This method calculates n-roots of this complex number.
	 * 
	 * @param n
	 *            degree of root.
	 * @return n-roots of this complex number.
	 */
	public ComplexNumber[] root(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("Argument must be positive number. Is " + n);
		}
		ComplexNumber[] roots = new ComplexNumber[n];
		double newMagnitude = pow(this.magnitude, 1.0 / n);
		for (int k = 0; k < n; k++) {
			roots[k] = fromMagnitudeAndAngle(newMagnitude, (angle + 2 * k * PI) / n);
		}
		return roots;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString(java.lang.Object)
	 */
	@Override
	public String toString() {
		return String.format("%s%s%s", realPart == 0 ? "" : realPart + "",
				imaginaryPart != 0 ? (imaginaryPart >= 0 ? (realPart != 0 ? "+" : "") : "-") : "",
				imaginaryPart == 0 ? "" : Math.abs(imaginaryPart) + "i");
	}

	// ---------------JUnit tests are using method equals to compare some solutions.-----------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(realPart, imaginaryPart);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ComplexNumber))
			return false;
		ComplexNumber other = (ComplexNumber) obj;
		if (abs(this.realPart - other.realPart) > DEFAULT_PRECISION)
			return false;
		if (abs(this.imaginaryPart - other.imaginaryPart) > DEFAULT_PRECISION)
			return false;
		return true;
	}

}
