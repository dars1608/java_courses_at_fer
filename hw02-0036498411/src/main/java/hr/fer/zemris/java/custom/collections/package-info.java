/**
 * This package contains classes used for solving homework assignments 1-4.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.collections;