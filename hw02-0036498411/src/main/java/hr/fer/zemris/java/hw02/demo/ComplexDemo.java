/**
 * 
 */
package hr.fer.zemris.java.hw02.demo;


import java.util.Arrays;

import hr.fer.zemris.java.hw02.ComplexNumber;

/**
 * This program demonstrates functionality of class <code>ComplexDemo</code>.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ComplexDemo {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments.
	 */
	public static void main(String[] args) {
		ComplexNumber c1 = new ComplexNumber(2, 3);
		ComplexNumber c2 = ComplexNumber.parse("2.5-3i");
		ComplexNumber c3 = c1.add(
				ComplexNumber.fromMagnitudeAndAngle(2, 1.57)).div(c2).power(3).root(2)[1];
		
		System.out.println(c3);
		System.out.println(Arrays.toString(new ComplexNumber(2, -1).root(5)));
	}
}
