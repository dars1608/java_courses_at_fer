/**
 * 
 */
package hr.fer.zemris.java.custom.collections;

/**
 * This class is an implementation of stack-like collection. It uses
 * {@link ArrayIndexedCollection} for storing objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ObjectStack {
	/**
	 * Backed-collection used to store data.
	 */
	private ArrayIndexedCollection data;

	/**
	 * Default constructor for class {@link ObjectStack}.
	 */
	public ObjectStack() {
		data = new ArrayIndexedCollection();
	}

	/**
	 * This method checks if the stack is empty.
	 * 
	 * @return <code>true</code> if collection contains no objects and
	 *         <code>false</code> otherwise.
	 */
	public boolean isEmpty() {
		return data.isEmpty();
	}

	/**
	 * This method returns the number of currently stored objects on this stack.
	 * 
	 * @return Number of currently stored objects
	 */
	public int size() {
		return data.size();
	}

	/**
	 * This method pushes given value on the stack.
	 * 
	 * @param value
	 *            Value which needs to be pushed on the stack.
	 * @throws <li>{@link
	 * 			  NullPointerException} if given value is null.
	 */
	public void push(Object value) {
		try {
			data.add(value);
		} catch (NullPointerException ex) {
			throw new NullPointerException("Null value isn't allowed to be placed on stack");
		}
	}

	/**
	 * This method removes last value pushed on stack from stack and returns it.
	 * 
	 * @return Last value pushed on stack.
	 * @throws <li>{@link
	 *             EmptyStackException} if the stack is empty.
	 */
	public Object pop() {
		if (data.size() == 0) {
			throw new EmptyStackException("Stack is empty. This operation can't be executed");
		}
		Object temp = data.get(data.size() - 1);
		data.remove(data.size() - 1);
		return temp;
	}

	/**
	 * This method returns last value pushed on stack.
	 * 
	 * @return Last value pushed on stack.
	 * @throws <li>{@link
	 *             EmptyStackException} if the stack is empty.
	 */
	public Object peek() {
		if (data.size() == 0) {
			throw new EmptyStackException("Stack is empty. This operation can't be executed");
		}
		return data.get(data.size() - 1);
	}

	/**
	 * This method removes all elements from stack.
	 */
	public void clear() {
		data.clear();
	}
}
