package hr.fer.zemris.java.custom.collections.demo;

import static org.junit.Assert.*;
import static hr.fer.zemris.java.custom.collections.demo.StackDemo.expressionSolver;

import org.junit.Test;

public class StackDemoTest {

	@Test
	public void testExpressionSolver() {
		assertEquals(4, expressionSolver("8 -2 / -1 *"));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testExpressionSolverException() {
		expressionSolver("8 -2 / -1 * - - -");
	}

}
