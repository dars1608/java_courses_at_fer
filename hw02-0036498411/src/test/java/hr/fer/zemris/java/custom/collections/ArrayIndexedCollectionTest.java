package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.Processor;

public class ArrayIndexedCollectionTest {
	private ArrayIndexedCollection testCollection;

	@Before
	public void setUp() throws Exception {
		testCollection = new ArrayIndexedCollection();

		for (int i = 0; i < 16; i++) {
			testCollection.add("a" + i);
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testArrayIndexedCollectionInt() {
		@SuppressWarnings("unused")
		ArrayIndexedCollection test = new ArrayIndexedCollection(-1);
	}

	@Test
	public void testArrayIndexedCollectionCollection() {
		ArrayIndexedCollection test = new ArrayIndexedCollection(testCollection);
		assertEquals(test.size(), testCollection.size());
		assertEquals(testCollection.size(), test.getCapacity());
		for (int i = 0, end = test.size(); i < end; i++) {
			if (!test.get(i).equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}

	@Test
	public void testArrayIndexedCollectionCollectionInt() {
		ArrayIndexedCollection test = new ArrayIndexedCollection(testCollection, 18);
		assertEquals(18, test.getCapacity());

	}

	@Test
	public void testSize() {
		assertEquals(16, testCollection.size());
	}

	@Test
	public void testAdd() {
		testCollection.add("a16");
		assertEquals(17, testCollection.size());
		assertEquals("a16", testCollection.get(16));
	}

	@Test(expected = NullPointerException.class)
	public void testAddNull() {
		testCollection.add(null);
	}
	
	@Test
	public void testGet() {
		assertEquals("a12", testCollection.get(12));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetIndex() {
		testCollection.get(-1);
	}

	@Test
	public void testClear() {
		int testCapacity = testCollection.getCapacity();
		testCollection.clear();
		assertEquals(0, testCollection.size());
		assertEquals(testCapacity, testCollection.getCapacity());
	}

	@Test
	public void testInsert() {
		ArrayIndexedCollection test = new ArrayIndexedCollection(testCollection);
		test.insert("x", 5);
		test.insert("y", 0);
		test.insert("z", test.size());
		assertEquals(19, test.size());
		assertEquals("x", test.get(6));
		assertEquals("y", test.get(0));
		assertEquals("z", test.get(18));
		assertEquals("a13", test.get(15));
	}

	@Test(expected = NullPointerException.class)
	public void testInsertNull() {
		testCollection.insert(null, 3);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testInsertIndex() {
		testCollection.insert("x", 58);
	}

	@Test
	public void testIndexOf() {
		assertEquals(15, testCollection.indexOf("a15"));
		assertNotEquals(15, testCollection.indexOf("a1"));
		assertEquals(-1, testCollection.indexOf("A100"));
		assertEquals(-1, testCollection.indexOf(null));
	}
	
	@Test
	public void testRemoveInt() {
		testCollection.remove(0);
		testCollection.remove(5);
		testCollection.remove(13);
		assertEquals(13, testCollection.size());
		assertEquals("a1", testCollection.get(0));
		assertEquals("a14", testCollection.get(12));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveIntIndex() {
		testCollection.remove(-1);
	}
	
	@Test
	public void testForEach() {
		Object[] test = new Object[16];
		Processor p = new Processor () {
			int i = 0;
			@Override
			public void process(Object value) {
				test[i] = value;
				i++;
			}
		};
		
		testCollection.forEach(p);
		for (int i = 0, end = testCollection.size(); i < end; i++) {
			if (!test[i].equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}

	@Test
	public void testContains() {
		assertTrue(testCollection.contains("a1"));
		assertTrue(!testCollection.contains("AAA"));
	}
	
	@Test
	public void testToArray() {
		Object[] test = testCollection.toArray();
		for (int i = 0, end = testCollection.size(); i < end; i++) {
			if (!test[i].equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}
	
	@Test
	public void testRemoveObject() {
		assertTrue(testCollection.remove("a1"));
		assertEquals(15, testCollection.size());
		assertEquals("a2", testCollection.get(1));
	}

	
	@Test
	public void testIsEmpty() {
		testCollection.clear();
		assertTrue(testCollection.isEmpty());
	}

	@Test
	public void testAddAll() {
		ArrayIndexedCollection test = new ArrayIndexedCollection();
		test.addAll(testCollection);
		assertEquals(16, test.size());
		for (int i = 0, end = test.size(); i < end; i++) {
			if (!test.get(i).equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}
	
	
	
	
	
}
