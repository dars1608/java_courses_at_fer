package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LinkedListIndexedCollectionTest {
	LinkedListIndexedCollection testCollection;
	
	@Before
	public void setUp() throws Exception {
		 testCollection = new LinkedListIndexedCollection();
		 
		 for (int i = 0; i < 16; i++) {
				testCollection.add("a" + i);
		 }
	}
	
	@Test
	public void testLinkedListIndexedCollectionCollection() {
		LinkedListIndexedCollection test = new LinkedListIndexedCollection(testCollection);
		
		assertEquals(test.size(), testCollection.size());
		for (int i = 0, end = test.size(); i < end; i++) {
			if (!test.get(i).equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}

	@Test
	public void testSize() {
		assertEquals(16, testCollection.size());
	}

	@Test
	public void testAdd() {
		testCollection.add("x");
		assertEquals(17, testCollection.size());
		assertEquals("x", testCollection.get(16));
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddNull() {
		testCollection.add(null);
	}

	@Test
	public void testContains() {
		assertTrue(testCollection.contains("a1"));
		assertTrue(!testCollection.contains("AAA"));
	}
	
	@Test
	public void testRemoveObject() {
		assertTrue(testCollection.remove("a0"));
		assertTrue(testCollection.remove("a15"));
		assertTrue(testCollection.remove("a5"));
		assertEquals("a1", testCollection.get(0));
		assertEquals("a6", testCollection.get(4));
		assertEquals("a14", testCollection.get(12));
		assertEquals(13, testCollection.size());
	}


	
	@Test
	public void testToArray() {
		Object[] test = testCollection.toArray();
		for (int i = 0, end = testCollection.size(); i < end; i++) {
			if (!test[i].equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}

	@Test
	public void testForEach() {
		Object[] test = new Object[16];
		Processor p = new Processor () {
			int i = 0;
			@Override
			public void process(Object value) {
				test[i] = value;
				i++;
			}
		};
		
		testCollection.forEach(p);
		for (int i = 0, end = testCollection.size(); i < end; i++) {
			if (!test[i].equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}

	@Test
	public void testClear() {
		testCollection.clear();
		assertEquals(0, testCollection.size());
		assertNull(testCollection.getFirst());
		assertNull(testCollection.getLast());
	}

	@Test
	public void testGet() {
		assertEquals("a15", testCollection.get(15));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetIndex() {
		testCollection.get(-1);
	}
	
	@Test
	public void testInsert() {
		LinkedListIndexedCollection test = new LinkedListIndexedCollection(testCollection);
		test.insert("x", 5);
		test.insert("y", 0);
		test.insert("z", test.size());
		assertEquals(19, test.size());
		assertEquals("x", test.get(6));
		assertEquals("y", test.get(0));
		assertEquals("z", test.get(18));
		assertEquals("a13", test.get(15));
	}
	
	@Test(expected = NullPointerException.class)
	public void testInsertNull() {
		testCollection.insert(null, 3);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testInsertIndex() {
		testCollection.insert("x", 58);
	}
	
	@Test
	public void testIndexOf() {
		assertEquals(15, testCollection.indexOf("a15"));
		assertNotEquals(15, testCollection.indexOf("a1"));
		assertEquals(-1, testCollection.indexOf("A100"));
		assertEquals(-1, testCollection.indexOf(null));
	}
	
	@Test
	public void testRemoveInt() {
		testCollection.remove(0);
		assertEquals(15, testCollection.size());
		assertEquals("a1", testCollection.get(0));
		assertEquals("a15", testCollection.get(14));
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void testRemoveIntIndex() {
		testCollection.remove(-1);
	}

	@Test
	public void testIsEmpty() {
		testCollection.clear();
		assertTrue(testCollection.isEmpty());
	}

	@Test
	public void testAddAll() {
		LinkedListIndexedCollection test = new LinkedListIndexedCollection();
		test.addAll(testCollection);
		assertEquals(16, test.size());
		for (int i = 0, end = test.size(); i < end; i++) {
			if (!test.get(i).equals(testCollection.get(i))) {
				fail("Collections are not copied.");
			}
		}
	}
	
}
