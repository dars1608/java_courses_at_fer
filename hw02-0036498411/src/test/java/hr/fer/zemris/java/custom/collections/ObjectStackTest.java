package hr.fer.zemris.java.custom.collections;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ObjectStackTest {
	ObjectStack testStack;
	
	@Before
	public void setUp() throws Exception {
		testStack = new ObjectStack();
		for(int i = 0; i<5; i++) {
			testStack.push("a"+i);
		}
	}

	@Test
	public void testIsEmpty() {
		testStack.clear();
		assertTrue(testStack.isEmpty());
	}

	@Test
	public void testSize() {
		assertEquals(5, testStack.size());
	}

	@Test
	public void testPush() {
		testStack.push("x");
		assertEquals("x", testStack.peek());
		assertEquals(6, testStack.size());
	}

	@Test
	public void testPop() {
		assertEquals("a4", testStack.pop());
		assertEquals(4, testStack.size());
	}
	
	@Test(expected=EmptyStackException.class)
	public void testPopException() {
		for(int i=0; i<7; i++) {
			testStack.pop();
		}
	}

	@Test
	public void testPeek() {
		assertEquals("a4", testStack.peek());
	}
	
	@Test(expected=EmptyStackException.class)
	public void testPeekException() {
		for(int i=0; i<5; i++) {
			testStack.pop();
		}
		assertEquals("a4", testStack.peek());
	}

	@Test
	public void testClear() {
		testStack.clear();
		assertEquals(0, testStack.size());
	}

}
