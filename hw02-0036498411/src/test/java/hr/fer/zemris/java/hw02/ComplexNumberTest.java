package hr.fer.zemris.java.hw02;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.Math.PI;
import static hr.fer.zemris.java.hw02.ComplexNumber.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

@SuppressWarnings("unused")
public class ComplexNumberTest {
	private static final double DEFAULT_PRECISION = 1E-10;
	private ComplexNumber c1, c2;

	@Before
	public void setUp() throws Exception {
		c1 = new ComplexNumber(0.5, sqrt(3) / 2);
		c2 = new ComplexNumber(sqrt(3) / 2, 0.5);
	}

	@Test
	public void testComplexNumber() {
		ComplexNumber test = new ComplexNumber(1.22, -5.2);
		assertTrue(abs(1.22 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(-5.2 - test.getImaginaryPart()) < DEFAULT_PRECISION);
	}

	@Test
	public void testFromReal() {
		ComplexNumber test = fromReal(24.1);
		assertTrue(abs(24.1 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getImaginaryPart()) < DEFAULT_PRECISION);
	}

	@Test
	public void testFromImaginary() {
		ComplexNumber test = fromImaginary(-25.15);
		assertTrue(abs(0 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(-25.15 - test.getImaginaryPart()) < DEFAULT_PRECISION);
	}

	@Test
	public void testFromMagnitudeAndAngle() {
		ComplexNumber test = fromMagnitudeAndAngle(1, PI / 3);
		assertTrue(abs(0.5 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(sqrt(3) / 2 - test.getImaginaryPart()) < DEFAULT_PRECISION);
	}

	@Test
	public void testParse() {
		ComplexNumber test = parse("3.51");
		assertTrue(abs(3.51 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getImaginaryPart()) < DEFAULT_PRECISION);
		test = parse("-3.17");
		assertTrue(abs(-3.17 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getImaginaryPart()) < DEFAULT_PRECISION);
		test = parse("-2.71i");
		assertTrue(abs(0 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(-2.71 - test.getImaginaryPart()) < DEFAULT_PRECISION);
		test = parse("i");
		assertTrue(abs(0 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(1 - test.getImaginaryPart()) < DEFAULT_PRECISION);
		test = parse("1");
		assertTrue(abs(1 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getImaginaryPart()) < DEFAULT_PRECISION);
		test = parse("  -3.21 +  3i");
		assertTrue(abs(-3.21 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(3 - test.getImaginaryPart()) < DEFAULT_PRECISION);

	}

	@Test
	public void testGetRealPart() {
		assertTrue(abs(0.5 - c1.getRealPart()) < DEFAULT_PRECISION);
	}

	@Test
	public void testGetImaginaryPart() {
		assertTrue(abs(0.5 - c2.getImaginaryPart()) < DEFAULT_PRECISION);
	}

	@Test
	public void testGetMagnitude() {
		assertTrue(abs(1 - c1.getMagnitude()) < DEFAULT_PRECISION);
	}

	@Test
	public void testGetAngle() {
		assertTrue(abs(PI / 3 - c1.getAngle()) < DEFAULT_PRECISION);
	}

	@Test
	public void testAdd() {
		ComplexNumber test = c1.add(c2);
		assertTrue(abs((sqrt(3) + 1) / 2 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs((sqrt(3) + 1) / 2 - test.getImaginaryPart()) < DEFAULT_PRECISION);

	}

	@Test(expected = NullPointerException.class)
	public void testAddException() {
		ComplexNumber test = c1.add(null);

	}

	@Test
	public void testSub() {
		ComplexNumber test = c1.sub(c2);
		assertTrue(abs((-sqrt(3) + 1) / 2 - test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs((sqrt(3) - 1) / 2 - test.getImaginaryPart()) < DEFAULT_PRECISION);

	}

	@Test(expected = NullPointerException.class)
	public void testSubException() {
		ComplexNumber test = c1.sub(null);
	}

	@Test
	public void testMul() {
		ComplexNumber test = c1.mul(c2);
		assertTrue(abs(test.getRealPart()) < DEFAULT_PRECISION);
		assertTrue(abs(1 - test.getImaginaryPart()) < DEFAULT_PRECISION);
	}

	@Test(expected = NullPointerException.class)
	public void testMulException() {
		ComplexNumber test = c1.mul(null);
	}

	@Test
	public void testDiv() {
		ComplexNumber test = c1.div(c2);
		assertTrue(abs(1 - test.getMagnitude()) < DEFAULT_PRECISION);
		assertTrue(abs(PI / 6 - test.getAngle()) < DEFAULT_PRECISION);
	}

	@Test(expected = NullPointerException.class)
	public void testDivNullException() {
		ComplexNumber test = c1.div(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDivArgumentException() {
		ComplexNumber test = c1.div(new ComplexNumber(0, 0));
	}

	@Test
	public void testPower() {
		ComplexNumber test = fromMagnitudeAndAngle(2, 0.5).power(3);
		assertTrue(abs(8 - test.getMagnitude()) < DEFAULT_PRECISION);
		assertTrue(abs(1.5 - test.getAngle()) < DEFAULT_PRECISION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPowerArgumentException() {
		ComplexNumber test = c1.power(-1);
	}

	@Test
	public void testRoot() {
		ComplexNumber[] testRoots = fromMagnitudeAndAngle(16, PI / 4).root(4);
		ComplexNumber[] expectedRoots = new ComplexNumber[4];

		for (int k = 0; k < 4; k++) {
			expectedRoots[k] = fromMagnitudeAndAngle(2, (PI / 4 + 2 * k * PI) / 4);
		}

		assertArrayEquals(expectedRoots, testRoots);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRootArgumentException() {
		ComplexNumber[] test = c1.root(-1);
	}
}
