/**
 * This method is used for creating buttons with the tags.
 */
function getAllTags(){
	$.ajax({
		  url: 'rest/pic',
    	  dataType: 'json',
    	  success: function(data) {
    			var tags = data;

    			var newItem = document.createElement('div');
    			var newParent = document.getElementById('tags');
    			newParent.innerHTML = '';
    			newParent.appendChild(newItem);
    			
    			if(tags.length==0) {
    				var newText = document.createTextNode('No result...');
    				newItem.appendChild(newText);
    			} else {
    				for(var i=0; i<tags.length; i++) {
    					var newButton = document.createElement('button');
    					var escapedSequence = htmlEscape(tags[i]);
    					newButton.setAttribute('onclick', 'getByTag("' + escapedSequence + '")');
    					newButton.innerHTML = escapedSequence;
    					
    					newItem.appendChild(newButton);
    				}
    			}
    	  }
      }
    );
}

/**
 * This method is used for showing the thumbnails of pictures with certain tag.
 * 
 * @param tag tag
 */
function getByTag(tag) {
	$.ajax({
		url: 'rest/pic/tag',
		data: {
			'tag' : tag
		},
  	 	dataType: 'json',
  	  	success: function(data) {
  	  		var names = data;
  	  		
  	  		var newElement = document.createElement('div');
  	  		var newParent = document.getElementById('thumbs');
  	  		newParent.innerHTML = '';
  	  		newParent.appendChild(newElement);
  	  		
  	  		var newImgDiv = document.createElement('div');
  	  		for(var i = 0; i<names.length; i++){
  	  			var escapedSequence = htmlEscape(names[i]);
  	  			
  	  			var newImg = document.createElement('img');
  	  			newImg.setAttribute('alt', escapedSequence);
  	  			newImg.setAttribute('src', 'rest/pic/thumb?name=' + escapedSequence);
  	  			newImg.setAttribute('onclick', 'getInfo("' + escapedSequence + '");');
  	  			
  	  			newImgDiv.appendChild(newImg);
  	  			
  	  			if((i+1)%5 == 0 || (i+1) == names.length){
	  				newElement.appendChild(newImgDiv);
	  				newImgDiv = document.createElement('div');
	  			}
  	  		}
  	  	}
	});
}

/**
 * This method is used for getting the info of selected picture.
 * 
 * @param name picture name
 */
function getInfo(name) {
	$.ajax({
		url: 'rest/pic/info',
		data: {
			'name' : name
		},
  	 	dataType: 'json',
  	  	success: function(data) {
  	  		var pic = data;
  	  		
  	  		var parent = document.getElementById('info');
  	  		parent.innerHTML = '';
  	  		
  	  		var imgDiv = document.createElement('div');
  	  		var imgElem = document.createElement('img');
  	  		imgDiv.appendChild(imgElem);
  	  		parent.appendChild(imgDiv);
  	  		
  	  		imgElem.setAttribute('alt', htmlEscape(pic.name));
  	  		imgElem.setAttribute('src', 'rest/pic/full?name=' + htmlEscape(pic.name));
  	  		
  	  		var descDiv = document.createElement('div');
  	  		var descElem = document.createElement('span');
  	  		descDiv.appendChild(descElem);
  	  		parent.appendChild(descDiv);
  	  		
  	  		descElem.innerHTML = htmlEscape(pic.description);
  	  		
  	  		var tagsDiv = document.createElement('div');
  	  		parent.appendChild(tagsDiv);
  	  		for(var i = 0; i<pic.tags.length; i++){
  	  			var tagElem = document.createElement('span');
  	  			tagElem.innerHTML = '#' + htmlEscape(pic.tags[i]) + ' ';
  	  			
  	  			tagsDiv.appendChild(tagElem);
  	  		}
  	  	}
	});
}