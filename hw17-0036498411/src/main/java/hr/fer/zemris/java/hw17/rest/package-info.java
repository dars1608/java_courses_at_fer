/**
 * This package contains classes for the rest application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw17.rest;