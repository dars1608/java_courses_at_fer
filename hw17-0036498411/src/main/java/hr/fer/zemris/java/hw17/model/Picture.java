package hr.fer.zemris.java.hw17.model;

import java.nio.file.Path;
import java.util.Objects;

/**
 * This class models an entity which represents a picture description. Every
 * picture has its:
 * <ul>
 * <li>name
 * <li>filePath
 * <li>description
 * <li>tags
 * <li>thumbnailPath
 * </ul>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Picture {

	/** File name */
	private String name;
	/** File path */
	private Path filePath;
	/** Description */
	private String description;
	/** Array of tags */
	private String[] tags;
	/** Thumbnail path */
	private Path thumbnailPath;

	/**
	 * Default constructor method for class {@link Picture}.
	 */
	public Picture() {
	}

	/**
	 * Constructor method for class {@link Picture}.
	 * 
	 * @param name
	 *            file name
	 * @param filePath
	 *            file path
	 * @param description
	 *            description
	 * @param tags
	 *            array of tags
	 * @param thumbnailPath
	 *            thumbnail path
	 * @throws NullPointerException
	 *             if any of the arguments is {@code null}
	 */
	public Picture(String name, Path filePath, String description, String[] tags, Path thumbnailPath) {
		this.name = Objects.requireNonNull(name);
		this.filePath = Objects.requireNonNull(filePath);
		this.description = Objects.requireNonNull(description);
		this.tags = Objects.requireNonNull(tags);
		this.thumbnailPath = Objects.requireNonNull(thumbnailPath);
	}

	/**
	 * Getter method for the {@code name}.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter method for the {@code name}.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter method for the {@code filePath}.
	 * 
	 * @return the filePath
	 */
	public Path getFilePath() {
		return filePath;
	}

	/**
	 * Setter method for the {@code filePath}.
	 * 
	 * @param filePath
	 *            the filePath to set
	 */
	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}

	/**
	 * Getter method for the {@code description}.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter method for the {@code description}.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter method for the {@code tags}.
	 * 
	 * @return the tags
	 */
	public String[] getTags() {
		return tags;
	}

	/**
	 * Setter method for the {@code tags}.
	 * 
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(String[] tags) {
		this.tags = tags;
	}

	/**
	 * Getter method for the {@code thumbnailPath}.
	 * 
	 * @return the thumbnailPath
	 */
	public Path getThumbnailPath() {
		return thumbnailPath;
	}

	/**
	 * Setter method for the {@code thumbnailPath}.
	 * 
	 * @param thumbnailPath
	 *            the thumbnailPath to set
	 */
	public void setThumbnailPath(Path thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}

}
