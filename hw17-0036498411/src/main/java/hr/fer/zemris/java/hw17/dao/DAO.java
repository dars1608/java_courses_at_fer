package hr.fer.zemris.java.hw17.dao;

import java.util.List;
import java.util.Set;

import hr.fer.zemris.java.hw17.model.Picture;

/**
 * This interface defines the communication between the application and the data
 * persistence layer. See
 * <a href="https://en.wikipedia.org/wiki/Data_access_object">this</a> for more
 * informations.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface DAO {

	/**
	 * This method is used for adding {@link Picture} object into the database.
	 * 
	 * @param name
	 *            file name
	 * @param description
	 *            description
	 * @param tags
	 *            tags
	 */
	void addPicture(String name, String description, String[] tags);

	/**
	 * This method is used for getting the unique tags of all pictures in the
	 * database.
	 * 
	 * @return tags
	 */
	Set<String> getTags();

	/**
	 * This method is used for getting the pictures with certain tag.
	 * 
	 * @param tag
	 *            tag
	 * @return list of {@link Picture} objects which have certain tag
	 */
	List<Picture> getPicturesByTag(String tag);

	/**
	 * This method is used for getting the picture with certain name.
	 * 
	 * @param name
	 *            file name
	 * @return {@link Picture} object, {@code null} if there's no such object
	 */
	Picture getByName(String name);
}
