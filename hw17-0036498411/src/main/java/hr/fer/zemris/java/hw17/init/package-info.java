/**
 * This package contains initialization of the application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw17.init;