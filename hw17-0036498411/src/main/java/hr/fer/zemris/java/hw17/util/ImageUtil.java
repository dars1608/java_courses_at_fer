package hr.fer.zemris.java.hw17.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.imageio.ImageIO;

/**
 * This is utility class used for handling the loading of the images and
 * creating their respective thumbnail pictures of size
 * {@value #THUMB_WITDH}x{@value #THUMB_HEIGHT}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class ImageUtil {

	/** Thumbnail width */
	private static final int THUMB_WITDH = 150;
	/** Thumbnail height */
	private static final int THUMB_HEIGHT = 150;
	/** Thumbnail type */
	private static final String THUMB_TYPE = "png";

	/**
	 * This method creates a thumbnail of the file with given path.
	 * 
	 * @param thumbnailPath
	 *            path where the thumbnail will be created
	 * @param filePath
	 *            original file path
	 * @return {@code true} if creation succeeds or the thumbnail already exists,
	 *         {@code false} otherwise
	 */
	public static boolean createThumbnail(Path thumbnailPath, Path filePath) {
		if (Files.exists(thumbnailPath)) {
			return true;
		}

		try {
			BufferedImage pic = ImageIO.read(filePath.toFile());
			Image thumbnail = pic.getScaledInstance(
					THUMB_WITDH, 
					THUMB_HEIGHT,
					Image.SCALE_AREA_AVERAGING
			);

			BufferedImage bufferedThumbnail = new BufferedImage(
					thumbnail.getWidth(null),
					thumbnail.getHeight(null),
					BufferedImage.TYPE_INT_RGB
			);
			bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);

			File file = thumbnailPath.toFile();
			file.createNewFile();

			ImageIO.write(bufferedThumbnail, THUMB_TYPE, file);
		} catch (IOException e) {
			return false;
		}

		return true;
	}

	/**
	 * This method is used for reading the image from the file into the byte array.
	 * 
	 * @param filePath
	 *            file path
	 * @return byte array representing encoded image file
	 * @throws IOException
	 *             if any error while reading occurs
	 */
	public static byte[] readFromFile(Path filePath) throws IOException {
		BufferedImage image = ImageIO.read(filePath.toFile());
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "png", baos);

		return baos.toByteArray();
	}
}
