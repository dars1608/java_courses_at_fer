package hr.fer.zemris.java.hw17.init;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import hr.fer.zemris.java.hw17.dao.DAO;
import hr.fer.zemris.java.hw17.dao.DAOProvider;
import hr.fer.zemris.java.hw17.dao.inmemory.PictureDB;

/**
 * This class is {@link ServletContextListener} used for initializing the simple
 * in-memory database which holds informations about the pictures which will be
 * shown in gallery.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebListener
public class Initialization implements ServletContextListener {

	/** File name of the file which holds descriptions of pictures */
	private static final String PIC_DESCRIPTION = "opisnik.txt";

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		try {
			URL thumbnailsDir = sce.getServletContext().getResource("/WEB-INF/thumbnails");
			
			clearThumbnails(Paths.get(thumbnailsDir.toURI()));
		}catch (IOException | URISyntaxException e) {
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {
			URL description = sce.getServletContext().getResource("/WEB-INF/" + PIC_DESCRIPTION);
			URL picturesDir = sce.getServletContext().getResource("/WEB-INF/pictures");
			URL thumbnailsDir = sce.getServletContext().getResource("/WEB-INF/thumbnails");

			initDB(Paths.get(description.toURI()), Paths.get(picturesDir.toURI()),
					thumbnailsDir == null ? null : Paths.get(thumbnailsDir.toURI()));

		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * This method is used internally for initializing the database based on info
	 * which is hold in file {@value #PIC_DESCRIPTION}.
	 * 
	 * @param descr
	 *            path of the description file
	 * @param picDir
	 *            pictures directory
	 * @param thumbDir
	 *            thumbnails directory
	 * @throws IOException
	 *             if reading fails
	 */
	private void initDB(Path descr, Path picDir, Path thumbDir) throws IOException {
		if (thumbDir == null) {
			thumbDir = picDir.getParent().resolve("thumbnails");
			Files.createDirectory(thumbDir);
		}

		PictureDB.setPicturesPath(picDir);
		PictureDB.setThumbnailsPath(thumbDir);
		
		DAO dao = DAOProvider.getDAO();
		BufferedReader br = Files.newBufferedReader(descr);
		while (br.ready()) {
			String name = br.readLine();

			if (!br.ready()) {
				throw new IOException("Wrong description format!");
			}
			String description = br.readLine();

			if (!br.ready()) {
				throw new IOException("Wrong description format!");
			}
			String[] tags = prepareTags(br.readLine());
			dao.addPicture(name, description, tags);
		}

	}

	/**
	 * This method is used internally for preparing the tags of the picture from a
	 * line of tags delimited by a comma.
	 * 
	 * @param line
	 *            line of tags
	 * @return string array of tags
	 */
	private String[] prepareTags(String line) {
		String[] tags = line.split(",");

		for (int i = 0; i < tags.length; i++) {
			tags[i] = tags[i].trim();
		}

		return tags;
	}
	
	/**
	 * This method is used internally for clearing the temporary thumbnails.
	 * 
	 * @param thumbnailsDir path of thumbnail directory
	 */
	private void clearThumbnails(Path thumbnailsDir) {
		File[] files = thumbnailsDir.toFile().listFiles();
		for(File f: files) {
			while(!f.delete());
			System.err.println(true);
		}
		
		thumbnailsDir.toFile().deleteOnExit();
	}
}
