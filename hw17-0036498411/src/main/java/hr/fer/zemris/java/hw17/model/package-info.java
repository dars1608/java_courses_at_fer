/**
 * This package contains entities used in gallery app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw17.model;