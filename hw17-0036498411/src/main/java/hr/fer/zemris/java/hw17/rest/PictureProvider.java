package hr.fer.zemris.java.hw17.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import hr.fer.zemris.java.hw17.dao.DAOProvider;
import hr.fer.zemris.java.hw17.model.Picture;
import hr.fer.zemris.java.hw17.util.ImageUtil;


/**
 * This class represents a core of the web application which uses Jersey Rest API.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@Path("/pic")
public class PictureProvider {
	
	/** Servlet context*/
	@Context
    private ServletContext context;
	
	/**
	 * This method is used for getting the tags and passing it on the view.
	 * 
	 * @return {@link Response} object
	 */
	@GET
	@Produces("application/json")
	public Response getTags() {
		Set<String> tags = DAOProvider.getDAO().getTags();
		JSONArray result = new JSONArray(tags);
		return Response.status(Status.OK).entity(result.toString()).build();
	}
	
	/**
	 * This method is used for getting the filenames of the pictures with certain tag.
	 * 
	 * @param tag tag
	 * @return {@link Response} object
	 */
	@GET
	@Produces("application/json")
	@Path("/tag")
	public Response getByTag(@QueryParam("tag") String tag) {
		if(tag == null || tag.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		List<Picture> pictures = DAOProvider.getDAO().getPicturesByTag(tag);
		List<String> fileNames = new ArrayList<>();
		
		pictures.forEach(p -> {
			if(!ImageUtil.createThumbnail(p.getThumbnailPath(), p.getFilePath())) {
				return;
			}
			
			fileNames.add(p.getName());
		});
		
		JSONArray result = new JSONArray(fileNames);
		return Response.status(Status.OK).entity(result.toString()).build();
	}
	
	/**
	 * This method is used for producing/getting the thumbnail of the picture with given filename.
	 * 
	 * @param name name of the original picture
	 * @return {@link Response} object
	 */
	@GET
	@Produces("image/png")
	@Path("/thumb")
	public Response getThumb(@QueryParam("name") String name) {
		if(name == null || name.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		java.nio.file.Path filePath = DAOProvider.getDAO().getByName(name).getThumbnailPath();
		try {
			BufferedImage image = ImageIO.read(filePath.toFile());
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    ImageIO.write(image, "png", baos);
		    
		    byte[] imageData = baos.toByteArray();
		    return Response.ok(new ByteArrayInputStream(imageData)).build();
		} catch(IOException ex) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	/**
	 * This method is used for getting the info about the picture with certain filename.
	 * 
	 * @param name file name
	 * @return {@link Response} object
	 */
	@GET
	@Produces("application/json")
	@Path("/info")
	public Response getInfo(@QueryParam("name") String name) {
		if(name == null || name.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		Picture pic = DAOProvider.getDAO().getByName(name);
		if(pic == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		JSONObject result = new JSONObject(pic);
		return Response.status(Status.OK).entity(result.toString()).build();		
	}
	
	/**
	 * This method is used for getting the full sized picture with given filename.
	 * 
	 * @param name file name
	 * @return {@link Response} object
	 */
	@GET
	@Produces("image/png")
	@Path("/full")
	public Response getFullPicture(@QueryParam("name") String name) {
		if(name == null || name.isEmpty()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		java.nio.file.Path filePath = DAOProvider.getDAO().getByName(name).getFilePath();
		try {
		    return Response.ok(new ByteArrayInputStream(ImageUtil.readFromFile(filePath))).build();
		} catch(IOException ex) {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
}
