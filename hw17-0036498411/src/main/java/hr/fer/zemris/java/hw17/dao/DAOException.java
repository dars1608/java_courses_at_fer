package hr.fer.zemris.java.hw17.dao;

/**
 * This class describes a {@link RuntimeException} which is thrown if any error
 * while working with DAO objects occurs.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DAOException extends RuntimeException {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor method for class {@link DAOException}.
	 */
	public DAOException() {
	}

	/**
	 * Constructor method for class {@link DAOException}.
	 * 
	 * @param message
	 *            detail message
	 * @param cause
	 *            cause
	 * @param enableSuppression
	 *            enable suppression
	 * @param writableStackTrace
	 *            enable stack trace
	 */
	public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Constructor method for {@link DAOException}.
	 * 
	 * @param message
	 *            detail message.
	 * @param cause
	 *            cause.
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor method for {@link DAOException}.
	 * 
	 * @param message
	 *            detail message.
	 */
	public DAOException(String message) {
		super(message);
	}

	/**
	 * Constructor method for {@link DAOException}.
	 * 
	 * @param cause
	 *            cause.
	 */
	public DAOException(Throwable cause) {
		super(cause);
	}
}