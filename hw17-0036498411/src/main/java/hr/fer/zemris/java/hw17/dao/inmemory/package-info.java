/**
 * This package contains a simple an in-memory implementation of DAO interface.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw17.dao.inmemory;