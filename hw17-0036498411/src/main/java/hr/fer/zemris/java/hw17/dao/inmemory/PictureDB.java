package hr.fer.zemris.java.hw17.dao.inmemory;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import hr.fer.zemris.java.hw17.dao.DAO;
import hr.fer.zemris.java.hw17.model.Picture;

/**
 * This class models a simple in-memory database which hods the {@link Picture}
 * objects used for describing entries of the gallery.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PictureDB implements DAO {
	
	/** Path of pictures directory*/
	private static Path picturesPath;
	/** Path of thumbnails directory*/
	private static Path thumbnailsPath;

	/** Map of (tag, pictures with that tag) tuples*/
	private Map<String, List<Picture>> byTag = new HashMap<>();
	/** List of pictures*/
	private List<Picture> pictureInfo = new ArrayList<>();

	/**
	 * Getter method for the {@code picturesPath}.
	 * 
	 * @return the picturesPath
	 */
	public static Path getPicturesPath() {
		return picturesPath;
	}

	/**
	 * Setter method for the {@code picturesPath}.
	 * 
	 * @param picturesPath
	 *            the picturesPath to set
	 */
	public static void setPicturesPath(Path picturesPath) {
		PictureDB.picturesPath = picturesPath;
	}

	/**
	 * Getter method for the {@code thumbnailsPath}.
	 * 
	 * @return the thumbnailsPath
	 */
	public static Path getThumbnailsPath() {
		return thumbnailsPath;
	}

	/**
	 * Setter method for the {@code thumbnailsPath}.
	 * 
	 * @param thumbnailsPath
	 *            the thumbnailsPath to set
	 */
	public static void setThumbnailsPath(Path thumbnailsPath) {
		PictureDB.thumbnailsPath = thumbnailsPath;
	}

	@Override
	public void addPicture(String name, String description, String[] tags) {
		if (picturesPath == null || thumbnailsPath == null) {
			throw new IllegalStateException("DB is not correctly initialized. Pictures directory: " + picturesPath
					+ " Thumbnails directory: " + thumbnailsPath);
		}

		Picture pic = new Picture();
		pic.setName(Objects.requireNonNull(name));
		pic.setFilePath(picturesPath.resolve(name));
		pic.setThumbnailPath(thumbnailsPath.resolve(name));
		pic.setDescription(Objects.requireNonNull(description));
		pic.setTags(Objects.requireNonNull(tags));

		for (String tag : tags) {
			if (!byTag.containsKey(tag)) {
				byTag.put(tag, new ArrayList<>());
			}

			byTag.get(tag).add(pic);
			pictureInfo.add(pic);
		}
	}

	@Override
	public Set<String> getTags() {
		return byTag.keySet();
	}
    
	@Override
	public List<Picture> getPicturesByTag(String tag) {
		return byTag.get(tag);
	}

	@Override
	public Picture getByName(String name) {
		return pictureInfo.stream().filter(p -> p.getName().equals(name)).findFirst().orElse(null);
	}
}
