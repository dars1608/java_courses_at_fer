package hr.fer.zemris.math;

import static java.lang.Math.PI;
import static java.lang.Math.atan;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.abs;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents an unmodifiable complex number. It supports basic
 * arithmetic operations like module, multiplication, division, addition,
 * subtraction, negation, power and n-th root of complex number(s).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Complex implements Comparable<Complex> {

	/** Complex representation of 0 */
	public static final Complex ZERO = new Complex(0, 0);
	/** Complex representation of 1 */
	public static final Complex ONE = new Complex(1, 0);
	/** Complex representation of -1 */
	public static final Complex ONE_NEG = new Complex(-1, 0);
	/** Complex representation of imaginary unit */
	public static final Complex IM = new Complex(0, 1);
	/** Complex representation of negative imaginary unit */
	public static final Complex IM_NEG = new Complex(0, -1);

	/** Precision when working with double */
	private static final double DOUBLE_PRECISION = 1E-10;

	/** {@link Pattern} used for parsing complex number of format <i>a+bi</i> */
	private static Pattern patternReImag = Pattern
			.compile("([-]?[0-9[.]]+\\.?[0-9[.]]?)\\s*([-|+])\\s*([0-9[.]]+\\.?[0-9[.]]?)[i$]+");
	/** {@link Pattern} used for parsing real number */
	private static Pattern patternRe = Pattern.compile("([-|+]?)\\s*([0-9[.]]+\\.?[0-9[.]]?)$");
	/** {@link Pattern} used for parsing complex number of format <i>bi</i> */
	private static Pattern patternImag = Pattern.compile("([-|+]?)\\s*([0-9[.]]+\\.?[0-9[.]]?)[i$]");

	/** Real part */
	private final double re;
	/** Imaginary part */
	private final double im;

	/**
	 * Default constructor method for class {@link Complex}.
	 */
	public Complex() {
		this(0, 0);
	}

	/**
	 * Constructor method for class {@link Complex}.
	 * 
	 * @param re
	 *            real part
	 * @param im
	 *            imaginary part
	 */
	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}

	/**
	 * Getter method for the real part.
	 * 
	 * @return the real part
	 */
	public double getRe() {
		return re;
	}

	/**
	 * Getter method for the imaginary part.
	 * 
	 * @return the imaginary part
	 */
	public double getIm() {
		return im;
	}

	/**
	 * This method is used for calculation of module of the complex number. (Module
	 * of complex number is its distance form the origin of complex coordinate
	 * system)
	 * 
	 * @return the module
	 */
	public double module() {
		return sqrt(re * re + im * im);
	}

	/**
	 * This method adds this complex number with other.
	 * 
	 * @param c
	 *            Other complex number
	 * @return the result of addition
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public Complex add(Complex c) {
		Objects.requireNonNull(c, "Argument can't be null.");

		return new Complex(this.re + c.re, this.im + c.im);
	}

	/**
	 * This method subtracts this complex number with other.
	 * 
	 * @param c
	 *            other complex number
	 * @return the result of subtraction
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public Complex sub(Complex c) {
		Objects.requireNonNull(c, "Argument can't be null.");

		return new Complex(this.re - c.re, this.im - c.im);
	}

	/**
	 * This method multiplies this complex number with other.
	 * 
	 * @param c
	 *            other complex number
	 * @return the result of multiplication
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public Complex multiply(Complex c) {
		Objects.requireNonNull(c, "Argument can't be null.");

		double resultRe = re * c.re - im * c.im;
		double resultIm = re * c.im + im * c.re;

		return new Complex(resultRe, resultIm);
	}

	/**
	 * This method divides this complex number with other.
	 * 
	 * @param c
	 *            other complex number
	 * @return the result of division
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 * @throws IllegalArgumentException
	 *             if arguments module is 0
	 */
	public Complex divide(Complex c) {
		Objects.requireNonNull(c, "Argument can't be null.");
		if (c.module() < DOUBLE_PRECISION) {
			throw new IllegalArgumentException("Arguments magnitude can't be 0. (division by zero)");
		}

		return fromMagnitudeAndAngle((module() / c.module()), (angle() - c.angle()));
	}

	/**
	 * This method returns the value of this complex number raised to the power of
	 * argument n.
	 * 
	 * @param n
	 *            exponent
	 * @return this number raised to the power of the second argument
	 * @throws IllegalArgumentException
	 *             if provided argument is negative
	 */
	public Complex power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Argument must be non negative number. Is " + n);
		}
		return fromMagnitudeAndAngle(pow(module(), n), angle() * n);
	}

	/**
	 * This method calculates n-roots of this complex number.
	 * 
	 * @param n
	 *            degree of the root
	 * @return n-roots of this complex number
	 */
	public List<Complex> root(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("Argument must be positive number. Is " + n);
		}

		List<Complex> roots = new ArrayList<>();
		double newMagnitude = pow(module(), 1.0 / n);
		double angle = angle();

		for (int k = 0; k < n; k++) {
			roots.add(fromMagnitudeAndAngle(newMagnitude, (angle + 2 * k * PI) / n));
		}

		return roots;
	}

	/**
	 * This method is used for parsing complex number form a string. Expected format
	 * of complex number is <i>a+bi</i> or <i>a-bi</i> where a and b are real
	 * constants (b is positive), and i is imaginary unit. Also, a and b aren't
	 * necessary (if not needed).
	 * 
	 * @param s
	 *            String which is parsed.
	 * @return Parsed complex number.
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>.
	 * @throws IllegalArgumentException
	 *             if argument isn't of given format
	 */
	public static Complex parse(String s) {
		if (s == null) {
			throw new NullPointerException("Argument can't be null.");
		}

		double real = 0;
		double imaginary = 0;
		s = s.trim();

		if (s.equals("-i")) {
			return new Complex(0, -1);
		} else if (s.equals("i")) {
			return new Complex(0, 1);
		} else if (s.replace(" ", "").contains("+i") || s.replace(" ", "").contains("-i")) {
			s = s.replaceAll("i", "1.0i");
		}

		Matcher matcherReImag = patternReImag.matcher(s);
		Matcher matcherRe = patternRe.matcher(s);
		Matcher matcherImag = patternImag.matcher(s);

		if (matcherReImag.matches()) {
			real = Double.parseDouble(matcherReImag.group(1));
			imaginary = Double.parseDouble(matcherReImag.group(2) + matcherReImag.group(3));
		} else if (matcherRe.matches()) {
			real = Double.parseDouble(matcherRe.group(1) + matcherRe.group(2));
		} else if (matcherImag.matches()) {
			imaginary = Double.parseDouble(matcherImag.group(1) + matcherImag.group(2));
		} else {
			throw new IllegalArgumentException("String " + s + " cannot be parsed to complex number.");
		}

		return new Complex(real, imaginary);
	}

	@Override
	public String toString() {
		if (abs(re) < DOUBLE_PRECISION && abs(im) < DOUBLE_PRECISION) {
			return "0";
		}
		return String.format("%s%s%s", abs(re) < DOUBLE_PRECISION ? "" : Double.toString(re),
				im > 0 && abs(re) > DOUBLE_PRECISION ? "+" : "", abs(im) < DOUBLE_PRECISION ? "" : im + "i");
	}

	@Override
	public int hashCode() {
		return Objects.hash(re, im);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Complex other = (Complex) obj;
		if (abs(re - other.re) > DOUBLE_PRECISION) {
			return false;
		}
		if (abs(im - other.im) > DOUBLE_PRECISION) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <b>NOTE</b>: two {@link Complex} objects are compared by their modules.
	 */
	@Override
	public int compareTo(Complex o) {
		if (o == null) {
			return -1;
		}

		double moduleThis = module();
		double moduleOther = o.module();

		if (abs(moduleThis - moduleOther) < DOUBLE_PRECISION) {
			return 0;
		}
		if (moduleThis > moduleOther) {
			return 1;
		} else {
			return -1;
		}
	}

	/**
	 * This method is used internally for conversion from trigonometric notation to
	 * of complex number to classic notation.
	 * 
	 * @param magnitude
	 *            magnitude of given complex number
	 * @param angle
	 *            angle of given complex number
	 * @return complex representation of given number
	 */
	private static Complex fromMagnitudeAndAngle(double magnitude, double angle) {
		return new Complex(magnitude * cos(angle), magnitude * sin(angle));
	}

	/**
	 * This method is used internally for calculating the complex angle.
	 * 
	 * @return angle in radians
	 */
	private double angle() {
		if (abs(re) < DOUBLE_PRECISION) {
			if (im > 0) {
				return PI;
			} else {
				return 3 * PI / 2;
			}
		} else if (abs(im) < DOUBLE_PRECISION) {
			if (re > 0) {
				return 0;
			} else {
				return PI;
			}
		}

		double angle = atan(im / re);
		if (re < 0 && im > 0 || re < 0 && im < 0) {
			angle += PI;
		}

		return angle;
	}
}
