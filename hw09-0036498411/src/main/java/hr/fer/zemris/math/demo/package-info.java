/**
 * This package contains demonstration programs for classes in package
 * {@link hr.fer.zemris.math}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.math.demo;