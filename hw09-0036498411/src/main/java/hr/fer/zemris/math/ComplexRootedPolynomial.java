package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class represents which models polynomial with complex coefficients
 * according to the template bellow:
 * <ul>
 * <i> f(z)=(z-z1)*(z-z2)*...*(z-zn)</i>
 * </ul>
 * where z1 to zn are complex roots of polynomial and z is unknown complex
 * number.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ComplexRootedPolynomial {

	/** Roots of the polynomial */
	private Complex[] roots;

	/**
	 * Constructor method for class {@link ComplexRootedPolynomial}.
	 * 
	 * @param roots
	 *            roots of polynomial
	 * @throws NullPointerException1
	 *             if argument is <code>11null</code>
	 * @throws IllegalArgumentException<
	 *             if there's less than one argument
	 */
	public ComplexRootedPolynomial(Complex... roots) {
		Objects.requireNonNull(roots);
		if (roots.length == 0) {
			throw new IllegalArgumentException("Constructor accepts more than 0 argument.");
		}

		this.roots = roots;
	}

	/**
	 * This method computes polynomial value at given complex point z.
	 * 
	 * @param z
	 *            complex point
	 * @return the result
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public Complex apply(Complex z) {
		Objects.requireNonNull(z, "Argument can't be null.");

		Complex result = Complex.ONE;

		for (Complex zi : roots) {
			result = result.multiply(z.sub(zi));
		}

		return result;
	}

	/**
	 * This method transforms this object to {@link ComplexPolynomial} retaining the
	 * meaning of given expression.
	 * 
	 * @return polynomial represented by {@link ComplexPolynomial} object
	 */
	public ComplexPolynomial toComplexPolynom() {
		List<Complex> factors = new ArrayList<>();

		factors.add(Complex.ONE);
		factors.add(roots[0].multiply(Complex.ONE_NEG));
		for (int i = 1; i < roots.length; i++) {
			List<Complex> temp = new ArrayList<>();
			temp.add(Complex.ONE);
			for (int j = 1, len = factors.size(); j < len; j++) {
				Complex c = factors.get(j);
				Complex c1 = factors.get(j - 1);
				temp.add(c.sub(c1.multiply(roots[i])));

			}
			temp.add(factors.get(factors.size() - 1).multiply(roots[i].multiply(Complex.ONE_NEG)));
			factors = temp;
		}

		return new ComplexPolynomial((Complex[]) factors.toArray(new Complex[factors.size()]));
	}

	/**
	 * This method finds index of closest root for given complex number z that is
	 * within.
	 * 
	 * @param z
	 *            complex number
	 * @param threshold
	 *            threshold in which the root must be
	 * @return index of closest root, if there is no such root, returns -1.
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public int indexOfClosestRootFor(Complex z, double threshold) {
		Objects.requireNonNull(z, "Argument z can't be null.");
		if (threshold < 0) {
			throw new IllegalArgumentException("Treshold can't be negative number.");
		}

		double minimal = Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < roots.length; i++) {
			double d = z.sub(roots[i]).module();
			if (d < threshold && d < minimal) {
				index = i + 1;
				minimal = d;
			}
		}

		return index;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("f(z)=");

		for (Complex c : roots) {
			c = c.multiply(Complex.ONE_NEG);
			String cS = c.toString();
			sb.append(String.format("(z%s%s)*", cS.startsWith("-") ? "" : "+", cS));
		}
		sb.deleteCharAt(sb.length() - 1);

		return sb.toString();
	}

}
