package hr.fer.zemris.math;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

import java.util.Objects;

/**
 * This class represents unmodifiable vector in three dimensional space. It
 * supports basic arithmetic with vectors like norm, addition, subtraction, dot
 * product, cross product and angle between two vectors.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Vector3 implements Comparable<Vector3> {

	/** Precision when working with doubles */
	private static final double DOUBLE_PRECISION = 1E-5;

	/** X-axis component */
	private final double x;
	/** Y-axis component */
	private final double y;
	/** Z-axis component */
	private final double z;

	/**
	 * Constructor method for class {@link Vector3}.
	 * 
	 * @param x
	 *            X-axis component
	 * @param y
	 *            Y-axis component
	 * @param z
	 *            Z-axis component
	 */
	public Vector3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Getter method for the {@code x}.
	 * 
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Getter method for the {@code y}.
	 * 
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * Getter method for the {@code z}.
	 * 
	 * @return the z
	 */
	public double getZ() {
		return z;
	}

	/**
	 * This method is used for calculating norm of vector described by this object.
	 * 
	 * @return norm of vector
	 */
	public double norm() {
		return sqrt(x * x + y * y + z * z);
	}

	/**
	 * This method is used for transformation of vector in normalized vector.
	 * Normalized vector is vector with the same angle as the the one described by
	 * this object, but the norm of the vector is 1.
	 * 
	 * @return normalized vector
	 */
	public Vector3 normalized() {
		double norm = norm();

		return new Vector3(x / norm, y / norm, z / norm);
	}

	/**
	 * This method is used for addition of two vectors.
	 * 
	 * @param other
	 *            other vector
	 * @return result vector
	 * @throws NullPointerException
	 *             if other vector reference is <code>null</code>
	 */
	public Vector3 add(Vector3 other) {
		Objects.requireNonNull(other, "Vector reference can't be null.");

		return new Vector3(x + other.x, y + other.y, z + other.z);
	}

	/**
	 * This method is used for subtraction of two vectors.
	 * 
	 * @param other
	 *            other vector
	 * @return result vector
	 * @throws NullPointerException
	 *             if other vector reference is <code>null</code>
	 */
	public Vector3 sub(Vector3 other) {
		Objects.requireNonNull(other, "Vector reference can't be null.");

		return new Vector3(x - other.x, y - other.y, z - other.z);
	}

	/**
	 * This method is used to calculate dot product of two vectors.
	 * 
	 * @param other
	 *            other vector
	 * @return dot product of two vectors
	 * @throws NullPointerException
	 *             if other vector reference is <code>null</code>
	 */
	public double dot(Vector3 other) {
		Objects.requireNonNull(other, "Vector reference can't be null.");

		return x * other.x + y * other.y + z * other.z;
	}

	/**
	 * This method is used to calculate cross product of two vectors.
	 * 
	 * @param other
	 *            other vector
	 * @return new {@link Vector3} object describing cross product of two vectors
	 * @throws NullPointerException
	 *             if other vector reference is <code>null</code>
	 */
	public Vector3 cross(Vector3 other) {
		Objects.requireNonNull(other, "Vector reference can't be null.");

		double resultX = y * other.z - z * other.y;
		double resultY = z * other.x - x * other.z;
		double resultZ = x * other.y - y * other.x;

		return new Vector3(resultX, resultY, resultZ);
	}

	/**
	 * This method is used for scaling the vector with given factor.
	 * 
	 * @param s
	 *            scaling factor
	 * @return scaled factor
	 */
	public Vector3 scale(double s) {
		return new Vector3(x * s, y * s, z * s);
	}

	/**
	 * This method is used for calculating cosinus of angle between two vectors.
	 * 
	 * @param other
	 *            other vector
	 * @return angle between the vectors
	 * @throws NullPointerException
	 *             if other vector reference is <code>null</code>
	 */
	public double cosAngle(Vector3 other) {
		Objects.requireNonNull(other, "Vector reference can't be null.");

		return dot(other) / (norm() * other.norm());
	}

	/**
	 * This method is used for transforming {@link Vector3} object to array of
	 * doubles.
	 * 
	 * @return double arrays with three elements representing three vector
	 *         components
	 */
	public double[] toArray() {
		return new double[] { x, y, z };
	}

	@Override
	public String toString() {
		return String.format("x=%f, y=%f, z=%f", x, y, z);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(z);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		Vector3 other = (Vector3) obj;
		if (abs(x - other.x) > DOUBLE_PRECISION) {
			return false;
		}
		if (abs(y - other.y) > DOUBLE_PRECISION) {
			return false;
		}
		if (abs(z - other.z) > DOUBLE_PRECISION) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * <p>
	 * <b>NOTE</b>: two {@link Vector3} objects are compared by their norms.
	 * </p>
	 */
	@Override
	public int compareTo(Vector3 o) {
		if (o == null) {
			return -1;
		}

		double normThis = norm();
		double normOther = o.norm();

		if (abs(normThis - normOther) < DOUBLE_PRECISION) {
			return 0;
		}
		if (normThis > normOther) {
			return 1;
		} else {
			return -1;
		}
	}
}
