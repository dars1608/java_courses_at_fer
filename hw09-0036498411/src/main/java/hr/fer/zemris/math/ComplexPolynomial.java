package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.lang.Math.abs;

/**
 * This class represents which models polynomial with complex coefficients
 * according to the template bellow:
 * <ul>
 * <i> f(z)=an * z^n + an-1 * z^(n-1) + .. a1 * z^+ a0</i>
 * </ul>
 * where ai are complex factors and z is some complex number.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ComplexPolynomial {

	/** Precision when working with doubles */
	private static final double DOUBLE_PRECISION = 1E-5;

	/** Factors of the polynomial */
	private Complex[] factors;

	/**
	 * Constructor method for class {@link ComplexPolynomial}.
	 * 
	 * @param factors
	 *            factors of the polynomial
	 * @throws NullPointerException
	 *             if provided argument is <code>null</code>
	 * @throws IllegalArgumentException
	 *             if there's less than one argument
	 */
	public ComplexPolynomial(Complex... factors) {
		Objects.requireNonNull(factors);
		if (factors.length == 0) {
			throw new IllegalArgumentException("Constructor accepts more than 0 argument.");
		}

		this.factors = factors;
	}

	/**
	 * This method checks the order of a polynomial (note: order = highest exponent
	 * in the expression)
	 * 
	 * @return the order of polynomial
	 */
	public short order() {
		return (short) (factors.length - 1);
	}

	/**
	 * This method calculates a new polynomial based on multiplication of this one
	 * and one provided as argument.
	 * 
	 * @param p
	 *            other {@link ComplexPolynomial}
	 * @return result of operation <i>f(z)*p(z)</i>
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		Objects.requireNonNull(p, "Argument can't be null.");

		List<Complex> currentFactors = new ArrayList<>();

		for (int i = 0; i < p.factors.length; i++) {
			Complex otherF = p.factors[i];

			for (int j = 0; j < factors.length; j++) {
				Complex newF = factors[j].multiply(otherF);

				if (j + i < currentFactors.size()) {
					currentFactors.set(j + i, currentFactors.get(j + i).add(newF));
				} else {
					currentFactors.add(newF);
				}
			}
		}

		return new ComplexPolynomial((Complex[]) currentFactors.toArray(new Complex[currentFactors.size()]));
	}

	/**
	 * This method is used for calculating the first derivation of the polynomial.
	 * 
	 * @return the first derivation of the polynomial
	 */
	public ComplexPolynomial derive() {
		int deg = factors.length - 1;
		if (deg == 0) {
			return new ComplexPolynomial(Complex.ZERO);
		}

		Complex[] deriveFactors = new Complex[factors.length - 1];
		for (int i = 0, len = factors.length - 1; i < len; i++) {
			deriveFactors[i] = new Complex(deg, 0).multiply(factors[i]);
			deg--;
		}

		return new ComplexPolynomial(deriveFactors);
	}

	/**
	 * This method computes polynomial value at given complex point z.
	 * 
	 * @param z
	 *            complex point
	 * @return the result
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	public Complex apply(Complex z) {
		Objects.requireNonNull(z, "Argument can't be null.");

		Complex result = Complex.ZERO;
		int n = factors.length - 1;
		for (Complex f : factors) {
			result = result.add(f.multiply(z.power(n)));
			n--;
		}

		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		int deg = factors.length - 1;
		for (Complex f : factors) {
			if (f.module() < DOUBLE_PRECISION) {
				deg--;
				continue;
			} else if (abs(f.getIm()) < DOUBLE_PRECISION || abs(f.getRe()) < DOUBLE_PRECISION) {
				if (f.getIm() < 0 || f.getRe() < 0) {
					sb.deleteCharAt(sb.length() - 1);
				}

				sb.append(String.format("%s%s+", f.toString(), deg > 0 ? "*z^" + deg : ""));
			} else {
				sb.append(String.format("(%s)%s +", f.toString(), deg > 0 ? "*z^" + deg : ""));
			}
			deg--;
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.insert(0, "f(z)=");

		return sb.toString();
	}
}
