/**
 * This package contains classes which model basic mathematical elements like
 * vector in 3D, complex number and polynoms.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.math;