package hr.fer.zemris.java.fractals;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * This class defines arithmetic used for calculating data used for showing
 * fractals derived from Newton-Raphson iteration. To learn more about that,
 * visit <a href = "https://www.chiark.greenend.org.uk/~sgtatham/newton/">this
 * page</a>.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class NRIteration {

	/**
	 * This method is used for preparing data used for showing fractals derived from
	 * Newton-Raphson iteration on {@link FractalViewer}. It allows that given data
	 * array is filled from some beginning index to ending index.
	 * 
	 * @param reMin
	 *            minimal value on real axis
	 * @param reMax
	 *            maximal value on real axis
	 * @param imMin
	 *            minimal value on imaginary axis
	 * @param imMax
	 *            maximal value on imaginary axis
	 * @param width
	 *            screen width
	 * @param height
	 *            screen height
	 * @param yMin
	 *            y-line from which data array is filled
	 * @param yMax
	 *            y-line to which data array is filled
	 * @param maxIter
	 *            maximal number of iterations
	 * @param data
	 *            data array
	 * @param convergenceThreshold
	 *            convergence threshold
	 * @param rootedPolynomial
	 *            polynomial based on which the fractal is calculated
	 * @param rootThreshold
	 *            root threshold
	 */
	public static void calculate(double reMin, double reMax, double imMin, double imMax, int width, int height,
			int yMin, int yMax, int maxIter, short[] data, double convergenceThreshold,
			ComplexRootedPolynomial rootedPolynomial, double rootThreshold) {

		int offset = width * yMin;
		ComplexPolynomial polynomial = rootedPolynomial.toComplexPolynom();
		ComplexPolynomial derived = polynomial.derive();

		for (int y = yMin; y < yMax; y++) {

			for (int x = 0; x < width; x++) {
				double cre = x / (width - 1.0) * (reMax - reMin) + reMin;
				double cim = (height - 1.0 - y) / (height - 1) * (imMax - imMin) + imMin;

				int iter = 0;
				Complex zn = new Complex(cre, cim);
				double module = Double.MAX_VALUE;

				do {
					Complex numerator = polynomial.apply(zn);
					Complex denominator = derived.apply(zn);
					Complex fraction = numerator.divide(denominator);
					Complex zn1 = zn.sub(fraction);
					module = zn1.sub(zn).module();
					zn = zn1;
				} while (module > convergenceThreshold && iter < maxIter);

				short index = (short) rootedPolynomial.indexOfClosestRootFor(zn, rootThreshold);

				if (index == -1) {
					data[offset++] = 0;
				} else {
					data[offset++] = index;
				}
			}
		}
	}
}
