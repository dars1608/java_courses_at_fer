package hr.fer.zemris.java.raytracer;

import static java.lang.Math.pow;

import java.util.List;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;

/**
 * This class encapsulates calculation for the ray tracer. To learn what ray
 * tracing is, visit
 * <a href="https://en.wikipedia.org/wiki/Ray_tracing_(graphics)">this link</a>.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Tracer {

	/**
	 * This method is used internally for coloring the pixel based on closest
	 * intersection of ray of light and shape modeled on the {@linkplain Scene
	 * scene}.
	 * 
	 * @param scene
	 *            {@link Scene} object
	 * @param ray
	 *            {@link Ray} object
	 * @param rgb
	 *            array with length of 3: {@code rgb[0]} = intensity of red,
	 *            {@code rgb[1]} = intensity of green, {@code rgb[2]} = intensity of
	 *            blue.
	 */
	public static void tracer(Scene scene, Ray ray, short[] rgb) {
		GraphicalObject object = getClosestObject(ray, scene.getObjects());

		if (object != null) {
			determineColor(object.findClosestRayIntersection(ray), rgb, scene, ray);
		} else {
			rgb[0] = 0;
			rgb[1] = 0;
			rgb[2] = 0;
		}
	}

	/**
	 * This method is used internally for determining color of pixel based on
	 * position and characteristics of different {@linkplain LightSource light
	 * sources}.
	 * 
	 * @param s
	 *            ray intersection
	 * @param rgb
	 *            array with length of 3: {@code rgb[0]} = intensity of red,
	 *            {@code rgb[1]} = intensity of green, {@code rgb[2]} = intensity of
	 *            blue.
	 * @param scene
	 *            {@link Scene} object
	 * @param rayObserver
	 *            ray from observer to intersection s
	 * 
	 */
	private static void determineColor(RayIntersection s, short[] rgb, Scene scene, Ray rayObserver) {
		double red = 15;
		double green = 15;
		double blue = 15;

		for (LightSource ls : scene.getLights()) {
			Ray ray = Ray.fromPoints(ls.getPoint(), s.getPoint());

			GraphicalObject object = getClosestObject(ray, scene.getObjects());
			RayIntersection s2 = object.findClosestRayIntersection(ray);

			if (s.getPoint().sub(ls.getPoint()).norm() - 1E-4 > s2.getDistance()) {
				continue;
			}

			Point3D l = ray.direction.negate();
			Point3D n = s.getNormal();

			double cos = l.scalarProduct(n);
			if (cos < 0) {
				cos = 0;
			}

			Point3D r = n.scalarMultiply(2 * l.scalarProduct(n)).sub(l).normalize();
			Point3D v = rayObserver.direction.negate();

			double cosn = r.scalarProduct(v);
			if (cosn < 0) {
				cosn = 0;
			} else {
				cosn = pow(cosn, s.getKrn());
			}

			red += (s.getKdr() * cos + s.getKrr() * cosn) * ls.getR();
			green += (s.getKdg() * cos + s.getKrg() * cosn) * ls.getG();
			blue += (s.getKdb() * cos + s.getKrb() * cosn) * ls.getB();

		}

		rgb[0] = (short) Math.round(red);
		rgb[1] = (short) Math.round(green);
		rgb[2] = (short) Math.round(blue);
	}

	/**
	 * This method is used internally for the object which has closest distance
	 * between start of the ray and intersection of the object and the ray.
	 * 
	 * @param ray
	 *            abstraction of a ray of light
	 * @param objects
	 *            list of {@linkplain GraphicalObject graphical objects}
	 * @return closest object
	 */
	private static GraphicalObject getClosestObject(Ray ray, List<GraphicalObject> objects) {
		int index = -1;
		double minDistance = Double.MAX_VALUE;

		int i = 0;
		for (GraphicalObject g : objects) {
			RayIntersection s = g.findClosestRayIntersection(ray);
			if (s == null) {
				i++;
				continue;
			}
			double d = s.getDistance();

			if (d < minDistance) {
				minDistance = d;
				index = i;
			}
			i++;
		}

		if (index != -1) {
			return objects.get(index);
		}
		return null;
	}

}
