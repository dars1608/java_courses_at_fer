package hr.fer.zemris.java.raytracer;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * This program is same as {@link RayCaster} program, but parallelizes the
 * calculation using Fork-Join framework and {@link RecursiveAction}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class RayCasterParallel {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(getIRayTracerProducer(), new Point3D(10, 0, 0), new Point3D(0, 0, 0),
				new Point3D(0, 0, 10), 20, 20);

	}

	/**
	 * This class is an implementation of the {@link RecursiveAction} which models a
	 * job of calculating rgb components which will later be shown on
	 * {@link RayTracerViewer}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private static class RayCasterCalculationJob extends RecursiveAction {

		/** Serial version (auto-generated) */
		private static final long serialVersionUID = -5535895143305813023L;

		/** Maximal number of rows which can be computed directly */
		private static int threshold = 32;

		/** Location of the observer */
		private Point3D eye;
		/** Corner of the screen */
		private Point3D screenCorner;
		/** X-axis */
		private Point3D xAxis;
		/** Y-axis */
		private Point3D yAxis;
		/** Height of the screen */
		private double height;
		/** Width of the screen */
		private double width;
		/** Horizontal parameter */
		private double horizontal;
		/** Vertical parameter */
		private double vertical;
		/** {@link Scene} where image is modeled */
		private Scene scene;
		/** Array containing intensities of color red on the screen */
		private short[] red;
		/** Array containing intensities of color green on the screen */
		private short[] green;
		/** Array containing intensities of blue red on the screen */
		private short[] blue;
		/** Minimal y-coordinate */
		private int yMin;
		/** Maximal y-coordinate */
		private int yMax;

		/**
		 * Constructor method for class {@link RayCasterCalculationJob}.
		 * 
		 * @param eye
		 *            location of the observer
		 * @param screenCorner
		 *            corner of the screen
		 * @param xAxis
		 *            x-axis
		 * @param yAxis
		 *            y-axis
		 * @param height
		 *            screen height
		 * @param width
		 *            screen width
		 * @param horizontal
		 *            horizontal parameter
		 * @param vertical
		 *            vertical parameter
		 * @param scene
		 *            {@link Scene} where image is modeled
		 * @param red
		 *            array containing intensities of color red on the screen
		 * @param green
		 *            array containing intensities of color green on the screen
		 * @param blue
		 *            array containing intensities of color blue on the screen
		 * @param yMin
		 *            minimal y-coordinate on the screen (used for recursive division of
		 *            work)
		 * @param yMax
		 *            maximal y-coordinate on the screen (used for recursive division of
		 *            work)
		 */
		public RayCasterCalculationJob(Point3D eye, Point3D screenCorner, Point3D xAxis, Point3D yAxis, double height,
				double width, double horizontal, double vertical, Scene scene, short[] red, short[] green, short[] blue,
				int yMin, int yMax) {

			this.eye = eye;
			this.screenCorner = screenCorner;
			this.xAxis = xAxis;
			this.yAxis = yAxis;
			this.height = height;
			this.width = width;
			this.horizontal = horizontal;
			this.vertical = vertical;
			this.scene = scene;
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.yMin = yMin;
			this.yMax = yMax;
		}

		@Override
		protected void compute() {
			if (yMax - yMin + 1 <= threshold) {
				computeDirectly();
				return;
			}
			invokeAll(
					new RayCasterCalculationJob(eye, screenCorner, xAxis, yAxis, height, width, horizontal, vertical,
							scene, red, green, blue, yMin, yMin + (yMax - yMin) / 2),
					new RayCasterCalculationJob(eye, screenCorner, xAxis, yAxis, height, width, horizontal, vertical,
							scene, red, green, blue, yMin + (yMax - yMin) / 2 + 1, yMax));

		}

		/**
		 * This method is used internally when the length of job comes to the value of
		 * {@value #threshold}.
		 */
		private void computeDirectly() {
			short[] rgb = new short[3];
			int offset = (int) (yMin * width);
			
			for (int y = yMin; y <= yMax; y++) {
				for (int x = 0; x < width; x++) {
					Point3D screenPoint = screenCorner.add(xAxis.scalarMultiply(horizontal * x / (width - 1)))
							.sub(yAxis.scalarMultiply(vertical * y / (height - 1)));
					
					Ray ray = Ray.fromPoints(eye, screenPoint);
					Tracer.tracer(scene, ray, rgb);
					
					red[offset] = rgb[0] > 255 ? 255 : rgb[0];
					green[offset] = rgb[1] > 255 ? 255 : rgb[1];
					blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
					
					offset++;
				}
			}
		}

	}

	/**
	 * This method is used internally for generating {@link IRayTracerProducer}
	 * object which initiates calculation of the data which will be shown on
	 * {@link RayTracerViewer}.
	 * 
	 * @return {@link IRayTracerProducer} object
	 */
	private static IRayTracerProducer getIRayTracerProducer() {

		return new IRayTracerProducer() {

			@Override
			public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical,
					int width, int height, long requestNo, IRayTracerResultObserver observer) {

				System.out.println("Započinjem izračune...");
				short[] red = new short[width * height];
				short[] green = new short[width * height];
				short[] blue = new short[width * height];

				Point3D zAxis = view.sub(eye).normalize();
				Point3D yAxis = viewUp.normalize().sub(zAxis.scalarMultiply(zAxis.scalarProduct(viewUp.normalize())));
				Point3D xAxis = zAxis.vectorProduct(yAxis).normalize();

				Point3D screenCorner = view.sub(xAxis.scalarMultiply(horizontal / 2))
						.add(yAxis.scalarMultiply(vertical / 2));
				Scene scene = RayTracerViewer.createPredefinedScene();

				ForkJoinPool pool = new ForkJoinPool();
				pool.invoke(new RayCasterCalculationJob(eye, screenCorner, xAxis, yAxis, height, width, horizontal,
						vertical, scene, red, green, blue, 0, height - 1));
				pool.shutdown();

				System.out.println("Izračuni gotovi...");
				observer.acceptResult(red, green, blue, requestNo);
				System.out.println("Dojava gotova...");
			}

		};
	}

}
