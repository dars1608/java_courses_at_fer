/**
 * This package contains classes used for modeling simplification of a
 * ray-tracer for rendering of 3D scenes.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.raytracer.model;