package hr.fer.zemris.java.fractals;

import java.util.Objects;
import java.util.concurrent.Callable;

import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * This class defines a job of calculating and preparing data for showing
 * fractals based on Newton-Raphson iteration, which will be executed on every
 * "worker" thread.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class NRIterationCalculationJob implements Callable<Void> {

	/** Minimal value on real axis */
	private final double reMin;
	/** Maximal value on real axis */
	private final double reMax;
	/** Minimal value on imaginary axis */
	private final double imMin;
	/** Maximal value on imaginary axis */
	private final double imMax;
	/** Screen width */
	private final int width;
	/** Screen height */
	private final int height;
	/** Y-line from which data array is filled */
	private final int yMin;
	/** Y-line to which data array is filled */
	private final int yMax;
	/** Maximal number of iterations */
	private final int maxIter;
	/** Data array */
	private final short[] data;
	/** Convergence threshold */
	private final double convergenceThreshold;
	/** Rooted complex polynomial based on which the fractal is generated */
	private final ComplexRootedPolynomial rootedPolynomial;
	/** Root threshold */
	private final double rootThreshold;

	/**
	 * Constructor method for class {@link NRIterationCalculationJob}.
	 * 
	 * @param reMin
	 *            minimal value on real axis
	 * @param reMax
	 *            maximal value on real axis
	 * @param imMin
	 *            minimal value on imaginary axis
	 * @param imMax
	 *            maximal value on imaginary axis
	 * @param width
	 *            screen width
	 * @param height
	 *            screen height
	 * @param yMin
	 *            y-line from which data array is filled
	 * @param yMax
	 *            y-line to which data array is filled
	 * @param maxIter
	 *            maximal number of iterations
	 * @param data
	 *            data array
	 * @param convergenceThreshold
	 *            convergence threshold
	 * @param rootedPolynomial
	 *            polynomial based on which the fractal is calculated
	 * @param rootThreshold
	 *            root threshold
	 */
	public NRIterationCalculationJob(double reMin, double reMax, double imMin, double imMax, int width, int height,
			int yMin, int yMax, int maxIter, short[] data, double convergenceThreshold,
			ComplexRootedPolynomial rootedPolynomial, double rootThreshold) {

		if (width < 0) {
			throw new IllegalArgumentException("Width must be greater than 0.");
		} else if (height < 0) {
			throw new IllegalArgumentException("Height must be greater than 0.");
		} else if (yMin > yMax) {
			throw new IllegalArgumentException("Ymax must be greater than ymin.");
		} else if (yMin < 0) {
			throw new IllegalArgumentException("Ymin must be greater than 0.");
		} else if (yMax > width * height) {
			throw new IllegalArgumentException("Ymax must be less than width*height.");
		} else if (rootThreshold < 0.0) {
			throw new IllegalArgumentException("Rooted threshold must be greater than 0.");
		} else if (convergenceThreshold < 0.0) {
			throw new IllegalArgumentException("Convergence threshold must be greater than 0.");
		}

		this.rootedPolynomial = Objects.requireNonNull(rootedPolynomial);
		this.data = Objects.requireNonNull(data);

		this.reMin = reMin;
		this.reMax = reMax;
		this.imMin = imMin;
		this.imMax = imMax;

		this.width = width;
		this.height = height;

		this.yMin = yMin;
		this.yMax = yMax;

		this.maxIter = maxIter;
		this.convergenceThreshold = convergenceThreshold;
		this.rootThreshold = rootThreshold;
	}

	@Override
	public Void call() throws Exception {
		NRIteration.calculate(reMin, reMax, imMin, imMax, width, height, yMin, yMax, maxIter, data,
				convergenceThreshold, rootedPolynomial, rootThreshold);
		return null;
	}

}
