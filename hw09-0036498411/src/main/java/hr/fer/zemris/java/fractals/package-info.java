/**
 * This package contains classes used for calculation of Newton-Raphson
 * iteration, and showing it to the
 * {@linkplain hr.fer.zemris.java.fractals.viewer.FractalViewer FractalViewer}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.fractals;