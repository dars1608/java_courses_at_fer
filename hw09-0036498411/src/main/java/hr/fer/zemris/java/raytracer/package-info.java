/**
 * This package contains ray-tracer program used for solving 3rd assignment of
 * 9th homework for JavaCourse@Fer.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.raytracer;