package hr.fer.zemris.java.raytracer.model;

import static java.lang.Math.sqrt;

import java.util.Objects;

/**
 * This class models a sphere in three dimensional space which can exist on
 * {@link Scene}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Sphere extends GraphicalObject {

	/** Vector defining where the center of sphere is located in space */
	private Point3D center;
	/** Sphere radius */
	private double radius;
	/** Object parameter for diffuse component of color red */
	private double kdr;
	/** Object parameter for diffuse component of color green */
	private double kdg;
	/** Object parameter for diffuse component of color blue */
	private double kdb;
	/** Object parameter for reflective component of color red */
	private double krr;
	/** Object parameter for reflective component of color green */
	private double krg;
	/** Object parameter for reflective component of color blue */
	private double krb;
	/** Object parameter for reflective component */
	private double krn;

	/**
	 * Constructor method for class {@link Sphere}.
	 * 
	 * @param center
	 *            Vector defining where the center of sphere is located in space
	 * @param radius
	 *            Sphere radius
	 * @param kdr
	 *            diffuse component of color red
	 * @param kdg
	 *            diffuse component of color green
	 * @param kdb
	 *            diffuse component of color blue
	 * @param krr
	 *            reflective component of color red
	 * @param krg
	 *            reflective component of color green
	 * @param krb
	 *            reflective component of color blue
	 * @param krn
	 *            reflective component
	 * @throws NullPointerException
	 *             if center reference is <code>null</code>
	 */
	public Sphere(
			Point3D center, 
			double radius,
			double kdr,
			double kdg,
			double kdb, 
			double krr, 
			double krg, 
			double krb,
			double krn) {

		this.center = Objects.requireNonNull(center);
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}

	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {
		double a = 1.0;
		double b = 2 * ray.direction.scalarProduct(ray.start.sub(center));
		double c = ray.start.sub(center).scalarProduct(ray.start.sub(center)) - radius * radius;

		double lambda1 = (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
		double lambda2 = (-b - sqrt(b * b - 4 * a * c)) / (2 * a);

		if (lambda1 == Double.NaN || lambda2 == Double.NaN) {
			return null;
		}

		Point3D t1 = ray.start.add(ray.direction.scalarMultiply(lambda1));
		Point3D t2 = ray.start.add(ray.direction.scalarMultiply(lambda2));

		Point3D n1 = t1.sub(center).normalize();
		Point3D n2 = t2.sub(center).normalize();

		double d1 = ray.start.sub(t1).norm();
		double d2 = ray.start.sub(t2).norm();

		if (lambda1 > 0 && lambda2 > 0) {
			if (d1 < d2) {
				return new RayIntersectionImpl(t1, d1, true, n1);
			} else {
				return new RayIntersectionImpl(t2, d2, true, n2);
			}
		} else if (lambda1 > 0) {
			return new RayIntersectionImpl(t1, d1, false, n1);
		} else if (lambda2 > 0) {
			return new RayIntersectionImpl(t2, d2, false, n2);
		}

		return null;

	}

	/**
	 * This class models the point of intersection of a ray of light (represented by
	 * {@link Ray} abstraction) and the sphere (represented by {@link Sphere}
	 * abstraction).
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class RayIntersectionImpl extends RayIntersection {

		/** Normal to object surface at this intersection point */
		private final Point3D normal;

		/**
		 * Constructor method for class {@link RayIntersectionImpl}.
		 * 
		 * @param point
		 *            point of intersection between ray and object
		 * @param distance
		 *            distance between start of ray and intersection
		 * @param outer
		 *            specifies if intersection is outer
		 * @param normal
		 */
		protected RayIntersectionImpl(Point3D point, double distance, boolean outer, Point3D normal) {
			super(point, distance, outer);
			this.normal = normal.normalize();
		}

		@Override
		public Point3D getNormal() {
			return normal;
		}

		@Override
		public double getKdr() {
			return kdr;
		}

		@Override
		public double getKdg() {
			return kdg;
		}

		@Override
		public double getKdb() {
			return kdb;
		}

		@Override
		public double getKrr() {
			return krr;
		}

		@Override
		public double getKrg() {
			return krg;
		}

		@Override
		public double getKrb() {
			return krb;
		}

		@Override
		public double getKrn() {
			return krn;
		}

	}

}
