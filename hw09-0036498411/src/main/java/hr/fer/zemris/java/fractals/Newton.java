package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * This program is used for producing and showing fractals based on
 * Newton-Raphson iteration
 * (<a href = "https://www.chiark.greenend.org.uk/~sgtatham/newton/">link</a>).
 * 
 * <p>
 * When program starts, user must enter complex roots of polynomial based on
 * which the fractal is produced. To complete the input, enter "done".
 * </p>
 * <b>NOTE:</b> expected input for roots is <i>a+bi</i> or <i>a-bi</i> where a
 * and b are real constants (b is positive), and i is imaginary unit. Also, a
 * and b aren't necessary (if not needed).
 * 
 * @see NRIteration
 * @author Darko Britvec
 * @version 1.0
 */
public class Newton {

	/** Message printed at the beginning of the program */
	public static final String WELLCOME_MESSAGE = "Welcome to Newton-Raphson iteration-based fractal viewer.\n"
			+ "Please enter at least two roots, one root per line. Enter 'done' when done.\n";
	/** Message printed when fractal producing is initiated */
	public static final String INIT_MESSAGE = "Image of fractal will appear shortly. Thank you.";

	/** Number of available processors */
	public static int numOfProcessors = Runtime.getRuntime().availableProcessors();
	/** Thread pool */
	public static ExecutorService pool = Executors.newFixedThreadPool(numOfProcessors, new ThreadFactory() {
		@Override
		public Thread newThread(Runnable r) {
			Thread worker = new Thread(r);
			worker.setDaemon(true);
			return worker;
		}
	});

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		System.out.print(WELLCOME_MESSAGE);
		List<Complex> roots = new ArrayList<>();

		try (Scanner sc = new Scanner(System.in)) {
			int count = 1;

			while (true) {
				System.out.printf("Root %d> ", count);
				String next = sc.nextLine();

				if (next.equals("done")) {
					break;
				}

				try {
					roots.add(Complex.parse(next));
					count++;
				} catch (IllegalArgumentException ex) {
					System.out.println("Input can't be parsed into complex number. Try again.");
				}
			}
		}

		if (roots.size() < 2) {
			System.out.println("More than one root expected. Quitting program...");
			return;
		}

		System.out.println("Image of fractal will appear shortly. Thank you.\n");

		ComplexRootedPolynomial polynomial = new ComplexRootedPolynomial(roots.toArray(new Complex[roots.size()]));
		FractalViewer.show(new NRFractalProducer(polynomial));
	}

	/**
	 * This class represents an implementation of {@link IFractalProducer} used for
	 * calculating and providing data for drawing Newton-Raphson fractals on
	 * {@link FractalViewer}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public static class NRFractalProducer implements IFractalProducer {

		/** Polynomial based on which the fractal is produced */
		private ComplexRootedPolynomial polynomial;

		/**
		 * Constructor method for class {@link NRFractalProducer}.
		 * 
		 * @param polynomial
		 *            polynomial based on which the fractal is produced
		 * @throws NullPointerException
		 *             if arguments is <code>null</code>
		 */
		public NRFractalProducer(ComplexRootedPolynomial polynomial) {
			this.polynomial = Objects.requireNonNull(polynomial);
		}

		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height,
				long requestNo, IFractalResultObserver observer) {

			final int maxIter = 16 * 16 * 16;
			final int numOfTracks = numOfProcessors * 8;

			short[] data = new short[width * height];
			int numOfYByTrack = height / numOfTracks;

			List<Future<Void>> results = new ArrayList<>();

			for (int i = 0; i < numOfTracks; i++) {
				int yMin = i * numOfYByTrack;
				int yMax = (i + 1) * numOfYByTrack;

				if (i == numOfTracks - 1) {
					yMax = height - 1;
				}

				NRIterationCalculationJob job = new NRIterationCalculationJob(reMin, reMax, imMin, imMax, width, height,
						yMin, yMax, maxIter, data, 1E-3, polynomial, 1E-3);

				results.add(pool.submit(job));
			}

			for (Future<Void> finishedJob : results) {
				try {
					finishedJob.get();
				} catch (InterruptedException | ExecutionException e) {
				}
			}

			observer.acceptResult(data, (short) (polynomial.toComplexPolynom().order() + 1), requestNo);
		}

	}
}
