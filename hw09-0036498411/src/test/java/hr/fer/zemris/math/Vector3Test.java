package hr.fer.zemris.math;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3Test {
	Vector3 a = new Vector3(1, 1, 1);
	Vector3 b = new Vector3(2, -1, 4.5);

	@Test
	public void testNorm() {
		assertEquals(Math.sqrt(3), a.norm(), 10E-10);
	}

	@Test
	public void testNormalized() {
		assertEquals(new Vector3(0.577350, 0.577350, 0.577350), a.normalized());
	}

	@Test
	public void testAdd() {
		assertEquals(new Vector3(3, 0, 5.5), a.add(b));
	}

	@Test
	public void testSub() {
		assertEquals(new Vector3(-1, 2, -3.5), a.sub(b));
	}

	@Test
	public void testDot() {
		assertEquals(5.5, a.dot(b), 10E-5);
	}

	@Test
	public void testCross() {
		assertEquals(new Vector3(5.5, -2.5, -3), a.cross(b));
	}

	@Test
	public void testScale() {
		assertEquals(new Vector3(2, 2, 2), a.scale(2));
	}

	@Test
	public void testCosAngle() {
		assertEquals( Math.cos(0.6155), a.cosAngle(new Vector3(1, 1, 0)), 10E-5);
	}

}
