package hr.fer.zemris.math;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.sqrt;
import static java.lang.Math.atan;
import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("unused")
public class ComplexTest {
	private static final double DEFAULT_PRECISION = 1E-10;
	private Complex c1, c2;

	@Before
	public void setUp() throws Exception {
		c1 = new Complex(0.5, sqrt(3) / 2);
		c2 = new Complex(sqrt(3) / 2, 0.5);
	}

	@Test
	public void testComplex() {
		Complex test = new Complex(1.22, -5.2);
		assertTrue(abs(1.22 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(-5.2 - test.getIm()) < DEFAULT_PRECISION);
	}


	@Test
	public void testParse() {
		Complex test = Complex.parse("3.51");
		assertTrue(abs(3.51 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getIm()) < DEFAULT_PRECISION);
		test = Complex.parse("-3.17");
		assertTrue(abs(-3.17 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getIm()) < DEFAULT_PRECISION);
		test = Complex.parse("-2.71i");
		assertTrue(abs(0 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(-2.71 - test.getIm()) < DEFAULT_PRECISION);
		test = Complex.parse("i");
		assertTrue(abs(0 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(1 - test.getIm()) < DEFAULT_PRECISION);
		test = Complex.parse("1");
		assertTrue(abs(1 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(0 - test.getIm()) < DEFAULT_PRECISION);
		test = Complex.parse("  -3.21 +  3i");
		assertTrue(abs(-3.21 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(3 - test.getIm()) < DEFAULT_PRECISION);

	}

	@Test
	public void testGetRealPart() {
		assertTrue(abs(0.5 - c1.getRe()) < DEFAULT_PRECISION);
	}

	@Test
	public void testGetImaginaryPart() {
		assertTrue(abs(0.5 - c2.getIm()) < DEFAULT_PRECISION);
	}


	@Test
	public void testAdd() {
		Complex test = c1.add(c2);
		assertTrue(abs((sqrt(3) + 1) / 2 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs((sqrt(3) + 1) / 2 - test.getIm()) < DEFAULT_PRECISION);

	}

	@Test(expected = NullPointerException.class)
	public void testAddException() {
		Complex test = c1.add(null);

	}

	@Test
	public void testSub() {
		Complex test = c1.sub(c2);
		assertTrue(abs((-sqrt(3) + 1) / 2 - test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs((sqrt(3) - 1) / 2 - test.getIm()) < DEFAULT_PRECISION);

	}

	@Test(expected = NullPointerException.class)
	public void testSubException() {
		Complex test = c1.sub(null);
	}

	@Test
	public void testMul() {
		Complex test = c1.multiply(c2);
		assertTrue(abs(test.getRe()) < DEFAULT_PRECISION);
		assertTrue(abs(1 - test.getIm()) < DEFAULT_PRECISION);
	}

	@Test(expected = NullPointerException.class)
	public void testMulException() {
		Complex test = c1.multiply(null);
	}

	@Test
	public void testDiv() {
		Complex test = c1.divide(c2);
		assertTrue(abs(1 - test.module()) < DEFAULT_PRECISION);
		assertTrue(abs(PI / 6 - atan(test.getIm()/test.getRe())) < DEFAULT_PRECISION);
	}

	@Test(expected = NullPointerException.class)
	public void testDivNullException() {
		Complex test = c1.divide(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDivArgumentException() {
		Complex test = c1.divide(new Complex(0, 0));
	}

	@Test
	public void testPower() {
		Complex test = new Complex(2*cos(0.5), 2*sin(0.5)).power(3);
		assertTrue(abs(8 - test.module()) < DEFAULT_PRECISION);
		assertTrue(abs(1.5 - atan(test.getIm()/test.getRe())) < DEFAULT_PRECISION);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testPowerArgumentException() {
		Complex test = c1.power(-1);
	}

	@Test
	public void testRoot() {
		List<Complex> testRoots = new Complex(16*cos( PI / 4), 16*sin( PI / 4)).root(4);
		List<Complex> expectedRoots = new ArrayList<>();

		for (int k = 0; k < 4; k++) {
			expectedRoots.add(new Complex(2*cos((PI / 4 + 2 * k * PI) / 4), 2*sin((PI / 4 + 2 * k * PI) / 4)));
		}

		assertArrayEquals(expectedRoots.toArray(new Complex[4]), testRoots.toArray(new Complex[4]));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRootArgumentException() {
		List<Complex> test = c1.root(-1);
	}
}
