/**
 * This package contains classes used for creating simple search engine program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.trazilica;