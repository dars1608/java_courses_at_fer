/**
 * This package contains
 * {@link hr.fer.zemris.java.hw16.trazilica.commands.ICommand shell commands}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw16.trazilica.commands;