package hr.fer.zemris.java.hw16.trazilica.commands;

import java.util.List;

import hr.fer.zemris.java.hw16.trazilica.SearchContext;
import hr.fer.zemris.java.hw16.trazilica.SearchContext.SearchResult;

/**
 * This interface defines a command of the search shell.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface ICommand {

	/**
	 * This method initiates the execution of the command.
	 * 
	 * @param arguments
	 *            command arguments
	 * @param context
	 *            {@link SearchContext} object
	 * @throws IllegalArgumentException
	 *             if argument isn't valid for certain command
	 * @throws NullPointerException
	 *             if context parameter is {@code null}
	 */
	void execute(String[] arguments, SearchContext context);

	/**
	 * This is auxiliary method used for printing out search results.
	 * 
	 * @param results
	 *            list of {@link SearchResult} objects
	 */
	public static void printResults(List<SearchResult> results) {
		if (results.isEmpty()) {
			System.out.println("Ne postoji podudaranje.");
		} else {
			for (int i = 0, len = results.size(); i < len; i++) {
				SearchResult result = results.get(i);
				System.out.printf("[%2d] (%.4f) %s\n", i, result.getSim(), result.getPath().toString());
			}
		}
	}
}
