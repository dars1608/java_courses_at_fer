package hr.fer.zemris.java.hw16.trazilica.commands;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import hr.fer.zemris.java.hw16.trazilica.SearchContext;
import hr.fer.zemris.java.hw16.trazilica.SearchContext.SearchResult;

/**
 * This class represents {@link ICommand} used for obtaining the content of the
 * document which was on the list of best results on the certain index.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class TypeCommand implements ICommand {
	
	/** Divider used in formating the output*/
	private static final String DIVIDER = "----------------------------------------------------------------";

	@Override
	public void execute(String[] arguments, SearchContext context) {
		Objects.requireNonNull(context);

		if (arguments == null || arguments.length != 1) {
			throw new IllegalArgumentException("Naredba \"type\" podržava isključivo jedan argument.");
		}

		int index;
		try {
			index = Integer.valueOf(arguments[0]);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Argument mora biti cijeli broj");
		}

		SearchResult result = null;
		try {
			result = context.getResultByIndex(index);
		} catch (IllegalStateException e) {
			System.out.println("Rezultati nisu spremni. Najprije pozovite \"query\" naredbu.");
			return;
		}

		if (result == null) {
			System.out.printf("Ne postoji rezultat indeksa %d.\n", index);
		} else {
			Path path = result.getPath();
			System.out.println(DIVIDER);
			System.out.printf("Dokument: %s\n", path.toString());
			System.out.println(DIVIDER);
			if (!Files.exists(path)) {
				System.out.println("Dokument više ne postoji na disku.");
			} else {
				try {
					System.out.print(new String(Files.readAllBytes(path), StandardCharsets.UTF_8));
				} catch (IOException e) {
					System.out.println("Dokument nije moguće pročitati.");
				}
			}

			System.out.println(DIVIDER);
			System.out.println();
		}
	}

}
