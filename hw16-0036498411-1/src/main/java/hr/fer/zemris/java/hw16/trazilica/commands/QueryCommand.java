package hr.fer.zemris.java.hw16.trazilica.commands;

import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw16.trazilica.SearchContext;
import hr.fer.zemris.java.hw16.trazilica.SearchContext.SearchResult;
import hr.fer.zemris.java.hw16.trazilica.SearchEngine;

/**
 * This class represents {@link ICommand} used for searching the words in
 * documents. The search is performed on {@link SearchEngine} which calculates
 * similarity coefficients of query and all of documents from documents
 * collection. It prints out {@value #NUMBER_OF_RESULTS} best results, or less
 * in case there's less than {@value #NUMBER_OF_RESULTS} results with similarity
 * coefficient greater than 0.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class QueryCommand implements ICommand {

	/** Number of results to be printed out */
	private static final int NUMBER_OF_RESULTS = 10;

	@Override
	public void execute(String[] arguments, SearchContext context) {
		Objects.requireNonNull(context);

		if (arguments == null || arguments.length == 0) {
			throw new IllegalArgumentException("Naredba \"query\" zahtjeva barem jedan argument.");
		}

		String text = String.join(" ", arguments);
		String[] parts = text.split("[^a-zA-ZžćčđšŽĆČĐŠ]+"); //so that john22doe is treated as 2 words
		StringBuilder sb = new StringBuilder("Query is: [");
		for (int i = 0; i < parts.length; i++) {
			sb.append(parts[i].toLowerCase());

			if (i != parts.length - 1) {
				sb.append(", ");
			}
		}
		sb.append("]");

		System.out.println(sb.toString());
		System.out.println("Najboljih 10 rezultata:");
		List<SearchResult> results = context.getBestResults(text, NUMBER_OF_RESULTS);
		ICommand.printResults(results);
		System.out.println();
	}
}
