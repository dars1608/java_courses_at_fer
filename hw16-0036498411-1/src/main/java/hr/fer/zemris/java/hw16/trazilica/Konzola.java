package hr.fer.zemris.java.hw16.trazilica;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

import hr.fer.zemris.java.hw16.trazilica.commands.ExitCommand;
import hr.fer.zemris.java.hw16.trazilica.commands.ICommand;
import hr.fer.zemris.java.hw16.trazilica.commands.QueryCommand;
import hr.fer.zemris.java.hw16.trazilica.commands.ResultsCommand;
import hr.fer.zemris.java.hw16.trazilica.commands.TypeCommand;

/**
 * This is a shell program used as simple search engine. User must provide the
 * path of the folder in which the documents which will be search are stored.
 * The shell has 4 commands:
 * <ul>
 * <li>"querry <i>your query</i>" - for obtaining the 10 documents with most
 * similar content as the given query
 * <li>"type <i>index</i>" - for printing out the document at the certain index
 * in the results list
 * <li>"results" - for printing out last search results
 * <li>"exit" - for exiting the shell
 * </ul>
 * 
 * <b>NOTE:</b> some parts of the program are in Croatian just because of the
 * task demands.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class Konzola {

	/**
	 * This method is called when program starts.
	 * 
	 * @param args
	 *            command line arguments
	 * @throws IOException
	 *             if any input/output exception occurs
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.err.println("Program prima jedan argument.");
			return;
		}

		Path path = Paths.get(args[0]);
		if (!Files.isDirectory(path)) {
			System.err.println("Argument mora biti staza do direktorija.");
			return;
		}

		SearchEngine engine = initSearchEngine(path);
		SearchContext context = initSearchContext(engine, path);
		Map<String, ICommand> commands = initCommands();

		System.out.printf("Veličina rječnika je %d riječi\n", engine.getDictionary().size());

		try (Scanner sc = new Scanner(System.in)) {
			final String enterCommand = "Enter command > ";
			while (true) {
				System.out.print(enterCommand);
				String input = sc.nextLine();

				if (input.isEmpty())
					continue;

				String parts[] = input.trim().replace("\\s+", " ").split(" ");
				String command = parts[0];
				if (!commands.containsKey(command)) {
					System.out.println("Nepoznata naredba.");
				} else {
					try {
						commands.get(command).execute(Arrays.copyOfRange(parts, 1, parts.length), context);
					} catch (IllegalArgumentException e) {
						System.out.println(e.getMessage());
					}
				}
			}
		}

	}

	/**
	 * This method is used internally for initializing a map of {@link ICommand}
	 * objects used in the shell.
	 * 
	 * @return map of {@link ICommand} objects
	 */
	private static Map<String, ICommand> initCommands() {
		Map<String, ICommand> commands = new HashMap<>();

		commands.put("query", new QueryCommand());
		commands.put("results", new ResultsCommand());
		commands.put("type", new TypeCommand());
		commands.put("exit", new ExitCommand());

		return commands;
	}

	/**
	 * This method is used internally for initializing the search context which will
	 * be the core of the shell.
	 * 
	 * @param engine
	 *            {@link SearchEngine} object
	 * @param path
	 *            path of the documents directory
	 * @return {@link SearchContext} object
	 * @throws RuntimeException
	 *             if context can't initialize
	 */
	private static SearchContext initSearchContext(SearchEngine engine, Path path) {
		SearchContext context = new SearchContext(engine);
		try (Stream<Path> documents = Files.list(path)) {
			documents.forEach(document -> {
				if (!Files.isReadable(document)) {
					return;
				}

				try {
					context.addDocument(document, new String(Files.readAllBytes(document), StandardCharsets.UTF_8));
				} catch (IOException ignorable) {}
			});
		} catch (IOException e) {
			throw new RuntimeException("Search context can't initialize.");
		}

		return context;
	}

	/**
	 * This method is used internally for initializing the search engine.
	 * 
	 * @param path
	 *            path od documents directory
	 * @return {@link SearchEngine} object
	 */
	private static SearchEngine initSearchEngine(Path path) {
		try (Stream<Path> documents = Files.list(path)) {
			SearchEngine engine = new SearchEngine();
			documents.forEach(document -> {
				if (!Files.isReadable(document)) {
					return;
				}
				try {
					engine.addToDictionary(new String(Files.readAllBytes(document), StandardCharsets.UTF_8));
				} catch (IOException ignorable) {}
			});

			return engine;
		} catch (IOException e) {
			throw new RuntimeException("Search engine can't initialize.", e);
		}

	}
}
