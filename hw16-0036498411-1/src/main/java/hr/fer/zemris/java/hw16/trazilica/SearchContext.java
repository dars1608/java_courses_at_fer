package hr.fer.zemris.java.hw16.trazilica;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * This class defines a search context which will be used as the core of the
 * search shell application. It can add different documents in the search base,
 * search the base and provide the search results (described with
 * {@link SearchResult} objects). It directly communicates with the
 * {@link SearchEngine} which performs all the calculation.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SearchContext {
	
	/** Precision when working with doubles*/
	private static final double PRECISION = 1E-5;

	/** Map containing (path of the document, document vector) tuples */
	private Map<Path, double[]> vectors = new HashMap<>();
	/** List of last obtained results */
	private List<SearchResult> bestResults;
	/** Search engine */
	private SearchEngine engine;

	/**
	 * Constructor method for class {@link SearchContext}.
	 * 
	 * @param engine
	 *            search engine
	 * @throws NullPointerException
	 *             if provided argument is {@code null}
	 */
	public SearchContext(SearchEngine engine) {
		this.engine = Objects.requireNonNull(engine);
	}

	/**
	 * This method is used for adding the document into the search base.
	 * 
	 * @param path
	 *            path of the document
	 * @param data
	 *            content of the document
	 * @throws NullPointerException
	 *             if provided argument is {@code null}
	 */
	public void addDocument(Path path, String data) {
		vectors.put(Objects.requireNonNull(path), Objects.requireNonNull(engine.getVector(data)));
	}

	/**
	 * This method is used for querying the document base and obtaining a certain
	 * number of the matching results.
	 * 
	 * @param input
	 *            query input
	 * @param numOfResults
	 *            number of results to be obtained
	 * @return list of {@link SearchResult} objects (list will be a size of given
	 *         parameter only if the similarity coefficients are greater than 0)
	 * @throws NullPointerException
	 *             if provided argument is {@code null}
	 */
	public List<SearchResult> getBestResults(String input, int numOfResults) {
		Objects.requireNonNull(input);
		if (numOfResults < 0) {
			throw new IllegalArgumentException("Argument numOfResults must be positive integer.");
		}

		double[] queryVector = engine.getVector(input);
		List<SearchResult> results = new ArrayList<>();
		bestResults = new ArrayList<>();

		for (Map.Entry<Path, double[]> entry : vectors.entrySet()) {
			double sim = SearchEngine.calculateSimilarity(queryVector, entry.getValue());
			results.add(new SearchResult(entry.getKey(), sim));
		}

		Collections.sort(results, Comparator.reverseOrder());
		for (int i = 0; i < numOfResults; i++) {
			SearchResult result = results.get(i);
			if (Math.abs(result.getSim()) < PRECISION) {
				break;
			}

			bestResults.add(result);
		}

		return bestResults;
	}

	/**
	 * This method is used for obtaining the {@link SearchResult} from the list of
	 * best results at the given index.
	 * 
	 * @param index
	 *            index at which the result is stored
	 * @return the result at given index, {@code null} if theres no result at given
	 *         index
	 * @throws IllegalStateException
	 *             if the result list isn't ready
	 */
	public SearchResult getResultByIndex(int index) {
		if (bestResults == null) {
			throw new IllegalStateException("Results aren't ready. First call SearchContext#getBestResults(..");
		}

		if (index < 0 || index >= bestResults.size()) {
			return null;
		}

		return bestResults.get(index);
	}

	/**
	 * This method is used for obtaining the last query results.
	 * 
	 * @return list of {@link SearchResult} objects
	 */
	public List<SearchResult> getLastResults() {
		return bestResults;
	}

	/**
	 * This class models a simple search result.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public class SearchResult implements Comparable<SearchResult> {

		/** Similarity coefficient */
		private final double sim;
		/** Path of the document */
		private final Path path;

		/**
		 * Constructor method for class {@link SearchResult}.
		 * 
		 * @param path
		 *            path of the document
		 * @param sim
		 *            coefficient of similarity
		 */
		public SearchResult(Path path, double sim) {
			this.path = Objects.requireNonNull(path);
			this.sim = sim;
		}

		/**
		 * Getter method for the {@code sim}.
		 * 
		 * @return the sim
		 */
		public double getSim() {
			return sim;
		}

		/**
		 * Getter method for the {@code path}.
		 * 
		 * @return the path
		 */
		public Path getPath() {
			return path;
		}

		@Override
		public int compareTo(SearchResult o) {
			if (o == null) {
				return -1;
			} else {
				return Double.compare(this.sim, o.sim);
			}
		}
	}
}
