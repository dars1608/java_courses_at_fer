package hr.fer.zemris.java.hw16.trazilica.commands;

import hr.fer.zemris.java.hw16.trazilica.SearchContext;

/**
 * This class represents {@link ICommand} used for exiting the shell.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ExitCommand implements ICommand {

	@Override
	public void execute(String[] arguments, SearchContext context) {
		if (arguments != null && arguments.length != 0) {
			throw new IllegalArgumentException("Naredba \"exit\" ne podržava argumente.");
		}

		System.exit(0);
	}

}
