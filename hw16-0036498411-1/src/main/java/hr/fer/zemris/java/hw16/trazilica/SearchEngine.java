package hr.fer.zemris.java.hw16.trazilica;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

/**
 * This class defines a simple search engine which is capable of obtaining the
 * words from text and store it in its internal "dictionary" which also stores
 * number of word occurrences. The engine will ignore words described in file
 * {@value #STOPPING_WORDS_FILENAME}. The engine can also calculate the
 * similarity coefficient of some text with the other text based on <a
 * href=https://en.wikipedia.org/wiki/Tf–idf>TF-IDF</a> vectors.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SearchEngine {

	/** File containing stopping words which will be ignored */
	private static final String STOPPING_WORDS_FILENAME = "hrvatski_stoprijeci.txt";
	/** Precision when working with doubles */
	private static final double PRECISION = 1E-5;

	/** Map of (word, number of occurrences) tuples */
	private HashMap<String, Integer> dictionary = new HashMap<>();
	/** Set of stopping words */
	private Set<String> stoppingWords;
	/** Final list of words */
	private List<String> listOfWords;
	/** Map of (word, idf component) tuples */
	private Map<String, Double> idfComponent;
	/** Number of documents which can be searched */
	private int numberOfDocuments = 0;

	/**
	 * Constructor method for class {@link SearchEngine}.
	 * 
	 * @throws IOException
	 *             if there's a problem with initializing stopping words
	 * @throws URISyntaxException
	 *             ignorable
	 */
	public SearchEngine() throws IOException {
		stoppingWords = loadStoppingWords();
	}

	/**
	 * Getter method for the {@code dictionary} value.
	 * 
	 * @return the {@code dictionary}
	 */
	public Map<String, Integer> getDictionary() {
		return dictionary;
	}

	/**
	 * Getter method for the {@code stoppingWords} value.
	 * 
	 * @return the {@code stoppingWords}
	 */
	public Set<String> getStoppingWords() {
		return stoppingWords;
	}
	
	/**
	 * Getter method for the {@code idfComponent} value.
	 * 
	 * @return the {@code idfComponent}
	 */
	public Map<String, Double> getIdfComponent(){
		return idfComponent;
	}

	/**
	 * This method is used for calculating the similarity of the two vector
	 * representations of a document.
	 * 
	 * @param vector1
	 *            first document vector
	 * @param vector2
	 *            second document vector
	 * @return similarity coefficient
	 */
	public static double calculateSimilarity(double[] vector1, double[] vector2) {
		double scalarProduct = 0;
		double mod1 = 0;
		double mod2 = 0;

		for (int i = 0; i < vector1.length; i++) {
			scalarProduct += vector1[i] * vector2[i];
			mod1 += vector1[i] * vector1[i];
			mod2 += vector2[i] * vector2[i];
		}

		if (Math.abs(scalarProduct) < PRECISION) {
			return 0.0;
		}

		return scalarProduct / (Math.sqrt(mod1) * Math.sqrt(mod2));
	}

	/**
	 * This method is used for adding the document to the dictionary.
	 * 
	 * @param doc
	 *            document
	 */
	public void addToDictionary(String doc) {
		listOfWords = null;
		idfComponent = null;
		
		Set<String> words = findWords(doc, word -> {
		});

		words.forEach(word -> {
			if (!stoppingWords.contains(word)) {
				if (dictionary.containsKey(word)) {
					dictionary.put(word, dictionary.get(word) + 1);
				} else {
					dictionary.put(word, 1);
				}
			}
		});

		numberOfDocuments++;
	}

	/**
	 * This method is used for getting the document vector (based on <a
	 * href=https://en.wikipedia.org/wiki/Tf–idf>TF-IDF</a> criterion).
	 * 
	 * @param document
	 *            document
	 * @return array of doubles representing document vector, {@code null} if
	 *         dictionary is empty
	 */
	public double[] getVector(String document) {
		if (dictionary.isEmpty()) {
			return null;
		}

		int numberOfWords = dictionary.size();
		final double[] vector = new double[numberOfWords];
		Set<String> words = findWords(document, word -> {
			int index = getWordIndex(word);
			if (index >= 0) {
				vector[index]++;
			}
		});

		words.forEach(word -> {
			int index = getWordIndex(word);

			if (index >= 0) {
				int occurrences = dictionary.get(word);
				if (occurrences == 0) {
					vector[index] = 0;
				} else {
					vector[index] = vector[index] * idfComponent.get(word);
				}
			}
		});

		return vector;
	}

	/**
	 * This method is used internally for extracting the words from text.
	 * 
	 * @param text
	 *            text to be parsed into words
	 * @param consumer
	 *            {@link Consumer} object determining what to do with the extracted
	 *            word
	 * @return set of extracted words
	 */
	private Set<String> findWords(String text, Consumer<String> consumer) {
		char[] data = text.toCharArray();
		Set<String> words = new HashSet<>();
		int begining = 0;
		int end = 0;

		while (end < data.length) {
			begining = end;
			for (; begining < data.length; begining++) {
				if (Character.isAlphabetic(data[begining])) {
					break;
				}
			}

			for (end = begining; end < data.length; end++) {
				if (!Character.isAlphabetic(data[end])) {
					break;
				}
			}

			if (begining != end) {
				String word = new String(data, begining, end - begining).toLowerCase();
				consumer.accept(word);
				words.add(word);
				end++;
			}
		}

		return words;
	}

	/**
	 * This method is used internally for determining the index of a word in the
	 * list of words.
	 * 
	 * @param word
	 *            word which is searched
	 * @return result of {@link Collections#binarySearch(List, Object)}
	 */
	private int getWordIndex(String word) {
		if (listOfWords == null || idfComponent == null) {
			listOfWords = new ArrayList<>(dictionary.keySet());
			idfComponent = new HashMap<>();

			for (String w : listOfWords) {
				int occurrences = dictionary.get(w);
				idfComponent.put(w, Math.log10(numberOfDocuments * 1.0 / occurrences));
			}

			Collections.sort(listOfWords);
		}

		return Collections.binarySearch(listOfWords, word);
	}

	/**
	 * This method is used internally for initializing the stopping words list.
	 * 
	 * @return set of stopping words
	 * @throws IOException
	 *             if error while reading file {@value #STOPPING_WORDS_FILENAME}
	 *             occurs
	 */
	private Set<String> loadStoppingWords() throws IOException {
		URI fileURI = null;

		try {
			fileURI = this.getClass().getResource(STOPPING_WORDS_FILENAME).toURI();
		} catch (URISyntaxException ignorable) {}

		if (fileURI != null) {
			return findWords(new String(Files.readAllBytes(Paths.get(fileURI))), w -> {});
		} else {
			return null;
		}
	}
}
