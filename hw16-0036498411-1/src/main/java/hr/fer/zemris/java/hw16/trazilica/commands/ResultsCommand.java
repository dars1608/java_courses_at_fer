package hr.fer.zemris.java.hw16.trazilica.commands;

import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw16.trazilica.SearchContext;
import hr.fer.zemris.java.hw16.trazilica.SearchContext.SearchResult;

/**
 * This class represents {@link ICommand} used for obtaining last search
 * results.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ResultsCommand implements ICommand {

	@Override
	public void execute(String[] arguments, SearchContext context) {
		Objects.requireNonNull(context);
		if (arguments != null && arguments.length != 0) {
			throw new IllegalArgumentException("Naredba \"results\" ne podržava argumente.");
		}

		List<SearchResult> results = context.getLastResults();
		if(results == null) {
			System.out.println("Rezultati nisu spremni. Najprije pozovite \"query\" naredbu.");
		} else {
			ICommand.printResults(results);
			System.out.println();
		}
	}

}
