package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Before;
import org.junit.Test;

public class ObjectMultistackTest {
	private ObjectMultistack test = new ObjectMultistack();

	@Before
	public void setUp() throws Exception {
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				test.push("key" + i, new ValueWrapper("value" + i + " " + j));
			}
		}
	}

	@Test
	public void testPush() {
		for (int i = 0; i < 100; i++) {
			test.push("key" + i, new ValueWrapper("valueX"));
		}

		for (int i = 0; i < 100; i++) {
			assertEquals("valueX", test.peek("key" + i).getValue());
		}
	}

	@Test(expected = NullPointerException.class)
	public void testPushNullKey() {
		test.push(null, new ValueWrapper("test"));
		fail();
	}

	@Test(expected = NullPointerException.class)
	public void testPushNullValue() {
		test.push("key0", null);
		fail();
	}

	@Test
	public void testPop() {
		for (int i = 0; i < 100; i++) {
			for (int j = 99; j >= 0; j--) {
				assertEquals("value" + i + " " + j, test.pop("key" + i).getValue());
				if(j==0) break;
				assertEquals("value" + i + " " + (j - 1), test.peek("key" + i).getValue());
			}

			assertTrue(test.isEmpty("key" + i));
		}
	}

	@Test
	public void testPeek() {
		for(int i = 0; i<100; i++) {
			assertEquals("value" + i + " 99", test.peek("key" + i).getValue());
		}
	}

	@Test
	public void testIsEmpty() {
		assertTrue(test.isEmpty("xzibit"));
	}
	
	@Test(expected=EmptyStackException.class)
	public void testEmptyStackEx1() {
		test.peek("xzibit");
	}
	
	@Test(expected=EmptyStackException.class)
	public void testEmptyStackEx2() {
		test.pop("xzibit");
	}

}
