package hr.fer.zemris.java.custom.scripting.exec;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Test;

public class ValueWrapperTest {
	
	@Test
	public void testNull() {
		ValueWrapper v1 = new ValueWrapper(null);
		ValueWrapper v2 = new ValueWrapper(null);
		v1.add(v2.getValue()); // v1 now stores Integer(0); v2 still stores null.
		
		assertEquals(Integer.valueOf(0), v1.getValue());
		assertNull(v2.getValue());
	}

	@Test
	public void testScientificNotation() {
		ValueWrapper v3 = new ValueWrapper("1.2E1");
		ValueWrapper v4 = new ValueWrapper(Integer.valueOf(1));
		v3.add(v4.getValue()); // v3 now stores Double(13); v4 still stores Integer(1).
		
		assertEquals(Double.valueOf(13), v3.getValue());
		assertEquals(Integer.valueOf(1), v4.getValue());
	}
	
	@Test
	public void testStringInteger() {
		ValueWrapper v5 = new ValueWrapper("12");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		v5.add(v6.getValue()); // v5 now stores Integer(13); v6 still stores Integer(1).
		
		assertEquals(Integer.valueOf(13), v5.getValue());
		assertEquals(Integer.valueOf(1), v6.getValue());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testException() {	
		ValueWrapper v7 = new ValueWrapper("Ankica");
		ValueWrapper v8 = new ValueWrapper(Integer.valueOf(1));
		v7.add(v8.getValue());

	}
	
	@Test
	public void testCompare1() {
		ValueWrapper v5 = new ValueWrapper("12");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		assertTrue(v5.numCompare(v6.getValue())>0);
	}
	
	@Test
	public void testCompare2() {
		ValueWrapper v5 = new ValueWrapper("12.0E-2");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		assertTrue(v5.numCompare(v6.getValue())<0);
	}
	
	@Test
	public void testCompare3() {
		ValueWrapper v5 = new ValueWrapper("1");
		ValueWrapper v6 = new ValueWrapper(Integer.valueOf(1));
		assertTrue(v5.numCompare(v6.getValue())==0);
	}
	
	@Test
	public void testTwoSciNotations() {
		ValueWrapper a = new ValueWrapper("1.2E-3");
		ValueWrapper b = new ValueWrapper("0.000000012E5");
		
		a.add(b.getValue());
		assertEquals(0.0024, a.getValue());
		
		a = new ValueWrapper("1.2E-3");
		b = new ValueWrapper("0.000000012E5");
		
		a.subtract(b.getValue());
		assertEquals(0.0, a.getValue());
		
		a = new ValueWrapper("1.2E-3");
		b = new ValueWrapper("0.000000012E5");
		
		a.multiply(b.getValue());
		assertEquals(0.0012*0.0012, a.getValue());
		
		a = new ValueWrapper("1.2E-3");
		b = new ValueWrapper("0.000000012E5");
		
		a.divide(b.getValue());
		assertEquals(1.0, a.getValue());
		
		a = new ValueWrapper("1.2E-3");
		b = new ValueWrapper("0.000000012E5");
		
		assertTrue(a.numCompare(b.getValue()) == 0);
		
	}
	
	@Test
	public void testTwoIntegers() {
		ValueWrapper a = new ValueWrapper(12);
		ValueWrapper b = new ValueWrapper(3);
		
		a.add(b.getValue());
		assertEquals(15, a.getValue());
		
		a = new ValueWrapper(12);
		b = new ValueWrapper(3);
		
		a.subtract(b.getValue());
		assertEquals(9, a.getValue());
		
		a = new ValueWrapper(12);
		b = new ValueWrapper(3);
		
		a.multiply(b.getValue());
		assertEquals(36, a.getValue());
		
		a = new ValueWrapper(12);
		b = new ValueWrapper(3);
		
		a.divide(b.getValue());
		assertEquals(4, a.getValue());
		
		a = new ValueWrapper(12);
		b = new ValueWrapper(3);
		
		assertTrue(a.numCompare(b.getValue()) > 0);
		
	}
	
	@Test
	public void testTwoDoubles() {
		ValueWrapper a = new ValueWrapper(12.0);
		ValueWrapper b = new ValueWrapper(3.0);
		
		a.add(b.getValue());
		assertEquals(15.0, a.getValue());
		
		a = new ValueWrapper(12.0);
		b = new ValueWrapper(3.0);
		
		a.subtract(b.getValue());
		assertEquals(9.0, a.getValue());
		
		a = new ValueWrapper(12.0);
		b = new ValueWrapper(3.0);
		
		a.multiply(b.getValue());
		assertEquals(36.0, a.getValue());
		
		a = new ValueWrapper(12.0);
		b = new ValueWrapper(3.0);
		
		a.divide(b.getValue());
		assertEquals(4.0, a.getValue());
		
		a = new ValueWrapper(12.0);
		b = new ValueWrapper(3.0);
		
		assertTrue(a.numCompare(b.getValue()) > 0);
	}
	
	@Test
	public void testDivisionByZero() {
		ValueWrapper a = new ValueWrapper(12);
		ValueWrapper b = new ValueWrapper(0);
		ValueWrapper c = new ValueWrapper(0.0);
		ValueWrapper d = new ValueWrapper("0.0");
		
		try {
			a.divide(b.getValue());
			fail();
		} catch(Exception e) {
			
		}
		
		try {
			a.divide(c.getValue());
			fail();
		} catch(Exception e) {
			
		}
		
		try {
			a.divide(d.getValue());
			fail();
		} catch(Exception e) {
			
		}
		
		try {
			b.divide(d.getValue());
			fail();
		} catch(Exception e) {
			
		}
		
	}
	
	@SuppressWarnings(value = "unused")
	@Test
	public void testPutAnything() {
		ValueWrapper a = new ValueWrapper(new RuntimeException());
		ValueWrapper b = new ValueWrapper(new int[] {1,2,3});
		ValueWrapper c = new ValueWrapper(new LinkedList<>());
	}

}
