package hr.fer.zemris.java.hw06.demo2;

/**
 * Demonstration program for class {@link PrimeCollection}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PrimesDemo2 {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		PrimesCollection primesCollection = new PrimesCollection(2);
		
		for (Integer prime : primesCollection) {
			for (Integer prime2 : primesCollection) {
				System.out.println("Got prime pair: " + prime + ", " + prime2);
			}
		}

	}

}
