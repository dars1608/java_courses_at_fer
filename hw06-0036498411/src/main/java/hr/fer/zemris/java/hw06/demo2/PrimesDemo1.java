package hr.fer.zemris.java.hw06.demo2;

/**
 * Demonstration program for class {@link PrimeCollection}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PrimesDemo1 {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		PrimesCollection primesCollection = new PrimesCollection(5);
		
		for (Integer prime : primesCollection) {
			System.out.println("Got prime: " + prime);
		}
	}

}
