/**
 * This package contains classes which demonstrate how simple observer pattern
 * works.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw06.observer1;
