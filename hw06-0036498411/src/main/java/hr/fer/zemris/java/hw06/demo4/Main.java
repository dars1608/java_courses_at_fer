package hr.fer.zemris.java.hw06.demo4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This program demonstrates working with {@link Stream} API.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class Main {

	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		List<String> lines = null;
		try {
			lines = Files
					.readAllLines(Paths.get(PATH_NAME));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		List<StudentRecord> records = convert(lines);

		// 1st task
		long number = vratiBodovaViseOd25(records);
		
		System.out.println("1st task");
		System.out.println(number);
		System.out.println();

		// 2nd task
		long number5 = vratiBrojOdlikasa(records);
		
		System.out.println("2nd task");
		System.out.println(number5);
		System.out.println();

		// 3rd task
		List<StudentRecord> aStudents = vratiListuOdlikasa(records);
		
		System.out.println("3rd task");
		aStudents.stream()
				.forEach(System.out::println);
		System.out.println();

		// 4th task
		List<StudentRecord> aStudentsSorted = vratiSortiranuListuOdlikasa(records);
		
		System.out.println("4th task");
		aStudentsSorted.stream()
				.forEach(System.out::println);
		System.out.println();

		// 5th task
		List<String> failedJMBAGs = vratiPopisNepolozenih(records);
		
		System.out.println("5th task");
		failedJMBAGs.stream()
				.forEach(System.out::println);
		System.out.println();

		// 6th task
		Map<Integer, List<StudentRecord>> mapByGrades = razvrstajStudentePoOcjenama(records);
		
		System.out.println("6th task");
		mapByGrades.forEach((i, j) -> {
			System.out.println(i);
			j.stream().forEach(System.out::println);
		});
		System.out.println();

		// 7th task
		Map<Integer, Integer> mapByGradesNum = vratiBrojStudenataPoOcjenama(records);
		
		System.out.println("7th task");
		mapByGradesNum.forEach((i, j) -> {
			System.out.println(i + " " + j);
		});
		System.out.println();

		// 8th task
		Map<Boolean, List<StudentRecord>> mapPassedOrNot = razvrstajProlazPad(records);
		
		System.out.println("8th task");
		System.out.println("Passed:");
		mapPassedOrNot.get(true).stream().forEach(System.out::println);
		System.out.println("Failed:");
		mapPassedOrNot.get(false).stream().forEach(System.out::println);
		System.out.println();
		
	}
	
	//NOTE: name of methods are defined in homework!
	
	/** Minimal sum of achieved points*/
	private static final int MINIMAL_POINTS = 25;
	/**
	 * This method is used for counting how many students have sum of points
	 * greater than {@value #MINIMAL_POINTS}.
	 * 
	 * @param records
	 * 			{@link List} of {@link StudentRecord} objects
	 * @return number of students having sum of points greater than
	 * 			{@value #MINIMAL_POINTS}
	 */
	public static long vratiBodovaViseOd25(List<StudentRecord> records) {
		return records.stream()
				.filter(i -> i.getMidtermExamPoints()
						+ i.getFinalExamPoints()
						+ i.getLaboratoryExcersisesPoints() > MINIMAL_POINTS)
				.count();

	}
	
	/** Excellent grade*/
	private static final int A_GRADE = 5;
	
	/**
	 * This method is used for counting how many students have final grade equal
	 * to {@value #A_GRADE}.
	 * 
	 * @param records
	 * 			List of {@link StudentRecord} objects
	 * 
	 * @return number of students having final grade equal to {@value #A_GRADE}
	 */
	public static long vratiBrojOdlikasa(List<StudentRecord> records) {
		return records.stream()
				.filter(i -> i.getFinalGrade() == A_GRADE)
				.count();
	}
	
	/**
	 * This method is used for collecting students with final grade {@value #A_GRADE}.
	 * 
	 * @param records
	 * 			List of {@link StudentRecord} objects
	 * @return list of students with final grade {@value #A_GRADE}
	 */
	public static List<StudentRecord> vratiListuOdlikasa(List<StudentRecord> records){
		return records.stream()
				.filter(i -> i.getFinalGrade() == A_GRADE)
				.collect(Collectors.toList());
	}
	
	/**
	 * This method is used for collecting students with final grade {@value #A_GRADE}, and
	 * sorting it by {@link Comparator} {@link #BY_POINTS}.
	 * 
	 * @param records
	 * 			List of {@link StudentRecord} objects
	 * @return Sorted list of students with final grade {@value #A_GRADE}
	 */
	public static List<StudentRecord> vratiSortiranuListuOdlikasa(List<StudentRecord> records){
		return records
				.stream()
				.filter(i -> i.getFinalGrade() == A_GRADE)
				.sorted(BY_POINTS.reversed())
				.collect(Collectors.toList());

	}
	
	/** Failed grade */
	private static final int F_GRADE = 1;
	/**
	 * This method is used for creating a list of jmbags of students who have
	 * failed the class (have their final grade equal to {@value #F_GRADE}).
	 * 
	 * @param records List of {@link StudentRecord} objects
	 * @return List of sorted jmbags of students who have failed the class
	 */
	public static List<String> vratiPopisNepolozenih(List<StudentRecord> records){
		return records.stream()
				.filter(i -> i.getFinalGrade() == F_GRADE)
				.map(StudentRecord::getJmbag)
				.sorted()
				.collect(Collectors.toList());
	}
	
	/**
	 * This method is used for mapping the students by their grades.
	 * 
	 * @param records List of {@link StudentRecord} objects
	 * @return Map with final grade as a key and list of {@link StudentRecord}
	 * 			objects as a value
	 */
	public static Map<Integer, List<StudentRecord>> razvrstajStudentePoOcjenama(List<StudentRecord> records){
		return records
				.stream()
				.collect(Collectors.groupingBy(
						StudentRecord::getFinalGrade));

	}
	
	/**
	 * This method is used for mapping number of students by grades 
	 * @param records List of {@link StudentRecord} objects
	 * @return Map with final grade as a key and  number of students with that grade
	 * 			as a value
	 */
	public static Map<Integer, Integer> vratiBrojStudenataPoOcjenama(List<StudentRecord> records){
		return records
				.stream()
				.collect(Collectors.toMap(
						StudentRecord::getFinalGrade,
						i -> Integer.valueOf(1),
						(oldCount, newCount) -> oldCount + newCount));
	}
	
	/**
	 * This method is used for partitioning students whether they passed
	 * or failed the class
	 *  
	 * @param records List of {@link StudentRecord} objects
	 * @return Map with key <code>true</code> for students who passed
	 * 			and key <code>false</code> for students who failed the class
	 */
	public static Map<Boolean, List<StudentRecord>> razvrstajProlazPad(List<StudentRecord> records){
		return records
				.stream()
				.collect(Collectors.partitioningBy(
						i -> i.getFinalGrade() > 1));
	}

	/**
	 * This method is used internally to convert lines form text file to
	 * {@link StudentRecord} objects.
	 * 
	 * @param lines
	 *            lines containing informations about some student
	 * @return list of {@link StudentRecord} objects
	 */
	private static List<StudentRecord> convert(
			List<String> lines) {
		List<StudentRecord> records = new ArrayList<>();
		for (String line : lines) {
			String[] parts = line.split("\\s+");
			if (parts.length != 7)
				continue;

			try {
				String jmbag = parts[0];
				String lastName = parts[1];
				String firstName = parts[2];
				double midtermExamPoints = Double
						.parseDouble(parts[3]);
				double finalExamPoints = Double
						.parseDouble(parts[4]);
				double laboratoryExcersisesPoints = Double
						.parseDouble(parts[5]);
				int finalGrade = Integer.parseInt(parts[6]);

				records.add(new StudentRecord(jmbag,
						lastName, firstName,
						midtermExamPoints, finalExamPoints,
						laboratoryExcersisesPoints,
						finalGrade));
			} catch (NumberFormatException ex) {
				continue;
			}
		}

		return records;
	}

	/** Comparator used for sorting {@link StudentRecord} by sum of all achieved points*/
	private static final Comparator<StudentRecord> BY_POINTS = (s1, s2) -> {
		double p1 = s1.getMidtermExamPoints()
				+ s1.getFinalExamPoints()
				+ s1.getLaboratoryExcersisesPoints();
		double p2 = s2.getMidtermExamPoints()
				+ s2.getFinalExamPoints()
				+ s2.getLaboratoryExcersisesPoints();
		return Double.compare(p1, p2);
	};
	
	/** Relative address for file from which student records are obtained*/
	private static final String PATH_NAME = "./src/main/resources/studenti.txt";
}
