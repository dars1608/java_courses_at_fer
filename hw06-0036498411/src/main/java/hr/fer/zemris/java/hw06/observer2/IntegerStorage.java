package hr.fer.zemris.java.hw06.observer2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * This class represents subject class which stores some value of interest of
 * some observers. Observer objects are described by interface
 * {@link IntegerStorageObserver}.
 * </p>
 * <p>
 * Observers, which are added to internal, list will be informed if the stored
 * value changes.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class IntegerStorage {

	/** Value of interest */
	private int value;
	/** List of observers who observe stored <code>value</code> */
	private List<IntegerStorageObserver> observers;
	/** Control flag*/
	private boolean isIterating = false;
	/** Observers to be removed after iteration */
	private List<IntegerStorageObserver> toBeRemoved;
	/** Observers to be added after iteration */
	private List<IntegerStorageObserver> toBeAdded;
	
	{
		observers = new ArrayList<>();
		toBeRemoved = new ArrayList<>();
		toBeAdded = new ArrayList<>();
	}

	/**
	 * Constructor method for class {@link IntegerStorage}. It sets stored value to
	 * initial value and initializes a list where future
	 * {@link IntegerStorageObserver} objects will be stored.
	 * 
	 * @param initialValue
	 *            Initial int value stored in this object
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
		observers = new ArrayList<>();
	}

	/**
	 * This method adds an {@link IntegerStorageObserver} to internal list of
	 * observers. Added observers will be informed if stored <code>value</code>
	 * changes.
	 * 
	 * @param observer
	 *            observer to be added
	 * @throws NullPointerException
	 *             if observer reference is <code>null</code>
	 */
	public void addObserver(IntegerStorageObserver observer) {
		Objects.requireNonNull(observer);
		if(isIterating) {
			toBeAdded.add(observer);
			return;
		}
		
		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	/**
	 * This method removes an {@link IntegerStorageObserver} from internal list of
	 * observer if present. Removed observer will not be informed if stored
	 * <code>value</code> changes.
	 * 
	 * @param observer
	 *            observer to be deleted
	 * @throws NullPointerException
	 *             if observer reference is <code>null</code>
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		if(isIterating) {
			toBeRemoved.add(observer);
			return;
		}
		
		observers.remove(Objects.requireNonNull(observer));
	}

	/**
	 * This method clears all {@link IntegerStorageObserver} objects from internal
	 * list. All current observers will no longer be informed if stored
	 * <code>value</code> changes.
	 */
	public void clearObservers() {
		observers.clear();
	}

	/**
	 * Getter method for stored int value.
	 * 
	 * @return stored value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Setter method for stored int value. Observers will not be informed if given
	 * argument is the same as stored value.
	 * 
	 * @param value
	 *            value to be set
	 */
	public void setValue(int value) {
		if (this.value != value) {
			IntegerStorageChange change = new IntegerStorageChange(this, this.value, value);
			this.value = value;
			
			if (observers != null) {
				isIterating = true;
				for (IntegerStorageObserver observer : observers) {
					observer.valueChanged(change);
				}
				isIterating = false;
				
				if(!toBeAdded.isEmpty()) {
					for(IntegerStorageObserver o : toBeAdded) {
						addObserver(o);
					}
					toBeAdded.clear();
				}
				
				if(!toBeRemoved.isEmpty()) {
					for(IntegerStorageObserver o: toBeRemoved) {
						removeObserver(o);
					}
					toBeRemoved.clear();
				}
			}
		}
	}
}
