/**
 * This package contains demonstration program for class
 * {@link hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.demo;