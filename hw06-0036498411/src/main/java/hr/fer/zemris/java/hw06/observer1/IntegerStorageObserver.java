package hr.fer.zemris.java.hw06.observer1;

/**
 * This interface describes an observer of {@link IntegerStorage} object.
 * Observer will be triggered when the subject (in this case
 * <code>IntegerStorage</code> object) calls the method
 * {@link IntegerStorage}{@link #valueChanged(IntegerStorage)} defined in this
 * interface.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface IntegerStorageObserver {

	/**
	 * This method describes an action which is performed after the observed subject
	 * informs the observer about some changes in its inner state.
	 * 
	 * @param istorage
	 *            observed {@link IntegerStorage}
	 * @throws NullPointerException
	 *             if istorage reference is <code>null</code>
	 */
	public void valueChanged(IntegerStorage istorage);
}
