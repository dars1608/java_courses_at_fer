/**
 * This package contains examples to demonsrate functionality of
 * {@link java.util.stream.Stream} api.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw06.demo4;