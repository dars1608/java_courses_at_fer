package hr.fer.zemris.java.hw06.observer2;

import java.util.Objects;

/**
 * This class represents an implementation of {@link IntegerStorageObserver}.
 * <p>
 * When it's triggered, it will write a message containing
 * observed value and its square on standard output.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class SquareValue implements IntegerStorageObserver {

	/**
	 * {@inheritDoc}
	 * <p>
	 * This implementation will write square of the integer stored in
	 * {@link IntegerStorage} on standard output.
	 * </p>
	 */
	@Override
	public void valueChanged(IntegerStorageChange change) {
		int value = Objects.requireNonNull(change).getValueAfter();

		String message = String.format("Provided new value: %d, square is %d", value, value * value);
		System.out.println(message);
	}

}
