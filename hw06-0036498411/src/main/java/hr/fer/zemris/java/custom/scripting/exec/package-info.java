/**
 * This package contains classes which are modeling the multistack structure.
 * 
 * @see hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.custom.scripting.exec;