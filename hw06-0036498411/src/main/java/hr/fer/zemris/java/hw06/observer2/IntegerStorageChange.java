package hr.fer.zemris.java.hw06.observer2;

import java.util.Objects;

/**
 * This class describes an object which will represent a change happened in
 * {@link IntegerStorage}. It contains a reference to IntegerStorage, the value
 * of stored integer before the change has occurred, and the new value of
 * currently stored integer.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class IntegerStorageChange {

	/** Internal storage which changes are observed */
	private final IntegerStorage istorage;
	/** Value previously stored in <code>istorage</code> */
	private final int valueBefore;
	/** New value stored in <code>istorage</code> */
	private final int valueAfter;

	/**
	 * Constructor method for class {@link IntegerStorageChange}.
	 * 
	 * @param istorage
	 *            observed subject
	 * @param valueBefore
	 *            Value previously stored in <code>istorage</code>
	 * @param valueAfter
	 *            New value stored in <code>istorage</code>
	 * @throws NullPointerException
	 *             if argument <code>istorage</code> is <code>null</code>
	 */
	public IntegerStorageChange(IntegerStorage istorage, int valueBefore, int valueAfter) {
		super();
		this.istorage = Objects.requireNonNull(istorage);
		this.valueBefore = valueBefore;
		this.valueAfter = valueAfter;
	}

	/**
	 * Getter method for the <code>valueBefore</code>.
	 * @return the valueBefore
	 */
	public int getValueBefore() {
		return valueBefore;
	}

	/**
	 * Getter method for the <code>valueAfter</code>.
	 * @return the valueAfter
	 */
	public int getValueAfter() {
		return valueAfter;
	}

	/**
	 * Getter method for the <code>istorage</code>.
	 * @return the istorage
	 */
	public IntegerStorage getIstorage() {
		return istorage;
	}

	
	
}
