package hr.fer.zemris.java.hw06.demo2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class represents fixed collection of primes. Primes are generated when
 * are needed. Primes will generate only when iterating.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PrimesCollection implements Iterable<Integer> {

	/** Number of primes which are stored in this collection */
	private final int size;

	/**
	 * Constructor method for class {@link PrimeCollection}.
	 * 
	 * @param size
	 *            size of collection
	 */
	public PrimesCollection(int size) {
		super();
		if (size < 1) {
			throw new IllegalArgumentException("Size must be greater than 0");
		}
		this.size = size;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new PrimesCollectionIterator();
	}

	/**
	 * Iterator class for the {@link PrimesCollection}. It generates primes while
	 * iterating.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	public class PrimesCollectionIterator implements Iterator<Integer> {

		/** Index of current prime */
		private int index;
		/** Last generated prime */
		private int lastPrime = 1;

		@Override
		public boolean hasNext() {
			return index < size;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @throws NoSuchElementException
		 *             if iterator came to an end
		 */
		@Override
		public Integer next() {
			if (hasNext()) {
				while (true) {
					lastPrime++;
					if (isPrime(lastPrime)) {
						break;
					}
				}

				index++;
				return lastPrime;
			}

			throw new NoSuchElementException("Iterator came to an end");
		}

	}

	/**
	 * This method is used internally to check if an integer number is prime or not.
	 * 
	 * @return <code>true</code> if number is prime, <code>false</code> otherwise
	 */
	private static boolean isPrime(int number) {
		if(number == 2) {
			return true;
		}
		
		if(number == 3) {
			return true;
		}
		
		if(number%2 == 0) {
			return false;
		}
		
		if(number%3 == 0) {
			return false;
		}
		
		int sqrt = (int) Math.sqrt(number) + 1;

		for (int i = 2; i < sqrt; i++) {
			if (number % i == 0) {
				return false;
			}
		}

		return true;
	}
}
