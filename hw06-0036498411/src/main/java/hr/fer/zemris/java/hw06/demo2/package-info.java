/**
 * This package contains class {@link PrimeCollection}, used for getting a
 * collection of certain number of prime integers.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw06.demo2;