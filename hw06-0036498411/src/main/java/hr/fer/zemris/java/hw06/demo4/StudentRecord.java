package hr.fer.zemris.java.hw06.demo4;

/**
 * This class represents a record of some student.
 * 
 * <p>
 * It contains informations such as:
 * <li>jmbag
 * <li>first name
 * <li>last name
 * <li>points achieved on midterm exam
 * <li>points achieved on final exam
 * <li>points achieved on laboratory exercises
 * <li>final grade
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class StudentRecord implements Comparable<StudentRecord> {

	/** Students unique identification number */
	private final String jmbag;
	/** Students first name */
	private final String firstName;
	/** Students last name */
	private final String lastName;
	/** Points achieved on midterm exam*/
	private final double midtermExamPoints;
	/** Points achieved on final exam*/
	private final double finalExamPoints;
	/** Points achieved on laboratory exercises*/
	private final double laboratoryExcersisesPoints;
	/** Students final grade */
	private final int finalGrade;

	/**
	 * Constructor method for class {@link StudentRecord}.
	 * 
	 * @param jmbag
	 *            Students unique identification number
	 * @param firstName
	 *            Students first name
	 * @param lastName
	 *            Students last name
	 * @param finalGrade
	 *            Students final grade
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, double midtermExamPoints, double finalExamPoints, double laboratoryExcersisesPoints, int finalGrade) {
		super();
		this.jmbag = jmbag;
		this.firstName = firstName;
		this.lastName = lastName;
		this.finalGrade = finalGrade;
		this.midtermExamPoints = midtermExamPoints;
		this.finalExamPoints = finalExamPoints;
		this.laboratoryExcersisesPoints = laboratoryExcersisesPoints;
	}

	/**
	 * Getter method for the <code>jmbag</code>.
	 * 
	 * @return the jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Getter method for the <code>firstName</code>.
	 * 
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Getter method for the <code>lastName</code>.
	 * 
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Getter method for the <code>finalGrade</code>.
	 * 
	 * @return the finalGrade
	 */
	public int getFinalGrade() {
		return finalGrade;
	}

	/**
	 * Getter method for the <code>midtermExamPoints</code>.
	 * @return the midtermExamPoints
	 */
	public double getMidtermExamPoints() {
		return midtermExamPoints;
	}

	/**
	 * Getter method for the <code>finalExamPoints</code>.
	 * @return the finalExamPoints
	 */
	public double getFinalExamPoints() {
		return finalExamPoints;
	}

	/**
	 * Getter method for the <code>laboratoryExcersisesPoints</code>.
	 * @return the laboratoryExcersisesPoints
	 */
	public double getLaboratoryExcersisesPoints() {
		return laboratoryExcersisesPoints;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((jmbag == null) ? 0 : jmbag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StudentRecord other = (StudentRecord) obj;
		if (jmbag == null) {
			if (other.jmbag != null) {
				return false;
			}
		} else if (!jmbag.equals(other.jmbag)) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(StudentRecord arg0) {
		return jmbag.compareTo(arg0.getJmbag());
	}
	
	@Override
	public String toString() {
		String ret = String.format("%s\t%s\t%s\t%s\t%s\t%s\t%d",
				jmbag, lastName, firstName, midtermExamPoints,
				finalExamPoints, laboratoryExcersisesPoints, finalGrade);
		
		return ret;
	}

}
