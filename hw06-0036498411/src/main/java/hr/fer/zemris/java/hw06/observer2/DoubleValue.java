package hr.fer.zemris.java.hw06.observer2;

import java.util.Objects;

/**
 * This class represents an implementation of {@link IntegerStorageObserver}. It
 * counts how many times observed value has been changed because it can be
 * triggered only certainn number of times.
 * <p>
 * When it's triggered it write a message on standard output containing the
 * value multiplied by 2
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DoubleValue implements IntegerStorageObserver {
	
	/** Counter*/
	private int count = 0;
	/** Maximal number of doubles*/
	private final int maxCount;
	
	/**
	 * Constructor method for class {@link DoubleValue}.
	 * 
	 * @param maxCount number of times observer can be triggered
	 */
	public DoubleValue(int maxCount) {
		if(maxCount<1) {
			throw new IllegalArgumentException("Argument must be a number greater than 0");
		}
		this.maxCount = maxCount;
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * This implementation will increase its inner counter, write a message on
	 * standard output containing the value multiplied by 2, and check how many
	 * times it has been triggered. If the number of times reaches the number which
	 * will be passed in constructor, it will automatically remove itself from
	 * observer list of observed subject
	 * </p>
	 */
	@Override
	public void valueChanged(IntegerStorageChange change) {
		int value = Objects.requireNonNull(change).getValueAfter();
		
		count++;
		String message = String.format("Double value: %d", value*2);
		System.out.println(message);
		
		if(count==maxCount) {
			change.getIstorage().removeObserver(this);
		}
	}

}
