package hr.fer.zemris.java.hw06.observer1;

import java.util.Objects;

/**
 * This class represents an implementation of {@link IntegerStorageObserver}. It
 * counts how many times observed value has been changed.
 * <p>
 * When it's triggered, it will increase its inner counter and write a message
 * containing the information about number of value changes since tracking on standard output.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ChangeCounter implements IntegerStorageObserver {

	/** Inner counter which counts changes of observed value */
	private int counter = 0;

	/**
	 * {@inheritDoc}
	 * <p>
	 * This implementation will increase its inner counter and write a message on
	 * standard output, containing the information about number of value changes
	 * since tracking.
	 * </p>
	 */
	@Override
	public void valueChanged(IntegerStorage istorage) {
		Objects.requireNonNull(istorage);

		counter++;
		String message = String.format("Number of value changes since tracking: %d", counter);
		System.out.println(message);
	}

}
