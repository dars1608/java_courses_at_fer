/**
 * This package contains classes used to create complete
 * <a href="https://en.wikipedia.org/wiki/Data_access_layer">data access
 * layer</a> used for communication with data source.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw15.dao;