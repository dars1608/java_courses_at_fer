package hr.fer.zemris.java.hw15.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represents a model of blog entry.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@NamedQueries({
	@NamedQuery(name="BlogEntry.getBlogByID",query="SELECT comment FROM BlogComment AS comment WHERE comment.blogEntry=:be AND comment.postedOn>:when"),
	@NamedQuery(name="BlogEntry.getAllBlogs", query="SELECT blog FROM BlogEntry AS blog")
})
@Entity
@Table(name="blog_entries")
@Cacheable(true)
public class BlogEntry {

	/** Maximal length of a blog*/
	public static final int MAX_TEXT_LENGTH = 4096;
	/** Maximal length of a title*/
	public static final int MAX_TITLE_LENGTH = 200;
	
	/** Unique id*/
	private Long id;
	/** Comments */
	private List<BlogComment> comments = new ArrayList<>();
	/** Date of creation*/
	private Date createdAt;
	/** Date of last modification*/
	private Date lastModifiedAt;
	/** Title*/
	private String title;
	/** Text*/
	private String text;
	/** User which created the entry*/
	private BlogUser creator;

	/**
	 * Getter method for the {@code id}.
	 * @return the id
	 */
	@Id @GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * Setter method for the {@code id}.
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Getter method for the {@code comments}.
	 * @return the comments
	 */
	@OneToMany(mappedBy="blogEntry",fetch=FetchType.LAZY, cascade=CascadeType.PERSIST, orphanRemoval=true)
	@OrderBy("postedOn")
	public List<BlogComment> getComments() {
		return comments;
	}
	
	public void setComments(List<BlogComment> comments) {
		this.comments = comments;
	}



	/**
	 * Getter method for the {@code createdAt}.
	 * @return the createdAt
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * Setter method for the {@code createdAt}.
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Getter method for the {@code lastModifiedAt}.
	 * @return the lastModifiedAt
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=true)
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}

	/**
	 * Setter method for the {@code lastModifiedAt}.
	 * @param lastModifiedAt the lastModifiedAt to set
	 */
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}

	/**
	 * Getter method for the {@code title}.
	 * @return the title
	 */
	@Column(length=MAX_TITLE_LENGTH,nullable=false)
	public String getTitle() {
		return title;
	}

	/**
	 * Setter method for the {@code title}.
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Getter method for the {@code text}.
	 * @return the text
	 */
	@Column(length=MAX_TEXT_LENGTH,nullable=false)
	public String getText() {
		return text;
	}

	/**
	 * Setter method for the {@code text}.
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Getter method for the {@code creator}.
	 * @return the creator
	 */
	@ManyToOne
	@JoinColumn(nullable=false)
	public BlogUser getCreator() {
		return creator;
	}

	/**
	 * Setter method for the {@code creator}.
	 * @param creator the creator to set
	 */
	public void setCreator(BlogUser creator) {
		this.creator = creator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogEntry other = (BlogEntry) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}