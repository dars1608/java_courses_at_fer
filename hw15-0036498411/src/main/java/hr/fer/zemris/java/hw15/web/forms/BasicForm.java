package hr.fer.zemris.java.hw15.web.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * This abstract class has logic for handling invalid form parameters.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class BasicForm {
	
	/** Map containing occurred exceptions. */
	protected Map<String, String> exceptions = new HashMap<>();

	/**
	 * Getter method for the {@code exceptions}.
	 * 
	 * @return the exceptions
	 */
	public Map<String, String> getExceptions() {
		return exceptions;
	}

	/**
	 * Setter method for the {@code exceptions}.
	 * 
	 * @param exceptions
	 *            the exceptions to set
	 */
	public void setExceptions(Map<String, String> exceptions) {
		this.exceptions = exceptions;
	}

	/**
	 * This method is used for obtaining the exception message under the given name.
	 * 
	 * @param name
	 *            name of the possible exception
	 * @return exception message or <code>null</code> if there's no such message
	 */
	public String getException(String name) {
		return exceptions.get(name);
	}

	/**
	 * This method is used for checking if there's any exceptions.
	 * 
	 * @return <code>true</code> if it is, <code>false</code> otherwise
	 */
	public boolean hasExceptions() {
		return !exceptions.isEmpty();
	}

	/**
	 * This method checks if there's a error message under given name.
	 * 
	 * @param ime
	 *            name of the possible exception
	 * @return <code>true</code> if there is such exception, <code>false</code>
	 *         otherwise
	 */
	public boolean hasException(String ime) {
		return exceptions.containsKey(ime);
	}
	
	/**
	 * This method is used internally for preparing the string data.
	 *
	 * @param s
	 *            string value
	 * @return trimmed value, if argument is <code>null</code> returns empty string
	 */
	protected String prepare(String s) {
		if (s == null)
			return "";
		return s.trim();
	}
	
	/**
	 * This method fills the form with parameters obtained through
	 * {@link HttpServletRequest object.
	 * 
	 * @param req
	 *            object with parameters
	 */
	public abstract void fillFromHttpRequest(HttpServletRequest req);

	/**
	 * This method is used for validating the form. If there are some false data,
	 * error messages are stored in exceptions map.
	 */
	public abstract void validate();
}
