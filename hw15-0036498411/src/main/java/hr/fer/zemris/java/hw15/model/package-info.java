/**
 * This package contains entities used in blog app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw15.model;