package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.web.forms.LoginForm;

/**
 * This class represents {@link HttpServlet} which provides login form and
 * obtains all current users from database.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/main")
public class LoginServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<BlogUser> users = DAOProvider.getDAO().getAllBlogUsers();
		req.setAttribute("users", users);
		req.getRequestDispatcher("/WEB-INF/pages/login.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LoginForm form = new LoginForm();
		form.fillFromHttpRequest(req);
		form.validate();

		if (!form.hasExceptions()) {
			req.getSession().setAttribute("current.user.id", form.getUser().getId());
			req.getSession().setAttribute("current.user.fn", form.getUser().getFirstName());
			req.getSession().setAttribute("current.user.ln", form.getUser().getLastName());
			req.getSession().setAttribute("current.user.nick", form.getNick());
			
			resp.sendRedirect(req.getContextPath() + "/servleti/main");
		} else {
			req.setAttribute("record", form);
			doGet(req, resp);
		}
	}
}
