package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.web.forms.BlogForm;

/**
 * This class represents {@link HttpServlet} used for preparing the form which
 * is used for editing the blog entry.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class EditBlogServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = (Long) req.getSession().getAttribute("current.user.id");
		Long eid = (Long) req.getAttribute("eid");

		if (id == null) {
			req.getRequestDispatcher("/WEB-INF/pages/unauthorized.jsp").forward(req, resp);
		} else if (eid == null) {
			req.setAttribute("errorMessage", "Upit nije moguće izvršiti.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
		} else {
			BlogEntry entry = DAOProvider.getDAO().getBlogEntry(eid);

			if (entry == null) {
				req.setAttribute("errorMessage", "Blog s identifikatorom " + eid + " ne postoji.");
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
				return;
			} else if (!entry.getCreator().getId().equals(id)) {
				req.setAttribute("errorMessage", "Nemate pristup uređivanju bloga s identifikatorom " + eid + ".");
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
				return;
			}

			BlogForm form = new BlogForm();
			form.fillFromRecord(entry);

			req.setAttribute("record", form);
			req.getRequestDispatcher("/WEB-INF/pages/edit.jsp").forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = (Long) req.getSession().getAttribute("current.user.id");
		if (id == null) {
			req.getRequestDispatcher("/WEB-INF/pages/unauthorized.jsp").forward(req, resp);
			return;
		}

		BlogForm form = new BlogForm();
		form.fillFromHttpRequest(req);
		form.validate();

		if (form.hasExceptions()) {
			req.setAttribute("record", form);
			req.getRequestDispatcher("/WEB-INF/pages/edit.jsp").forward(req, resp);

		} else {
			BlogEntry entry = DAOProvider.getDAO().getBlogEntry(Long.valueOf(form.getId()));
			if (entry == null) {
				req.setAttribute("errorMessage", "Blog s identifikatorom " + form.getId() + " ne postoji.");
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
				return;
			}

			if (!entry.getCreator().getId().equals(id)) {
				req.setAttribute("errorMessage",
						"Nemate pristup uređivanju bloga s identifikatorom " + form.getId() + ".");
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
				return;
			}

			form.fillIntoRecord(entry);
			String address = req.getContextPath() + "/servleti/author/"
					+ req.getSession().getAttribute("current.user.nick") + "/" + form.getId();
			resp.sendRedirect(address);
		}
	}
}
