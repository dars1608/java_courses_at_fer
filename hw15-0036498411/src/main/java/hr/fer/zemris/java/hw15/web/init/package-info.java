/**
 * This package contains initialization class of blog app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw15.web.init;