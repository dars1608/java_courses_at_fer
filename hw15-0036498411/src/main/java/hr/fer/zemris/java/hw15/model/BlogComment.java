package hr.fer.zemris.java.hw15.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class represents a model of blog comment.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@Entity
@Table(name = "blog_comments")
public class BlogComment {

	/** Maximal comment length*/
	public static final int MAX_MESSAGE_LENGTH = 4096;
	
	/** Unique id */
	private Long id;
	/** On which entry */
	private BlogEntry blogEntry;
	/** Users email */
	private String usersEMail;
	/** Message */
	private String message;
	/** Timestamp */
	private Date postedOn;

	/**
	 * Getter method for the {@code id}.
	 * 
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * Setter method for the {@code id}.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Getter method for the {@code blogEntry}.
	 * 
	 * @return the blogEntry
	 */
	@ManyToOne
	@JoinColumn(nullable = false)
	public BlogEntry getBlogEntry() {
		return blogEntry;
	}

	/**
	 * Setter method for the {@code blogEntry}.
	 * 
	 * @param blogEntry
	 *            the blogEntry to set
	 */
	public void setBlogEntry(BlogEntry blogEntry) {
		this.blogEntry = blogEntry;
	}

	/**
	 * Getter method for the {@code usersEMail}.
	 * 
	 * @return the usersEMail
	 */
	@Column(length = BlogUser.MAX_EMAIL_LENGTH, nullable = false)
	public String getUsersEMail() {
		return usersEMail;
	}

	/**
	 * Setter method for the {@code usersEMail}.
	 * 
	 * @param usersEMail
	 *            the usersEMail to set
	 */
	public void setUsersEMail(String usersEMail) {
		this.usersEMail = usersEMail;
	}

	/**
	 * Getter method for the {@code message}.
	 * 
	 * @return the message
	 */
	@Column(length = MAX_MESSAGE_LENGTH, nullable = false)
	public String getMessage() {
		return message;
	}

	/**
	 * Setter method for the {@code message}.
	 * 
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Getter method for the {@code postedOn}.
	 * 
	 * @return the postedOn
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * Setter method for the {@code postedOn}.
	 * 
	 * @param postedOn
	 *            the postedOn to set
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlogComment other = (BlogComment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}