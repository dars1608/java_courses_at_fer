package hr.fer.zemris.java.hw15.dao;

import java.util.List;

import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * This interface defines the communication between the application and the data
 * persistence layer. See
 * <a href="https://en.wikipedia.org/wiki/Data_access_object">this</a> for more
 * informations.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface DAO {

	/**
	 * This method is used for obtaining the {@link BlogEntry} object by its id.
	 * 
	 * @param id
	 *            primary key
	 * @return obtained entry
	 * @throws DAOException
	 *             if any error occurs
	 */
	BlogEntry getBlogEntry(Long id) throws DAOException;

	/**
	 * This method is used for obtaining the {@link BlogUser} object by its nick
	 * value.
	 * 
	 * @param nick
	 *            users nickname
	 * @return obtained user
	 * @throws DAOException
	 *             if any error occurs
	 */
	BlogUser getBlogUser(String nick) throws DAOException;

	/**
	 * This method is used for saving new {@link BlogUser} into the database.
	 * 
	 * @param user
	 *            user to be saved
	 * @throws DAOException
	 *             if any error occurs
	 */
	void saveBlogUser(BlogUser user) throws DAOException;

	/**
	 * This method is used for saving new {@link BlogEntry} into the database.
	 * 
	 * @param entry
	 *            entry to be saved
	 * @throws DAOException
	 *             if any error occurs
	 */
	void saveBlogEntry(BlogEntry entry) throws DAOException;

	/**
	 * This method is used for saving new {@link BlogComment} into the database.
	 * 
	 * @param entry
	 *            entry to be saved
	 * @throws DAOException
	 *             if any error occurs
	 */
	void saveBlogComment(BlogComment entry) throws DAOException;

	/**
	 * This method is used for obtaining the list of blog entries of some user.
	 * 
	 * @param id
	 *            user id
	 * @return list of {@link BlogEntry} objects
	 */
	List<BlogEntry> getBlogEntries(Long id);

	/**
	 * This method is used for obtaining the list of all blog entries.
	 * 
	 * @return list of {@link BlogEntry} objects
	 */
	List<BlogEntry> getAllBlogEntries();
	
	/**
	 * This method is used for obtaining the list of all users.
	 * 
	 * @return list of {@link BlogUser} objects
	 */
	List<BlogUser> getAllBlogUsers();

}