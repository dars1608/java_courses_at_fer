package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * This class represents {@link HttpServlet} used as a hub; it can redirect the
 * request to:
 * <ul>
 * <li>{@link NewBlogServlet} if the request path ends with /new
 * <li>{@link EditBlogServlet} if the request path ends with /edit
 * <li>{@link ShowBlogServlet} if the request path ends with a number
 * representing entry id
 * </ul>
 * 
 * <p>
 * Its default behavior is getting the authors blogs from the database.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/author/*")
public class AuthorServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	/** Pattern used for parsing author's name */
	private static final Pattern FOR_AUTHOR = Pattern.compile("/([^/]+)");
	/** Pattern used for accessing author page */
	private static final Pattern FOR_AUTHOR_ONLY = Pattern.compile("/([^/]+)/*$");
	/** Pattern determining if the accessed servlet is /new */
	private static final Pattern FOR_NEW = Pattern.compile("/[^/]+/new");
	/** Pattern determining if the accessed servlet is /edit */
	private static final Pattern FOR_EDIT = Pattern.compile("/[^/]+/edit");
	/** Pattern determining if the blog entry needs to be accessed */
	private static final Pattern FOR_EID = Pattern.compile("/[^/]+/([0-9]+)");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo();
		pathInfo = pathInfo == null ? "" : pathInfo.trim();

		String contextNick = null;
		Matcher m = FOR_AUTHOR.matcher(pathInfo);
		if (m.find()) {
			contextNick = m.group(1);
		} else {
			req.setAttribute("errorMessage", "Loš zahtjev.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		BlogUser user = DAOProvider.getDAO().getBlogUser(contextNick);
		if (user == null) {
			req.setAttribute("errorMessage", "Ne postoji korisnik " + contextNick + ".");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		Long id = (Long) req.getSession().getAttribute("current.user.id");
		if (id != null) {
			if (user.getId().equals(id)) {
				req.setAttribute("activeUser", true);
			}
		}

		if (FOR_NEW.matcher(pathInfo).matches()) {
			new NewBlogServlet().doGet(req, resp);
			return;

		} else if (FOR_EDIT.matcher(pathInfo).matches()) {
			req.setAttribute("eid", Long.valueOf(req.getParameter("eid")));
			new EditBlogServlet().doGet(req, resp);
			return;
		}

		m = FOR_EID.matcher(pathInfo);
		if (m.matches()) {
			Long eid = Long.valueOf(m.group(1));
			req.setAttribute("eid", eid);
			new ShowBlogServlet().doGet(req, resp);
			return;
		}

		m = FOR_AUTHOR_ONLY.matcher(pathInfo);
		if (!m.matches()) {
			req.setAttribute("errorMessage", "Loš zahtjev.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		List<BlogEntry> blogs = DAOProvider.getDAO().getBlogEntries(user.getId());
		req.setAttribute("blogs", blogs);
		req.setAttribute("contextNick", contextNick);
		req.getRequestDispatcher("/WEB-INF/pages/author.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo().trim();

		if (FOR_NEW.matcher(pathInfo).matches()) {
			new NewBlogServlet().doPost(req, resp);
		} else if (FOR_EDIT.matcher(pathInfo).matches()) {
			new EditBlogServlet().doPost(req, resp);
		} else if (FOR_EID.matcher(pathInfo).matches()) {
			new ShowBlogServlet().doPost(req, resp);
		} else {
			super.doPost(req, resp);
		}
	}
}
