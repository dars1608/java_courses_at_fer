package hr.fer.zemris.java.hw15.web.forms;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * This class represents a form of {@link BlogComment} entity.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CommentForm extends BasicForm {

	/** On which entry */
	private BlogEntry blogEntry;
	/** Users email */
	private String usersEMail;
	/** Message */
	private String message;
	/** Timestamp */
	private Date postedOn;

	/**
	 * Getter method for the {@code blogEntry}.
	 * 
	 * @return the blogEntry
	 */
	public BlogEntry getBlogEntry() {
		return blogEntry;
	}

	/**
	 * Setter method for the {@code blogEntry}.
	 * 
	 * @param blogEntry
	 *            the blogEntry to set
	 */
	public void setBlogEntry(BlogEntry blogEntry) {
		this.blogEntry = blogEntry;
	}

	/**
	 * Getter method for the {@code usersEMail}.
	 * 
	 * @return the usersEMail
	 */
	public String getUsersEMail() {
		return usersEMail;
	}

	/**
	 * Setter method for the {@code usersEMail}.
	 * 
	 * @param usersEMail
	 *            the usersEMail to set
	 */
	public void setUsersEMail(String usersEMail) {
		this.usersEMail = usersEMail;
	}

	/**
	 * Getter method for the {@code message}.
	 * 
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Setter method for the {@code message}.
	 * 
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Getter method for the {@code postedOn}.
	 * 
	 * @return the postedOn
	 */
	public Date getPostedOn() {
		return postedOn;
	}

	/**
	 * Setter method for the {@code postedOn}.
	 * 
	 * @param postedOn
	 *            the postedOn to set
	 */
	public void setPostedOn(Date postedOn) {
		this.postedOn = postedOn;
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		usersEMail = prepare(req.getParameter("email"));
		message = prepare(req.getParameter("message"));
		postedOn = new Date();
	}

	/**
	 * This method is used after the validation for filling the blog comment record
	 * with informations from the form.
	 * 
	 * @param r
	 *            {@link BlogComment} object to be filled
	 * @see CommentForm#validate()
	 */
	public void fillIntoRecord(BlogComment r) {
		r.setId(null);
		r.setUsersEMail(usersEMail);
		r.setMessage(message);
		r.setPostedOn(postedOn);
		r.setBlogEntry(blogEntry);
	}

	@Override
	public void validate() {
		exceptions.clear();

		if (message.isEmpty()) {
			exceptions.put("message", "Napišite nešto ili nemojte ništa!");
		} else if (message.length() > BlogComment.MAX_MESSAGE_LENGTH) {
			exceptions.put("message",
					String.format("Poruka smije sadržavati maksimalno %d znakova", BlogComment.MAX_MESSAGE_LENGTH));
		}
		
		if(usersEMail.isEmpty()) {
			exceptions.put("email", "EMail je obavezan!");
		} else {
			int l = usersEMail.length();
			int p = usersEMail.indexOf('@');
			if (l < 3 || l > BlogUser.MAX_EMAIL_LENGTH || p == -1 || p == 0 || p == l - 1) {
				exceptions.put("email", "EMail nije ispravnog formata.");
			}
		}
	}
}
