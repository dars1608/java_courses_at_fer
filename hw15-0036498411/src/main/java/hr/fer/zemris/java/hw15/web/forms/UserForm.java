package hr.fer.zemris.java.hw15.web.forms;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.util.PasswordUtil;

/**
 * This class represents a form used for creating new blog user.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class UserForm extends BasicForm {

	/** Minimal password length*/
	private static final int MIN_PASSWORD_LENGTH = 8;
	/** Encoder for checking the nickname*/
	private static final CharsetEncoder ASCII_ENCODER = 
			Charset.forName("US-ASCII").newEncoder();
	
	/** Users last name */
	private String lastName;
	/** Users first name */
	private String firstName;
	/** Users e-mail */
	private String email;
	/** Users nick */
	private String nick;
	/** Users password */
	private String password;

	/**
	 * Getter method for the {@code lastName}.
	 * 
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter method for the {@code lastName}.
	 * 
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter method for the {@code firstName}.
	 * 
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter method for the {@code firstName}.
	 * 
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter method for the {@code email}.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter method for the {@code email}.
	 * 
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter method for the {@code nick}.
	 * 
	 * @return the nick
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * Setter method for the {@code nick}.
	 * 
	 * @param nick
	 *            the nick to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Getter method for the {@code password}.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter method for the {@code password}.
	 * 
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.firstName = prepare(req.getParameter("firstName"));
		this.lastName = prepare(req.getParameter("lastName"));
		this.email = prepare(req.getParameter("email"));
		this.nick = prepare(req.getParameter("nick"));
		this.password = prepare(req.getParameter("password"));
	}

	/**
	 * This method fills the form based on given {@link BlogUser} object.
	 * 
	 * @param r
	 *            blog user object
	 */
	public void fillFromRecord(BlogUser r) {
		this.firstName = r.getFirstName();
		this.lastName = r.getLastName();
		this.email = r.getEmail();
		this.nick = r.getNick();
		this.password = "";

	}

	/**
	 * This method is used after the validation for filling the blog user record
	 * with informations from the form.
	 * 
	 * @param r
	 *            {@link BlogUser} object to be filled
	 * @see UserForm#validate()
	 */
	public void fillIntoRecord(BlogUser r) {
		r.setFirstName(this.firstName);
		r.setLastName(this.lastName);
		r.setEmail(this.email);
		r.setNick(this.nick);
		r.setPasswordHash(PasswordUtil.getPasswordHash(this.password));
	}

	@Override
	public void validate() {
		exceptions.clear();

		if (this.firstName.isEmpty()) {
			exceptions.put("firstName", "Ime je obavezno!");
		} else if (this.firstName.length() > BlogUser.MAX_FN_LENGTH) {
			exceptions.put("firstName", String.format("Ime može sadržavati maksimalno %d znakova.", BlogUser.MAX_FN_LENGTH));
		}

		if (this.lastName.isEmpty()) {
			exceptions.put("lastName", "Prezime je obavezno!");
		} else if (this.lastName.length() > BlogUser.MAX_LN_LENGTH) {
			exceptions.put("lastName", String.format("Prezime može sadržavati maksimalno %d znakova.", BlogUser.MAX_LN_LENGTH));
		}

		if (this.email.isEmpty()) {
			exceptions.put("email", "EMail je obavezan!");
		} else {
			int l = email.length();
			int p = email.indexOf('@');
			if (l < 3 || l > BlogUser.MAX_EMAIL_LENGTH || p == -1 || p == 0 || p == l - 1) {
				exceptions.put("email", "EMail nije ispravnog formata.");
			}
		}

		if (this.nick.isEmpty()) {
			exceptions.put("nick", "Nadimak je obavezan");
		} else if (this.nick.length() > BlogUser.MAX_NICK_LENGTH) {
			exceptions.put("nick", String.format("Nadimak može sadržavati maksimalno %d znakova.", BlogUser.MAX_NICK_LENGTH));
		} else if(!chechIfAscii(nick)) {
			exceptions.put("nick", "Nadimak može sadržavati isključivo znakove engleske abecede i arapske znamenke");
		} else {
			if (DAOProvider.getDAO().getBlogUser(this.nick) != null) {
				exceptions.put("nick", "Korisnik s tim nadimkom već postoji. Izaberite novi.");
			}
		}

		if (this.password.isEmpty()) {
			exceptions.put("password", "Lozinka je obavezna.");
		} else {
			if (this.password.length() < MIN_PASSWORD_LENGTH) {
				exceptions.put("password", String.format("Lozinka mora imati barem %d znakova.", MIN_PASSWORD_LENGTH));
			}
		}
	}

	/**
	 * This method is used internally for checking if nickname is legal (eng. alphabet or digit).
	 * 
	 * @param s string to be checked
	 * @return {@code true} if if it's valid, {@code false} otherwise
	 */
	private static boolean chechIfAscii(String s) {
		char[] chars = s.toCharArray();
		
		for(char c : chars) {
			if(!(ASCII_ENCODER.canEncode(c) || Character.isDigit(c))) {
				return false;
			}
		}
		
		return true;
	}
}
