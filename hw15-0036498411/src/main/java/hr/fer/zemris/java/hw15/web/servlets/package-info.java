/**
 * This package contains web servlets used in blog application.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw15.web.servlets;