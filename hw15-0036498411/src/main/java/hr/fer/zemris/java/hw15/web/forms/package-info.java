/**
 * This package contains forms used in blog app.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw15.web.forms;