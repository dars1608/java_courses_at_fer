/**
 * This package contains classes used for modeling DAO interface with JPA API.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw15.dao.jpa;