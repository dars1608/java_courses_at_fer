package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.web.forms.UserForm;

/**
 * This class represents {@link HttpServlet} used for providing the form for
 * creating new application user.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@WebServlet(urlPatterns = "/servleti/register")
public class RegisterServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;
	/** Flag determining if the user has been created */
	private boolean created = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (created) {
			created = false;
			resp.sendRedirect(req.getContextPath() + "/servleti/main");
		} else {
			req.getRequestDispatcher("/WEB-INF/pages/register.jsp").forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserForm form = new UserForm();
		form.fillFromHttpRequest(req);
		form.validate();

		if (!form.hasExceptions()) {
			BlogUser user = new BlogUser();
			form.fillIntoRecord(user);
			DAOProvider.getDAO().saveBlogUser(user);
			created = true;
		} else {
			req.setAttribute("record", form);
		}

		doGet(req, resp);
	}
}
