package hr.fer.zemris.java.hw15.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * This class represents a model of blog user.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "BlogUser.getByNick", query = "SELECT user FROM BlogUser AS user WHERE user.nick=:nick"),
		@NamedQuery(name = "BlogUser.getBlogs", query = "SELECT blog FROM BlogEntry AS blog WHERE blog.creator.id=:cuid"),
		@NamedQuery(name = "BlogUser.getAll", query = "SELECT user FROM BlogUser AS user")
})
@Table(name = "blog_users")
public class BlogUser {

	/** Maximal first name length*/
	public static final int MAX_FN_LENGTH = 30;
	/** Maximal last name length*/
	public static final int MAX_LN_LENGTH = 50;
	/** Maximal nickname length*/
	public static final int MAX_NICK_LENGTH = 30;
	/** Maximal email length*/
	public static final int MAX_EMAIL_LENGTH = 254;
	
	/** Unique id */
	private Long id;
	/* First name */
	private String firstName;
	/** Last name */
	private String lastName;
	/** Unique nickname */
	private String nick;
	/** E-mail address */
	private String email;
	/** Password hash */
	private String passwordHash;
	/** Users blogs */
	private List<BlogEntry> blogs;

	/**
	 * Getter method for the {@code id}.
	 * 
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	/**
	 * Setter method for the {@code id}.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Getter method for the {@code firstName}.
	 * 
	 * @return the firstName
	 */
	@Column(length = MAX_FN_LENGTH, nullable = false)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Setter method for the {@code firstName}.
	 * 
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter method for the {@code lastName}.
	 * 
	 * @return the lastName
	 */
	@Column(length = MAX_LN_LENGTH, nullable = false)
	public String getLastName() {
		return lastName;
	}

	/**
	 * Setter method for the {@code lastName}.
	 * 
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter method for the {@code nick}.
	 * 
	 * @return the nick
	 */
	@Column(length = MAX_NICK_LENGTH, nullable = false, unique = true)
	public String getNick() {
		return nick;
	}

	/**
	 * Setter method for the {@code nick}.
	 * 
	 * @param nick
	 *            the nick to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Getter method for the {@code email}.
	 * 
	 * @return the email
	 */
	@Column(length = MAX_EMAIL_LENGTH, nullable = false)
	public String getEmail() {
		return email;
	}

	/**
	 * Setter method for the {@code email}.
	 * 
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter method for the {@code passwordHash}.
	 * 
	 * @return the passwordHash
	 */
	@Column(length = 40, nullable = false)
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * Setter method for the {@code passwordHash}.
	 * 
	 * @param passwordHash
	 *            the passwordHash to set
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	/**
	 * Getter method for the {@code blogs}.
	 * 
	 * @return the blogs
	 */
	@OneToMany(mappedBy = "creator", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@OrderBy("postedOn")
	public List<BlogEntry> getBlogs() {
		return blogs;
	}

	/**
	 * Setter method for the {@code blogs}.
	 * 
	 * @param blogs
	 *            the blogs to set
	 */
	public void setBlogs(List<BlogEntry> blogs) {
		this.blogs = blogs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		BlogUser other = (BlogUser) obj;

		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}

		return true;
	}
}
