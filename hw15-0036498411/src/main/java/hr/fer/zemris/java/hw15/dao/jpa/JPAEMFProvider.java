package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManagerFactory;

/**
 * This class represents an {@link EntityManagerFactory} provider.
 * 
 * @author marcupic
 * @version 1.0
 */
public class JPAEMFProvider {

	/** Current {@link EntityManagerFactory} object (singleton)*/
	public static EntityManagerFactory emf;
	
	/**
	 * Getter method for the current entity manager factory.
	 * 
	 * @return {@link EntityManagerFactory} object
	 */
	public static EntityManagerFactory getEmf() {
		return emf;
	}
	
	/**
	 * Setter method for the current entity manager factory.
	 * 
	 * @param emf {@link EntityManagerFactory} object to be set
	 */
	public static void setEmf(EntityManagerFactory emf) {
		JPAEMFProvider.emf = emf;
	}
}