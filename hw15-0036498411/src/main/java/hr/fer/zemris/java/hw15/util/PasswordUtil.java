package hr.fer.zemris.java.hw15.util;

import static java.security.MessageDigest.getInstance;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * This class represents utility method used for password validation.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class PasswordUtil {

	/**
	 * This method is used for calculating password sha-1 hash.
	 * 
	 * @param password password as string
	 * @return password hash as string
	 */
	public static String getPasswordHash(String password) {
		MessageDigest sha = null;

		try {
			sha = getInstance("SHA-1");
		} catch (NoSuchAlgorithmException ignorable) {
		}
		
		sha.update(password.getBytes());
		return bytetohex(sha.digest());
		
	}
	
	/**
	 * This method is used for converting byte array to hex-encoded String.
	 * 
	 * @param byteArray
	 *            byte array to be converted
	 * @return converted hex-encoded String
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	private static String bytetohex(byte[] byteArray) {
		Objects.requireNonNull(byteArray, "Argument must be non null.");

		char[] retArr = new char[byteArray.length * 2];
		for (int i = 0; i < byteArray.length; i++) {
			byte big = (byte) ((byteArray[i] >> 4) & 0x0f);
			byte small = (byte) (byteArray[i] & 0x0f);

			retArr[2 * i] = getHex(big);
			retArr[2 * i + 1] = getHex(small);
		}

		return new String(retArr);
	}

	/**
	 * This method is used internally to convert nibble (half of byte) argument to
	 * hexadecimal number.
	 * 
	 * @param b
	 *            nibble to convert
	 * @return converted hexadecimal number
	 */
	private static char getHex(byte b) {
		return b < 10 ? (char) ('0' + b) : (char) ('a' + b - 10);
	}
}
