package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.web.forms.BlogForm;

/**
 * This class represents {@link HttpServlet} used for providing the form for
 * creating new blog entry.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class NewBlogServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = (Long) req.getSession().getAttribute("current.user.id");
		if (id == null) {
			req.getRequestDispatcher("/WEB-INF/pages/unauthorized.jsp").forward(req, resp);
		} else {
			req.setAttribute("contextNick", req.getSession().getAttribute("current.user.nick"));
			req.getRequestDispatcher("/WEB-INF/pages/new.jsp").forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = (Long) req.getSession().getAttribute("current.user.id");
		if (id == null) {
			req.getRequestDispatcher("/WEB-INF/pages/unauthorized.jsp").forward(req, resp);
			return;
		}

		BlogForm form = new BlogForm();
		form.fillFromHttpRequest(req);
		form.validate();

		if (form.hasExceptions()) {
			req.setAttribute("record", form);
			req.getRequestDispatcher("/WEB-INF/pages/new.jsp").forward(req, resp);

		} else {
			BlogEntry entry = new BlogEntry();
			form.fillIntoRecord(entry);
			DAOProvider.getDAO().saveBlogEntry(entry);

			String address = req.getContextPath() + "/servleti/author/"
					+ req.getSession().getAttribute("current.user.nick");
			resp.sendRedirect(address);
		}

	}

}
