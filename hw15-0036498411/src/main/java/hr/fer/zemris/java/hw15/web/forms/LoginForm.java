package hr.fer.zemris.java.hw15.web.forms;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.util.PasswordUtil;

/**
 * This class represents a form used for login.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LoginForm extends BasicForm {

	/** Users nickname */
	private String nick;
	/** User password */
	private String password;
	/** User*/
	private BlogUser user;

	/**
	 * Getter method for the {@code nick}.
	 * 
	 * @return the nick
	 */
	public String getNick() {
		return nick;
	}

	/**
	 * Setter method for the {@code nick}.
	 * 
	 * @param nick
	 *            the nick to set
	 */
	public void setNick(String nick) {
		this.nick = nick;
	}

	/**
	 * Getter method for the {@code password}.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter method for the {@code password}.
	 * 
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * Getter method for the {@code user}.
	 * @return the user
	 */
	public BlogUser getUser() {
		return user;
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		this.nick = prepare(req.getParameter("nick"));
		this.password = prepare(req.getParameter("password"));

	}

	@Override
	public void validate() {
		if (nick.isEmpty()) {
			exceptions.put("nick", "Nadimak je obavezan.");
		}

		if (password.isEmpty()) {
			exceptions.put("password", "Lozinka je obavezna.");
		}

		if (hasExceptions()) {
			return;
		}

		BlogUser user = DAOProvider.getDAO().getBlogUser(nick);
		if (user == null) {
			exceptions.put("nick", "Korisnik s tim nicknameom ne postoji.");
			return;
		}

		String passwordHash = PasswordUtil.getPasswordHash(password);
		String realPasswordHash = user.getPasswordHash();

		if (!passwordHash.equals(realPasswordHash)) {
			exceptions.put("password", "Pogrešna lozinka.");
		} else {
			this.user = user;
		}

	}

}
