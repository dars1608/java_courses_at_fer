package hr.fer.zemris.java.hw15.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;
import hr.fer.zemris.java.hw15.web.forms.CommentForm;

/**
 * This class represents {@link HttpServlet} used for obtaining the blog entry
 * content and comments. It also provides form for posting new comment.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ShowBlogServlet extends HttpServlet {

	/** Serial version */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = (Long) req.getAttribute("eid");

		if (id != null) {
			BlogEntry blogEntry = DAOProvider.getDAO().getBlogEntry(id);
			if (blogEntry != null) {
				req.setAttribute("blogEntry", blogEntry);
			} else {
				req.setAttribute("errorMessage", "Upit nije moguće izvršiti. Identifikator bloga nije ispravan.");
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
				return;
			}

			String nick = (String) req.getSession().getAttribute("current.user.nick");
			if (nick != null) {
				BlogUser user = DAOProvider.getDAO().getBlogUser(nick);
				if (user.getId() == blogEntry.getCreator().getId()) {
					req.setAttribute("activeUser", true);
				}
			}
		} else {
			req.setAttribute("errorMessage", "Upit nije moguće izvršiti. Identifikator bloga nije ispravan.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		req.getRequestDispatcher("/WEB-INF/pages/show.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Long id = (Long) req.getSession().getAttribute("current.user.id");
		CommentForm form = new CommentForm();

		form.fillFromHttpRequest(req);
		if (id != null) {
			form.setUsersEMail(DAOProvider.getDAO()
					.getBlogUser((String) req.getSession().getAttribute("current.user.nick")).getEmail());
		}

		form.validate();

		String sid = req.getParameter("eid");
		if (sid == null) {
			req.setAttribute("errorMessage", "Upit nije moguće izvršiti. Nije poslan identifikator bloga.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		Long eid = null;
		try {
			eid = Long.valueOf(sid);
		} catch (NumberFormatException ex) {
			req.setAttribute("errorMessage", "Upit nije moguće izvršiti. Identifikator bloga nije ispravan.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("eid", eid);
		BlogEntry entry = DAOProvider.getDAO().getBlogEntry(eid);
		if (entry == null) {
			req.setAttribute("errorMessage", "Upit nije moguće izvršiti. Ne postoji entry s takvim identifikatorom.");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			return;
		}

		form.setBlogEntry(entry);
		if (!form.hasExceptions()) {
			BlogComment comment = new BlogComment();
			form.fillIntoRecord(comment);
			DAOProvider.getDAO().saveBlogComment(comment);

			resp.sendRedirect(req.getContextPath() + req.getServletPath() + req.getPathInfo());
		} else {
			req.setAttribute("record", form);
			doGet(req, resp);
		}
	}
}
