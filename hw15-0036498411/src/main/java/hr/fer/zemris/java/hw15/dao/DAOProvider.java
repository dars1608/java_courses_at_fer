package hr.fer.zemris.java.hw15.dao;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

/**
 * This class describes a provider of {@link DAO} objects.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DAOProvider {

	/** Current dao implementation */
	private static DAO dao = loadDAO();
	
	/**
	 * This method returns current implementation of {@link DAO} interface.
	 * 
	 * @return DAO object
	 */
	public static DAO getDAO() {
		return dao;
	}
	
	/**
	 * This is the helper method used for loading the {@link DAO} object into the
	 * memory.
	 * 
	 * @return initialized DAO object
	 */
	private static DAO loadDAO() {
		DAO dao = null;
		InputStream is = DAO.class.getResourceAsStream("dao.properties");
		Properties p = new Properties();

		try {
			p.load(is);
		} catch (IOException ex) {
			throw new DAOException("Could not load dao.properties file.", ex);
		}
		String fqcn = p.getProperty("daoimpl");

		try {
			Class<?> referenceToClass = DAO.class.getClassLoader().loadClass(fqcn);
			dao = (DAO) referenceToClass.getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException ex) {

			throw new DAOException("Could not load DAO implementation.", ex);
		}
		
		return dao;
	}
}