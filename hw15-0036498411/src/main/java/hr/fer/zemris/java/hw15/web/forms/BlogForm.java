package hr.fer.zemris.java.hw15.web.forms;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import hr.fer.zemris.java.hw15.dao.DAOProvider;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * This class represents a form of {@link BlogEntry} entity.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class BlogForm extends BasicForm {

	/** Unique id */
	private String id;
	/** Date of creation */
	private Date createdAt;
	/** Date of last modification */
	private Date lastModifiedAt;
	/** Title */
	private String title;
	/** Text */
	private String text;
	/** User which created the entry */
	private BlogUser creator;

	/**
	 * Getter method for the {@code id}.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter method for the {@code id}.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void String(String id) {
		this.id = id;
	}

	/**
	 * Getter method for the {@code createdAt}.
	 * 
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * Setter method for the {@code createdAt}.
	 * 
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * Getter method for the {@code lastModifiedAt}.
	 * 
	 * @return the lastModifiedAt
	 */
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}

	/**
	 * Setter method for the {@code lastModifiedAt}.
	 * 
	 * @param lastModifiedAt
	 *            the lastModifiedAt to set
	 */
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}

	/**
	 * Getter method for the {@code title}.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Setter method for the {@code title}.
	 * 
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Getter method for the {@code text}.
	 * 
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * Setter method for the {@code text}.
	 * 
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Getter method for the {@code creator}.
	 * 
	 * @return the creator
	 */
	public BlogUser getCreator() {
		return creator;
	}

	/**
	 * Setter method for the {@code creator}.
	 * 
	 * @param creator
	 *            the creator to set
	 */
	public void setCreator(BlogUser creator) {
		this.creator = creator;
	}

	@Override
	public void fillFromHttpRequest(HttpServletRequest req) {
		createdAt = new Date();
		lastModifiedAt = new Date();
		id = prepare(req.getParameter("id"));
		title = prepare(req.getParameter("title"));
		text = prepare(req.getParameter("text"));
		creator = DAOProvider.getDAO().getBlogUser((String) req.getSession().getAttribute("current.user.nick"));
	}

	/**
	 * This method fills the form based on given {@link BlogEntry} object.
	 * 
	 * @param r
	 *            blog entry object
	 */
	public void fillFromRecord(BlogEntry r) {
		this.id = r.getId().toString();
		this.createdAt = r.getCreatedAt();
		this.lastModifiedAt = r.getLastModifiedAt();
		this.title = r.getTitle();
		this.text = r.getText();
		this.creator = r.getCreator();
	}

	/**
	 * This method is used after the validation for filling the blog record with
	 * informations from the form.
	 * 
	 * @param r
	 *            {@link BlogEntry} object to be filled
	 * @see BlogForm#validate()
	 */
	public void fillIntoRecord(BlogEntry r) {
		if (this.id.isEmpty()) {
			r.setId(null);
		} else {
			r.setId(Long.valueOf(this.id));
		}

		if (r.getCreatedAt() == null) {
			r.setCreatedAt(this.createdAt);
		}

		r.setLastModifiedAt(this.lastModifiedAt);
		r.setTitle(this.title);
		r.setText(this.text);
		r.setCreator(this.creator);
	}

	@Override
	public void validate() {
		exceptions.clear();
		
		if (title.isEmpty()) {
			exceptions.put("title", "Naslov je obavezan.");
		} else if(title.length() > BlogEntry.MAX_TITLE_LENGTH) {
			exceptions.put("title", "Naslov smije sadržavati maksimalno 200 znakova");
		}

		if (text.isEmpty()) {
			exceptions.put("text", "Napišite barem nešto!");
		} else if (text.length() > BlogEntry.MAX_TEXT_LENGTH) {
			exceptions.put("text", "Blog može sadržavati maksimalno 4096 znakova");
		}
	}

}
