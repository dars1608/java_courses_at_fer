package hr.fer.zemris.java.hw15.dao.jpa;

import java.util.List;

import hr.fer.zemris.java.hw15.dao.DAO;
import hr.fer.zemris.java.hw15.dao.DAOException;
import hr.fer.zemris.java.hw15.model.BlogComment;
import hr.fer.zemris.java.hw15.model.BlogEntry;
import hr.fer.zemris.java.hw15.model.BlogUser;

/**
 * This class represents an implementation of {@link DAO} interface which uses
 * <a href="https://en.wikipedia.org/wiki/Java_Persistence_API">JPA</a>
 * technology.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class JPADAOImpl implements DAO {

	@Override
	public BlogEntry getBlogEntry(Long id) throws DAOException {
		try {
			return JPAEMProvider.getEntityManager().find(BlogEntry.class, id);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public BlogUser getBlogUser(String nick) throws DAOException {
		try {
			return JPAEMProvider.getEntityManager()
					.createNamedQuery("BlogUser.getByNick", BlogUser.class)
					.setParameter("nick", nick)
					.getResultStream()
					.findFirst()
					.orElse(null);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void saveBlogUser(BlogUser user) throws DAOException {
		try {
			JPAEMProvider.getEntityManager().persist(user);
		} catch (Exception e) {
			throw new DAOException(e);
		}

	}

	@Override
	public List<BlogEntry> getBlogEntries(Long id) {
		try {
			return JPAEMProvider.getEntityManager()
					.createNamedQuery("BlogUser.getBlogs", BlogEntry.class)
					.setParameter("cuid", id)
					.getResultList();
		} catch(Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<BlogEntry> getAllBlogEntries() {
		try {
			return JPAEMProvider.getEntityManager()
					.createNamedQuery("BlogEntry.getAllBlogs", BlogEntry.class)
					.getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void saveBlogEntry(BlogEntry entry) throws DAOException {
		try {
			JPAEMProvider.getEntityManager().persist(entry);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public void saveBlogComment(BlogComment entry) throws DAOException {
		try {
			JPAEMProvider.getEntityManager().persist(entry);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<BlogUser> getAllBlogUsers() {
		try {
			return JPAEMProvider.getEntityManager()
					.createNamedQuery("BlogUser.getAll", BlogUser.class)
					.getResultList();
		} catch (Exception e) {
			throw new DAOException(e);
		}
		
	}

}