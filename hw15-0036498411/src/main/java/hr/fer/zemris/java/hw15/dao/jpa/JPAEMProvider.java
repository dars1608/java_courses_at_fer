package hr.fer.zemris.java.hw15.dao.jpa;

import javax.persistence.EntityManager;

import hr.fer.zemris.java.hw15.dao.DAOException;

/**
 * This class represents jpa entity manager provider.
 * 
 * @author marcupic
 * @version 1.0
 */
public class JPAEMProvider {

	/** Map used for storing entity managers for active thread*/
	private static ThreadLocal<EntityManager> locals = new ThreadLocal<>();

	/**
	 * Getter method for the entity manager.
	 * 
	 * @return {@link EntityManager} object
	 */
	public static EntityManager getEntityManager() {
		EntityManager em = locals.get();
		
		if (em == null) {
			em = JPAEMFProvider.getEmf().createEntityManager();
			em.getTransaction().begin();
			locals.set(em);
		}
		
		return em;
	}

	/**
	 * This method is used for closing the entity manager.
	 * 
	 * @throws DAOException if any exception occurs
	 */
	public static void close() throws DAOException {
		EntityManager em = locals.get();
		if (em == null) {
			return;
		}
		DAOException dex = null;
		
		try {
			em.getTransaction().commit();
		} catch (Exception ex) {
			dex = new DAOException("Unable to commit transaction.", ex);
		}
		
		try {
			em.close();
		} catch (Exception ex) {
			if (dex != null) {
				dex = new DAOException("Unable to close entity manager.", ex);
			}
		}
		
		locals.remove();
		if (dex != null)
			throw dex;
	}

}