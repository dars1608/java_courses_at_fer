<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Moj blog</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mystyle.css">
</head>
<body>

    <header>
        <c:choose>
            <c:when test="${sessionScope['current.user.id'] != null}">
                <span> <c:out value="${sessionScope['current.user.fn']}" /> <c:out
                        value="${sessionScope['current.user.ln']}" /> <a
                    href="${pageContext.request.contextPath}/servleti/author/${sessionScope['current.user.nick']}">Moj blog</a> <a
                    href="${pageContext.request.contextPath}/servleti/logout">Odjavi se</a>
                </span>
            </c:when>
            <c:otherwise>
<span>Niste prijavljeni</span>
</c:otherwise>
        </c:choose>
    </header>

    <h1>Moj blog</h1>
    <c:if test="${sessionScope['current.user.id'] == null}">
        <h3>Prijavi se:</h3>

        <form action="main" method="post">
            <div>
                <span class="formLabel">Nadimak</span><input type="text" name="nick"
                    value='<c:out value="${record.nick}"/>' size="20">
                <c:if test="${record.hasException('nick')}">
                    <span class="greska"><c:out value="${record.getException('nick')}" /></span>
                </c:if>
            </div>

            <div>
                <span class="formLabel">Lozinka</span><input type="password" name="password" size="20">
                <c:if test="${record.hasException('password')}">
                    <span class="greska"><c:out value="${record.getException('password')}" /></span>
                </c:if>
            </div>

            <div class="formControls">
                <span class="formLabel">&nbsp;</span> <input type="submit" name="metoda" value="Prijavi se">
            </div>
        </form>

        <div>
            <span>Ako nemate svoj korisnički račun, stvorite ga </span><a href="register">ovdje.</a>
        </div>
    </c:if>

    <div>
        <h3>Popis registriranih korisnika:</h3>
    </div>

    <div>
        <c:forEach var="user" items="${users}">
            <div>
                <span><a href="${pageContext.request.contextPath}/servleti/author/${user.nick}">${user.firstName} ${user.lastName }</a></span>
            </div>
        </c:forEach>
    </div>
</body>
</html>