<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Uredi blog</title>
</head>
<body>
    <header>
        <c:choose>
            <c:when test="${sessionScope['current.user.id'] != null}">
                <span> <c:out value="${sessionScope['current.user.fn']}" /> <c:out
                        value="${sessionScope['current.user.ln']}" /> <a
                    href="${pageContext.request.contextPath}/servleti/author/${sessionScope['current.user.nick']}">Moj blog</a> <a
                    href="${pageContext.request.contextPath}/servleti/logout">Odjavi se</a>
                </span>
            </c:when>
            <c:otherwise>
<span>Niste prijavljeni</span>javljeni
</c:otherwise>
        </c:choose>
        <a href="${pageContext.request.contextPath}/servleti/main">Početna stranica</a>
    </header>
    <h1>Uredite vaš blog</h1>
    <div>
        <form action="" method="post">
            <input hidden="true" name="id" value='<c:out value="${record.id}"/>'>
            <div>
                <span class="formLabel">Naslov</span>
            </div>
            <div>
                <input type="text" name="title" value='<c:out value="${record.title}"/>' size="50">
            </div>
            <div>
                <c:if test="${record.hasException('title')}">
                    <span class="greska"><c:out value="${record.getException('title')}" /></span>
                </c:if>
            </div>

            <div>
                <span class="formLabel">Vaš blog</span>
            </div>
            <div>
                <textarea rows="20" cols="100" name="text"><c:out value="${record.text}" /></textarea>
            </div>
            <div>
                <c:if test="${record.hasException('text')}">
                    <span class="greska"><c:out value="${record.getException('text')}" /></span>
                </c:if>
            </div>

            <div class="formControls">
                <span class="formLabel">&nbsp;</span> <input type="submit" name="method" value="Ažuriraj">
            </div>
        </form>
    </div>
</body>
</html>