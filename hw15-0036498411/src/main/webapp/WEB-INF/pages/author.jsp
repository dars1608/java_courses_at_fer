<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Blog korisnika: <c:out value="${contextNick}" />
</title>
</head>
<body>
    <header>
        <c:choose>
            <c:when test="${sessionScope['current.user.id'] != null}">
                <span> <c:out value="${sessionScope['current.user.fn']}" /> <c:out
                        value="${sessionScope['current.user.ln']}" /> <a href="${pageContext.request.contextPath}/servleti/logout">Odjavi se</a>
                </span>
            </c:when>
            <c:otherwise>
<span>Niste prijavljeni</span>
</c:otherwise>
        </c:choose>
        <a href="${pageContext.request.contextPath}/servleti/main">Početna stranica</a>
    </header>
    <h1>Blogs</h1>

    <c:if test="${activeUser != null}">
        <div>
            <h4>
                Stvori <a href="${contextNick}/new">novi</a> blog.
            </h4>
        </div>
    </c:if>

    <ul>
        <c:forEach var="blog" items="${blogs}">
            <li><a href="${contextNick}/${blog.id}">${blog.title}</a> <c:if test="${activeUser != null }">
                    <a href="${contextNick}/edit?eid=${blog.id}"><br>Uredi...</a>
                </c:if></li>
        </c:forEach>
    </ul>

</body>
</html>