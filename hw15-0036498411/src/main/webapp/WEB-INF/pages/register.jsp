<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registracija</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mystyle.css">
</head>
<body>
    <header>
        <c:choose>
            <c:when test="${sessionScope['current.user.id'] != null}">
                <span> <c:out value="${sessionScope['current.user.fn']}" /> <c:out
                        value="${sessionScope['current.user.ln']}" /> <a
                    href="${pageContext.request.contextPath}/servleti/author/${sessionScope['current.user.nick']}">Moj blog</a> <a
                    href="${pageContext.request.contextPath}/servleti/logout">Odjavi se</a>
                </span>
            </c:when>
            <c:otherwise>
<span>Niste prijavljeni</span>
</c:otherwise>
        </c:choose>
        <a href="${pageContext.request.contextPath}/servleti/main">Početna stranica</a>
    </header>
    <h1>Registracija</h1>
    <div>
        <span>Da biste stvorili račun potrebno je popuniti podatke:</span>
    </div>

    <form action="register" method="post">

        <div>
            <span class="formLabel">Ime</span><input type="text" name="firstName"
                value='<c:out value="${record.firstName}"/>' size="20">
            <c:if test="${record.hasException('firstName')}">
                <span class="greska"><c:out value="${record.getException('firstName')}" /></span>
            </c:if>
        </div>

        <div>
            <span class="formLabel">Prezime</span><input type="text" name="lastName"
                value='<c:out value="${record.lastName}"/>' size="20">
            <c:if test="${record.hasException('lastName')}">
                <span class="greska"><c:out value="${record.getException('lastName')}" /></span>
            </c:if>
        </div>

        <div>
            <span class="formLabel">EMail</span><input type="text" name="email" value='<c:out value="${record.email}"/>'
                size="50">
            <c:if test="${record.hasException('email')}">
                <span class="greska"><c:out value="${record.getException('email')}" /></span>
            </c:if>
        </div>

        <div>
            <span class="formLabel">Nadimak</span><input type="text" name="nick" value='<c:out value="${record.nick}"/>'
                size="20">
            <c:if test="${record.hasException('nick')}">
                <span class="greska"><c:out value="${record.getException('nick')}" /></span>
            </c:if>
        </div>

        <div>
            <span class="formLabel">Lozinka</span><input type="password" name="password" size="20">
            <c:if test="${record.hasException('password')}">
                <span class="greska"><c:out value="${record.getException('password')}" /></span>
            </c:if>
        </div>

        <div class="formControls">
            <span class="formLabel">&nbsp;</span> <input type="submit" name="metoda" value="Pohrani"> <input
                type="submit" name="metoda" value="Odustani">
        </div>

    </form>
</body>
</html>