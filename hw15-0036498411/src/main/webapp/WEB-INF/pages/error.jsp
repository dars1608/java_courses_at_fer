<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
</style>
<title>ERROR</title>
</head>
<body>

    <div align="center">
        <b>Dogodila se greška prilikom pristupa stranici.</b> <br> ${errorMessage} <br> <a
            href="${pageContext.request.contextPath}/servleti/main">Povratak na početnu stranicu...</a>
    </div>
</body>
</html>
