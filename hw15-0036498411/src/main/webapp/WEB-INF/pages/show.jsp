<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mystyle.css">
</head>
<body>
    <header>
        <c:choose>
            <c:when test="${sessionScope['current.user.id'] != null}">
                <span> <c:out value="${sessionScope['current.user.fn']}" /> <c:out
                        value="${sessionScope['current.user.ln']}" /> <a
                    href="${pageContext.request.contextPath}/servleti/author/${sessionScope['current.user.nick']}">Moj blog</a> <a
                    href="${pageContext.request.contextPath}/servleti/logout">Odjavi se</a>
                </span>
            </c:when>
            <c:otherwise>
                <span>Niste prijavljeni</span>
            </c:otherwise>
        </c:choose>
        <a href="${pageContext.request.contextPath}/servleti/main">Početna stranica</a>
    </header>

    <h1>
        <c:out value="${blogEntry.title}" />
    </h1>
    <div>
        <i> Kreirano:<fmt:formatDate value="${blogEntry.createdAt}" pattern="yyyy-MM-dd HH:mm:ss" /><br>
            Uređivano:<fmt:formatDate value="${blogEntry.lastModifiedAt}" pattern="yyyy-MM-dd HH:mm:ss" /><br>
        </i>
    </div>
    <div>
        <textarea rows="20" cols="100" name="message" disabled="disabled"><c:out value="${blogEntry.text}" /></textarea>
    </div>
    <c:if test="${activeUser}">
        <a href="edit?eid=${blogEntry.id}"><br>Uredi...</a>
    </c:if>
    <c:if test="${!blogEntry.comments.isEmpty()}">
        <ul>
            <c:forEach var="e" items="${blogEntry.comments}">
                <li><div style="font-weight: bold">
                        [Korisnik =
                        <c:out value="${e.usersEMail}" />
                        ]
                        <fmt:formatDate value="${e.postedOn}" pattern="yyyy-MM-dd HH:mm:ss" />
                    </div>
                    <div style="padding-left: 10px;">
                        <c:out value="${e.message}" />
                    </div></li>
            </c:forEach>
        </ul>
    </c:if>


    <form action="" method="post">
        <div>

            <input hidden="true" name="eid" value='<c:out value="${blogEntry.id}"/>'>
            <c:choose>
                <c:when test="${sessionScope['current.user.id'] == null}">
                    <div>
                        <span class="formLabel">EMail:</span><input type="text" name="email" size="50">
                        <c:if test="${record.hasException('email')}">
                            <span class="greska"><c:out value="${record.getException('email')}" /></span>
                        </c:if>
                    </div>
                </c:when>
            </c:choose>
            <div>
                <span class="formLabel">Vaš komentar:</span>
                <textarea rows="5" cols="50" name="message"><c:out value="${record.message}" /></textarea>
                <c:if test="${record.hasException('message')}">
                    <span class="greska"><c:out value="${record.getException('message')}" /></span>
                </c:if>
            </div>
        </div>
        <div class="formControls">
            <span class="formLabel">&nbsp;</span> <input type="submit" name="method" value="Dodaj komentar">
        </div>
    </form>
</body>
</html>