package hr.fer.zemris.java.hw07.crypto;

import static org.junit.Assert.*;

import org.junit.Test;

public class utilTest {

	@Test
	public void testHextobyte() {
		byte[] test = Util.hextobyte("01aE22");
		assertArrayEquals(new byte[] {1, -82, 34}, test);
	}

	@Test
	public void testBytetohex() {
		String test = Util.bytetohex(new byte[] {1, -82, 34});
		assertEquals("01ae22", test);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testBadFormat() {
		Util.hextobyte("01ae222");
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testBadFormat2() {
		Util.hextobyte("01ae2H");
		
	}
	
	@Test(expected=NullPointerException.class)
	public void testNullEx() {
		Util.hextobyte(null);
	}
	
	@Test(expected=NullPointerException.class)
	public void testNullEx2() {
		Util.bytetohex(null);
	}
	
	@Test
	public void testEmptyArray() {
		String test = Util.bytetohex(new byte[] {});
		assertEquals("", test);
	}
	
	@Test
	public void testEmptyString() {
		byte[] test = Util.hextobyte("");
		assertArrayEquals(new byte[] {}, test);
	}

}
