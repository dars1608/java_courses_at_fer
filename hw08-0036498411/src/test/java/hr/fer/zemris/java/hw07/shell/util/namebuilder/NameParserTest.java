package hr.fer.zemris.java.hw07.shell.util.namebuilder;

import static org.junit.Assert.*;

import org.junit.Test;

import hr.fer.zemris.java.hw07.shell.util.ParserException;

public class NameParserTest { 
	NameBuilderInfo info = new NameBuilderInfo() {
		private StringBuilder sb = new StringBuilder();
		private String[] g = new String[] {
				"slika2-split",
				"2",
				"split"
		};
		
		@Override
		public StringBuilder getStringBuilder() {
			return sb;
		}
		
		@Override
		public String getGroup(int index) {
			return g[index].toString();
		}
	};

	@Test
	public void test1() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${2}-${1,03}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}
	
	@Test
	public void test2() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${2}-${1,3}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-  2.jpg", info.getStringBuilder().toString());
	}
	
	@Test
	public void testMultiSpaces() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${    2   }-${   1   ,    03}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}
	
	@Test
	public void testStrangeFormat() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-$ {2}-${1,03}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-$ {2}-002.jpg", info.getStringBuilder().toString());
	}
	
	@Test(expected = ParserException.class)
	public void testNegative() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${-2}-${1,03}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}
	
	
	@Test(expected = ParserException.class)
	public void testNegative2() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${2}-${1,-3}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}
	
	@Test(expected = ParserException.class)
	public void testInvalidSupstitutionExpression() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${2a}-${1,03}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}
	
	
	@Test(expected = ParserException.class)
	public void testInvalidSupstitutionExpression2() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${2.2}-${1,03}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}
	
	@Test(expected = ParserException.class)
	public void testInvalidSupstitutionExpression3() {
		NameBuilderParser parser = new NameBuilderParser("gradovi-${2.2}-${1,a3}.jpg");
		
		NameBuilder b = parser.getNameBuilder();
		b.execute(info);
		
		assertEquals("gradovi-split-002.jpg", info.getStringBuilder().toString());
	}

}
