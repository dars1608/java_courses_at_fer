﻿ * This command is used for removing directory which was last
 * stored by "pushd" on internal stack. If there's no stored
 * directories, command informs user and does nothing.