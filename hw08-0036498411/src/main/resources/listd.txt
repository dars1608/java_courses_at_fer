﻿ * This command is used for listing directories stored on current
 * directory stack; it writes directories pushed on stack by "pushd"
 * command in chronological order (from last to first pushed).