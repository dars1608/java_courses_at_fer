﻿ * This command is used for copying files.
 * It expects two arguments: source file name and destination file name
 * (i.e. paths and names). Is destination file exists, program asks user is it
 * allowed to overwrite it. Copy command works only with files (no directories).
 * If the second argument is directory, program assumes that user wants to copy
 * the original file into that directory using the original file name.