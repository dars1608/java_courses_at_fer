﻿ * This command is used for obtaining info about other commands
 * defined in MyShell program. If started with no arguments, it lists names 
 * of all supported commands. If started with single argument, it prints name and the
 * description of selected command (or prints appropriate error message if no such
 * command exists).