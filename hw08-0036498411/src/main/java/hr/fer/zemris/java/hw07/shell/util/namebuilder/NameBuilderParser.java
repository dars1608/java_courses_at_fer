package hr.fer.zemris.java.hw07.shell.util.namebuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw07.shell.util.ParserException;

/**
 * This class is simple parser for {@link NameBuilder} objects. Objects are
 * parsed based on expressions obtained from the shell. Parser parses three
 * types of builders: first is for basic string, second and third are for
 * substitution expressions like <code>${2}</code> which stands for whatever is
 * in second group, or <code>${2, 3}</code> which means whatever is in second
 * group and 3 reserved spaces.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class NameBuilderParser {

	/** Characters obtained from expressions */
	private char[] data;
	/** Index of last processed character */
	private int currentIndex;
	/** Currently processed character */
	char c;
	/** Last parsed {@link NameBuilder} */
	private NameBuilder builder;
	/** Complex name builder based on whole input expression */
	private NameBuilder retBuilder;

	/**
	 * Constructor method for class {@link NameBuilderParser}.
	 * 
	 * @param expression
	 *            expression to be parsed
	 */
	public NameBuilderParser(String expression) {
		data = Objects.requireNonNull(expression).toCharArray();
	}

	/**
	 * This method initiates parsing of {@link NameBuilder} object from expression
	 * passed through constructor.
	 * 
	 * @return parsed object, <code>null</code> if there's nothing to parse
	 * @throws ParserException
	 *             if error while parsing occurs
	 */
	public NameBuilder getNameBuilder() {
		if (retBuilder == null) {
			generateReturnBuilder();
		}

		return retBuilder;
	}

	/**
	 * This method is used internally for generating return {@link NameBuilder}
	 * which will wrap all simple builders which were parsed from expression.
	 */
	public void generateReturnBuilder() {
		List<NameBuilder> list = new ArrayList<>();

		do {
			parseBuilder();
			if (builder == null)
				break;
			list.add(builder);
		} while (true);

		retBuilder = i -> list.forEach(j -> j.execute(i));
	}

	/**
	 * This method is used internally for defining which mode of parsing must be
	 * active and initiating that mode. Each mode will set current
	 * {@link NameBuilder} to parsed one. If whole expression is parsed, it will set
	 * current builder to <code>null</code>
	 */
	private void parseBuilder() {
		if (currentIndex >= data.length) {
			builder = null;
			return;
		}

		c = data[currentIndex];

		if (c == '$' && currentIndex + 1 < data.length && data[currentIndex + 1] == '{') {
			parseSupstitutionExpression();

		} else {
			parseNormalExpression();
		}

	}

	/**
	 * This method is used internally for parsing parts of expressions that are
	 * basic strings.
	 */
	private void parseNormalExpression() {
		StringBuilder sb = new StringBuilder();
		sb.append(c);
		currentIndex++;
		
		while (currentIndex < data.length && data[currentIndex] != '$') {
			c = data[currentIndex++];
			sb.append(c);
		}
		
		builder = i -> i.getStringBuilder().append(sb.toString());
	}

	/**
	 * This method is used internally for parsing substitution expressions.
	 */
	private void parseSupstitutionExpression() {
		StringBuilder sb = new StringBuilder();
		int group = 0;
		String groupS = null;
		boolean hasAdditional = false;

		currentIndex += 2;
		if (currentIndex >= data.length) {
			throw new ParserException("Illegal supstitution expression format.");
		}

		skipBlankSpaces();
		while (currentIndex < data.length) {
			c = data[currentIndex++];
			if (c == '}') {
				break;
			} else if (c == ',') {
				hasAdditional = true;
				groupS = sb.toString().trim();
				sb = new StringBuilder();
			} else {
				sb.append(c);
			}
		}

		if (c != '}') {
			throw new ParserException("Illegal supstitution expression format.");
		}

		try {
			if (!hasAdditional) {
				group = Integer.parseInt(sb.toString().trim());
			} else {
				group = Integer.parseInt(groupS);
			}
		} catch (NumberFormatException ex) {
			throw new ParserException("Illegal number format.");
		}

		if (group >= 0) {
			int index = group;

			if (hasAdditional) {
				String additional = sb.toString().trim();

				int o = 0;
				try {
					o = Integer.parseInt(additional);
				} catch (NumberFormatException ex) {
					throw new ParserException("Illegal number format.");
				}
				if (o < 0) {
					throw new ParserException("Illegal supstitution expression format. Offset can't be negative.");
				}
				int offset = o;

				builder = i -> {
					String output = null;
					if (additional.startsWith("0")) {
						output = getLeadingZeroes(offset, i.getGroup(index), false);
					} else {
						output = getLeadingZeroes(offset, i.getGroup(index), true);
					}

					i.getStringBuilder().append(output);

				};

			} else {
				builder = i -> i.getStringBuilder().append(i.getGroup(index));
			}
		} else {
			throw new ParserException("Illegal supstitution expression format. Group index can't be negative.");
		}
	}

	/**
	 * This method is used internally for getting the leading signs (zeroes or
	 * spaces).
	 * 
	 * @param offset
	 *            minimal length
	 * @param string
	 *            string which is modified
	 * @param isSpaces
	 *            flag which denotes if leading sign is space or zero
	 * @return output string
	 */
	private String getLeadingZeroes(int offset, String string, boolean isSpaces) {
		if (string.length() >= offset) {
			return string;
		} else {
			char[] arr = new char[offset - string.length()];

			if (isSpaces) {
				Arrays.fill(arr, ' ');
			} else {
				Arrays.fill(arr, '0');
			}

			return new String(arr) + string;
		}
	}

	/**
	 * This method is used internally for skipping blank spaces stored in
	 * <code>data</code> field.
	 */
	private void skipBlankSpaces() {
		while (currentIndex < data.length) {
			char c = data[currentIndex];
			if (!isBlank(c)) {
				break;
			}
			currentIndex++;
		}
	}

	/**
	 * This method checks if given character is blank character.
	 * 
	 * @param c
	 *            checked character
	 * @return <code>true</code> if character is blank, <code>false</code> otherwise
	 */
	private static boolean isBlank(char c) {
		return "\n\r\t ".contains(Character.toString(c));
	}

}
