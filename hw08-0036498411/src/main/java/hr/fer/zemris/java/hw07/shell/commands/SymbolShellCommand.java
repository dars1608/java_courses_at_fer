package hr.fer.zemris.java.hw07.shell.commands;

import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} which changes or obtains following shell symbols:
 * <ul>
 * <li> prompt symbol - printed before user input
 * <li> multiline symbol - printed whenever user wants to write multiple lines
 * <li> more lines symbol - user must enter this input whenever he/she wants to enter the input in the next line
 * </ul>
 * <p>
 * Replacement symbol must be one character.
 * </p>
 * @author Darko Britvec
 * @version 1.0
 */
public class SymbolShellCommand extends AbstractCommand implements ShellCommand {
	
	/**
	 * Constructor method for class {@link SymbolShellCommand}.
	 */
	public SymbolShellCommand() {
		super("symbol");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(
				arguments, env, i -> i.size() == 1 || i.size() == 2);
		
		if(argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		if (argumentsList.size() == 1) {
			String message = "Symbol for %s is %c.";
			String name = argumentsList.get(0);
			
			if (name.equals("PROMPT")) {
				env.writeln(String.format(message, "PROMPT", env.getPromptSymbol()));
			} else if (name.equals("MORELINES")) {
				env.writeln(String.format(message, "MORELINES", env.getMorelinesSymbol()));
			} else if (name.equals("MULTILINE")) {
				env.writeln(String.format(message, "MULTILINE", env.getMultilineSymbol()));
			} else {
				env.writeln("Invalid symbol name: " + name);
			}
			
		} else  {
			String name = argumentsList.get(0);
			String symbolString = argumentsList.get(1);
			
			if (symbolString.length() == 1) {
				Character symbol = Character.valueOf(symbolString.charAt(0));
				String message = "Symbol for %s changed from %c to %c.";
				
				if (name.equals("PROMPT")) {
					env.writeln(String.format(message, "PROMPT", env.getPromptSymbol(), symbol));
					env.setPromptSymbol(symbol);
				} else if (name.equals("MORELINES")) {
					env.writeln(String.format(message, "MORELINES", env.getMorelinesSymbol(), symbol));
					env.setMorelinesSymbol(symbol);
				} else if (name.equals("MULTILINE")) {
					env.writeln(String.format(message, "MULTILINE", env.getMultilineSymbol(), symbol));
					env.setMultilineSymbol(symbol);
				} else {
					env.writeln("Invalid symbol name: " + name);
				}
			} else {
				env.writeln("Invalid replacement symbol" + symbolString 
						+ ". Symbol must be 1 character only.");
			}
		} 

		return ShellStatus.CONTINUE;
	}
}
