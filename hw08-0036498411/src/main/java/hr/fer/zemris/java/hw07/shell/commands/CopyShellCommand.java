package hr.fer.zemris.java.hw07.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for copying files. The copy
 * command expects two arguments: source file name and destination file name
 * (i.e. paths and names). Is destination file exists, program asks user is it
 * allowed to overwrite it. Copy command works only with files (no directories).
 * If the second argument is directory, program assumes that user wants to copy
 * the original file into that directory using the original file name.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CopyShellCommand extends AbstractCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link CopyShellCommand}.
	 */
	public CopyShellCommand() {
		super("copy");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 2);
		if (argumentsList != null) {
			Path p1 = getPath(argumentsList.get(0), env.getCurrentDirectory());
			Path p2 = getPath(argumentsList.get(1), env.getCurrentDirectory());
			
			File originalFile = p1.toFile();
			File newFile = p2.toFile();
			boolean overwrite = false;

			if (originalFile.isDirectory()) {
				env.writeln("Command copy works only with files.");
				return ShellStatus.CONTINUE;
			}
			if (newFile.isDirectory()) {
				newFile = new File(newFile, originalFile.getName());
			}

			if (newFile.exists()) {
				while (true) {
					env.writeln("File already exitst. Do you want to overwrite it? y/n");
					env.write(env.getPromptSymbol() + " ");

					String ans = env.readLine();
					if (ans.trim().toLowerCase().equals("y")) {
						overwrite = true;
						break;
					} else if (ans.trim().toLowerCase().equals("n")) {
						return ShellStatus.CONTINUE;
					}

					env.writeln("Illegal answer. Next time be more careful.");
				}
			}

			copyFile(originalFile.toPath(), newFile.toPath(), env, overwrite);

			return ShellStatus.CONTINUE;

		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used internally for copying data from source to destination.
	 * If destination path is directory, new file with the same name as original is
	 * created. If overwrite flag is <code>true</code>, destination file will be
	 * overwritten.
	 * 
	 * @param pathIn
	 *            path of file to be copied
	 * @param pathOut
	 *            path of copied file
	 * @param env
	 *            {@link Environment} of shell
	 * @param overwrite
	 *            flag indicating if user wants to overwrite existing file
	 */
	private void copyFile(Path pathIn, Path pathOut, Environment env, boolean overwrite) {
		
		if(overwrite && !pathOut.toFile().canWrite()) {
			env.writeln(String.format("File %s can't be overwritten. Is read-only.",
					pathOut.toAbsolutePath()));
			return;
		}
		
		try (BufferedInputStream bsIn = new BufferedInputStream(
					Files.newInputStream(pathIn, StandardOpenOption.READ));
			BufferedOutputStream bsOut = new BufferedOutputStream(
					Files.newOutputStream(pathOut,
						overwrite ? StandardOpenOption.TRUNCATE_EXISTING : StandardOpenOption.CREATE))) {

			while (true) {
				byte[] buff = new byte[1024];
				int r = bsIn.read(buff);
				if (r < 1) {
					break;
				}
				bsOut.write(buff, 0, r);
			}
		} catch (IOException ex) {
			env.writeln(String.format(
					"File %s can't be opened.", pathIn.toAbsolutePath()));
			return;
		}

		env.writeln(String.format(
				"File %s copied to %s.", pathIn.toString(), pathOut.toString()));
	}
}
