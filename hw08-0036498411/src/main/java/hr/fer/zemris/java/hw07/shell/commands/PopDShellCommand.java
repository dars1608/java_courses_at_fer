package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.CdStackCommand;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for obtaining directory which
 * was current before resolving {@link PushDShellCommand}. If there were no
 * executions of "pushd" command, command informs user and does nothing. If
 * directory no longer exists, command informs user and pops that directory from
 * stack.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PopDShellCommand extends AbstractCommand implements CdStackCommand, ShellCommand {

	/**
	 * Constructor method for class {@link PopDShellCommand}.
	 */
	public PopDShellCommand() {
		super("popd");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 0);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(SHARED_STACK_KEY);
		if (stack == null || stack.isEmpty()) {
			env.writeln("Stack is empty. Command failed to complete.");
			return ShellStatus.CONTINUE;
		}

		Path p = stack.pop();
		if (p.toFile().exists()) {
			env.setCurrentDirectory(p);
		} else {
			env.writeln(String.format("Directory %s no longer exists. Current directory stays unchanged.",
					p.toAbsolutePath().toString()));
		}

		return ShellStatus.CONTINUE;
	}

}
