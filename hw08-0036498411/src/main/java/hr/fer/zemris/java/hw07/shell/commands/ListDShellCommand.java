package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.CdStackCommand;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for listing directories
 * stored on current directory stack; it writes directories pushed on stack by
 * {@link PushDShellCommand} in chronological order (from last to first pushed).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ListDShellCommand extends AbstractCommand implements CdStackCommand, ShellCommand {

	/**
	 * Constructor method for class {@link ListDShellCommand}.
	 */
	public ListDShellCommand() {
		super("listd");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 0);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(SHARED_STACK_KEY);

		if (stack == null || stack.isEmpty()) {
			env.writeln("There's no stored directories.");
			return ShellStatus.CONTINUE;
		}

		Stack<Path> tempStack = new Stack<>();
		while (!stack.isEmpty()) {
			Path p = stack.pop();
			env.writeln(p.toString());
			tempStack.push(p);

		}

		while (!tempStack.isEmpty()) {
			stack.push(tempStack.pop());
		}

		return ShellStatus.CONTINUE;
	}

}
