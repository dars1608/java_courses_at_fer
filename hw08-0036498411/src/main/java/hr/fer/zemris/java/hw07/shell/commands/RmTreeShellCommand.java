package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for deleting file tree from
 * file system.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class RmTreeShellCommand extends AbstractCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link RmTreeShellCommand}.
	 */
	public RmTreeShellCommand() {
		super("rmtree");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		File f = getPath(argumentsList.get(0), env.getCurrentDirectory()).toFile();

		if (!f.isDirectory()) {
			env.writeln("Given argument isn't directory path.");
		} else {
			try {
				removeTree(f);
				env.writeln(String.format("Directory %s and all of its components are deleted.", f.getAbsolutePath()));
			} catch (RuntimeException ex) {
				env.writeln("Unexpected error occured while deleting tree. Command was forced to stop");
			}
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used for deleting file tree.
	 * 
	 * @param f
	 *            file representing root directory of file tree
	 */
	private static void removeTree(File f) {
		File[] files = f.listFiles();

		for (File file : files) {
			if (file.isDirectory()) {
				removeTree(file);
				continue;
			}

			if (!file.delete()) {
				throw new RuntimeException();
			}
		}

		f.delete();
	}

}
