package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.charset.Charset;
import java.util.Objects;
import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} which writes available
 * {@link Charset} names to console.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CharsetsShellCommand extends AbstractCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link CharsetsShellCommand}.
	 */
	public CharsetsShellCommand() {
		super("charsets");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		Objects.requireNonNull(env, "Environment reference can't be null");
		Objects.requireNonNull(arguments, "Arguments reference can't be null");
		
		if (arguments.isEmpty()) {
			SortedMap<String, Charset> charsets = Charset.availableCharsets();

			charsets.keySet().stream().forEach(i -> {
				env.writeln(i);
			});
		} else {
			env.writeln("Command \"charsets\" takes no arguments.");
		}

		return ShellStatus.CONTINUE;
	}
}
