/**
 * This package contains abstract classes and interfaces used to describe
 * commands of the program {@linkplain hr.fer.zemris.java.hw07.shell.MyShell
 * MyShell}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.shell.commands.util;