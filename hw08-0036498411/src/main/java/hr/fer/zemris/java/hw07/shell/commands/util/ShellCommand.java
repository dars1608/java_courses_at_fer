package hr.fer.zemris.java.hw07.shell.commands.util;

import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;

/**
 * This interface describes a command which can be executed in {@link MyShell}.
 * Every command should have its own name, description and implementation of
 * execution.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface ShellCommand {

	/**
	 * This method performs an execution of a command.
	 * 
	 * @param env
	 *            {@link Environment} object which describes current {@link MyShell}
	 *            state
	 * @param arguments
	 *            command arguments
	 * @return {@link ShellStatus#CONTINUE} if execution succeeds,
	 *         {@link ShellStatus#TERMINATE} if it fails
	 */
	ShellStatus executeCommand(Environment env, String arguments);

	/**
	 * Getter method for the command name.
	 * 
	 * @return the command name
	 */
	String getCommandName();

	/**
	 * Getter method for the command description (usage instructions).
	 * 
	 * @return description; since the description can span more than one line, it is
	 *         wrapped in unmodified {@link List} of strings
	 */
	List<String> getCommandDescription();

}
