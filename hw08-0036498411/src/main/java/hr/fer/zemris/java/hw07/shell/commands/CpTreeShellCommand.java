package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for copying file tree from
 * source to destination. Command takes two arguments; the first argument is
 * path of file tree to be copied, the second is path of destination directory
 * Command internally delegates the work to {@link CopyShellCommand}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CpTreeShellCommand extends AbstractCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link CpTreeShellCommand}.
	 */
	public CpTreeShellCommand() {
		super("cptree");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 2);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		Path src = getPath(argumentsList.get(0), env.getCurrentDirectory());
		Path dest = getPath(argumentsList.get(1), env.getCurrentDirectory());

		if (src.toFile().isFile() || dest.toFile().isFile()) {
			env.writeln("Both arguments must be paths of directories.");
		} else {
			try {
				copyTree(src, dest, src.toFile().getParentFile().toPath(), env);
				env.writeln(String.format("Directory %s and all of its components are copied to %s.",
						src.toAbsolutePath(), dest.toAbsolutePath()));
			} catch (RuntimeException ex) {
				env.writeln("Unexpected error occured while copying tree. Command was forced to stop");
			}
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used for recursive copying of the file tree from source to
	 * destination.
	 * 
	 * @param src
	 *            source folder
	 * @param dest
	 *            destination folder
	 * @param root
	 *            root directory of the file tree
	 * @param env
	 *            {@link Environment} of the shell
	 */
	private static void copyTree(Path src, Path dest, Path root, Environment env) {
		File dir = new File(dest.toFile(), root.relativize(src).toString());
		dir.mkdirs();

		File[] files = src.toFile().listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				copyTree(file.toPath(), dest, root, env);
				continue;
			}

			env.commands().get("copy").executeCommand(env, String.format("\"%s\" \"%s\"", file.toPath(), dir));
		}
	}

}
