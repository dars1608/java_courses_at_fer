package hr.fer.zemris.java.hw07.shell.util.namebuilder;

/**
 * This interface describes an object which is able to generate part of the file
 * name based on {@link NameBuilderInfo} object.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
@FunctionalInterface
public interface NameBuilder {
	
	/**
	 * Method used for generating part of the file name based on provided argument.
	 * Argument info must provide {@link StringBuilder} for generating part of the
	 * name.
	 * 
	 * @param info
	 */
	void execute(NameBuilderInfo info);
}
