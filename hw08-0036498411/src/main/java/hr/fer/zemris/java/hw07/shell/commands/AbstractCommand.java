package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;
import hr.fer.zemris.java.hw07.shell.util.ArgumentsExtractor;
import hr.fer.zemris.java.hw07.shell.util.DescriptionLoader;
import hr.fer.zemris.java.hw07.shell.util.ParserException;

/**
 * This class represents abstract {@link ShellCommand} which will be extended in
 * real implementations. It assures that every command extending this command will
 * have its name and description which will be loaded from text file. It also has
 * method {@link AbstractCommand#getArguments(String, Environment, Predicate)}
 * useful for obtaining arguments from argument line.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class AbstractCommand implements ShellCommand {
	/** List of lines containing description of this command */
	private final List<String> commandDescription;
	/** Name of this command */
	private final String commandName;

	/**
	 * Constructor method for class {@link AbstractCommand}.
	 */
	public AbstractCommand(String commandName) {
		this.commandName = Objects.requireNonNull(commandName);
		commandDescription = DescriptionLoader.load(AbstractCommand.class.getResourceAsStream(commandName+".txt"));
	}

	@Override
	public abstract ShellStatus executeCommand(Environment env, String arguments);

	@Override
	public String getCommandName() {
		return commandName;
	}

	@Override
	public List<String> getCommandDescription() {
		return commandDescription;
	}

	/**
	 * This method is used whenever some command needs to extract arguments from
	 * line of arguments. It also checks if arguments are <code>null</code> and if
	 * number of arguments is correct.
	 * 
	 * @param arguments line of arguments
	 * @param env {@link Environment} of shell
	 * @param validLength valid number of arguments
	 * @return list of arguments, <code>null</code> if arguments are invalid
	 */
	protected static List<String> getArguments(String arguments, Environment env, Predicate<List<String>> validLength) {
		Objects.requireNonNull(env, "Environment reference can't be null.");
		Objects.requireNonNull(arguments, "Arguments reference can't be null.");
		Objects.requireNonNull(validLength, "Predicate reference can't be null.");

		List<String> argumentsList = null;
		try {
			argumentsList = ArgumentsExtractor.extract(arguments);
		} catch (ParserException e) {
			env.writeln(e.getMessage());
			return null;
		}

		if (!validLength.test(argumentsList)) {
			env.writeln(String.format("Invalid number of arguments. Is: %s.", argumentsList.size()));
			return null;
		}

		return argumentsList;
	}
	
	/**
	 * This method is used for converting path to relative path, if it isn't absolute.
	 * 
	 * @param pathName string representing path to be converted
	 * @param root root directory
	 * @return converted path
	 */
	protected static Path getPath(String pathName, Path root) {
		Path p = Paths.get(pathName);

		return root.resolve(p).normalize();
	}
}
