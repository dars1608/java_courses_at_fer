package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for setting the current
 * directory of the shell. It expects one argument; <i>pathName</i> which is
 * relative or absolute path.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class CdShellCommand extends AbstractCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link CdShellCommand}.
	 */
	public CdShellCommand() {
		super("cd");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);
		
		if(argumentsList == null) {
			return ShellStatus.CONTINUE;
		}
		
		Path p = getPath(argumentsList.get(0), env.getCurrentDirectory());		
		if(!p.toFile().isDirectory()) {
			env.writeln("Given argument isn't directory path.");
			return ShellStatus.CONTINUE;
		}
		
		env.setCurrentDirectory(p);
		
		return ShellStatus.CONTINUE;
	}

}
