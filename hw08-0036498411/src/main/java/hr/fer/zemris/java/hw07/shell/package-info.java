/**
 * This package contains classes used for building simple shell program
 * {@linkplain hr.fer.zemris.java.hw07.shell.MyShell MyShell}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.shell;