package hr.fer.zemris.java.hw07.shell.commands;

import java.util.List;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} which is used for fetching an
 * absolute path of current directory of the shell. It accepts no arguments.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PwdShellCommand extends AbstractCommand implements ShellCommand{

	/**
	 * Constructor method for class {@link PwdShellCommand}.
	 */
	public PwdShellCommand() {
		super("pwd");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 0);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		env.writeln(env.getCurrentDirectory().toString());
		return ShellStatus.CONTINUE;
	}

}
