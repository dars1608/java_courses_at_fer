package hr.fer.zemris.java.hw07.shell;

import static java.util.Collections.unmodifiableSortedMap;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw07.shell.commands.CatShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.CdShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.CharsetsShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.CopyShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.CpTreeShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.DropDShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.HelpShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.HexDumpShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.ListDShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.LsShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.MassRenameShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.PopDShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.PushDShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.PwdShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.RmTreeShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.SymbolShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.TreeShellCommand;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This is simple shell program with some useful functions:
 * <ul>
 * <li>{@linkplain CatShellCommand cat} - printing file on shell
 * <li>{@linkplain CharsetsShellCommand charsets} - printing available charsets
 * <li>{@linkplain CopyShellCommand copy} - copying file
 * <li>{@linkplain HexDumpShellCommand hexdump} - printing hex-encoded
 * representation of file
 * <li>{@linkplain LsShellCommand ls} - printing listing of directory
 * <li>{@linkplain SymbolShellCommand symbol} - changing default symbols in
 * shell
 * <li>{@linkplain TreeShellCommand tree} - printing directory tree on shell
 * <li>{@linkplain CpTreeShellCommand cptree} - copying file tree
 * <li>{@linkplain RmTreeShellCommand rmtree} - deleting file tree
 * <li>{@linkplain PwdShellCommand pwd} - printing current directory on shell
 * <li>{@linkplain CdShellCommand cd} - changing current directory
 * <li>{@linkplain PushDShellCommand pushd} - changing current directory and
 * storing old one on the internal stack
 * <li>{@linkplain PopDShellCommand popd} - obtaining and changing to old
 * current directory
 * <li>{@linkplain ListDShellCommand listd} - writing history of changes of
 * current directory (with pushd and popd commands)
 * <li>{@linkplain DropDShellCommand dropd} - deleting last stored directory
 * from stack
 * <li>{@linkplain MassRenameShellCommand massrename} - multiple functionalities
 * command
 * </ul>
 * <p>
 * Writing in multiple lines is also possible by entering more lines symbol (
 * "\" by default). All arguments must be separated by one or more space
 * characters. Shell also provides {@linkplain HelpShellCommand help} command
 * for obtaining additional info. Program terminates by entering "exit" in
 * shell.
 * </p>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class MyShell { //all test files are put in src/test/resources/

	/** Default prompt symbol */
	private static final char DEFAULT_PROMPT = '>';
	/** Default multiline symbol */
	private static final char DEFAULT_MULTILINE = '|';
	/** Default more lines symbol */
	private static final char DEFAULT_MORE_LINES = '\\';
	/** Message written on standard output at the beginning of the program */
	private static final String WELCOME_MESSAGE = "Welcome to MyShell v 1.0";
	/** Initial path of the shell */
	private static final String INITIAL_DIR = ".";

	/** Represents {@link Environment} object describing the shell functionality */
	private final Environment env;
	/** Map of defined {@link ShellCommand} objects */
	private final SortedMap<String, ShellCommand> commandMap;
	/** Current prompts symbol */
	private char promptSymbol;
	/** Current multiline symbol */
	private char multilineSymbol;
	/** Current more lines symbol */
	private char moreLinesSymbol;
	/** Writer used for writing on standard output */
	private BufferedWriter bw;
	/** Reader used for reading from standard input */
	private BufferedReader br;
	/** Current path in the shell */
	private Path currentDirectory;
	/** Map of shared data between processes in the shell */
	private Map<String, Object> sharedData;

	/**
	 * Constructor method for class {@link MyShell}.
	 */
	public MyShell() {
		this.env = new ShellEnvironment();
		this.commandMap = initCommands();
		this.sharedData = new HashMap<>();

		this.promptSymbol = DEFAULT_PROMPT;
		this.multilineSymbol = DEFAULT_MULTILINE;
		this.moreLinesSymbol = DEFAULT_MORE_LINES;
		this.currentDirectory = Paths.get(INITIAL_DIR).toAbsolutePath();

		bw = new BufferedWriter(new OutputStreamWriter(System.out, Charset.forName("UTF-8")));
		br = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
	}
	
	/**
	 * This method is called when program starts. Command line arguments aren't
	 * used.
	 * 
	 * @param args
	 *            Command line arguments
	 */
	public static void main(String[] args) {
		new MyShell().runShell();

	}

	/**
	 * This method is called by main method at the start of the program. It
	 * initiates shell and communicates with user.
	 */
	public void runShell() {
		env.writeln(WELCOME_MESSAGE);

		while (true) {
			env.write(promptSymbol + " ");
			String line = env.readLine().trim();

			String commandName = line.split("\\s+")[0];
			String arguments = line.substring(commandName.length()).trim();

			if (!commandMap.containsKey(commandName)) {
				env.writeln("Undefined command.");
				continue;
			}

			ShellCommand command = commandMap.get(commandName);
			if (command.executeCommand(env, arguments) == ShellStatus.TERMINATE) {
				break;
			}
		}

		try {
			br.close();
			bw.close();
		} catch (IOException ignorable) {
		}
	}

	/**
	 * This method is used for reading from standard input (in the shell).
	 * 
	 * @return obtained line
	 * @throws ShellIOException
	 *             if reading fails
	 */
	private String readFromConsole() {
		StringBuilder sb = new StringBuilder();

		try {
			while (true) {
				String line = br.readLine();
				if (line.endsWith(Character.toString(moreLinesSymbol))) {
					sb.append(line.substring(0, line.length() - 1));
					env.write(multilineSymbol + " ");
				} else {
					sb.append(line);
					break;
				}
			}
		} catch (IOException ex) {
			throw new ShellIOException(ex);
		}

		return sb.toString();

	}

	/**
	 * This method is used internally for writing some text on standard output.
	 * 
	 * @param text
	 *            text to be written
	 * @throws ShellIOException
	 *             if writing fails
	 */
	private void printToConsole(String text) {
		try {
			bw.write(text);
			bw.flush();
		} catch (IOException ex) {
			throw new ShellIOException(ex);
		}

	}

	/**
	 * This method is used internally for initializing map of commands defined in
	 * the shell. Every command is described by {@link ShellCommand} object and
	 * stored at their name (lower cased). It uses {@link ClassLoader} to load
	 * all classes from package {@link hr.fer.zemris.java.hw07.shell.commands},
	 * instantiate them and store them into unmodifiable map, under its names.
	 * 
	 * @return unmodifiable sorted map of commands
	 */
	private SortedMap<String, ShellCommand> initCommands() {
		SortedMap<String, ShellCommand> map = new TreeMap<>();

		try {
			File dir = new File(
					Thread.currentThread()
					.getContextClassLoader()
					.getResource("hr.fer.zemris.java.hw07.shell.commands".replace('.', '/'))
					.getFile());
			File[] files = dir.listFiles();

			for (File file : files) {
				if (!file.getName().contains("ShellCommand"))
					continue;

				Class<?> cls = Class.forName(
						"hr.fer.zemris.java.hw07.shell.commands."+ file.getName().split("\\.")[0]);
				
				ShellCommand command = (ShellCommand) cls.getConstructor().newInstance();
				map.put(command.getCommandName(), command);
			}

		} catch (Exception ignorable) {
			ignorable.printStackTrace();
		}

		return unmodifiableSortedMap(map);
	}

	/**
	 * This class represents instance of {@link Environment} which describes
	 * functionality of {@link MyShell}.
	 * 
	 * @author Darko Britvec
	 * @version 1.0
	 */
	private class ShellEnvironment implements Environment {

		@Override
		public String readLine() throws ShellIOException {
			return readFromConsole();
		}

		@Override
		public void write(String text) throws ShellIOException {
			printToConsole(text);
		}

		@Override
		public void writeln(String text) throws ShellIOException {
			printToConsole(text + "\n");

		}

		@Override
		public SortedMap<String, ShellCommand> commands() {
			return commandMap;
		}

		@Override
		public Character getMultilineSymbol() {
			return multilineSymbol;
		}

		@Override
		public void setMultilineSymbol(Character symbol) {
			multilineSymbol = Objects.requireNonNull(symbol);

		}

		@Override
		public Character getPromptSymbol() {
			return promptSymbol;
		}

		@Override
		public void setPromptSymbol(Character symbol) {
			promptSymbol = Objects.requireNonNull(symbol);

		}

		@Override
		public Character getMorelinesSymbol() {
			return moreLinesSymbol;
		}

		@Override
		public void setMorelinesSymbol(Character symbol) {
			moreLinesSymbol = Objects.requireNonNull(symbol);

		}

		@Override
		public Path getCurrentDirectory() {
			return currentDirectory.normalize();
		}

		@Override
		public void setCurrentDirectory(Path path) {
			currentDirectory = Objects.requireNonNull(path).toAbsolutePath();
		}

		@Override
		public Object getSharedData(String key) {
			return sharedData.get(Objects.requireNonNull(key));
		}

		@Override
		public void setSharedData(String key, Object value) {
			sharedData.put(Objects.requireNonNull(key), value);
		}
	}
}
