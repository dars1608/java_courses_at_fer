package hr.fer.zemris.java.hw07.shell.util;

import static java.util.Collections.unmodifiableList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class is helper class used for loading description of
 * {@link ShellCommand} objects from text file.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public abstract class DescriptionLoader {
	
	/**
	 * This method is used for loading description of
	 * {@link ShellCommand} objects from text file.
	 * 
	 * @param pathName string representing path of text file
	 * @return unmodifiable list of strings; every string contains single line of file
	 */
	public static List<String> load(InputStream is) {
		BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
		try {
			List<String> list = new ArrayList<>();
			while(br.ready()) {
				list.add(br.readLine());
			}
			
			return unmodifiableList(list);
		} catch (IOException e) {
			return unmodifiableList(List.of("No description found"));
		}
	}
}
