package hr.fer.zemris.java.hw07.shell;

/**
 * This enumeration defines status which {@link MyShell} object gets after executing some {@link ShellCommand}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public enum ShellStatus {
	/** Status describing valid outcome*/
	CONTINUE,
	/** Status describing valid outcome*/
	TERMINATE
}
