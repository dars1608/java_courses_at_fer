package hr.fer.zemris.java.hw07.shell.commands.util;

/**
 * This interface provides common key for working with current directory stack.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface CdStackCommand {

	/**
	 * Key used for obtaining shared stack of paths. Stack is located in shared
	 * memory of the shell
	 */
	static final String SHARED_STACK_KEY = "cdstack";

}
