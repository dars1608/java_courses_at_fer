package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.CdStackCommand;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for setting the current
 * directory while memorizing last current directory (in other words, it stores
 * last current directory in shared memory of the shell).
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class PushDShellCommand extends AbstractCommand implements CdStackCommand, ShellCommand{

	/**
	 * Constructor method for class {@link PushDShellCommand}.
	 */
	public PushDShellCommand() {
		super("pushd");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		Path p = getPath(argumentsList.get(0), env.getCurrentDirectory());
		File f = p.toFile();

		if (!f.isDirectory()) {
			env.writeln("Given argument isn't path of a directory.");
			return ShellStatus.CONTINUE;
		}

		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(SHARED_STACK_KEY);
		if (stack == null) {
			stack = new Stack<>();
			env.setSharedData(SHARED_STACK_KEY, stack);
		}

		stack.push(env.getCurrentDirectory());
		env.setCurrentDirectory(p);

		return ShellStatus.CONTINUE;
	}

}
