package hr.fer.zemris.java.hw07.shell.util.namebuilder;

/**
 * This interface describes an object which will provide instructions and
 * resources for generating file name.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface NameBuilderInfo {

	/**
	 * Getter method for {@link StringBuilder} object used for generating file name.
	 * 
	 * @return
	 */
	StringBuilder getStringBuilder();

	/**
	 * Getter method for index of part of the file name which must be generated.
	 * 
	 * @param index
	 * @return
	 */
	String getGroup(int index);
}
