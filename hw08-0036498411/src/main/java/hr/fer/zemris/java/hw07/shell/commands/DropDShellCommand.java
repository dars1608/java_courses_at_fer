package hr.fer.zemris.java.hw07.shell.commands;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.CdStackCommand;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} used for removing directory which
 * was last stored by {@link PushDShellCommand} on internal stack. If there's no
 * stored directories, command informs user and does nothing.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class DropDShellCommand extends AbstractCommand implements ShellCommand, CdStackCommand {

	/**
	 * Constructor method for class {@link DropDShellCommand}.
	 */
	public DropDShellCommand() {
		super("dropd");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 0);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		@SuppressWarnings("unchecked")
		Stack<Path> stack = (Stack<Path>) env.getSharedData(SHARED_STACK_KEY);
		if (stack == null || stack.isEmpty()) {
			env.writeln("Stack is empty. Command failed to complete.");
		} else {
			stack.pop();
		}

		return ShellStatus.CONTINUE;
	}

}
