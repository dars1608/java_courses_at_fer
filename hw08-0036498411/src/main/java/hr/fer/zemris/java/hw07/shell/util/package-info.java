/**
 * This package contains helper classes for
 * {@linkplain hr.fer.zemris.java.hw07.shell.MyShell MyShell} program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.shell.util;