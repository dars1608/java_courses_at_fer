package hr.fer.zemris.java.hw07.shell.commands;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.MyShell;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} which terminates {@link MyShell}.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class ExitShellCommand extends AbstractCommand implements ShellCommand {
	
	/**
	 * Constructor method for class {@link ExitShellCommand}.
	 */
	public ExitShellCommand() {
		super("exit");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if(!arguments.isEmpty()) {
			env.writeln("Command \"exit\" takes no arguments.");
			return ShellStatus.CONTINUE;
		}
		return ShellStatus.TERMINATE;
	}
}
