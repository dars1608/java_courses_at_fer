package hr.fer.zemris.java.hw07.shell.commands.util;

import java.util.List;

import hr.fer.zemris.java.hw07.shell.util.namebuilder.NameBuilderInfo;

/**
 * This class is an implementation of {@link NameBuilderInfo} used internally
 * for creating new file name. It internally stores groups generated with
 * {@link Matcher} generated from old file name, and {@link StringBuilder} used
 * for generating new file name.
 */
public class NameBuilderInfoImpl implements NameBuilderInfo {
	/** {@link StringBuilder} used for generating new file name */
	private StringBuilder sb = new StringBuilder();
	/** Parts of old file name which can be reused */
	private List<String> groups;

	/**
	 * Constructor method for class {@link NameBuilderInfoImpl}.
	 * 
	 * @param groups
	 */
	public NameBuilderInfoImpl(List<String> groups) {
		this.groups = groups;
	}

	@Override
	public StringBuilder getStringBuilder() {
		return sb;
	}

	@Override
	public String getGroup(int index) {
		return groups.get(index);
	}

}