package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.NameBuilderInfoImpl;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;
import hr.fer.zemris.java.hw07.shell.util.ParserException;
import hr.fer.zemris.java.hw07.shell.util.namebuilder.NameBuilder;
import hr.fer.zemris.java.hw07.shell.util.namebuilder.NameBuilderInfo;
import hr.fer.zemris.java.hw07.shell.util.namebuilder.NameBuilderParser;

/**
 * This class represents {@link ShellCommand} with multiple functions.
 * Invocation of this command must be following format:
 * <ul>
 * <i> massrename sourceDirectory destinationDirectory command filter
 * [renamingExpression] </i>
 * 
 * <p>
 * <li>sourceDirectory - directory containing files which are processed
 * <li>destinationDirectory - directory where files are moved (if command =
 * "execute")
 * <li>command:
 * <ul>
 * <li>filter - filter files matching given filter
 * <li>groups - grouping parts of file names
 * <li>show - demonstrating how file names would look if given renaming
 * expression resolves
 * <li>execute - moving/renaming files
 * </ul>
 * <li>filter - regex used for filtering file names and grouping parts of file
 * name
 * <li>renamingExpression - used only in commands "show" and "execute" for
 * generating new file names
 * 
 * </ul>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class MassRenameShellCommand extends AbstractCommand implements ShellCommand {

	/** Path of source directory */
	private Path src;
	/** Path of destination directory */
	private Path dest;
	/** Shell {@link Environment} */
	private Environment env;
	/** {@link Pattern} used for filtering and grouping */
	private Pattern filter;

	/**
	 * Constructor method for class {@link MassRenameShellCommand}.
	 */
	public MassRenameShellCommand() {
		super("massrename");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 4 || i.size() == 5);

		if (argumentsList == null) {
			return ShellStatus.CONTINUE;
		}

		this.env = env;
		this.src = getPath(argumentsList.get(0), env.getCurrentDirectory());
		this.dest = getPath(argumentsList.get(1), env.getCurrentDirectory());

		if (!src.toFile().isDirectory() || !dest.toFile().isDirectory()) {
			env.writeln("Both source and destination paths must be paths of direcory.");
			return ShellStatus.CONTINUE;
		}

		String command = argumentsList.get(2);
		try {
			this.filter = Pattern.compile(argumentsList.get(3), Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
		} catch (PatternSyntaxException ex) {
			env.writeln("Invalid syntax in regex. Command failed to complete.");
			return ShellStatus.CONTINUE;
		}

		switch (command) {
		case "filter":
			if (argumentsList.size() != 4) {
				env.writeln("Command mode \"filter\" accepts only one regex expression.");
			} else {
				filterMode();
			}
			break;
		case "groups":
			if (argumentsList.size() != 4) {
				env.writeln("Command mode \"groups\" accepts only one regex expression.");
			} else {
				groupingMode();
			}
			break;
		case "show":
			if (argumentsList.size() != 5) {
				env.writeln("Command mode \"show\" must accept additional argument \"renamingExpression\".");
			} else {
				showExecuteMode(argumentsList.get(4), false);
			}
			break;
		case "execute":
			if (argumentsList.size() != 5) {
				env.writeln("Command mode \"execute\" must accept additional argument \"renamingExpression\".");
			} else {
				showExecuteMode(argumentsList.get(4), true);
			}
			break;
		default:
			env.writeln(String.format("Unsupported operation: %s.", command));
		}

		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used internally when command resolves "show" or "execute"
	 * mode.
	 * 
	 * @param renamingExpression
	 *            expression which will be parsed into {@link NameBuilder} object
	 * @param isExecute
	 *            flag used to determine whether the "show" or "execute" mode is
	 *            active.
	 */
	private void showExecuteMode(String renamingExpression, boolean isExecute) {
		File[] files = src.toFile().listFiles();

		for (File file : files) {
			Matcher m = filter.matcher(file.getName());

			if (file.isFile() && m.matches()) {
				NameBuilder builder = null;
				try {
					builder = new NameBuilderParser(renamingExpression).getNameBuilder();
				} catch (ParserException e) {
					env.writeln(e.getMessage());
					return;
				}

				List<String> groups = new ArrayList<>();
				for (int i = 0, len = m.groupCount(); i <= len; i++) {
					groups.add(m.group(i));
				}

				NameBuilderInfo info = new NameBuilderInfoImpl(groups);
				builder.execute(info);

				env.writeln(String.format("%s => %s", file.getName(), info.getStringBuilder().toString()));
				if (isExecute) {
					try {
						Files.move(file.toPath(), dest.resolve(info.getStringBuilder().toString()),
								StandardCopyOption.ATOMIC_MOVE);
					} catch (IOException e) {
						env.writeln("Moving/renaming files failed to complete.");
					}
				}
			}
		}

	}

	/**
	 * This method is used internally when command resolves "grouping" mode. Method
	 * groups parts of file names of files contained in source directory.
	 */
	private void groupingMode() {
		File[] files = src.toFile().listFiles();

		for (File file : files) {
			Matcher m = filter.matcher(file.getName());
			if (file.isFile() && m.matches()) {
				StringBuilder sb = new StringBuilder(file.getName());

				for (int i = 0, len = m.groupCount(); i <= len; i++) {
					sb.append(String.format(" %d: %s", i, m.group(i)));
				}

				env.writeln(sb.toString());
			}
		}

	}

	/**
	 * This method is used internally when command is in "filter" mode. Method
	 * filters file names of files contained in source directory by given regex.
	 */
	private void filterMode() {
		File[] files = src.toFile().listFiles();

		for (File file : files) {
			if (file.isFile() && filter.matcher(file.getName()).matches()) {
				env.writeln(file.getName());
			}
		}
	}
}
