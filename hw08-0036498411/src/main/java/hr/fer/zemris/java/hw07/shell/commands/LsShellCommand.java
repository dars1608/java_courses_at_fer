package hr.fer.zemris.java.hw07.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

import hr.fer.zemris.java.hw07.shell.Environment;
import hr.fer.zemris.java.hw07.shell.ShellStatus;
import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This class represents {@link ShellCommand} which takes one argument; path of
 * some directory. Method writes file directory listing of that directory.
 * Output format of this command is:
 * <ul>
 * <li>First column indicates if current object is directory (d), readable (r),
 * writable (w) and executable (x).
 * <li>Second column contains object size in bytes that is right aligned and
 * occupies 10 characters.
 * <li>Third column contains file creation date/time
 * <li>Forth column contains file name.
 * </ul>
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public class LsShellCommand extends AbstractCommand implements ShellCommand {

	/**
	 * Constructor method for class {@link LsShellCommand}.
	 */
	public LsShellCommand() {
		super("ls");
	}

	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		Objects.requireNonNull(env, "Environment reference can't be null");
		Objects.requireNonNull(arguments, "Arguments reference can't be null");

		List<String> argumentsList = getArguments(arguments, env, i -> i.size() == 1);
		if(argumentsList != null) {
			directoryListing(argumentsList.get(0), env);
		}
		
		return ShellStatus.CONTINUE;
	}

	/**
	 * This method is used internally for writing directory listing on shell.
	 * 
	 * @param pathName
	 *            path of directory to be listed
	 * @param env
	 *            {@link Environment} of shell
	 */
	private void directoryListing(String pathName, Environment env) {
		File p = getPath(pathName, env.getCurrentDirectory()).toFile();

		if (!p.isDirectory()) {
			env.writeln("Given argument isn't path of a directory.");
			return;
		}

		File[] files = p.listFiles();
		for (File f : files) {
			try {
				env.writeln(getInfo(f));
			} catch (IOException ignorable) {
			}
		}

	}

	/**
	 * This method is used for extracting info about some file.
	 * 
	 * @param f
	 *            {@link File} which info is extracted
	 * @return formated info string
	 * @throws IOException
	 *             if file doesn't exist
	 */
	private String getInfo(File f) throws IOException {
		StringBuilder sb = new StringBuilder();

		Path path = f.toPath();
		BasicFileAttributeView faView = Files.getFileAttributeView(path, BasicFileAttributeView.class,
				LinkOption.NOFOLLOW_LINKS);
		BasicFileAttributes attributes = faView.readAttributes();

		sb.append(f.isDirectory() ? 'd' : '-');
		sb.append(f.canRead() ? 'r' : '-');
		sb.append(f.canWrite() ? 'w' : '-');
		sb.append(f.canExecute() ? 'x' : '-');

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		FileTime fileTime = attributes.creationTime();
		String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));

		return String.format(sb.toString() + " %10d %s %s", f.length(), formattedDateTime, f.getName());
	}
}
