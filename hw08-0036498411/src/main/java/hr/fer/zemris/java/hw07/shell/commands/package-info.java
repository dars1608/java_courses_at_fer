/**
 * This package contains {@linkplain hr.fer.zemris.java.hw07.shell.ShellCommand
 * ShellCommand} objects which describe commands of
 * {@linkplain hr.fer.zemris.java.hw07.shell.MyShell MyShell} program.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
package hr.fer.zemris.java.hw07.shell.commands;