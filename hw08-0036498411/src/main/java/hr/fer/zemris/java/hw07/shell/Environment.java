package hr.fer.zemris.java.hw07.shell;

import java.nio.file.Path;
import java.util.SortedMap;

import hr.fer.zemris.java.hw07.shell.commands.util.ShellCommand;

/**
 * This interface defines an environment of shell object. It predicts that class
 * which implements this interface should have a way of communicating with user
 * through standard input/output. It should also have a map of some sort of
 * command objects mapped by name, which will represent commands defined in the
 * shell. Finally, it should store characters representing prompt, multiline and
 * more lines symbols, which will be written on console when needed.
 * 
 * @author Darko Britvec
 * @version 1.0
 */
public interface Environment {

	/**
	 * This method is used for reading from console (<code>System.in</code>).
	 * 
	 * @return line which is obtained from the console
	 * @throws ShellIOException
	 *             if reading fails
	 */
	String readLine() throws ShellIOException;

	/**
	 * This method is used for writing some text on the console
	 * (<code>System.out</code>)
	 * 
	 * @param text
	 *            text to be written
	 * @throws ShellIOException
	 *             if writing fails
	 */
	void write(String text) throws ShellIOException;

	/**
	 * This method writes one line on the console (<code>System.out</code>)
	 * 
	 * @param text
	 *            line to be written
	 * @throws ShellIOException
	 *             if writing fails
	 */
	void writeln(String text) throws ShellIOException;

	/**
	 * This method is used for obtaining commands defined in the shell environment.
	 * 
	 * @return unmodifiable {@link SortedMap} of pairs: ({@link String} key,
	 *         {@link ShellCommand} value)
	 */
	SortedMap<String, ShellCommand> commands();

	/**
	 * Getter method for the multiline symbol.
	 * 
	 * @return current multiline symbol
	 */
	Character getMultilineSymbol();

	/**
	 * Setter method for the multiline symbol.
	 * 
	 * @param symbol
	 *            the multiline symbol to be set
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	void setMultilineSymbol(Character symbol);

	/**
	 * Getter method for the prompt symbol.
	 * 
	 * @return current prompt symbol
	 */
	Character getPromptSymbol();

	/**
	 * Setter method for the prompt symbol.
	 * 
	 * @param symbol
	 *            the prompt symbol to be set
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	void setPromptSymbol(Character symbol);

	/**
	 * Getter method for the more lines symbol.
	 * 
	 * @return current more lines symbol
	 */
	Character getMorelinesSymbol();

	/**
	 * Setter method for the more lines symbol.
	 * 
	 * @param symbol
	 *            the more lines symbol to be set
	 * @throws NullPointerException
	 *             if given argument is <code>null</code>
	 */
	void setMorelinesSymbol(Character symbol);

	/**
	 * This method is used for fetching and normalizing the absolute path of the
	 * current directory shell process.
	 * 
	 * @return absolute path of current directory
	 */
	Path getCurrentDirectory();

	/**
	 * This method sets the current directory of shell process.
	 * 
	 * @param path
	 *            path of directory to be set
	 * @throws IllegalArgumentException
	 *             if given path argument isn't path of a directory
	 * @throws NullPointerException
	 *             if argument is <code>null</code>
	 */
	void setCurrentDirectory(Path path);

	/**
	 * This method is used for getting the data shared between shell processes which
	 * is stored under given key.
	 * 
	 * @param key
	 *            the key
	 * @return value stored at given key
	 * @throws NullPointerException
	 *             if given key reference is <code>null</code>
	 */
	Object getSharedData(String key);

	/**
	 * This method is used for setting the data shared between shell processes which
	 * will be stored under given key.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            to be stored
	 * @throws NullPointerException
	 *             if given key reference is <code>null</code>
	 */
	void setSharedData(String key, Object value);
}
